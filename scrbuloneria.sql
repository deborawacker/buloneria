USE [master]
GO
/****** Object:  Database [Buloneria]    Script Date: 24/10/2015 14:17:26 ******/
CREATE DATABASE [Buloneria]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Buloneria', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Buloneria.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Buloneria_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\Buloneria_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Buloneria] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Buloneria].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Buloneria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Buloneria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Buloneria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Buloneria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Buloneria] SET ARITHABORT OFF 
GO
ALTER DATABASE [Buloneria] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Buloneria] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Buloneria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Buloneria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Buloneria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Buloneria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Buloneria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Buloneria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Buloneria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Buloneria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Buloneria] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Buloneria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Buloneria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Buloneria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Buloneria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Buloneria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Buloneria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Buloneria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Buloneria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Buloneria] SET  MULTI_USER 
GO
ALTER DATABASE [Buloneria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Buloneria] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Buloneria] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Buloneria] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Buloneria]
GO
/****** Object:  StoredProcedure [dbo].[RepArtMasVendidosACliente]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RepArtMasVendidosACliente]
	-- Add the parameters for the stored procedure here
	@IdCliente int, 
	@FechaMin date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- TERMINAR
	SELECT *
	FROM Clientes
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ActualizarCantRecibidaLineasOC]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ActualizarCantRecibidaLineasOC]
	-- Add the parameters for the stored procedure here
	@nroLineaOrdenCompra int, 
	@cantRecibida int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE lineasordenescompras
	SET cantRecibida+=@cantRecibida
	WHERE nroLineaOrdenCompra=@nroLineaOrdenCompra;
END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_AgregarArticulo] 
@descripcion varchar(50),
@codGrupoArticulo varchar(10),
@cantMinima int,
@cantActual int,
@codigoArticulo varchar(10),
@id_alicuota decimal(4,2),
@PrecioVenta int
	
AS
begin
declare @codArt int
declare @alic int
set @codArt=(select codGrupoArticulo from gruposarticulos where grupoarticulo = @codGrupoArticulo)
set @alic = (select id_alicuota from alicuotas where alicuota = @id_alicuota)
insert into articulos 
values(@descripcion,@codArt,@cantMinima,@cantActual,@codigoArticulo,@alic,@PrecioVenta)

end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarArticuloFaltante]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarArticuloFaltante]
@codigoArticulo varchar(10),
@nroOrdenCompra int,
@cantfaltante int,
@nroFaltante int
as
begin

declare @codArticulo int
declare @nroLineaOC int
set @codArticulo =(select codArticulo from articulos where codigoArticulo = @codigoArticulo)
set @nroLineaOC = (select nroLineaOrdenCompra from lineasordenescompras 
inner join ordenescompras on lineasordenescompras.nroordencompra = ordenescompras.nroordencompra
inner join articulos on lineasordenescompras.codarticulo = articulos.codarticulo
where aRTICULOS.codArticulo = @codArticulo and ordenescompras.nroordencompra = @nroordencompra
)

insert into articulosfaltantes values(@nroLineaOC,@nroFaltante,@cantfaltante)

end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarArticuloProveedor]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarArticuloProveedor] @codigoArticulo varchar(10),@codProv int
as
begin
declare @codArticulo int
set @codArticulo = (select codArticulo from articulos where codigoArticulo=@codigoArticulo)
insert into articulosproveedores(codArticulo,codProv) values (@codArticulo,@codProv)
end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarCliente]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarCliente] @cuit bigint, @razonsocial varchar(50),@nomfantasia varchar(50),@direccion varchar(100),@nombreciudad varchar(50),@nombreprov varchar(50),@codigopostal int,@telefono varchar(20),@fax varchar(20), @celular varchar(20),@email varchar(100),@tipoiva varchar(20)
as begin 
declare @cod_ciudad int
set @cod_ciudad =(select cod_localidad from localidades inner join provincias on localidades.cod_provincia = provincias.cod_provincia where localidades.nombrelocalidad = @nombreciudad and provincias.nombreprovincia = @nombreprov)
insert into Clientes
(cuit, razonsocial,nomfantasia,direccion, ciudad, codpostal,telefono,fax,celular,email,tipoiva )
values (@cuit,@razonsocial,@nomfantasia,@direccion,@cod_ciudad,@codigopostal,@telefono,@fax,@celular,@email,@tipoiva)
declare  @codcli int
set @codcli = (select Scope_Identity())
insert into cuentascorrientes values(@codcli)
select @codcli
end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarConcepto]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarConcepto]
@concepto varchar(20),
@descripcion varchar(20),
@tipoMovimiento varchar(20)

as 
begin

insert into conceptos values(@concepto,@descripcion,@tipoMovimiento)

end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarFaltante]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarFaltante]
@nroLineaOC int,
@cantFaltante int

as 
begin
insert into Faltantes(codLineaOrdenCompra,cantFaltante) values(@nroLineaOC,@cantFaltante)
end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarGrupoArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_AgregarGrupoArticulo] @descripcion varchar(50),@grupoarticulo varchar(10)

AS
BEGIN

insert into	gruposarticulos(descripcion,grupoarticulo)
values(@descripcion,@grupoarticulo)

END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarLinea]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_AgregarLinea] 
@nroOrdenCompra int,
@codArticulo varchar(10),
@cantPedir int,
@cantRecibida int,
@precioUnitario int

AS
BEGIN 
insert into lineasordenescompras( nroOrdenCompra,codArticulo,cantPedir,cantRecibida,precioUnitario)
values(@nroOrdenCompra,@codArticulo,@cantPedir,@cantRecibida,@precioUnitario);
select Scope_Identity()
END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarLineaNotaPedido]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarLineaNotaPedido] 
@nroNotaPedido int,
@codigoArticulo varchar(10),
@cantidad int,
@precioUnitario int
AS
BEGIN

declare @codArticulo int
set @codArticulo =(select codArticulo from articulos where codigoArticulo = @codigoArticulo)
insert into lineasnotaspedidos(nronotapedido,codarticulo,cantidad,preciounitario)
values(@nronotapedido,@codArticulo,@cantidad,@preciounitario)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarMovCtaCte]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarMovCtaCte]
@fecha datetime,
@monto int,
@saldo int,
@nroCtaCte int,
@descripcion varchar(20),
@nromovimiento int
as
begin
declare @concepto varchar(20)
set @concepto = (select Conceptos.concepto from Conceptos where Conceptos.descripcion = @descripcion)
insert into Movimientos (fecha,concepto,monto,nroctacte,saldo,nromovimiento) values (@fecha,@concepto,@monto,@nroCtaCte,@saldo,@nromovimiento)

end

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarProveedor]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarProveedor] @cuit bigint, @razonsocial varchar(50),@nomfantasia varchar(50),@direccion varchar(100),@nombreciudad varchar(50),@nombreprov varchar(50),@codigopostal int,@telefono varchar(20),@fax varchar(20), @celular varchar(20),@email varchar(100),@tipoiva varchar(20)
as begin 
declare @cod_ciudad int
set @cod_ciudad =(select cod_localidad from localidades inner join provincias on localidades.cod_provincia = provincias.cod_provincia where localidades.nombrelocalidad = @nombreciudad and provincias.nombreprovincia = @nombreprov)
insert into Proveedores 
(cuit, razonsocial,nomfantasia,direccion, ciudad, codpostal,telefono,fax,celular,email,tipoiva )
values (@cuit,@razonsocial,@nomfantasia,@direccion,@cod_ciudad,@codigopostal,@telefono,@fax,@celular,@email,@tipoiva)
select Scope_Identity()
end

GO
/****** Object:  StoredProcedure [dbo].[sp_CambiarEstadoOrdenCompra]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_CambiarEstadoOrdenCompra] @nroOrdenCompra int,@estado varchar(10)

AS
BEGIN

UPDATE ordenescompras set estado=@estado where nroOrdenCompra= @nroOrdenCompra

END

GO
/****** Object:  StoredProcedure [dbo].[sp_consultaralicuotas]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_consultaralicuotas]
	
AS
	SELECT alicuota from alicuotas

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarArticulos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ConsultarArticulos]
AS
begin
SELECT 
codArticulo,
art.descripcion,
art.codGrupoArticulo,
gart.descripcion as descripcionGrupo,
cantMinima,
cantActual,
codigoArticulo,
precioVenta,
alic.alicuota
from Articulos art INNER JOIN GruposArticulos gart
    ON art.codGrupoArticulo=gart.codGrupoArticulo
	INNER JOIN Alicuotas alic
	ON art.id_alicuota=alic.id_alicuota
end

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarClientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarClientes]
as
select codcli,cuit,razonsocial,nomfantasia,direccion,codpostal,telefono,fax,celular,email,localidades.nombrelocalidad,provincias.nombreprovincia,tiposiva.nombretipo,tiposiva.descripcion from clientes
inner join localidades on clientes.ciudad = localidades.Cod_Localidad
inner join Provincias on Localidades.cod_Provincia = provincias.cod_provincia
inner join tiposiva on clientes.tipoiva = tiposiva.nombretipo

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarGruposArticulos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ConsultarGruposArticulos]
AS
BEGIN
	SELECT descripcion,grupoarticulo from gruposarticulos
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarLineasArticulos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
creaTE PROCEDURE [dbo].[sp_ConsultarLineasArticulos]
AS
BEGIN

SELECT nroordencompra,codarticulo,cantpedir,preciounitario
from lineasordenescompras

END

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarLocalidadesProvincia]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarLocalidadesProvincia] (@provincia varchar(50))
as select NombreLocalidad from localidades inner join Provincias on localidades.Cod_provincia = Provincias.Cod_Provincia where Provincias.NombreProvincia = @provincia

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarProveedores]
as
select codprov,cuit,razonsocial,nomfantasia,direccion,codpostal,telefono,fax,celular,email,localidades.nombrelocalidad,provincias.nombreprovincia,tiposiva.nombretipo,tiposiva.descripcion from proveedores
inner join localidades on proveedores.ciudad = localidades.Cod_Localidad
inner join Provincias on Localidades.cod_Provincia = provincias.cod_provincia
inner join tiposiva on proveedores.tipoiva = tiposiva.nombretipo

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarProveedoresArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ConsultarProveedoresArticulo] @codigoArticulo varchar(10)

AS

select proveedores.codprov,cuit,razonsocial,nomfantasia,direccion,codpostal,telefono,fax,celular,email,localidades.nombrelocalidad,provincias.nombreprovincia,tiposiva.nombretipo,tiposiva.descripcion from articulosproveedores
inner join proveedores on articulosproveedores.codprov = proveedores.codprov
inner join localidades on proveedores.ciudad = localidades.Cod_Localidad
inner join Provincias on Localidades.cod_Provincia = provincias.cod_provincia
inner join tiposiva on proveedores.tipoiva = tiposiva.nombretipo
inner join articulos on articulosproveedores.codarticulo = articulos.codarticulo
where codigoArticulo = @codigoarticulo

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarProvincias]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarProvincias]
as
select NombreProvincia from Provincias

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarTiposIVA]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarTiposIVA] 
as select * from tiposiva

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarArticulo] @codigoArticulo varchar(10)
as begin 
delete from articulosproveedores where codarticulo = (select codarticulo from articulos where codigoarticulo = @codigoarticulo)
delete from Articulos where codigoArticulo = @codigoArticulo
end

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarArticuloProveedor]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarArticuloProveedor] @codigoarticulo varchar (10)
as delete from articulosproveedores where codarticulo = (select codarticulo from articulos where codigoarticulo = @codigoarticulo)

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarCliente]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarCliente] @cod_cli int
as delete from clientes where codcli = @cod_cli

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarConcepto]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarConcepto]
@concepto varchar(20)
as
begin
delete from conceptos where concepto = @concepto
end

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarFaltante]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_EliminarFaltante]
@nroLineaOC int
as
begin
delete from Faltantes where codLineaOrdenCompra = @nroLineaOC 
end

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarGrupoArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarGrupoArticulo] @grupoArticulo varchar(10)
as
delete from gruposarticulos where codgrupoarticulo = (select codgrupoarticulo from gruposarticulos where grupoArticulo = @grupoArticulo)

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarMovCtaCte]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarMovCtaCte] @nroCtaCte int
as
begin

delete from Movimientos where nroctacte=@nroctacte 

end

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarNotaPedido]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_EliminarNotaPedido] 
@nroNotaPedido int
as 
begin
delete from lineasnotaspedidos where nroNotaPedido = @nroNotaPedido
delete from notaspedidos where nroNotaPedido = @nroNotaPedido
end

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarProveedor]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarProveedor] @cod_prov int
as delete from proveedores where codprov = @cod_prov

GO
/****** Object:  StoredProcedure [dbo].[sp_GenerarNotaPedido]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GenerarNotaPedido]
@fechaEmision datetime,
@tipoFormaPago varchar(20),
@codCli int,
@idUsuario int
as

begin
declare @formaPago int
set @formaPago = (select formaPago from formaspagos where tipoFormaPago = @tipoFormaPago)
insert into notaspedidos(fechaEmision,formaPago,codCli,idUsuario)
values (@fechaEmision,@formaPago,@codCli,@idUsuario)
select Scope_Identity()

end

GO
/****** Object:  StoredProcedure [dbo].[sp_GenerarOrdenCompra]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GenerarOrdenCompra] 
@fechaEmision datetime,
@fechaPlazo datetime,
@formaPago varchar(20),
@codProv int,
@estado varchar(10),
@usuario varchar(15),
@fechaHora datetime

	
AS
BEGIN
declare @nroFormaPago int
set @nroFormaPago = (select formaPago from FormasPagos where tipoFormaPago= @formaPago)
insert into OrdenesCompras 
values(@fechaEmision,@fechaPlazo,@nroFormaPago,@codProv,@estado,@fechaHora,@usuario)
select Scope_Identity()
END

GO
/****** Object:  StoredProcedure [dbo].[sp_IncrementarStockArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_IncrementarStockArticulo] 
@IdArticulo int,
@cantidad int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Articulos 
	set  cantActual+= @cantidad
where codArticulo = @IdArticulo
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarArticulo] 
@codArticulo int,
@codigoArticulo varchar(10),
@cantActual int, 
@descripcion varchar(50),
@cantMinima int, 
@precioVenta int
as begin
update Articulos set descripcion=@descripcion,
					cantMinima=@cantMinima,
					cantActual= @cantActual
where codArticulo = @codArticulo
end
/*precioVenta=@precioVenta*/
GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarArticuloFaltante]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarArticuloFaltante]
@nroLineaOrdenCompra int,
@cantFaltante int
as
begin
update articulosfaltantes set nrolineaordencompra=@nrolineaordencompra,cantfaltante=@cantfaltante
--where nroarticulofaltante=@nroarticulofaltante
end

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarCliente]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarCliente] 
@codcli int,
@cuit bigint, 
@razonsocial varchar(50),
@nomfantasia varchar(20),
@direccion varchar(100),
@nombreciudad varchar(50),
@nombreprovincia varchar(50),
@codpostal int, 
@telefono varchar(20), 
@fax varchar(20), 
@celular varchar(20), 
@email varchar(100),
@tipoiva varchar(20)
as begin
declare @cod_ciudad int
set @cod_ciudad = (select cod_localidad from localidades inner join provincias on localidades.cod_provincia = provincias.cod_provincia where localidades.nombrelocalidad = @nombreciudad and provincias.nombreprovincia = @nombreprovincia)
update Clientes set tipoiva = @tipoiva, nomfantasia = @nomfantasia, direccion = @direccion, ciudad = @cod_ciudad, codpostal = @codpostal, telefono = @telefono, fax = @fax, celular = @celular, email = @email
where codcli = @codcli
end

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarFaltante]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ModificarFaltante]
@nroFaltante int,
@nroOrdenCompra int
as
begin
update faltantesdeordenescompras set nroordencompra=@nroordencompra
where nrofaltante=@nrofaltante
end

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarGrupoArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ModificarGrupoArticulo] @grupoArticulo varchar(10),@descripcion varchar(50)
AS
BEGIN

update gruposarticulos set descripcion=@descripcion
where grupoArticulo=@grupoArticulo 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarProveedor]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarProveedor] @codprov int,@cuit bigint, @razonsocial varchar(50),@nomfantasia varchar(20),@direccion varchar(100),@nombreciudad varchar(50),@nombreprovincia varchar(50),@codpostal int, @telefono varchar(20), @fax varchar(20), @celular varchar(20), @email varchar(100),@tipoiva varchar(20)
as begin
declare @cod_ciudad int
set @cod_ciudad = (select cod_localidad from localidades inner join provincias on localidades.cod_provincia = provincias.cod_provincia where localidades.nombrelocalidad = @nombreciudad and provincias.nombreprovincia = @nombreprovincia)
update Proveedores set tipoiva = @tipoiva,nomfantasia = @nomfantasia, direccion = @direccion, ciudad = @cod_ciudad, codpostal = @codpostal, telefono = @telefono, fax = @fax, celular = @celular, email = @email
where codprov = @codprov
end

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarStockArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ModificarStockArticulo]
@codArticulo int,
@cantidad int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Articulos SET cantActual+=@cantidad WHERE codArticulo=@codArticulo
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarConceptos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarConceptos]
as
begin

select concepto,descripcion,tipomovimiento from conceptos

end

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarCuentasCorrientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarCuentasCorrientes]
as
begin

select CuentasCorrientes.nroctacte,CuentasCorrientes.codcli,
Clientes.codCli,
Clientes.celular,
Clientes.codPostal,
Clientes.cuit,
Clientes.direccion,
Clientes.email,
Clientes.fax,
Clientes.nomFantasia,
Clientes.razonsocial,
Clientes.telefono,
Localidades.nombrelocalidad,
Provincias.nombreprovincia

from CuentasCorrientes

inner join Clientes on CuentasCorrientes.codcli = Clientes.codcli
inner join Localidades on Clientes.ciudad = Localidades.cod_localidad
inner join Provincias on Localidades.cod_provincia = Provincias.cod_provincia

end

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarFaltantes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarFaltantes]
@codFaltanteOC int
as

BEGIN
SELECT codFaltante,cantFaltante, cantPedir,precioUnitario,codigoArticulo,descripcion,cantMinima,cantActual
FROM Faltantes f INNER JOIN lineasordenescompras loc
ON f.nroLineaOC=loc.nroLineaOrdenCompra
INNER JOIN Articulos art
ON art.codArticulo=loc.codArticulo
WHERE codFaltante=@codFaltanteOC

END

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarFaltantesOC]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarFaltantesOC] 
as
begin

SELECT codFaltante,nroOrdenCompra from FaltantesOC  

END

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarFormasPago]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_RecuperarFormasPago]
as
select tipoformapago from formaspagos

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarHistorialNotasPedidos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_RecuperarHistorialNotasPedidos]
as
begin
declare @datetimeinicio datetime
set @datetimeinicio = getdate() - 92
SELECT nroNotaPedido,fechaEmision,FormasPagos.tipoFormaPago,Clientes.codCli,Clientes.cuit,Clientes.razonSocial,Clientes.nomFantasia,Clientes.direccion,Clientes.codpostal,Localidades.nombrelocalidad,provincias.nombreprovincia,Clientes.telefono,Clientes.fax,Clientes.celular,Clientes.email,Clientes.tipoiva,tiposIva.descripcion
from notaspedidos
inner join FormasPagos on NotasPedidos.formaPago = FormasPagos.formaPago
inner join Clientes on NotasPedidos.codCli = Clientes.codCli
inner join Localidades on Clientes.ciudad = Localidades.cod_localidad
inner join tiposIva on Clientes.tipoiva = tiposiva.nombretipo
inner join provincias on localidades.cod_provincia = provincias.cod_provincia 
where notaspedidos.Fechaemision <= @datetimeinicio
end

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarHistorialOrdenesCompras]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RecuperarHistorialOrdenesCompras]
AS
BEGIN
declare @datetimeinicio datetime
set @datetimeinicio = getdate() - 92
SELECT nroOrdenCompra,fechaEmision,fechaPlazo,FormasPagos.tipoFormaPago,estado,Proveedores.codprov,Proveedores.cuit,Proveedores.razonSocial,Proveedores.nomFantasia,Proveedores.direccion,Proveedores.codpostal,Localidades.nombrelocalidad,provincias.nombreprovincia,Proveedores.telefono,Proveedores.fax,Proveedores.celular,Proveedores.email,Proveedores.tipoiva,tiposIva.descripcion,usuario,fechahora
from OrdenesCompras
inner join FormasPagos on OrdenesCompras.formaPago = FormasPagos.formaPago
inner join Proveedores on OrdenesCompras.codProv = Proveedores.codProv
inner join Localidades on Proveedores.ciudad = Localidades.cod_localidad
inner join tiposIva on proveedores.tipoiva = tiposiva.nombretipo
inner join provincias on localidades.cod_provincia = provincias.cod_provincia 
where ordenescompras.Fechaemision <= @datetimeinicio
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarLineasNP]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_RecuperarLineasNP]
@NroNP int
as
select cantidad,precioUnitario,Articulos.descripcion,Articulos.cantMinima,Articulos.cantActual,Articulos.codigoArticulo,alicuotas.alicuota, GruposArticulos.GrupoArticulo as 'CodigoGrupoArticulo', GruposArticulos.descripcion as 'descripciongrupo'
from lineasnotaspedidos
inner join Articulos on LineasNotasPedidos.codarticulo = articulos.codarticulo 
inner join Alicuotas on Articulos.id_alicuota = Alicuotas.id_alicuota 
inner join GruposArticulos on Articulos.CodGrupoArticulo = gruposarticulos.CodGrupoArticulo
where nroNotaPedido = @NroNP

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarLineasOC]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarLineasOC] @NroOC int
as
select 
nroLineaOrdenCompra, 
cantPedir, 
cantRecibida,
art.precioVenta,
art.descripcion,
art.cantMinima,
art.cantActual,
art.codArticulo,
art.codigoArticulo,
alic.alicuota,  
gart.codGrupoArticulo as 'codigoGrupoArticulo', 
gart.descripcion as 'descripcionGrupo'
from lineasordenescompras loc inner join Articulos  art
        on loc.codarticulo = art.codarticulo 
        inner join Alicuotas alic
        on art.id_alicuota = alic.id_alicuota 
        inner join GruposArticulos gart
        on art.CodGrupoArticulo = gart.CodGrupoArticulo
where nroOrdenCompra = @NroOC
GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarMovimientosCtasCtes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_RecuperarMovimientosCtasCtes] 
@codCli int
as
begin

select Movimientos.fecha,Movimientos.monto,Movimientos.saldo, Movimientos.nromovimiento,
conceptos.concepto,Conceptos.descripcion,Conceptos.tipoMovimiento

from Movimientos
inner join Conceptos on Movimientos.concepto = Conceptos.concepto
where nroctacte = (select nroctacte from CuentasCorrientes where codcli = @codCli)

end

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarNotasPedidos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RecuperarNotasPedidos]
AS
BEGIN
declare @datetimefin datetime, @datetimeinicio datetime
set @datetimefin = getdate()  
set @datetimeinicio = getdate() - 91
SELECT 
nroNotaPedido,fechaEmision,
formasPagos.tipoFormaPago,
clientes.codcli,clientes.celular,clientes.codpostal,clientes.cuit,clientes.direccion,clientes.email,
clientes.fax,clientes.nomFantasia,clientes.razonSocial,clientes.telefono,
tiposIva.descripcion,tiposIva.nombreTipo,
Localidades.nombrelocalidad,
provincias.nombreprovincia

from notaspedidos
inner join FormasPagos on notaspedidos.formaPago = FormasPagos.formaPago
inner join clientes on notaspedidos.codcli = clientes.codcli
inner join tiposIva on clientes.tipoiva = tiposiva.nombretipo
inner join Localidades on clientes.ciudad = Localidades.cod_localidad
inner join provincias on localidades.cod_provincia = provincias.cod_provincia 
where notaspedidos.Fechaemision between @datetimeinicio and @datetimefin
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarOrdenesCompras]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_RecuperarOrdenesCompras]
AS
BEGIN
declare @datetimefin datetime, @datetimeinicio datetime
set @datetimefin = getdate()  
set @datetimeinicio = getdate() - 91
SELECT nroOrdenCompra,fechaEmision,fechaPlazo,FormasPagos.tipoFormaPago,estado,Proveedores.codprov,Proveedores.cuit,Proveedores.razonSocial,Proveedores.nomFantasia,Proveedores.direccion,Proveedores.codpostal,Localidades.nombrelocalidad,provincias.nombreprovincia,Proveedores.telefono,Proveedores.fax,Proveedores.celular,Proveedores.email,Proveedores.tipoiva,tiposIva.descripcion, Ordenescompras.usuario, ordenescompras.fechahora
from OrdenesCompras
inner join FormasPagos on OrdenesCompras.formaPago = FormasPagos.formaPago
inner join Proveedores on OrdenesCompras.codProv = Proveedores.codProv
inner join Localidades on Proveedores.ciudad = Localidades.cod_localidad
inner join tiposIva on proveedores.tipoiva = tiposiva.nombretipo
inner join provincias on localidades.cod_provincia = provincias.cod_provincia 
where ordenescompras.Fechaemision between @datetimeinicio and @datetimefin
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RepArtMasComprados]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RepArtMasComprados]
	-- Add the parameters for the stored procedure here 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT art.codArticulo, art.descripcion, PrecioVenta, count(art.codArticulo) AS 'CantidadComprada'
	FROM OrdenesCompras oc INNER JOIN lineasordenescompras loc
	ON oc.nroOrdenCompra=loc.nroOrdenCompra
	INNER JOIN Articulos art
	ON loc.codArticulo=art.codArticulo 
	GROUP BY  art.codArticulo, art.descripcion, PrecioVenta
	ORDER BY count(art.codArticulo) DESC
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RepConsultarProveedoresDeArticulo]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RepConsultarProveedoresDeArticulo]
	-- Add the parameters for the stored procedure here
	@IdArticulo int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.descripcion, p.razonSocial, l.NombreLocalidad, p.direccion, p.email, p.telefono
	FROM Proveedores p INNER JOIN ArticulosProveedores ap
	ON p.codProv=ap.codProv
	INNER JOIN Articulos a
	ON ap.codArticulo=a.codArticulo
	INNER JOIN Localidades l
	on p.ciudad=l.Cod_Localidad 
	WHERE a.codArticulo=@IdArticulo
END

GO
/****** Object:  Table [dbo].[Alicuotas]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alicuotas](
	[id_alicuota] [int] IDENTITY(1,1) NOT NULL,
	[alicuota] [decimal](4, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_alicuota] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Articulos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Articulos](
	[codArticulo] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[codGrupoArticulo] [int] NOT NULL,
	[cantMinima] [int] NOT NULL,
	[cantActual] [int] NOT NULL,
	[codigoArticulo] [varchar](10) NOT NULL,
	[id_alicuota] [int] NOT NULL,
	[precioVenta] [int] NOT NULL,
 CONSTRAINT [PK__Articulos__34C8D9D1] PRIMARY KEY CLUSTERED 
(
	[codArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ArticulosProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArticulosProveedores](
	[codArticulo] [int] NOT NULL,
	[codProv] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codArticulo] ASC,
	[codProv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[codCli] [int] IDENTITY(1,1) NOT NULL,
	[cuit] [bigint] NOT NULL,
	[razonSocial] [varchar](50) NOT NULL,
	[nomFantasia] [varchar](50) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[ciudad] [int] NOT NULL,
	[codPostal] [int] NOT NULL,
	[telefono] [varchar](20) NOT NULL,
	[fax] [varchar](20) NULL,
	[celular] [varchar](20) NULL,
	[email] [varchar](100) NULL,
	[tipoIva] [varchar](20) NOT NULL,
 CONSTRAINT [PK__Clientes__182C9B23] PRIMARY KEY CLUSTERED 
(
	[codCli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Conceptos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Conceptos](
	[concepto] [varchar](20) NOT NULL,
	[descripcion] [varchar](20) NOT NULL,
	[tipoMovimiento] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CuentasCorrientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CuentasCorrientes](
	[nroctacte] [int] IDENTITY(1,1) NOT NULL,
	[codcli] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroctacte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CuentasCorrientesProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CuentasCorrientesProveedores](
	[nroCtaCte] [int] IDENTITY(1,1) NOT NULL,
	[codProv] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroCtaCte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Faltantes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faltantes](
	[codFaltante] [int] NULL,
	[cantFaltante] [int] NULL,
	[nroLineaOC] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FaltantesOC]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FaltantesOC](
	[codFaltante] [int] IDENTITY(1,1) NOT NULL,
	[nroOrdenCompra] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FormasPagos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormasPagos](
	[formaPago] [int] IDENTITY(1,1) NOT NULL,
	[tipoFormaPago] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[formaPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GruposArticulos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GruposArticulos](
	[codGrupoArticulo] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[grupoArticulo] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codGrupoArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LineasNotasPedidos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LineasNotasPedidos](
	[nroNotaPedido] [int] NOT NULL,
	[codArticulo] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[precioUnitario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroNotaPedido] ASC,
	[codArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[lineasordenescompras]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lineasordenescompras](
	[nroLineaOrdenCompra] [int] IDENTITY(1,1) NOT NULL,
	[nroOrdenCompra] [int] NOT NULL,
	[codArticulo] [int] NOT NULL,
	[cantPedir] [int] NOT NULL,
	[precioUnitario] [int] NOT NULL,
	[cantRecibida] [int] NOT NULL,
 CONSTRAINT [PK_lineasordenescompras] PRIMARY KEY CLUSTERED 
(
	[nroLineaOrdenCompra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LineasRemitosClientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LineasRemitosClientes](
	[cant] [int] NOT NULL,
	[codArticulo] [int] NOT NULL,
	[nroRemitoCliente] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LineasRemitosProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LineasRemitosProveedores](
	[cant] [int] NOT NULL,
	[codArticulo] [int] NOT NULL,
	[nroRemitoProveedor] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Localidades]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Localidades](
	[Cod_Localidad] [int] IDENTITY(1,1) NOT NULL,
	[NombreLocalidad] [varchar](50) NOT NULL,
	[Cod_Provincia] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Localidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Movimientos](
	[fecha] [datetime] NOT NULL,
	[concepto] [varchar](20) NOT NULL,
	[monto] [int] NOT NULL,
	[nroctacte] [int] NOT NULL,
	[nromovimiento] [int] NOT NULL,
	[saldo] [int] NOT NULL,
	[idMov] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idMov] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NotasPedidos]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotasPedidos](
	[nroNotaPedido] [int] IDENTITY(1,1) NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
	[formaPago] [int] NOT NULL,
	[codCli] [int] NOT NULL,
	[idUsuario] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[nroNotaPedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrdenesCompras]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdenesCompras](
	[nroOrdenCompra] [int] IDENTITY(1,1) NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
	[fechaPlazo] [datetime] NOT NULL,
	[formaPago] [int] NULL,
	[codProv] [int] NOT NULL,
	[estado] [varchar](10) NOT NULL,
	[fechahora] [datetime] NOT NULL,
	[usuario] [varchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroOrdenCompra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedores](
	[codProv] [int] IDENTITY(1,1) NOT NULL,
	[cuit] [bigint] NOT NULL,
	[razonSocial] [varchar](50) NOT NULL,
	[nomFantasia] [varchar](50) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[ciudad] [int] NOT NULL,
	[codPostal] [int] NOT NULL,
	[telefono] [varchar](20) NOT NULL,
	[fax] [varchar](20) NULL,
	[celular] [varchar](20) NULL,
	[email] [varchar](100) NULL,
	[tipoIva] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[codProv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Provincias](
	[Cod_Provincia] [int] NOT NULL,
	[NombreProvincia] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Cod_Provincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RemitosClientes]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RemitosClientes](
	[nroRemitoCliente] [int] IDENTITY(1,1) NOT NULL,
	[codCli] [int] NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroRemitoCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RemitosProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RemitosProveedores](
	[nroRemitoProveedor] [int] IDENTITY(1,1) NOT NULL,
	[fechaEmision] [datetime] NOT NULL,
	[nroOrdenCompra] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[nroRemitoProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tiposIva]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tiposIva](
	[nombreTipo] [varchar](20) NOT NULL,
	[descripcion] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[nombreTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vistaPrueba]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vistaPrueba]
AS
SELECT        nroOrdenCompra, fechaEmision, fechaPlazo, formaPago
FROM            dbo.OrdenesCompras
WHERE        (fechaEmision < { fn CURDATE() })

GO
/****** Object:  View [dbo].[vw_ConsultarProveedores]    Script Date: 24/10/2015 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--practico una vista:
create view [dbo].[vw_ConsultarProveedores]
as
select codprov,cuit,razonsocial,nomfantasia,direccion,codpostal,telefono,fax,celular,email,localidades.nombrelocalidad,provincias.nombreprovincia,tiposiva.nombretipo,tiposiva.descripcion 
from proveedores
inner join localidades on proveedores.ciudad = localidades.Cod_Localidad
inner join Provincias on Localidades.cod_Provincia = provincias.cod_provincia
inner join tiposiva on proveedores.tipoiva = tiposiva.nombretipo

GO
SET IDENTITY_INSERT [dbo].[Alicuotas] ON 

INSERT [dbo].[Alicuotas] ([id_alicuota], [alicuota]) VALUES (1, CAST(20.00 AS Decimal(4, 2)))
SET IDENTITY_INSERT [dbo].[Alicuotas] OFF
SET IDENTITY_INSERT [dbo].[Articulos] ON 

INSERT [dbo].[Articulos] ([codArticulo], [descripcion], [codGrupoArticulo], [cantMinima], [cantActual], [codigoArticulo], [id_alicuota], [precioVenta]) VALUES (1, N'Tornillo', 1, 20, 5116, N'abc5', 1, 11)
INSERT [dbo].[Articulos] ([codArticulo], [descripcion], [codGrupoArticulo], [cantMinima], [cantActual], [codigoArticulo], [id_alicuota], [precioVenta]) VALUES (2, N'Tuerca', 1, 25, 9844, N'abc02', 1, 22)
INSERT [dbo].[Articulos] ([codArticulo], [descripcion], [codGrupoArticulo], [cantMinima], [cantActual], [codigoArticulo], [id_alicuota], [precioVenta]) VALUES (3, N'Bulon', 1, 22, 7662, N'r4', 1, 33)
SET IDENTITY_INSERT [dbo].[Articulos] OFF
INSERT [dbo].[ArticulosProveedores] ([codArticulo], [codProv]) VALUES (1, 3)
INSERT [dbo].[ArticulosProveedores] ([codArticulo], [codProv]) VALUES (2, 3)
INSERT [dbo].[ArticulosProveedores] ([codArticulo], [codProv]) VALUES (2, 4)
INSERT [dbo].[ArticulosProveedores] ([codArticulo], [codProv]) VALUES (3, 3)
INSERT [dbo].[ArticulosProveedores] ([codArticulo], [codProv]) VALUES (3, 4)
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([codCli], [cuit], [razonSocial], [nomFantasia], [direccion], [ciudad], [codPostal], [telefono], [fax], [celular], [email], [tipoIva]) VALUES (4, 20287658791, N'Metalurgica Riobamba', N'Riobamba Met', N'Riobamba', 1, 2098, N'45565656', N'45656565', N'15552525525', N'metriob@hotmail.com', N'Monotributo')
SET IDENTITY_INSERT [dbo].[Clientes] OFF
INSERT [dbo].[Conceptos] ([concepto], [descripcion], [tipoMovimiento]) VALUES (N'Cancelación de Nota', N'NP Eliminada', N'Haber')
INSERT [dbo].[Conceptos] ([concepto], [descripcion], [tipoMovimiento]) VALUES (N'NotaPedido', N'Nota de Pedido', N'Debe')
INSERT [dbo].[Conceptos] ([concepto], [descripcion], [tipoMovimiento]) VALUES (N'Pago', N'Pago a Cuenta', N'Haber')
SET IDENTITY_INSERT [dbo].[FormasPagos] ON 

INSERT [dbo].[FormasPagos] ([formaPago], [tipoFormaPago]) VALUES (1, N'Efectivo')
INSERT [dbo].[FormasPagos] ([formaPago], [tipoFormaPago]) VALUES (3, N'Cheques')
INSERT [dbo].[FormasPagos] ([formaPago], [tipoFormaPago]) VALUES (4, N'Transf. Bancaria')
SET IDENTITY_INSERT [dbo].[FormasPagos] OFF
SET IDENTITY_INSERT [dbo].[GruposArticulos] ON 

INSERT [dbo].[GruposArticulos] ([codGrupoArticulo], [descripcion], [grupoArticulo]) VALUES (1, N'ungrupo
', N'01')
INSERT [dbo].[GruposArticulos] ([codGrupoArticulo], [descripcion], [grupoArticulo]) VALUES (7, N'dsfdsfdssd', N'12')
SET IDENTITY_INSERT [dbo].[GruposArticulos] OFF
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (1, 1, 2, 5)
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (2, 2, 2, 6)
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (2, 3, 2, 22)
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (3, 3, 12, 22)
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (4, 1, 2, 5)
INSERT [dbo].[LineasNotasPedidos] ([nroNotaPedido], [codArticulo], [cantidad], [precioUnitario]) VALUES (4, 2, 2, 6)
SET IDENTITY_INSERT [dbo].[lineasordenescompras] ON 

INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (2, 8, 2, 2, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (3, 9, 3, 7, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (4, 10, 1, 5, 2, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (5, 10, 2, 4, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (8, 12, 1, 3, 2, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (9, 13, 2, 2, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (10, 15, 2, 2, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (11, 16, 1, 2, 2, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (12, 16, 2, 4, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (13, 17, 2, 2, 1, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (14, 18, 3, 122, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (15, 19, 2, 122, 6, 122)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (16, 19, 1, 122, 5, 122)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (17, 20, 3, 100, 22, 100)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (18, 21, 3, 1000, 22, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (19, 22, 3, 140, 22, 280)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (20, 23, 3, 13, 22, 13)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (21, 24, 3, 12, 22, 12)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (22, 25, 1, 122, 5, 122)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (23, 26, 1, 10, 5, 10)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (24, 27, 2, 2, 6, 2)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (25, 28, 3, 12, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (26, 29, 1, 20, 5, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (27, 29, 3, 22, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (28, 30, 2, 20, 6, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (29, 31, 2, 25, 6, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (30, 31, 3, 35, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (31, 32, 2, 70, 6, 140)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (32, 32, 3, 25, 22, 25)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (33, 33, 1, 25, 25, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (34, 33, 2, 25, 32, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (35, 34, 3, 35, 12, 35)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (36, 35, 1, 35, 25, 22)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (37, 35, 2, 25, 32, 25)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (38, 36, 1, 25, 25, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (39, 36, 2, 70, 32, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (40, 37, 1, 25, 25, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (41, 37, 2, 35, 32, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (42, 38, 1, 25, 25, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (43, 38, 2, 35, 32, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (44, 39, 1, 12, 25, 12)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (45, 39, 3, 21, 0, 21)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (46, 40, 2, 25, 32, 25)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (47, 41, 2, 25, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (48, 41, 3, 25, 33, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (49, 42, 2, 11, 22, 11)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (50, 42, 3, 12, 33, 12)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (51, 43, 2, 25, 22, 25)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (52, 43, 3, 22, 33, 22)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (53, 44, 2, 25, 22, 25)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (54, 44, 3, 12, 33, 12)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (55, 45, 2, 25, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (56, 45, 3, 32, 33, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (57, 46, 2, 1, 22, 1)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (58, 46, 3, 12, 33, 12)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (59, 47, 2, 21, 22, 21)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (60, 47, 3, 22, 33, 22)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (61, 48, 2, 31, 22, 31)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (62, 48, 3, 32, 33, 32)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (63, 49, 2, 31, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (64, 49, 3, 32, 33, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (65, 50, 2, 1000, 22, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (66, 50, 3, 1000, 33, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (67, 51, 3, 1000, 33, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (68, 51, 2, 100, 22, 100)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (69, 51, 1, 100, 11, 100)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (70, 52, 2, 500, 22, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (71, 52, 3, 500, 33, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (72, 52, 1, 500, 11, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (73, 53, 3, 1200, 33, 1200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (74, 53, 2, 1200, 22, 1200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (75, 54, 1, 1000, 11, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (76, 54, 2, 1000, 22, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (77, 54, 3, 1000, 33, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (78, 55, 3, 500, 33, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (79, 55, 2, 500, 22, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (80, 56, 1, 100, 11, 100)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (81, 56, 2, 100, 22, 100)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (82, 57, 3, 1000, 33, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (83, 57, 2, 1000, 22, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (84, 58, 1, 500, 11, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (85, 58, 2, 500, 22, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (86, 58, 3, 500, 33, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (87, 59, 2, 200, 22, 200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (88, 59, 3, 200, 33, 400)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (89, 60, 1, 300, 11, 300)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (90, 60, 2, 300, 22, 200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (91, 60, 3, 300, 33, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (92, 61, 2, 700, 22, 400)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (93, 61, 3, 700, 33, 200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (94, 62, 1, 1000, 11, 1500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (95, 62, 2, 1000, 22, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (96, 62, 3, 1000, 33, 1200)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (97, 63, 1, 2000, 11, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (98, 63, 2, 2000, 22, 4000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (99, 63, 3, 2000, 33, 1000)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (100, 64, 1, 1000, 11, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (101, 64, 2, 1000, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (102, 64, 3, 1000, 33, 200)
GO
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (103, 65, 1, 500, 11, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (104, 65, 2, 500, 22, 500)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (105, 65, 3, 500, 33, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (106, 66, 1, 1000, 11, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (107, 66, 2, 1000, 22, 0)
INSERT [dbo].[lineasordenescompras] ([nroLineaOrdenCompra], [nroOrdenCompra], [codArticulo], [cantPedir], [precioUnitario], [cantRecibida]) VALUES (108, 66, 3, 1000, 33, 0)
SET IDENTITY_INSERT [dbo].[lineasordenescompras] OFF
SET IDENTITY_INSERT [dbo].[Localidades] ON 

INSERT [dbo].[Localidades] ([Cod_Localidad], [NombreLocalidad], [Cod_Provincia]) VALUES (1, N'Totoras', 1)
SET IDENTITY_INSERT [dbo].[Localidades] OFF
SET IDENTITY_INSERT [dbo].[Movimientos] ON 

INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000B4FFE AS DateTime), N'NotaPedido', 4, 2, 1, 4, 351)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000BD12F AS DateTime), N'Cancelación de Nota', 4, 2, 2, 0, 352)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000BEEF3 AS DateTime), N'NotaPedido', 4, 2, 3, 4, 353)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000C1C0A AS DateTime), N'Cancelación de Nota', 4, 2, 4, 0, 354)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000D1AB7 AS DateTime), N'NotaPedido', 4, 2, 5, 4, 355)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000D3DEF AS DateTime), N'Cancelación de Nota', 4, 2, 6, 0, 356)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000DBF01 AS DateTime), N'NotaPedido', 4, 2, 7, 4, 357)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000DE701 AS DateTime), N'Cancelación de Nota', 4, 2, 8, 0, 358)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000E92B8 AS DateTime), N'NotaPedido', 4, 2, 9, 4, 359)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000F8E75 AS DateTime), N'Cancelación de Nota', 4, 2, 10, 0, 360)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A168000FCB31 AS DateTime), N'NotaPedido', 4, 2, 11, 4, 361)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1680010A23E AS DateTime), N'NotaPedido', 4, 2, 12, 8, 362)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1680010D6AE AS DateTime), N'Cancelación de Nota', 4, 2, 13, 4, 363)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1680011C06B AS DateTime), N'NotaPedido', 20, 2, 14, 24, 364)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A16800122A5A AS DateTime), N'Cancelación de Nota', 20, 2, 15, 4, 365)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A16800142B9C AS DateTime), N'Cancelación de Nota', 4, 2, 16, 0, 366)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A16800143569 AS DateTime), N'Pago', 150, 2, 17, -150, 367)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1660188B1FE AS DateTime), N'Pago', 200, 1, 1, -200, 368)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1660188CBDD AS DateTime), N'NotaPedido', 2, 1, 2, -198, 369)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1660189ED8E AS DateTime), N'Cancelación de Nota', 2, 1, 3, -200, 370)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A167018A4F6B AS DateTime), N'NotaPedido', 7, 1, 4, -193, 371)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A1680013400A AS DateTime), N'NotaPedido', 6, 1, 5, -187, 372)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A16800138227 AS DateTime), N'Cancelación de Nota', 6, 1, 6, -193, 373)
INSERT [dbo].[Movimientos] ([fecha], [concepto], [monto], [nroctacte], [nromovimiento], [saldo], [idMov]) VALUES (CAST(0x0000A16800146667 AS DateTime), N'Cancelación de Nota', 7, 1, 7, -200, 374)
SET IDENTITY_INSERT [dbo].[Movimientos] OFF
SET IDENTITY_INSERT [dbo].[NotasPedidos] ON 

INSERT [dbo].[NotasPedidos] ([nroNotaPedido], [fechaEmision], [formaPago], [codCli], [idUsuario]) VALUES (1, CAST(0x0000A4E100F64664 AS DateTime), 1, 4, NULL)
INSERT [dbo].[NotasPedidos] ([nroNotaPedido], [fechaEmision], [formaPago], [codCli], [idUsuario]) VALUES (2, CAST(0x0000A4E100F76AE2 AS DateTime), 1, 4, 1)
INSERT [dbo].[NotasPedidos] ([nroNotaPedido], [fechaEmision], [formaPago], [codCli], [idUsuario]) VALUES (3, CAST(0x0000A4F70171B5DB AS DateTime), 1, 4, 1)
INSERT [dbo].[NotasPedidos] ([nroNotaPedido], [fechaEmision], [formaPago], [codCli], [idUsuario]) VALUES (4, CAST(0x0000A50C0111AC8A AS DateTime), 1, 4, 1)
SET IDENTITY_INSERT [dbo].[NotasPedidos] OFF
SET IDENTITY_INSERT [dbo].[OrdenesCompras] ON 

INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (8, CAST(0x0000A498011BB54C AS DateTime), CAST(0x0000A498011BB54C AS DateTime), 3, 3, N'EnCurso', CAST(0x0000A498011BCC5E AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (9, CAST(0x0000A498011D0348 AS DateTime), CAST(0x0000A498011D0348 AS DateTime), 4, 4, N'EnCurso', CAST(0x0000A498011D65A4 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (10, CAST(0x0000A498011E1328 AS DateTime), CAST(0x0000A498011E1328 AS DateTime), 1, 3, N'EnCurso', CAST(0x0000A498011E52A8 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (12, CAST(0x0000A4980129C09C AS DateTime), CAST(0x0000A4980129C09C AS DateTime), 1, 3, N'EnCurso', CAST(0x0000A4980129D72C AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (13, CAST(0x0000A49F010FB5D0 AS DateTime), CAST(0x0000A4AD010FB5D0 AS DateTime), 1, 3, N'EnCurso', CAST(0x0000A49F010FF1EC AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (15, CAST(0x0000A4BD00C44E3C AS DateTime), CAST(0x0000A4BD00000000 AS DateTime), 1, 3, N'EnCurso', CAST(0x0000A4BD00C44E3D AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (16, CAST(0x0000A4BD00CB69F4 AS DateTime), CAST(0x0000A4CC00000000 AS DateTime), 1, 3, N'Anulado', CAST(0x0000A4BD00CB69F6 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (17, CAST(0x0000A4C400DB9CB9 AS DateTime), CAST(0x0000A4D300000000 AS DateTime), 3, 3, N'EnCurso', CAST(0x0000A4C400DB9CBA AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (18, CAST(0x0000A4C4012CCD7E AS DateTime), CAST(0x0000A4D300000000 AS DateTime), 1, 4, N'EnCurso', CAST(0x0000A4C4012CCD7E AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (19, CAST(0x0000A4C4013B896B AS DateTime), CAST(0x0000A4D300000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A4C4013B896B AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (20, CAST(0x0000A4C40159A602 AS DateTime), CAST(0x0000A4D300000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A4C40159A602 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (21, CAST(0x0000A4C5017D6686 AS DateTime), CAST(0x0000A4D400000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A4C5017D6693 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (22, CAST(0x0000A4C600D25E5E AS DateTime), CAST(0x0000A4D500000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A4C600D25E5E AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (23, CAST(0x0000A4C600E089EE AS DateTime), CAST(0x0000A4D500000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A4C600E089EE AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (24, CAST(0x0000A4C6015579F3 AS DateTime), CAST(0x0000A4D500000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A4C6015579F3 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (25, CAST(0x0000A4C700C7B788 AS DateTime), CAST(0x0000A4D600000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A4C700C7B789 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (26, CAST(0x0000A4D301791E40 AS DateTime), CAST(0x0000A4E200000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A4D301791E40 AS DateTime), N'usu')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (27, CAST(0x0000A4E100F898BE AS DateTime), CAST(0x0000A4E100000000 AS DateTime), 4, 4, N'Finalizado', CAST(0x0000A4E100F898BE AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (28, CAST(0x0000A4F701718761 AS DateTime), CAST(0x0000A50600000000 AS DateTime), 1, 4, N'Anulado', CAST(0x0000A4F701718762 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (29, CAST(0x0000A50C00FB382C AS DateTime), CAST(0x0000A50D00000000 AS DateTime), 3, 3, N'Anulado', CAST(0x0000A50C00FB382D AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (30, CAST(0x0000A50C00FF59A6 AS DateTime), CAST(0x0000A51E00000000 AS DateTime), 4, 4, N'Anulado', CAST(0x0000A50C00FF59A6 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (31, CAST(0x0000A52100F8850F AS DateTime), CAST(0x0000A52300000000 AS DateTime), 3, 3, N'Finalizado', CAST(0x0000A52100F88510 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (32, CAST(0x0000A52100F8B1C4 AS DateTime), CAST(0x0000A52300000000 AS DateTime), 3, 4, N'Finalizado', CAST(0x0000A52100F8B1C4 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (33, CAST(0x0000A52600DBD233 AS DateTime), CAST(0x0000A53500000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52600DBD233 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (34, CAST(0x0000A52600DBEF5A AS DateTime), CAST(0x0000A53500000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52600DBEF5A AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (35, CAST(0x0000A52600DC1510 AS DateTime), CAST(0x0000A53500000000 AS DateTime), 4, 3, N'Pendiente', CAST(0x0000A52600DC1510 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (36, CAST(0x0000A52600DE527C AS DateTime), CAST(0x0000A53500000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52600DE527C AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (37, CAST(0x0000A52600E0A98B AS DateTime), CAST(0x0000A53500000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52600E0A98C AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (38, CAST(0x0000A52800F29CDD AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52800F29CDD AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (39, CAST(0x0000A52800F2D953 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52800F2D953 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (40, CAST(0x0000A52800F58E36 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A52800F58E37 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (41, CAST(0x0000A52800F5F4D8 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52800F5F4D9 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (42, CAST(0x0000A52800F74FDA AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52800F74FDC AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (43, CAST(0x0000A52800F9A03F AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52800F9A03F AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (44, CAST(0x0000A52800FB4DDE AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52800FB4DDE AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (45, CAST(0x0000A528010189E1 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A528010189E3 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (46, CAST(0x0000A5280102F4E2 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A5280102F4E3 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (47, CAST(0x0000A5280104E217 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A5280104E218 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (48, CAST(0x0000A52801053EA7 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A52801053EA7 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (49, CAST(0x0000A528010E3876 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A528010E3877 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (50, CAST(0x0000A528017DF438 AS DateTime), CAST(0x0000A53200000000 AS DateTime), 4, 4, N'Finalizado', CAST(0x0000A528017DF438 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (51, CAST(0x0000A528017E29A9 AS DateTime), CAST(0x0000A53700000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A528017E29A9 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (52, CAST(0x0000A528017E687A AS DateTime), CAST(0x0000A54000000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A528017E687A AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (53, CAST(0x0000A528017E95A4 AS DateTime), CAST(0x0000A54100000000 AS DateTime), 4, 4, N'Finalizado', CAST(0x0000A528017E95A4 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (54, CAST(0x0000A53400E97769 AS DateTime), CAST(0x0000A54300000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A53400E9776A AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (55, CAST(0x0000A53400E99D6C AS DateTime), CAST(0x0000A54300000000 AS DateTime), 1, 4, N'Finalizado', CAST(0x0000A53400E99D6C AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (56, CAST(0x0000A53B00CF7F16 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A53B00CF7F17 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (57, CAST(0x0000A53B00CFBEDC AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A53B00CFBEDC AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (58, CAST(0x0000A53B00D157B8 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Finalizado', CAST(0x0000A53B00D157B8 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (59, CAST(0x0000A53B00D2C9C7 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 4, N'Pendiente', CAST(0x0000A53B00D2C9C7 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (60, CAST(0x0000A53B00D4333D AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00D4333D AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (61, CAST(0x0000A53B00D4CF11 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 4, N'Pendiente', CAST(0x0000A53B00D4CF11 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (62, CAST(0x0000A53B00DA4EEF AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00DA4EF1 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (63, CAST(0x0000A53B00DEBDF8 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00DEBDF9 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (64, CAST(0x0000A53B00E1A248 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00E1A248 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (65, CAST(0x0000A53B00E34116 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00E34117 AS DateTime), N'debi')
INSERT [dbo].[OrdenesCompras] ([nroOrdenCompra], [fechaEmision], [fechaPlazo], [formaPago], [codProv], [estado], [fechahora], [usuario]) VALUES (66, CAST(0x0000A53B00E52117 AS DateTime), CAST(0x0000A54A00000000 AS DateTime), 1, 3, N'Pendiente', CAST(0x0000A53B00E52118 AS DateTime), N'debi')
SET IDENTITY_INSERT [dbo].[OrdenesCompras] OFF
SET IDENTITY_INSERT [dbo].[Proveedores] ON 

INSERT [dbo].[Proveedores] ([codProv], [cuit], [razonSocial], [nomFantasia], [direccion], [ciudad], [codPostal], [telefono], [fax], [celular], [email], [tipoIva]) VALUES (3, 37288215291, N'Metalúrgica Dorrego', N'Metalúrgica Dorrego', N'Dorrego 2321', 1, 2000, N'4354433', N'', N'', N'metdorrego@arnet.com.ar', N'Resp inscripto')
INSERT [dbo].[Proveedores] ([codProv], [cuit], [razonSocial], [nomFantasia], [direccion], [ciudad], [codPostal], [telefono], [fax], [celular], [email], [tipoIva]) VALUES (4, 20342221231, N'Bulontuerc', N'Bulontuerc', N'Mendoza 4533', 1, 2000, N'4321222', N'4322222', N'', N'bulontuerc@bulontuerc.com.ar', N'Resp inscripto')
SET IDENTITY_INSERT [dbo].[Proveedores] OFF
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (1, N'Buenos Aires')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (2, N'Catamarca')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (3, N'Chaco')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (4, N'Chubut')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (5, N'Cordoba')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (6, N'Corrientes')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (7, N'Entre Rios')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (8, N'Formosa')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (9, N'Jujuy')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (10, N'La Pampa')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (11, N'La Rioja')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (12, N'Mendoza')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (13, N'Misiones')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (14, N'Neuquen')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (15, N'Rio Negro')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (16, N'Salta')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (17, N'San Juan')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (18, N'San Luis')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (19, N'Santa Cruz')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (20, N'Santa Fe')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (21, N'Santiago del Estero')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (22, N'Tierra del Fuego')
INSERT [dbo].[Provincias] ([Cod_Provincia], [NombreProvincia]) VALUES (23, N'Tucuman')
INSERT [dbo].[tiposIva] ([nombreTipo], [descripcion]) VALUES (N'Monotributo', N'Está inscripto en iva monotributo')
INSERT [dbo].[tiposIva] ([nombreTipo], [descripcion]) VALUES (N'Resp inscripto', N'Está inscripto en iva')
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Articulos__45F365D3]    Script Date: 24/10/2015 14:17:26 ******/
ALTER TABLE [dbo].[Articulos] ADD  CONSTRAINT [UQ__Articulos__45F365D3] UNIQUE NONCLUSTERED 
(
	[codigoArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__CuentasC__407E6F740F36E0E0]    Script Date: 24/10/2015 14:17:26 ******/
ALTER TABLE [dbo].[CuentasCorrientes] ADD UNIQUE NONCLUSTERED 
(
	[codcli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__GruposAr__CCA6CCDCDE82CBA6]    Script Date: 24/10/2015 14:17:26 ******/
ALTER TABLE [dbo].[GruposArticulos] ADD UNIQUE NONCLUSTERED 
(
	[grupoArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [uq]    Script Date: 24/10/2015 14:17:26 ******/
ALTER TABLE [dbo].[lineasordenescompras] ADD  CONSTRAINT [uq] UNIQUE NONCLUSTERED 
(
	[nroOrdenCompra] ASC,
	[codArticulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[lineasordenescompras] ADD  CONSTRAINT [DF_lineasordenescompras_cantRecibida]  DEFAULT ((0)) FOR [cantRecibida]
GO
ALTER TABLE [dbo].[Articulos]  WITH CHECK ADD  CONSTRAINT [FK__Articulos__codGr__36B12243] FOREIGN KEY([codGrupoArticulo])
REFERENCES [dbo].[GruposArticulos] ([codGrupoArticulo])
GO
ALTER TABLE [dbo].[Articulos] CHECK CONSTRAINT [FK__Articulos__codGr__36B12243]
GO
ALTER TABLE [dbo].[Articulos]  WITH CHECK ADD  CONSTRAINT [FK_Articulos_Alicuotas] FOREIGN KEY([id_alicuota])
REFERENCES [dbo].[Alicuotas] ([id_alicuota])
GO
ALTER TABLE [dbo].[Articulos] CHECK CONSTRAINT [FK_Articulos_Alicuotas]
GO
ALTER TABLE [dbo].[ArticulosProveedores]  WITH CHECK ADD  CONSTRAINT [FK__Articulos__codAr__3F466844] FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[ArticulosProveedores] CHECK CONSTRAINT [FK__Articulos__codAr__3F466844]
GO
ALTER TABLE [dbo].[ArticulosProveedores]  WITH CHECK ADD FOREIGN KEY([codProv])
REFERENCES [dbo].[Proveedores] ([codProv])
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK__Clientes__ciudad__1920BF5C] FOREIGN KEY([ciudad])
REFERENCES [dbo].[Localidades] ([Cod_Localidad])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK__Clientes__ciudad__1920BF5C]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK__Clientes__tipoIv__1A14E395] FOREIGN KEY([tipoIva])
REFERENCES [dbo].[tiposIva] ([nombreTipo])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK__Clientes__tipoIv__1A14E395]
GO
ALTER TABLE [dbo].[CuentasCorrientes]  WITH CHECK ADD  CONSTRAINT [FK__CuentasCo__codcl__3B40CD36] FOREIGN KEY([codcli])
REFERENCES [dbo].[Clientes] ([codCli])
GO
ALTER TABLE [dbo].[CuentasCorrientes] CHECK CONSTRAINT [FK__CuentasCo__codcl__3B40CD36]
GO
ALTER TABLE [dbo].[CuentasCorrientesProveedores]  WITH CHECK ADD FOREIGN KEY([codProv])
REFERENCES [dbo].[Proveedores] ([codProv])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([nroNotaPedido])
REFERENCES [dbo].[NotasPedidos] ([nroNotaPedido])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([nroNotaPedido])
REFERENCES [dbo].[NotasPedidos] ([nroNotaPedido])
GO
ALTER TABLE [dbo].[LineasNotasPedidos]  WITH CHECK ADD FOREIGN KEY([nroNotaPedido])
REFERENCES [dbo].[NotasPedidos] ([nroNotaPedido])
GO
ALTER TABLE [dbo].[lineasordenescompras]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[lineasordenescompras]  WITH CHECK ADD FOREIGN KEY([nroOrdenCompra])
REFERENCES [dbo].[OrdenesCompras] ([nroOrdenCompra])
GO
ALTER TABLE [dbo].[LineasRemitosClientes]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[LineasRemitosClientes]  WITH CHECK ADD FOREIGN KEY([nroRemitoCliente])
REFERENCES [dbo].[RemitosClientes] ([nroRemitoCliente])
GO
ALTER TABLE [dbo].[LineasRemitosProveedores]  WITH CHECK ADD FOREIGN KEY([codArticulo])
REFERENCES [dbo].[Articulos] ([codArticulo])
GO
ALTER TABLE [dbo].[LineasRemitosProveedores]  WITH CHECK ADD FOREIGN KEY([nroRemitoProveedor])
REFERENCES [dbo].[RemitosProveedores] ([nroRemitoProveedor])
GO
ALTER TABLE [dbo].[Localidades]  WITH CHECK ADD FOREIGN KEY([Cod_Provincia])
REFERENCES [dbo].[Provincias] ([Cod_Provincia])
GO
ALTER TABLE [dbo].[NotasPedidos]  WITH CHECK ADD  CONSTRAINT [FK__NotasPedi__codCl__0B91BA14] FOREIGN KEY([codCli])
REFERENCES [dbo].[Clientes] ([codCli])
GO
ALTER TABLE [dbo].[NotasPedidos] CHECK CONSTRAINT [FK__NotasPedi__codCl__0B91BA14]
GO
ALTER TABLE [dbo].[NotasPedidos]  WITH CHECK ADD FOREIGN KEY([formaPago])
REFERENCES [dbo].[FormasPagos] ([formaPago])
GO
ALTER TABLE [dbo].[OrdenesCompras]  WITH CHECK ADD FOREIGN KEY([codProv])
REFERENCES [dbo].[Proveedores] ([codProv])
GO
ALTER TABLE [dbo].[OrdenesCompras]  WITH CHECK ADD  CONSTRAINT [FK_OrdenesCompras_FormasPagos] FOREIGN KEY([formaPago])
REFERENCES [dbo].[FormasPagos] ([formaPago])
GO
ALTER TABLE [dbo].[OrdenesCompras] CHECK CONSTRAINT [FK_OrdenesCompras_FormasPagos]
GO
ALTER TABLE [dbo].[Proveedores]  WITH CHECK ADD FOREIGN KEY([ciudad])
REFERENCES [dbo].[Localidades] ([Cod_Localidad])
GO
ALTER TABLE [dbo].[Proveedores]  WITH CHECK ADD  CONSTRAINT [FK_Proveedores_tiposIva] FOREIGN KEY([tipoIva])
REFERENCES [dbo].[tiposIva] ([nombreTipo])
GO
ALTER TABLE [dbo].[Proveedores] CHECK CONSTRAINT [FK_Proveedores_tiposIva]
GO
ALTER TABLE [dbo].[RemitosClientes]  WITH CHECK ADD FOREIGN KEY([codCli])
REFERENCES [dbo].[Clientes] ([codCli])
GO
ALTER TABLE [dbo].[RemitosProveedores]  WITH CHECK ADD FOREIGN KEY([nroOrdenCompra])
REFERENCES [dbo].[OrdenesCompras] ([nroOrdenCompra])
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[25] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "OrdenesCompras"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vistaPrueba'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vistaPrueba'
GO
USE [master]
GO
ALTER DATABASE [Buloneria] SET  READ_WRITE 
GO
