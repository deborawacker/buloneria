﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Controladora.Seguridad
{
    public class ControladoraGestionarUsuarios
    {
        private static ControladoraGestionarUsuarios Instancia = null;

        private ControladoraGestionarUsuarios() { }

        public static ControladoraGestionarUsuarios ObtenerInstancia()
        {
            if (Instancia == null) Instancia = new ControladoraGestionarUsuarios();
            return Instancia;
        }

        /*public bool AgregarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            if ((Entidades.Seguridad.Usuario)Modelo.Seguridad.Facade.Buscar(oUsuario) == null)
            {
                string pwdRnd = Servicios.GenerarPassword.CrearContraseñaAleatoria(8);
                string pwdEncrypt = Servicios.Encriptacion.EncriptarPassword(pwdRnd);
                oUsuario.CambiarContraseña(null, pwdEncrypt); 

                if (Modelo.Seguridad.Facade.Agregar(oUsuario))
                {
                    string mensaje = "Sus datos para ingresar al sistema son: \nNombre de Usuario: " + oUsuario.NombreUsuario + "\nContraseña: " + pwdRnd + "\n\n ";
                    string asunto = "Su clave de uso del sistema";
                    Servicios.MailSender.EnviarMail(oUsuario.Email, mensaje, asunto);
                    return true;
                }
                else return false;
            }
            else return false;
        }*/

        public bool GuardarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {

            /*string claveNoEncriptada = oUsuario.Clave;
            Servicios.GenerarPassword.CrearContraseñaAleatoria(8);
            string claveEncriptada = Servicios.Encriptacion.EncriptarPassword(oUsuario.Clave);
            oUsuario.Clave = claveEncriptada;
            oUsuario.CambiarContraseña(null, claveEncriptada);*/
            string pwdRnd = Servicios.GenerarPassword.CrearContraseñaAleatoria(8);
            string pwdEncrypt = Servicios.Cryptos.Encriptar(pwdRnd);
            oUsuario.ClaveNoEncriptada = pwdRnd;
            oUsuario.CambiarContraseña(null, pwdEncrypt);
            if (Modelo.Seguridad.Facade.Agregar(oUsuario))
            {
                return true;

            }
            else
            {
                return false;
            }

        }

        public ReadOnlyCollection<Entidades.Seguridad.Usuario> RecuperarUsuarios()
        {
            return ((ReadOnlyCollection<Entidades.Seguridad.Usuario>)Modelo.Seguridad.Facade.Recuperar("Usuarios"));
        }

        public bool EliminarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            Modelo.Seguridad.Facade.Eliminar(oUsuario);
            return true;
        }

        public bool ModificarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            Modelo.Seguridad.Facade.Modificar(oUsuario);
            return true;
        }

        public bool HabilitarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            Modelo.Seguridad.Facade.Modificar(oUsuario);
            return true;
        }

        public bool InhabilitarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            Modelo.Seguridad.Facade.Modificar(oUsuario);
            return true;
        }

        public ReadOnlyCollection<Entidades.Seguridad.Usuario> Filtrar(string parametro)
        {
            ReadOnlyCollection<Entidades.Seguridad.Usuario> ColeccionUsuarios = ((ReadOnlyCollection<Entidades.Seguridad.Usuario>)Modelo.Seguridad.Facade.Recuperar("Usuarios"));
            List<Entidades.Seguridad.Usuario> ListaFiltrada = new List<Entidades.Seguridad.Usuario>(); //aca en la lista filtrada voy a guardar la lista que encuentre segun el parametro
            foreach (Entidades.Seguridad.Usuario oUsu in ColeccionUsuarios)  //para cada usuario que voy encontrando en la coleccion
            {
                if (oUsu.NombreUsuario.ToLower().Contains(parametro.ToLower()) || oUsu.NombreYApellido.ToLower().Contains(parametro.ToLower())) //tengo que comparar si el nombre contiene algo del parametro, y del apellido si contiene algo del parametro
                {
                    ListaFiltrada.Add(oUsu); //agrega a la lista filtrada el usuario que tiene algo de lo que encontro
                }
            }
            return ListaFiltrada.AsReadOnly(); //devuelve la lista como solo lectura
        }

        public


        bool ResetearContraseña(Entidades.Seguridad.Usuario usuarioMail)
        {
            Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)Modelo.Seguridad.Facade.Buscar(usuarioMail);

            if (oUsuario != null)
            {
                string nuevaPass = Servicios.GenerarPassword.CrearContraseñaAleatoria(8);
                string nuevaPassEncriptada = Servicios.Encriptacion.EncriptarPassword(nuevaPass);
                oUsuario.CambiarContraseña(oUsuario.Clave, nuevaPassEncriptada);
                if (Modelo.Seguridad.Facade.Modificar(oUsuario))
                {
                    string mensaje = "Se ha reseteado su contraseña \n Usuario: " + oUsuario.NombreUsuario + "\n Nueva Contraseña: " + nuevaPass;
                    string asunto = "Contraseña Reseteada";
                    return Servicios.MailSender.EnviarMail(oUsuario.Email, mensaje, asunto);
                }
                else return false;
            }
            else return false;


        }

        /*public ReadOnlyCollection<Entidades.Seguridad.Grupo> RecuperarGrupos()
        {
            return ((ReadOnlyCollection<Entidades.Seguridad.Grupo>)Modelo.Seguridad.Facade.Recuperar("Grupos"));
        }*/
    }
}
