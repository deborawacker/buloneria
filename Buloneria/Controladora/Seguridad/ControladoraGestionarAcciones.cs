﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Seguridad
{
    public class ControladoraGestionarAcciones
    {
        private static ControladoraGestionarAcciones _Instancia;
        public static ControladoraGestionarAcciones ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new ControladoraGestionarAcciones();
            return _Instancia;
        }
        private ControladoraGestionarAcciones()
        {
           
        }

        public List<Entidades.Seguridad.Accion> RecuperarAcciones()
        {
            return ((List<Entidades.Seguridad.Accion>)Modelo.Seguridad.Facade.Recuperar("Acciones"));
        }
        public void AsignarAccionesAPerfil(Entidades.Seguridad.Perfil perfil, List<Entidades.Seguridad.Accion> accionesAAgregar)
        {
            Modelo.Seguridad.CatalogoAcciones.ObtenerInstancia().AsignarAccionesAPerfil(perfil, accionesAAgregar);
        }
    }
}
