﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Seguridad
{
	public class ControladoraCambiarContraseña
	{
        private static ControladoraCambiarContraseña Instancia = null;
        public static ControladoraCambiarContraseña ObtenerInstancia() 
        {
            if (Instancia == null) 
            {
                Instancia = new ControladoraCambiarContraseña();
            }
            return Instancia;
        }

        private ControladoraCambiarContraseña() { }

        public bool CambiarContraseña(Entidades.Seguridad.Usuario oUsuario, string PassVieja, string PassNueva) 
        {
            string PassViejaEncriptada = Servicios.Cryptos.Encriptar(PassVieja);
            string PassNuevaEncriptada = Servicios.Cryptos.Encriptar(PassNueva);
            if (oUsuario.CambiarContraseña(PassViejaEncriptada, PassNuevaEncriptada))
            {
               Modelo.Seguridad.Facade.Modificar(oUsuario);
               return true;
            }
            else return false;
        }
	}
}
