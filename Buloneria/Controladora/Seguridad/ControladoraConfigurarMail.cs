﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Net.Mail;
namespace Controladora.Seguridad
{
	public class ControladoraConfigurarMail
	{
        private static ControladoraConfigurarMail Instancia = null;
        public static ControladoraConfigurarMail ObtenerInstancia()
        {
            if (Instancia == null)
            {
                Instancia = new ControladoraConfigurarMail();
            }
            return Instancia;
        }
        public Entidades.Seguridad.DatosCorreo RecuperarDatosCorreo()
        {
            Entidades.Seguridad.DatosCorreo oDatosCorreo = new Entidades.Seguridad.DatosCorreo();
            oDatosCorreo.DireccionMail = System.Configuration.ConfigurationManager.AppSettings["MailAddress"].ToString();
            oDatosCorreo.Smtp = System.Configuration.ConfigurationManager.AppSettings["SMTP"].ToString();
            oDatosCorreo.PuertoSmtp = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPORT"].ToString());
            oDatosCorreo.UsaCredencialesDefault = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["UseDefaultCredentials"]);
            oDatosCorreo.Usuario = System.Configuration.ConfigurationManager.AppSettings["Usuario"].ToString();
            oDatosCorreo.Clave = System.Configuration.ConfigurationManager.AppSettings["Clave"].ToString();
            oDatosCorreo.SSL = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableSSL"].ToString());
            oDatosCorreo.NombreEmisor = System.Configuration.ConfigurationManager.AppSettings["NombreEmisor"].ToString();
            return oDatosCorreo;
        }
        public bool ProbarDatos(Entidades.Seguridad.DatosCorreo oDatosCorreo)
        {
   
            return Servicios.MailSender.EnviarMail("martingallardo_271@hotmail.com","Prueba de Configuracion Exitosa","Prueba de Configuracion de SMTP");

        }
        public bool ModificarDatosCorreo(Entidades.Seguridad.DatosCorreo oDatosCorreo)
        {
            if (ProbarDatos(oDatosCorreo))
            {
                return GuardarDatos(oDatosCorreo);   
            }
            else return false;
       
        }
        private bool GuardarDatos(Entidades.Seguridad.DatosCorreo oDatosCorreo)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["MailAddress"].Value = oDatosCorreo.DireccionMail;
            config.AppSettings.Settings["Clave"].Value = oDatosCorreo.Clave;
            config.AppSettings.Settings["NombreEmisor"].Value = oDatosCorreo.NombreEmisor;
            config.AppSettings.Settings["SMTPPORT"].Value = oDatosCorreo.PuertoSmtp.ToString();
            config.AppSettings.Settings["SMTP"].Value = oDatosCorreo.Smtp;
            config.AppSettings.Settings["EnableSSL"].Value = oDatosCorreo.SSL.ToString();
            config.AppSettings.Settings["UseDefaultCredentials"].Value = oDatosCorreo.UsaCredencialesDefault.ToString();
            config.AppSettings.Settings["Usuario"].Value = oDatosCorreo.Usuario;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            return true;
        }

    }
}
