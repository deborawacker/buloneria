﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Controladora.Seguridad
{
	public class ControladoraIniciarSesion
	{
        private static Controladora.Seguridad.ControladoraIniciarSesion Instancia = null;
        
        private ControladoraIniciarSesion ()
        {
        }
        public static Controladora.Seguridad.ControladoraIniciarSesion ObtenerInstancia()
        {
            if (Instancia == null)
                Instancia = new Controladora.Seguridad.ControladoraIniciarSesion();
            return Instancia;
        }
        public Entidades.Seguridad.Usuario IniciarSesion(string NombreUsuario, string Pass)
        {
           Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)Modelo.Seguridad.Facade.Buscar(NombreUsuario);

           string passEnc = Servicios.Cryptos.Encriptar(Pass);
            
           if (oUsuario != null && oUsuario.Clave == passEnc)//  && oUsuario.EstadoSituacion == true)
           {
               Entidades.Auditoria.LogInOut oLogin = new Entidades.Auditoria.LogInOut();
               oLogin.FechaHora = System.DateTime.Now;
               oLogin.NombreEquipo = Environment.MachineName;
               oLogin.Operacion = Entidades.Auditoria.TipoInicioSesion.Ingreso;
               oLogin.Usuario = oUsuario.NombreUsuario;
               Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().AgregarRegistro(oLogin);
               return oUsuario;
           }
            
           return null;
        }
        /*public ReadOnlyCollection<Entidades.Seguridad.Perfil> RecuperarPerfilesXGrupos(System.Collections.ObjectModel.ReadOnlyCollection<Entidades.Seguridad.Grupo> Grupos)
        {
            List<Entidades.Seguridad.Perfil> perf = new List<Entidades.Seguridad.Perfil>();
            foreach (Entidades.Seguridad.Perfil item in (ReadOnlyCollection<Entidades.Seguridad.Perfil>)Modelo.Seguridad.Facade.Recuperar("Perfiles"))
            {
                foreach (Entidades.Seguridad.Grupo grupo in Grupos)
                {
                    if (item.Grupo.Nombre == grupo.Nombre) perf.Add(item);  
                }

            }

            return perf.AsReadOnly();
        }*/

         

        public void CerrarSesion(Entidades.Seguridad.Usuario user)
        {
            if (user != null )
            {
                Entidades.Auditoria.LogInOut oLogOut = new Entidades.Auditoria.LogInOut();
                oLogOut.FechaHora = System.DateTime.Now;
                oLogOut.NombreEquipo = Environment.MachineName;
                oLogOut.Operacion = Entidades.Auditoria.TipoInicioSesion.Egreso;
                oLogOut.Usuario = user.NombreUsuario;
                Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().AgregarRegistro(oLogOut);
            }
        }
    }
}
