﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Seguridad
{
	public class ControladoraCerrarSesion
	{
        private static ControladoraCerrarSesion Instancia = null;
        private ControladoraCerrarSesion() { }
        public static ControladoraCerrarSesion ObtenerInstancia()
        {
            if (Instancia == null)
            {
                Instancia = new ControladoraCerrarSesion();
            }
            return Instancia;
        }
        public void CerrarSesion(Entidades.Seguridad.Usuario oUsuario)
        {
            Entidades.Auditoria.LogInOut oLogOut = new Entidades.Auditoria.LogInOut();
            oLogOut.FechaHora = System.DateTime.Now;
            oLogOut.NombreEquipo = Environment.MachineName;
            oLogOut.Operacion = Entidades.Auditoria.TipoInicioSesion.Egreso;
            oLogOut.Usuario = oUsuario.NombreUsuario;
            Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().AgregarRegistro(oLogOut);
        }

	}
}
