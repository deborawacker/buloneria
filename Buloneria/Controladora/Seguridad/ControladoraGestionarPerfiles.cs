﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Controladora.Seguridad
{
    public class ControladoraGestionarPerfiles
    {
        private static ControladoraGestionarPerfiles _Instancia;
        public static ControladoraGestionarPerfiles ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new ControladoraGestionarPerfiles();
            return _Instancia;
        }
        private ControladoraGestionarPerfiles()
        {
           
        }
        public bool AgregarPerfil(Entidades.Seguridad.Perfil perfil)
        {
            if (!Convert.ToBoolean(Modelo.Seguridad.Facade.Buscar("Perfil")))
            {
                return Modelo.Seguridad.Facade.Agregar(perfil);
                
            }
            else return false;   
        }
        public bool EliminarPerfil(Entidades.Seguridad.Perfil perfil)
        {
            return Modelo.Seguridad.Facade.Eliminar(perfil);
        }

        public List<Entidades.Seguridad.Perfil> RecuperarPerfiles()
        {
            return ((List<Entidades.Seguridad.Perfil>)Modelo.Seguridad.Facade.Recuperar("Perfiles"));
        }

       /* public ReadOnlyCollection<Entidades.Seguridad.Grupo> RecuperarGrupos()
        {
            return ((ReadOnlyCollection<Entidades.Seguridad.Grupo>)Modelo.Seguridad.Facade.Recuperar("Grupos"));
        }*/

       /* public ReadOnlyCollection<Entidades.Seguridad.Formulario> RecuperarFormularios()
        {
            return ((ReadOnlyCollection<Entidades.Seguridad.Formulario>)Modelo.Seguridad.Facade.Recuperar("Formularios"));
        }*/

        /*public ReadOnlyCollection<Entidades.Seguridad.Permiso> RecuperarPermisos()
        {
            return ((ReadOnlyCollection<Entidades.Seguridad.Permiso>)Modelo.Seguridad.Facade.Recuperar("Permisos"));
        }*/

       
 
    }
}
