﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Entidades.Auditoria;

namespace Controladora.Auditoria
{
    public class ControladoraCUVisualizarListadosAuditoriaLog
    {
        #region Singleton
        private static ControladoraCUVisualizarListadosAuditoriaLog _Instancia;
        public static ControladoraCUVisualizarListadosAuditoriaLog ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new ControladoraCUVisualizarListadosAuditoriaLog();
            return _Instancia;
        }
        private ControladoraCUVisualizarListadosAuditoriaLog()
        { } 
        #endregion

        public IEnumerable<LogInOut> RecuperarIngresosEgresosPorCriterio(LoginOutCriterio criterio)
        {
            return Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().RecuperarLogInOutPorCriterio(criterio);
        }

        public List<LogInOut> RecuperarListadosIngresosEgresos()
        {
            return Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().RecuperarLogInOut();
        }
        public List<LogInOut> Actualizar()
        {
            Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().ActualizarColeccion();
            return Modelo.Auditoria.CatalogoAuditoriaLogInOut.ObtenerInstancia().RecuperarLogInOut();
        }
    }
}
