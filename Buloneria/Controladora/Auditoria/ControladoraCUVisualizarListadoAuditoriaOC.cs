﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;
using Entidades.Sistema;

namespace Controladora.Auditoria
{
    public class ControladoraCUVisualizarListadoAuditoriaOC
    {
        private static ControladoraCUVisualizarListadoAuditoriaOC _Instancia;
        public static ControladoraCUVisualizarListadoAuditoriaOC ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new ControladoraCUVisualizarListadoAuditoriaOC();
            return _Instancia;
        }
        private ControladoraCUVisualizarListadoAuditoriaOC()
        { }
        public List<Entidades.Auditoria.AuditoriaOrdenCompra> RecuperarListadosEstados()
        {
            return Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().RecuperarAuditoriaOC();
        }

        public List<Entidades.Auditoria.AuditoriaOrdenCompra> RecuperarHistorialOC(int nroOC)
        {
            return Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().RecuperarHistorialOC(nroOC);
        }
        public List<Entidades.Auditoria.AuditoriaOrdenCompra> Actualizar()
        {
            Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().ActualizarColeccion();
            return Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().RecuperarAuditoriaOC();
        }

        public IEnumerable<AuditoriaOrdenCompra> RecuperarListadoPorCriterio(Entidades.Auditoria.AuditoriaOrdenCompraCriterio criterio)
        {
            return Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().RecuperarListadoPorCriterio(criterio);
        }
    }
}
