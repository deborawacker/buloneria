﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;

namespace Controladora.Auditoria
{
    public class ControladoraAuditoriaProveedor
    {

        #region Singleton
        private static ControladoraAuditoriaProveedor _Instancia;
        public static ControladoraAuditoriaProveedor ObtenerInstancia()
        {
            return _Instancia ?? (_Instancia = new ControladoraAuditoriaProveedor());
        }

        private ControladoraAuditoriaProveedor()
        { } 
        #endregion
         
        public void RegistrarAuditoriaProveedor(Proveedor oAuditoriaProveedor)
        {
            Modelo.Auditoria.CatalogoAuditoriaProveedor.ObtenerInstancia().AgregarRegistro(oAuditoriaProveedor);
        }

        public List<Proveedor> RecuperarAuditoriaProveedor()
        {
            return Modelo.Auditoria.CatalogoAuditoriaProveedor.ObtenerInstancia().RecuperarAuditoriaProveedor();
        }

        public IEnumerable<Proveedor> RecuperarListadoPorCriterio(ProveedorCriterio criterio)
        {
            return Modelo.Auditoria.CatalogoAuditoriaProveedor.ObtenerInstancia().RecuperarListadoPorCriterio(criterio);
        }
    }
}
