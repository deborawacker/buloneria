﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace Controladora.BackupRestore
{
    public class CtrlBackupRestore
    {
        private static CtrlBackupRestore _CtrlBackupRestore;
        
        //private CtrlBackupRestore()
        //{
        //Modelo.Seguridad.Facade.   
        //}

        public static CtrlBackupRestore ObtenerInstancia()
        {
            if (_CtrlBackupRestore == null) _CtrlBackupRestore = new CtrlBackupRestore();
            return _CtrlBackupRestore;
        }

        public bool GuardarBackup(string BaseDeDatos)
        {
            if (!Directory.Exists("C:\\Backup"))
            {
                Directory.CreateDirectory("C:\\Backup");
            }
            
            Servicios.Conexion.ObtenerInstancia().Conectar(BaseDeDatos);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "sp_CreateBackup";
            cmd.ExecuteNonQuery();
             
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return true;
        }

       
        public bool Restore(string BaseDeDatos)
        {

            string commandName="";
            switch (BaseDeDatos)
            {
                case "Auditoria":
                    commandName = "sp_RestoreAuditoria";
                    break;
                case "Sistema":
                    commandName = "sp_RestoreSistema";
                    break;
                case "Seguridad":
                    commandName = "sp_RestoreSeguridad";
                    break;
                default:
                    break;
            }

            try
            {
                Servicios.Conexion.ObtenerInstancia().Conectar("BackupRestore");
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = commandName;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }

           
        }
    }
}
