﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGestionarClientes
    {
        #region Singleton

        private static ControladoraCUGestionarClientes _Instancia;

        public static ControladoraCUGestionarClientes ObtenerInstancia()
        { 
            if(_Instancia==null) 
                _Instancia = new ControladoraCUGestionarClientes();
            return _Instancia;

        }

        private ControladoraCUGestionarClientes()
        { 
        }

        #endregion

        #region Methods

        public List<Cliente> RecuperarClientes() 
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().RecuperarClientes();  
        }

        public List<TipoIva> RecuperarTiposIva()
        {
            return Modelo.Sistema.CatalogoTiposIVA.ObtenerInstancia().RecuperarTiposIva();        
        }

        public List<Provincia> RecuperarProvincias()
        {
            return Modelo.Sistema.CatalogoProvincias.ObtenerInstancia().RecuperarProvincias();        
        }

        public bool AgregarCliente(Cliente oCliente)  
        {
            bool encontrado = Modelo.Sistema.CatalogoClientes.ObtenerInstancia().BuscarCuit(oCliente);  
            if (encontrado == false)  
            {
                Modelo.Sistema.CatalogoClientes.ObtenerInstancia().AgregarCliente(oCliente); 
                return true;   
            }
            else
            {
                return false;
            }
        }

        public void EliminarCliente(Cliente oCliente)
        {
            Modelo.Sistema.CatalogoClientes.ObtenerInstancia().EliminarCliente(oCliente);
        }

        public List<Cliente> FiltrarClientes(string criterioFiltrado) 
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().FiltrarClientes(criterioFiltrado);
        }

        public void ModificarCliente(Cliente oCliente)
        {
            Modelo.Sistema.CatalogoClientes.ObtenerInstancia().ModificarCliente(oCliente);
        }

        public Cliente BuscarCliente(int idCliente)
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().BuscarCliente(idCliente);
        }

        public Ciudad BuscarCiudadPorDescripcion(string ciudadDescripcion)
        {
            Ciudad ret = null;
            var provincias = Modelo.Sistema.CatalogoProvincias.ObtenerInstancia().RecuperarProvincias();
            foreach (var p in provincias)
            {
                foreach (Ciudad c in p.Ciudades)
                {
                    if (c.Nombre.ToUpper().Equals(ciudadDescripcion.Trim().ToUpper()))
                    {
                        ret = c;
                    }
                }
            }
            return ret;
        }


        public TipoIva BuscarTipoIvaPorDescripcion(string tipoIvaDescripcion)
        {
            var tiposIva = Modelo.Sistema.CatalogoTiposIVA.ObtenerInstancia().RecuperarTiposIva();

            return tiposIva.FirstOrDefault(ti => ti.Nombre.Equals(tipoIvaDescripcion));
        }

        public void Guardar(Cliente cliente)
        {
            Modelo.Sistema.CatalogoClientes.ObtenerInstancia().GuardarCliente(cliente);
        }
        #endregion







       
    }
}
