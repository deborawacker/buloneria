﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCURegistrarPagoCliente
    {
        private static ControladoraCURegistrarPagoCliente _Instancia; //creo patron singleton para controladora

        public static ControladoraCURegistrarPagoCliente ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCURegistrarPagoCliente();
            return _Instancia;
        }

        private ControladoraCURegistrarPagoCliente()
        {

        }//constructor privado, singleton

        //metodo recuperar clientes
        public List<Entidades.Sistema.Cliente> RecuperarClientes()
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().RecuperarClientes();
        }     

        Entidades.Sistema.MovimientoCtaCte oMovimiento = new Entidades.Sistema.MovimientoCtaCte();

        Entidades.Sistema.Concepto oConcepto =  new Entidades.Sistema.Concepto();
        
        
        //metodo registrar pago
        public bool RegistrarPago(Entidades.Sistema.MovimientoCtaCte oMovimientoCtaCte,Entidades.Sistema.Cliente cliente)
        {
            Entidades.Sistema.CuentaCorriente oCtaCte = Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().BuscarCuentaCorriente(cliente);
            oConcepto.Concepto1 = "Pago";
            Entidades.Sistema.Concepto concepto = Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().BuscarConcepto(oConcepto);

            //busco el ultimo numero de movimiento y le sumo uno para establecer el numero de movimiento actual
            int nroMov = 0;
            foreach (Entidades.Sistema.MovimientoCtaCte oM in oCtaCte.MovimientosCtaCte)
            {
                if (oM != null)
                {
                    if (oM.NroMovimiento > nroMov)
                    {
                        nroMov = oM.NroMovimiento;
                    }
                }
            }
            nroMov += 1;

            oMovimientoCtaCte.NroMovimiento = nroMov;
            oCtaCte.Saldo -= oMovimientoCtaCte.Monto;
            oMovimientoCtaCte.Concepto = concepto;
            oCtaCte.AgregarMovimiento(oMovimientoCtaCte);
            Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().ModificarCuentaCorriente(oCtaCte);
            return true;        

        }



        public int RecuperarSaldoCliente(Entidades.Sistema.Cliente ocliente)
        {
            return Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().BuscarCuentaCorriente(ocliente).Saldo;
        }
    }
}
