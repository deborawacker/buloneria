﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGenerarNP
    {
        private static ControladoraCUGenerarNP _Instancia; //creo patron singleton para controladora

        public static ControladoraCUGenerarNP ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUGenerarNP();
            return _Instancia;
        }

        private ControladoraCUGenerarNP()
        {
        }//constructor privado, singleton

        //metodo recuperar formas pago
        public List<Entidades.Sistema.FormaPago> RecuperarFormasPago() //este metodo devuelve un listado de formas de pago
        {
            
           return Modelo.Sistema.CatalogoFormasPago.ObtenerInstancia().RecuperarFormasPago(); //me comunico con la capa modelo, y llamo al metodo recuperar formas pago
        }

        //metodo recuperar clientes
        public List<Entidades.Sistema.Cliente> RecuperarClientes()
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().RecuperarClientes();
        }

        //metodo recuperar grupos de articulos
        public List<Entidades.Sistema.GrupoArticulo> RecuperarGruposArticulos()
        {            
            return Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
        }

        //metodo recuperar articulos de un grupo especifico
        public List<Entidades.Sistema.Articulo> RecuperarArticulosGrupo(Entidades.Sistema.GrupoArticulo grupo)
        { 
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulosGrupo(grupo);
        }

        public bool AgregarNotaPedido(Entidades.Sistema.NotaPedido oNP) 
        {
            //Aca devuelvo un bool
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().AgregarNotaPedido(oNP)>0; 
             
        }
        //metodo agregar NP 
       /* public bool AgregarNotaPedido(Entidades.Sistema.NotaPedido oNP)
        {
            bool resultado = Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().BuscarNotaPedido(oNP);
            if (resultado == false)
            {
                Entidades.Sistema.CuentaCorriente oCuentaCorriente = Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().BuscarCuentaCorriente(oNP.Cliente);
                Entidades.Sistema.Concepto oCon = new Entidades.Sistema.Concepto();
                oCon.Concepto1 = "NotaPedido";
                Entidades.Sistema.Concepto oConcepto = Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().BuscarConcepto(oCon);
                Entidades.Sistema.MovimientoCtaCte oMov = new Entidades.Sistema.MovimientoCtaCte();
                oMov.Concepto = oConcepto;
                oMov.Fecha = oNP.FechaEmision;
                //calculo el total de la nota de pedido
                decimal subtotaliva = 0;
                int subtotal = 0;
                foreach (Entidades.Sistema.LineaNotaPedido linea in oNP.LineasNotaPedido)
                {
                    int subtotallinea = linea.PrecioUnitario * linea.CantPedida;
                    subtotal += subtotallinea;
                    decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                    subtotaliva += subtotallineaiva;
                }
                oMov.Monto = Convert.ToInt32(subtotal + subtotaliva);
                
                //busco el ultimo numero de movimiento y le sumo uno para establecer el numero de movimiento actual
                int nroMov=0;
                foreach(Entidades.Sistema.MovimientoCtaCte oM in oCuentaCorriente.MovimientosCtaCte)
               {
                   if (oM != null)
                   {
                       if(oM.NroMovimiento>nroMov)
                       {
                           nroMov = oM.NroMovimiento;
                       }
                   }
               }
                nroMov += 1;

                oMov.NroMovimiento = nroMov;

                oCuentaCorriente.AgregarMovimiento(oMov);
                oCuentaCorriente.Saldo += oMov.Monto;
                Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().ModificarCuentaCorriente(oCuentaCorriente);
               
                Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().AgregarNotaPedido(oNP);
                foreach(Entidades.Sistema.LineaNotaPedido oLin in oNP.LineasNotaPedido)
                {
                    (Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos().Find(delegate(Entidades.Sistema.Articulo oArt) { return oArt.CodArticulo == oLin.Articulo.CodArticulo; })).CantActual -= oLin.CantPedida;
                    Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().ModificarArticulo((Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos().Find(delegate(Entidades.Sistema.Articulo oArt) { return oArt.CodArticulo == oLin.Articulo.CodArticulo; })));
                }
                 
               
                return true;
            }
            else
            {
                return false;
            }
        }*/
    }
}
