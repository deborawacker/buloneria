﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGestionarNP
    {
        private static ControladoraCUGestionarNP _Instancia; //tambien creo patron singleton para controladora

        public static ControladoraCUGestionarNP ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        { 
            if(_Instancia==null) _Instancia = new ControladoraCUGestionarNP();
            return _Instancia;
        }

        private ControladoraCUGestionarNP()
        { 
        }//constructor privado, singleton
        
        //metodo recuperar NP
        public List<Entidades.Sistema.NotaPedido> RecuperarNotasPedidos()
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().RecuperarNotasPedidos();
        }

        //metodo eliminar NP
        public bool EliminarNotaPedido(Entidades.Sistema.NotaPedido np)
        {
            bool resultado = Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().BuscarNotaPedido(np);
            if (resultado == true) //si es verdadero quiere decir que encontro un nronotapedido igual
            {

                Entidades.Sistema.CuentaCorriente oCuentaCorriente = Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().BuscarCuentaCorriente(np.Cliente);
                Entidades.Sistema.Concepto oCon = new Entidades.Sistema.Concepto();
                oCon.Concepto1 = "NP Eliminada";
                Entidades.Sistema.Concepto oConcepto = Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().BuscarConcepto(oCon);
                Entidades.Sistema.MovimientoCtaCte oMov = new Entidades.Sistema.MovimientoCtaCte();
                oMov.Concepto = oConcepto;
                oMov.Fecha = System.DateTime.Now;
                //calculo el total de la nota de pedido
                decimal subtotaliva = 0;
                int subtotal = 0;
                foreach (Entidades.Sistema.LineaNotaPedido linea in np.LineasNotaPedido)
                {
                    int subtotallinea = linea.PrecioUnitario * linea.CantPedida;
                    subtotal += subtotallinea;
                    decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                    subtotaliva += subtotallineaiva;
                }
                oMov.Monto = Convert.ToInt32(subtotal + subtotaliva);

                //busco el ultimo numero de movimiento y le sumo uno para establecer el numero de movimiento actual
                int nroMov = 0;
                foreach (Entidades.Sistema.MovimientoCtaCte oM in oCuentaCorriente.MovimientosCtaCte)
                {
                    if (oM != null)
                    {
                        if (oM.NroMovimiento > nroMov)
                        {
                            nroMov = oM.NroMovimiento;
                        }
                    }
                }
                nroMov += 1;

                oMov.NroMovimiento = nroMov;

                oCuentaCorriente.AgregarMovimiento(oMov);
                oCuentaCorriente.Saldo -= oMov.Monto;
                Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().ModificarCuentaCorriente(oCuentaCorriente);

                Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().EliminarNotaPedido(np);
                foreach (Entidades.Sistema.LineaNotaPedido oLin in np.LineasNotaPedido)
                {
                    (Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos().Find(delegate(Entidades.Sistema.Articulo oArt) { return oArt.CodArticulo == oLin.Articulo.CodArticulo; })).CantActual += oLin.CantPedida;
                    //Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().ModificarArticulo((Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos().Find(delegate(Entidades.Sistema.Articulo oArt) { return oArt.CodArticulo == oLin.Articulo.CodArticulo; })));
                }


                return true;
            }
            else
            {
                return false; //devuelve false porque no pude eliminar
            }
        }

        //metodo filtrar NP
        public List<Entidades.Sistema.NotaPedido> FiltrarNotasPedidos(string texto)
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().FiltrarNotasPedidos(texto);
        }

        //metodo recuperar historial NP
        public List<Entidades.Sistema.NotaPedido> 
        RecuperarHistorialNotasPedidos()
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().RecuperarHistorialNotasPedidos();
        }

        public Entidades.Sistema.NotaPedido BuscarNotasDePedidoPorNro(int nroNotaPedidoBuscada)
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().BuscarNotasDePedidoPorNro(nroNotaPedidoBuscada);
        }

        public void ActualizarEstadoNotaPedido(Entidades.Sistema.NotaPedido notaPedidoSeleccionada)
        {
            Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().ActualizarEstadoNotaPedido(notaPedidoSeleccionada);
        }
    }    
}
