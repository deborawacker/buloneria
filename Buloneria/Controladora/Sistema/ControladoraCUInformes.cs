﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Controladora.Sistema
{
    public class ControladoraCUInformes
    {
        private static ControladoraCUInformes _Instancia; //tambien creo patron singleton para controladora
        
        public static ControladoraCUInformes ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUInformes();
            return _Instancia;
        }

        private ControladoraCUInformes()
        {
        }//constructor privado, singleton


        //metodo recuperar artículos con faltantes de stock, de tipo datatable
        public DataTable RecuperarArticulosFaltStock()
        {
            return Modelo.Sistema.CatalogoInformes.ObtenerInstancia().RecuperarArticulosFaltStock();
        }

        //metodo recuperar articulos mas vendidos, de tipo datatable
        public DataTable RecuperarArtMasVendidos(DateTime fechaDesde, DateTime fechaHasta)
        {
            return Modelo.Sistema.CatalogoInformes.ObtenerInstancia().RecuperarArtMasVendidos(fechaDesde,fechaHasta);
        }

        //metodo recuperar montos de compras por mes de clientes, de tipo datatable
        public DataTable RecuperarMontosComprasMesClientes(DateTime fechadesde, DateTime fechahasta)
        {
            return Modelo.Sistema.CatalogoInformes.ObtenerInstancia().RecuperarMontosComprasMesClientes(fechadesde,fechahasta);
        }

        //metodo recuperar precios entre proveedores de un articulo, de tipo datatable
        public DataTable RecuperarPreciosComparativosArt(string codigoArticulo)
        {
            return Modelo.Sistema.CatalogoInformes.ObtenerInstancia().RecuperarPreciosComparativosArt(codigoArticulo);
        }

        //traigo los articulos
        public List<Entidades.Sistema.Articulo> RecuperarArticulos()
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos();
        }

    }
}
