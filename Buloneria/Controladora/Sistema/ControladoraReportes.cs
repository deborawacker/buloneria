﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraReportes
    {

        private static ControladoraReportes _Instancia; //tambien creo patron singleton para controladora
        public static ControladoraReportes ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraReportes();
            return _Instancia;
        }

        public List<Entidades.Sistema.Cliente> BuscarClientePorCodigoORazonSocial(string codigo, string razonSocial) 
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().FiltrarClientesPorCodigoORazonSocial(codigo,razonSocial);
        }
        public List<Entidades.Sistema.Reportes.ArticulosPedidosXClientes> BuscarClientePorArticulosVendidos(string articulo, string razonSocial, DateTime desde, DateTime hasta)
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().FiltrarClientesPorArticulosVendidos(articulo, razonSocial, desde, hasta);
            
        }



        public Entidades.Sistema.Cliente BuscarClientePorCuit(Int64 nroCuitClienteSeleccionado)
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().BuscarClientePorCUIT(nroCuitClienteSeleccionado);
        }


        public List<Entidades.Sistema.Articulo> BuscarArticulosPorCodigoDescripcion(string codigo, string descripcion)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().FiltrarArticulosPorCodigoDescripcion(codigo, descripcion);
        }



        public Entidades.Sistema.Articulo BuscarArticuloPorCodigo(string codArticulo)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticuloPorCodigo(codArticulo);
        }
    }
}
