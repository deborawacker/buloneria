﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;
using Entidades.Seguridad;
using Entidades.Sistema;
using Modelo.Sistema;
using Proveedor = Entidades.Sistema.Proveedor;

namespace Controladora.Sistema
{
    public class ControladoraCUIngresoMercaderia
    {
        #region Singleton
        private static ControladoraCUIngresoMercaderia _Instancia;

        public static ControladoraCUIngresoMercaderia ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new ControladoraCUIngresoMercaderia();
            return _Instancia;
        }

        private ControladoraCUIngresoMercaderia()
        {
        }
        #endregion

        public List<OrdenCompra> RecuperarOrdenesCompras(Proveedor proveedor)
        {
            return CatalogoOrdenesCompras.ObtenerInstancia().BuscarOrdenCompraPendientesOEnCursoPorProveedor(proveedor);
        }
         
        //metodo recuperar proveedor
        public List<Proveedor> RecuperarProveedores()
        {
            return CatalogoProveedores.ObtenerInstancia().RecuperarProveedores();
        }

        //metodo ingresar merc
        public void IngresarMercaderia(OrdenCompra orden, Usuario usuario) //en el ingresar le voy a mandar la orden y el usuario 
        { 
            orden.Estado = orden.TieneLineasConCantidadFaltante ? EstadoOrdenCompra.Pendiente : EstadoOrdenCompra.Finalizado;

            #region Auditoria de la Orden Compra
            AuditoriaOrdenCompra oAuditoriaOrdenCompra = new AuditoriaOrdenCompra();
            oAuditoriaOrdenCompra.NroOrdenCompra = orden.NroOrdenCompra;
            oAuditoriaOrdenCompra.FechaRegistro = DateTime.Now;
            oAuditoriaOrdenCompra.Usuario = usuario.NombreUsuario;
            oAuditoriaOrdenCompra.Estado = orden.Estado;
            Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().AgregarRegistro(oAuditoriaOrdenCompra);
            #endregion

            CatalogoOrdenesCompras.ObtenerInstancia().ActualizarIngresoMercaderia(orden);

        }

        public OrdenCompra BuscarOrdenCompraPorNumero(int nroOrdenCompra)
        {
            return CatalogoOrdenesCompras.ObtenerInstancia().BuscarOrdenCompraPendientesOEnCursoPorNro(nroOrdenCompra);
        } 
    }
}
