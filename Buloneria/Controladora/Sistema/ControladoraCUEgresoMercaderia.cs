﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema;

namespace Controladora.Sistema
{
    public class ControladoraCUEgresoMercaderia
    {
        #region SingletonPattern

        private static ControladoraCUEgresoMercaderia _Instancia;

        public static ControladoraCUEgresoMercaderia ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new ControladoraCUEgresoMercaderia();
            return _Instancia;
        }

        private ControladoraCUEgresoMercaderia()
        {
            //TODO: ver session al volver a seleccionar la misma nota de pedido
        }
        #endregion



        public List<NotaPedido> BuscarNotasDePedidoPorCriterio(NotaPedidoCriterio npCriterio)
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().BuscarNotaPedidoPorCriterio(npCriterio);
        }

        public NotaPedido BuscarNotasDePedidoPorNro(int nroNotaPedidoBuscada)
        {
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().BuscarNotasDePedidoPorNro(nroNotaPedidoBuscada);
        }

        public Articulo BuscarArticuloPorCriterio(ArticuloCriterio criterio)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticuloPorCriterio(criterio);
        }
         
        public LineaNotaPedido BuscarLineaNotaPedidoEnNotaPedido(int NroLineaNotaPedidoBuscada, NotaPedido notaPedidoSeleccionada)
        {
            return notaPedidoSeleccionada.LineasNotaPedido.FirstOrDefault(l => l.NroLineaNotaPedido == NroLineaNotaPedidoBuscada);
        } 

        public void ActualizarEgresoMercaderia(NotaPedido notaPedido)
        {
            notaPedido.PasarASiguienteEstado();

            Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().ActualizarEstadoNotaPedido(notaPedido);

            Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().ActualizarCantidadEntregadaLineasNotaPedido(notaPedido);
             
            List<Articulo> articulos = notaPedido.LineasNotaPedido.Select(lnp=>lnp.Articulo).ToList();

            Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().Update(articulos);
        } 
    }
}
