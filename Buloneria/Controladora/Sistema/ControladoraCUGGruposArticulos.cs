﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGGruposArticulos
    {
        private static ControladoraCUGGruposArticulos _Instancia; //tambien creo patron singleton para controladora

        public static ControladoraCUGGruposArticulos ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        { 
            if(_Instancia==null) _Instancia = new ControladoraCUGGruposArticulos();
            return _Instancia;

        }

        private ControladoraCUGGruposArticulos()
        { 
        }//constructor privado, singleton

        //metodo recuperar grupos articulos
        public List<Entidades.Sistema.GrupoArticulo> RecuperarGruposArticulos()
        {
            return Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
        }

        //metodo agregar grupo articulo 
        public bool AgregarGrupoArticulo(Entidades.Sistema.GrupoArticulo oGrupoArticulo)
        {
            bool resultado = Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().BuscarGrupoArticulo(oGrupoArticulo);
            if (resultado == false)
            {
                Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().AgregarGrupoArticulo(oGrupoArticulo);
                return true; //devuelve true, porque lo agrego            
            }
            else 
            {
                return false; //devuelve false, porque no lo agrego
            }

        }

        //metodo filtrar grupo articulo
        public List<Entidades.Sistema.GrupoArticulo> FiltrarGruposArticulos(string texto)
        {
            return Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().FiltrarGruposArticulos(texto);
        }

        //metodo modificar grupo articulo
        public bool ModificarGrupoArticulo(Entidades.Sistema.GrupoArticulo oGrupoArticulo)
        {
            return Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().ModificarGrupoArticulo(oGrupoArticulo);
        }

        //metodo eliminar 
        public bool EliminarGrupoArticulo(Entidades.Sistema.GrupoArticulo oGrupoArticulo)
        {
            bool resultado = Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticulosGrupo(oGrupoArticulo);
            if (resultado == false) //si es falso, quiere decir que no encontro ningun codarticulo igual
            {
                Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().EliminarGrupoArticulo(oGrupoArticulo);
                return true; //devuelve true porque lo pudo eliminar
            }
            else
            {
                return false; //devuelve false porque no lo pude eliminar
            }
        }
    }
}
