﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGestionarProveedores
    {
        #region Singleton

        private static ControladoraCUGestionarProveedores _Instancia;

        public static ControladoraCUGestionarProveedores ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new ControladoraCUGestionarProveedores();
            return _Instancia;

        }

        private ControladoraCUGestionarProveedores()
        {
        }

        #endregion

        public List<Proveedor> RecuperarProveedores()
        {
            return Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().RecuperarProveedores();
        }

        public List<Provincia> RecuperarProvincias()
        {
            return Modelo.Sistema.CatalogoProvincias.ObtenerInstancia().RecuperarProvincias();
        }

        public List<TipoIva> RecuperarTiposIva()
        {
            return Modelo.Sistema.CatalogoTiposIVA.ObtenerInstancia().RecuperarTiposIvaProveedores();
        }

        public List<Proveedor> FiltrarProveedores(string criterio)
        {
            return Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().FiltrarProveedores(criterio);
        }
         
        public Proveedor BuscarProveedor(int idProveedor)
        {
            return Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().BuscarProveedor(idProveedor);
        }


        public Ciudad BuscarCiudadPorDescripcion(string ciudadDescripcion)
        {
            Ciudad ret = null;
            var provincias = Modelo.Sistema.CatalogoProvincias.ObtenerInstancia().RecuperarProvincias();
            foreach (var p in provincias)
            {
                foreach (Ciudad c in p.Ciudades)
                {
                    if (c.Nombre.ToUpper().Equals(ciudadDescripcion.Trim().ToUpper()))
                    {
                        ret = c;
                    }
                }
            }
            return ret;
        }

        public TipoIva BuscarTipoIvaPorDescripcion(string tipoIvaDescripcion)
        {
            var tiposIva = Modelo.Sistema.CatalogoTiposIVA.ObtenerInstancia().RecuperarTiposIva();

            return tiposIva.FirstOrDefault(ti => ti.Nombre.Equals(tipoIvaDescripcion));
        }

        public void GuardarProveedor(Proveedor proveedor)
        {
            if (proveedor.CodProveedor != 0)
            {
                Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().ModificarProveedor(proveedor);
            }
            else
            {
                Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().AgregarProveedor(proveedor);
            }

            RegistrarAuditoria(proveedor);
        }
         
        public bool EliminarProveedor(Proveedor oProveedor)
        {
            RegistrarAuditoria(oProveedor);

            return Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().EliminarProveedor(oProveedor);
        }

        public void RegistrarAuditoria(Proveedor proveedor)
        { 

            Entidades.Auditoria.Proveedor oAuditoriaProveedor = new Entidades.Auditoria.Proveedor();
            oAuditoriaProveedor.FechaHora = System.DateTime.Now;
            oAuditoriaProveedor.SetearDatosProveedor(proveedor);
            oAuditoriaProveedor.FechaHora = DateTime.Now;
            oAuditoriaProveedor.Usuario = proveedor.NombreUsuario;
            oAuditoriaProveedor.Accion = proveedor.ModoAuditoria;

            Auditoria.ControladoraAuditoriaProveedor.ObtenerInstancia().RegistrarAuditoriaProveedor(oAuditoriaProveedor);
        }

    }
}
