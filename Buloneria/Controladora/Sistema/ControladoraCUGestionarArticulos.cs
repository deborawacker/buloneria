﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema;

namespace Controladora.Sistema
{
    public class ControladoraCUGestionarArticulos
    {
        private static ControladoraCUGestionarArticulos _Instancia;

        public static ControladoraCUGestionarArticulos ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new ControladoraCUGestionarArticulos();
            return _Instancia;
        }

        private ControladoraCUGestionarArticulos()
        {
        }

        public List<Entidades.Sistema.Articulo> RecuperarArticulos()
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos();
        }

        //creo metodo de controladora recuperar alicuotas
        public List<Entidades.Sistema.Alicuota> RecuperarAlicuotas()
        {
            return Modelo.Sistema.CatalogoAlicuotas.ObtenerInstancia().RecuperarAlicuotas();
        }

        //creo metodo de controladora recuperar grupos de articulos
        public List<Entidades.Sistema.GrupoArticulo> RecuperarGruposArticulos()
        {
            return Modelo.Sistema.CatalogoGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
        }

        //creo metodo de controladora recuperar proveedores
        public List<Entidades.Sistema.Proveedor> RecuperarProveedores()
        {
            return Modelo.Sistema.CatalogoProveedores.ObtenerInstancia().RecuperarProveedores();
        }

        //creo metodo de controladora agregar articulo
        public bool AgregarArticulo(Entidades.Sistema.Articulo oArticulo)
        {
            bool resultado = Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticulo(oArticulo); //en resultado guardo el bool que devuelve el metodo buscararticulo de la capa modelo
            if (resultado == false) //si el resultado es false, quiere decir que se guardo null, es decir que no se encontro codarticulo
            {
                Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().AgregarArticulo(oArticulo); //entonces, agrega el nuevo articulo
                return true; //y devuelve true, que lo agrego
            }
            else
            {
                return false; //en cambio, si guardo algo distinto a null, significa que el codigo ya existe, por lo tanto no guarda codarticulo y devuelve false
            }
        }

        //creo metodo de controladora filtrar articulo
        public List<Entidades.Sistema.Articulo> FiltrarArticulos(string criterio)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().FiltrarArticulos(criterio);
        }

        //creo metodo de controladora modificar articulo
        /*public bool ModificarArticulo(Entidades.Sistema.Articulo oArticulo)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().ModificarArticulo(oArticulo);        
        }*/

        //creo metodo de controladora eliminar articulo, solo se puede eliminar si no esta asociado a una orden de compra
        public void EliminarArticulo(Entidades.Sistema.Articulo oArticulo) //el metodo me devuelve un bool
        {
            Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().EliminarArticulo(oArticulo);  //en el caso de que nunca encontro dos codarticulo que coincidan, devuelve true, que es lo que devuelve el metodo eliminararticulo del modelo
        }

        public Entidades.Sistema.Articulo BuscarArticulo(String codArticuloBuscado)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticuloPorCodigo(codArticuloBuscado);
        }

        public Entidades.Sistema.GrupoArticulo BuscarGrupoArticulo(string grupoArticuloDescription)
        {
            return this.RecuperarGruposArticulos().FirstOrDefault(ga => ga.Descripcion == grupoArticuloDescription);
        }

        public void GuardarArticulo(Articulo articulo)
        {
            if (articulo.IdArticulo != 0)
            {
                Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().Update(articulo);
            }
            else
            {
                Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().AgregarArticulo(articulo);
            }
        }

        public Entidades.Sistema.Alicuota BuscarAlicuota(Decimal valor)
        {
            return this.RecuperarAlicuotas().FirstOrDefault(a => a.Valor == valor);
        }
    }
}
