﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGenerarNotaPedido
    {
        private static ControladoraCUGenerarNotaPedido _Instancia; //creo patron singleton para controladora

        public static ControladoraCUGenerarNotaPedido ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUGenerarNotaPedido();
            return _Instancia;
        }

        private ControladoraCUGenerarNotaPedido()
        {
        }

        //metodo recuperar clientes
        public List<Cliente> RecuperarClientes()
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().RecuperarClientes();
        }

        public Cliente BuscarCliente(int codClienteBuscado)
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().BuscarCliente(codClienteBuscado);
        }

        public List<FormaPago> RecuperarFormasPago()
        {
            return (List<FormaPago>)Modelo.Sistema.Facade.Recuperar("FormaPago");
        }

        public List<Articulo> RecuperarArticulos()
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().RecuperarArticulos();
        }

        public Articulo BuscarArticulo(int idArticuloBuscado)
        {
            return Modelo.Sistema.CatalogoArticulos.ObtenerInstancia().BuscarArticulo(idArticuloBuscado);
        }

        public bool AgregarNotaPedido(Entidades.Sistema.NotaPedido oNP)
        {
            //Aca devuelvo un bool
            return Modelo.Sistema.CatalogoNotasPedidos.ObtenerInstancia().AgregarNotaPedido(oNP) > 0;

        }
    }
}
