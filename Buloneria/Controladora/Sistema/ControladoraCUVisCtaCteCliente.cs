﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUVisCtaCteCliente
    {
        private static ControladoraCUVisCtaCteCliente _Instancia; //creo patron singleton para controladora

        public static ControladoraCUVisCtaCteCliente ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUVisCtaCteCliente();
            return _Instancia;
        }

        private ControladoraCUVisCtaCteCliente()
        {
        }//constructor privado, singleton

        //metodo recuperar clientes
        public List<Entidades.Sistema.Cliente> RecuperarClientes()
        {
            return Modelo.Sistema.CatalogoClientes.ObtenerInstancia().RecuperarClientes();
        }

        //metodo buscar cuenta corriente
        public Entidades.Sistema.CuentaCorriente BuscarCuentaCorriente(Entidades.Sistema.Cliente oCliente, DateTime fechaDesde, DateTime fechaHasta)
        {
            Entidades.Sistema.CuentaCorriente octacte = Modelo.Sistema.CatalogoCuentasCorrientes.ObtenerInstancia().BuscarCuentaCorriente(oCliente);
            Entidades.Sistema.CuentaCorriente oct = new Entidades.Sistema.CuentaCorriente();
            oct.Cliente = octacte.Cliente;
            oct.Saldo = octacte.Saldo;
            oct.NroCtaCte = octacte.NroCtaCte;
            foreach (var item in octacte.MovimientosCtaCte)
            {

                if (fechaDesde <= item.Fecha && fechaHasta >= item.Fecha)
                {
                    oct.AgregarMovimiento(item);
                }
                
            }
            return oct;
        }


    }
}
