﻿using Audit = Entidades.Auditoria;
using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGenerarOrdenCompra
    {
        private static ControladoraCUGenerarOrdenCompra _Instancia; //tambien creo patron singleton para controladora

        public static ControladoraCUGenerarOrdenCompra ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null)
                _Instancia = new ControladoraCUGenerarOrdenCompra();

            return _Instancia;
        }

        private ControladoraCUGenerarOrdenCompra()
        {
        }

        public List<Proveedor> RecuperarProveedores()
        {
            return (List<Proveedor>)Modelo.Sistema.Facade.Recuperar("Proveedor");
        }

        public Proveedor BuscarProveedor(int codProveedorBuscado)
        {
            return (Proveedor)Modelo.Sistema.Facade.Buscar(codProveedorBuscado, "Proveedor");
        }

        public List<FormaPago> RecuperarFormasPago()
        {
            return (List<FormaPago>)Modelo.Sistema.Facade.Recuperar("FormaPago");
        }

        public List<Articulo> RecuperarArticulosProveedor(Proveedor proveedorSeleccionado)
        {
            return (List<Articulo>)Modelo.Sistema.Facade.Recuperar("Articulo", proveedorSeleccionado);
        }

        public Articulo BuscarArticulo(int idArticuloBuscado)
        {
            return (Articulo)Modelo.Sistema.Facade.Buscar(idArticuloBuscado, "Articulo");

        }

        public bool AgregarOrdenCompra(OrdenCompra oOrdenCompra, Entidades.Seguridad.Usuario usuario)
        {

            oOrdenCompra.Usuario = usuario.NombreUsuario;
            oOrdenCompra.FechaHora = DateTime.Now;
            oOrdenCompra.Estado = EstadoOrdenCompra.EnCurso;

            //Agrega OC
            oOrdenCompra.NroOrdenCompra = Modelo.Sistema.Facade.Agregar(oOrdenCompra);

            if (oOrdenCompra.NroOrdenCompra > 0)
            {
                //Agrega OC en curso a Auditoria
                Audit.AuditoriaOrdenCompra oAuditoriaOrdenCompra = new Audit.AuditoriaOrdenCompra
                {
                    NroOrdenCompra = oOrdenCompra.NroOrdenCompra,
                    FechaRegistro = DateTime.Now,
                    Usuario = usuario.NombreUsuario,
                    Estado = oOrdenCompra.Estado
                };

                Modelo.Sistema.Facade.Agregar(oAuditoriaOrdenCompra);
                return true;
            }

            return false;


        }
    }
}
