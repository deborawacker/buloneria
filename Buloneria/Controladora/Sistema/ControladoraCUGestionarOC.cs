﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;
using Entidades.Sistema;

namespace Controladora.Sistema
{
    public class ControladoraCUGestionarOC
    {
        #region Singleton

        private static ControladoraCUGestionarOC _Instancia; //tambien creo patron singleton para controladora

        public static ControladoraCUGestionarOC ObtenerInstancia() //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUGestionarOC();
            return _Instancia;
        }

        private ControladoraCUGestionarOC()
        {
        }

        #endregion

        //metodo recuperar ordenes compras
        public List<OrdenCompra> RecuperarOrdenesCompras()
        {
            List<OrdenCompra> ordenesCompras = Modelo.Sistema.CatalogoOrdenesCompras.ObtenerInstancia().RecuperarOrdenesCompras();

            //HACK: setea a pendiente las OC en curso vencidas
            foreach (var item in ordenesCompras.Where(item => (item.Estado == EstadoOrdenCompra.EnCurso) && (item.FechaPlazo < DateTime.Today)))
            {
                item.Estado = EstadoOrdenCompra.Pendiente;
            }

            return ordenesCompras;
        }

        //finaliza con faltante la orden compra antes se llamaba estado anulado
        public void FinalizarConFaltante(OrdenCompra oOrdenCompra, Entidades.Seguridad.Usuario usuario)
        {
            if (oOrdenCompra.Estado != EstadoOrdenCompra.EnCurso && oOrdenCompra.Estado != EstadoOrdenCompra.Pendiente)
                throw new Exception(String.Format("No se puede finalizar con faltante una Orden en estado: {0}",
                    oOrdenCompra.Estado));

            oOrdenCompra.Estado = EstadoOrdenCompra.FinConFalt;

            #region Auditoria

            AuditoriaOrdenCompra oaud = new AuditoriaOrdenCompra();
            oaud.NroOrdenCompra = oOrdenCompra.NroOrdenCompra;
            oaud.Estado = oOrdenCompra.Estado;
            oaud.FechaRegistro = oOrdenCompra.FechaHora;
            oaud.Usuario = oOrdenCompra.Usuario;
            Modelo.Auditoria.CatalogoAuditoriaOC.ObtenerInstancia().AgregarRegistro(oaud);

            #endregion

            //Cambia estado
            Modelo.Sistema.CatalogoOrdenesCompras.ObtenerInstancia().CambiarEstadoOrdenCompra(oOrdenCompra);

        }

        //metodo filtrar ordenes compras
        public List<OrdenCompra> FiltrarOrdenesCompras(string texto)
        {
            return Modelo.Sistema.CatalogoOrdenesCompras.ObtenerInstancia().FiltrarOrdenesCompras(texto);
        }

        //metodo recuperar historial ordenes compras
        public List<OrdenCompra>
        RecuperarHistorialOrdenesCompras()
        {
            return Modelo.Sistema.CatalogoOrdenesCompras.ObtenerInstancia().RecuperarHistorial();
        }

        public OrdenCompra BuscarOrdenCompra(int nroCodOCSeleccionado)
        {
            return Modelo.Sistema.CatalogoOrdenesCompras.ObtenerInstancia().BuscarOrdenCompra(nroCodOCSeleccionado);
        }
    }
}
