﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladora.Sistema
{
    public class ControladoraCUGConceptos
    {
        private static ControladoraCUGConceptos _Instancia; //creo patron singleton para controladora 

        public static ControladoraCUGConceptos ObtenerInstancia()  //obtenerinstancia metodo publico, singleton
        {
            if (_Instancia == null) _Instancia = new ControladoraCUGConceptos();
            return _Instancia;
        }

        private ControladoraCUGConceptos()
        {
        } //constructor privado, singleton
        
        //metodo recuperar conceptos
        public List<Entidades.Sistema.Concepto> RecuperarConceptos()
        {
            return Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().RecuperarConceptos();
        }

        //metodo agregar concepto
        public bool AgregarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            Entidades.Sistema.Concepto oconcepto = Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().BuscarConcepto(oConcepto);
            if (oconcepto == null)
            {
                Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().AgregarConcepto(oConcepto);
                return true; //devuelve true, porque lo agrego            
            }
            else
            {
                return false; //devuelve false, porque no lo agrego
            }            
        }

        //metodo eliminar concepto
        public bool EliminarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            return Modelo.Sistema.CatalogoConceptos.ObtenerInstancia().EliminarConcepto(oConcepto);
        }


    }
}
