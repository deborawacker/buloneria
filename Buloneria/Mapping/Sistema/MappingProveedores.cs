﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Entidades.Sistema;

namespace Mapping.Sistema
{
    public static class MappingProveedores //defino la clase como estatica, y los metodos tambien tienen que ser estaticos, esto es porque esta clase de mapping solo hace metodos y no tiene atributos, no cambian de estado los atributos porque no los tiene
    {
        //voy a definir un metodo recuperar proveedor -uso try catch -no uso transaccion
        public static List<Proveedor> RecuperarProveedores()
        {
            List<Proveedor> colProveedores = new List<Proveedor>();
            try 
            {
                SqlCommand cmdProveedores = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdProveedores.CommandType = System.Data.CommandType.StoredProcedure;
                cmdProveedores.CommandText = "sp_ConsultarProveedores";
                cmdProveedores.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drProveedores = cmdProveedores.ExecuteReader();
                while (drProveedores.Read())
                {
                    Proveedor oProveedor = new Proveedor();
                    oProveedor.Celular = drProveedores["celular"].ToString(); //convierte del registro drProveedores de la bd al objeto de la clase, y _TipoMovimiento string
                    oProveedor.CodPostal = Convert.ToInt32(drProveedores["codPostal"]); //convierte del registro de la bd al objeto de la clase, y de _TipoMovimiento entero
                    oProveedor.CodProveedor = Convert.ToInt32(drProveedores["codProv"]);
                    oProveedor.Cuit = Convert.ToInt64(drProveedores["cuit"]);
                    oProveedor.Direccion = drProveedores["direccion"].ToString();
                    oProveedor.Email = drProveedores["email"].ToString();
                    oProveedor.Fax = drProveedores["fax"].ToString();
                    oProveedor.NombreFantasia = drProveedores["nomFantasia"].ToString();
                    oProveedor.RazonSocial = drProveedores["razonSocial"].ToString();
                    oProveedor.Telefono = drProveedores["telefono"].ToString();
                    Entidades.Sistema.TipoIva oTipoIva = new Entidades.Sistema.TipoIva();
                    oTipoIva.Descripcion = drProveedores["descripcion"].ToString();
                    oTipoIva.Nombre = drProveedores["nombreTipo"].ToString();
                    Entidades.Sistema.Ciudad oCiudad = new Entidades.Sistema.Ciudad();
                    oCiudad.Nombre = drProveedores["nombrelocalidad"].ToString();
                    Entidades.Sistema.Provincia oProvincia = new Entidades.Sistema.Provincia();
                    oProvincia.Nombre = drProveedores["nombreprovincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    oProveedor.TipoIva = oTipoIva;
                    oProveedor.Ciudad = oCiudad;
                    colProveedores.Add(oProveedor);
                }
            }
            catch (SqlException ex) 
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colProveedores;
        }

        //voy a definir un metodo agregar proveedor -uso try catch -y uso transaccion
        public static int AgregarProveedor(Proveedor proveedor)
        {
            int codigoproveedor = 0;  //aca tengo que declarar la variable fuera del try, y tambien inicializarla para poder usarla dentro del try
            try
            {
                SqlCommand cmd = new SqlCommand(); //con todo esto conecto a la base de datos
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;  //aca le indico que va a ser un _TipoMovimiento stored procedure el que uso en la base de datos
                cmd.CommandText = "sp_AgregarProveedor";  //el nombre del stored procedure de la base de datos
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.Parameters.Add("@cuit", System.Data.SqlDbType.BigInt).Value = proveedor.Cuit;  //al parametro de @cuit de la base le voy a pasar un valor, que es = proveedor.cuit 
                cmd.Parameters.Add("@razonsocial", System.Data.SqlDbType.VarChar, 50).Value = proveedor.RazonSocial;
                cmd.Parameters.Add("@nomfantasia", System.Data.SqlDbType.VarChar, 50).Value = proveedor.NombreFantasia;
                cmd.Parameters.Add("@direccion", System.Data.SqlDbType.VarChar, 100).Value = proveedor.Direccion;
                cmd.Parameters.Add("@nombreciudad", System.Data.SqlDbType.VarChar, 50).Value = proveedor.Ciudad.Nombre;
                cmd.Parameters.Add("@nombreprov", System.Data.SqlDbType.VarChar, 50).Value = proveedor.Ciudad.Provincia.Nombre;
                cmd.Parameters.Add("@codigopostal", System.Data.SqlDbType.Int).Value = proveedor.CodPostal;
                cmd.Parameters.Add("@telefono", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Telefono;
                
                cmd.Parameters.Add("@fax", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Fax;
                cmd.Parameters.Add("@celular", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Celular;
                cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 100).Value = proveedor.Email;
                cmd.Parameters.Add("@tipoiva", System.Data.SqlDbType.VarChar, 20).Value = proveedor.TipoIva.Nombre;
                codigoproveedor = Convert.ToInt32(cmd.ExecuteScalar()); //declara una variable codigoproveedor de _TipoMovimiento entero,y le guarda el autonumerico que se crea cuando se inserta el prov (con executescalar se inserta y se devuelve todo junto,lo hace el mismo metodo)
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return codigoproveedor; //devuelve el codigo de proveedor que se genera automaticamente, recien lo devuelve cuando termino todo
        }

        
        public static bool ModificarProveedor(Proveedor proveedor)
        {
            bool resultado=false;

            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion(); 
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_ModificarProveedor";
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.Parameters.Add("@CodProveedor", System.Data.SqlDbType.Int).Value = proveedor.CodProveedor;
                cmd.Parameters.Add("@Cuit", System.Data.SqlDbType.BigInt).Value = proveedor.Cuit;
                cmd.Parameters.Add("@RazonSocial", System.Data.SqlDbType.VarChar, 50).Value = proveedor.RazonSocial;
                cmd.Parameters.Add("@NombreFantasia", System.Data.SqlDbType.VarChar, 50).Value = proveedor.NombreFantasia;
                cmd.Parameters.Add("@Direccion", System.Data.SqlDbType.VarChar, 100).Value = proveedor.Direccion;
                cmd.Parameters.Add("@NombreCiudad", System.Data.SqlDbType.VarChar, 50).Value = proveedor.Ciudad.Nombre;
                cmd.Parameters.Add("@NombreProvincia", System.Data.SqlDbType.VarChar, 50).Value = proveedor.Ciudad.Provincia.Nombre;
                cmd.Parameters.Add("@CodPostal", System.Data.SqlDbType.Int).Value = proveedor.CodPostal;
                cmd.Parameters.Add("@Telefono", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Telefono;
                cmd.Parameters.Add("@Fax", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Fax;
                cmd.Parameters.Add("@Celular", System.Data.SqlDbType.VarChar, 20).Value = proveedor.Celular;
                cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100).Value = proveedor.Email;
                cmd.Parameters.Add("@NombreTipoIva", System.Data.SqlDbType.VarChar, 20).Value = proveedor.TipoIva.Nombre;
                cmd.ExecuteNonQuery();  //solo ejecuta el stored procedure, no devuelve ningun escalar

                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion
                resultado = true;
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();  //si encuentra un error en el anterior, tira todo atras
                resultado = false;
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar(); 
            }

            return resultado;
        }

        //voy a definir un metodo eliminar proveedor -uso try catch -y uso transaccion
        public static void EliminarProveedor(Proveedor proveedor)
        {
            try 
            { 
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure; 
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion(); 
                cmd.CommandText = "sp_EliminarProveedor";
                cmd.Parameters.Add("@cod_prov", System.Data.SqlDbType.Int).Value = proveedor.CodProveedor;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion
            }
            catch(SqlException exception)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
                throw exception;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        public static Proveedor BuscarProveedor(int codProveedor)
        {
            //HACK: hacer un store propio que lea por codigo
            return (Proveedor) RecuperarProveedores().Where(p => p.CodProveedor == codProveedor).ToList().FirstOrDefault();
        }
    }
}
