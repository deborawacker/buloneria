﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Entidades.Sistema.Reportes;

namespace Mapping.Sistema
{
    public class MappingArticulosPedidosXClientes
    {
        public static List<ArticulosPedidosXClientes> RecuperarArticulosPedidosXClientes(string ArticuloDesc, string RazonSocial, DateTime datetimeinicio, DateTime datetimefin)
        {
            var ListaCliente = new List<ArticulosPedidosXClientes>();
            try
            {
                SqlCommand cmdPedidosXClientes = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdPedidosXClientes.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter parm1 = new SqlParameter("@DateTimeInicio", datetimeinicio.ToString());
                SqlParameter parm2 = new SqlParameter("@DateTimeFin", datetimefin.ToString());
                SqlParameter parm3 = new SqlParameter("@RazonSocial", RazonSocial);
                SqlParameter parm4 = new SqlParameter("@ArticuloDesc", ArticuloDesc);
                cmdPedidosXClientes.Parameters.Add(parm1);
                cmdPedidosXClientes.Parameters.Add(parm2);
                cmdPedidosXClientes.Parameters.Add(parm3);
                cmdPedidosXClientes.Parameters.Add(parm4);
                cmdPedidosXClientes.CommandText = "sp_ReporteArticulosPedidosXCliente";
                cmdPedidosXClientes.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drPedidosXClientes = cmdPedidosXClientes.ExecuteReader();


                while (drPedidosXClientes.Read())
                {
                    ArticulosPedidosXClientes oPedidoXCliente = new ArticulosPedidosXClientes();
                    oPedidoXCliente.RazonSocial = drPedidosXClientes["razonSocial"].ToString();
                    oPedidoXCliente.CodCli = Convert.ToInt32(drPedidosXClientes["codCli"]);
                    oPedidoXCliente.ArticuloDesc = drPedidosXClientes["descripcion"].ToString(); ;
                    oPedidoXCliente.Cantidad = Convert.ToInt32(drPedidosXClientes["cantidad"]);
                    oPedidoXCliente.NroNotaPedido = Convert.ToInt32(drPedidosXClientes["nroNotaPedido"]);
                    oPedidoXCliente.FechaEmision = Convert.ToDateTime(drPedidosXClientes["fechaEmision"]);
                    oPedidoXCliente.NombreApellido = drPedidosXClientes["nombreApellido"].ToString();
                    ListaCliente.Add(oPedidoXCliente);
                }
                Servicios.Conexion.ObtenerInstancia().Desconectar();

            }
            catch (Exception e)
            {
                var error = e.Message;
                Servicios.Conexion.ObtenerInstancia().Desconectar();

            }

            return ListaCliente;
        }
    }
}
