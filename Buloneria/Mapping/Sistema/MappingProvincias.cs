﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;


namespace Mapping.Sistema //dentro del mapping creo el recuperar provincia de la bd para crear el objeto provincia
{
    public static class MappingProvincias 
    {
        //voy a definir un metodo recuperar provincia -uso try catch -no uso transaccion
        public static List<Entidades.Sistema.Provincia> RecuperarProvincias() //voy a crear un método recupera las provincias de la bd, que devuelve una lista de Entidad, Sistema, Provincia
        {
            List<Entidades.Sistema.Provincia> colProvincias = new List<Entidades.Sistema.Provincia>(); //declaro e instancio la coleccion provincias, como una list
            try
            {
                SqlCommand cmdProvincias = new SqlCommand(); //declaro una clase y la instancio, que me permite realizar operaciones sobre el motor, se llama comando provincias: cmdProvincias
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdProvincias.CommandType = System.Data.CommandType.StoredProcedure; //le indico que lo que voy a ejecutar en la base es un store procedure
                cmdProvincias.CommandText = "sp_ConsultarProvincias"; //le paso cuál es el nombre del store procedure
                cmdProvincias.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion(); //la conexion a la bd de la capa Servicios
                SqlDataReader drProvincias = cmdProvincias.ExecuteReader(); //ejecuta el comando sobre el motor y lo asigna un objeto sql data reader, devuelve una coleccion de registros de la base (todavia no hablamos de objetos)
                while (drProvincias.Read())  //la condicion while para que recorra la lista de registros y me permite visualizar cada uno de ellos de a uno por vez
                {
                    Entidades.Sistema.Provincia oProvincia = new Entidades.Sistema.Provincia(); //creo un objeto de _TipoMovimiento provincia, el oProvincia
                    oProvincia.Nombre = drProvincias["NombreProvincia"].ToString(); //le indico el nombre de la columna del registro actual, que es NombreProvincia en la bd (o tambien puede ser el indice "0", porque es la primera columna), y lo convierte a string y se lo asigna a oProvincia.Nombre, que es el "Nombre" que le di en la clase 
                    SqlCommand cmdCiudades = new SqlCommand(); //declaro una clase y la instancio, que me permite realizar operaciones sobre el motor, se llama comando ciudades: cmdCiudades
                    cmdCiudades.CommandType = System.Data.CommandType.StoredProcedure; //le indico que lo que voy a ejecutar en la base es un store procedure
                    cmdCiudades.CommandText = "sp_ConsultarLocalidadesProvincia"; //le paso cual es el nombre del store procedure
                    cmdCiudades.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion(); //la conexion a la bd de la capa Servicios
                    cmdCiudades.Parameters.Add("@provincia", System.Data.SqlDbType.VarChar, 50).Value = oProvincia.Nombre; //creo un parametro "@provincia"porque para esa provincia necesito que me traiga todas la localidades, y es de _TipoMovimiento varchar(50) como fue creado en la bd el store procedure, y le tengo asignar lo que tengo en el oProvincia.Nombre que es la provincia que esta recorriendo en ese momento
                    SqlDataReader drCiudades = cmdCiudades.ExecuteReader(); //ejecuta el comando sobre el motor y lo asigna un objeto sql data reader, devuelve una coleccion de registros de la base (todavia no hablamos de objetos)
                    while (drCiudades.Read())  //la condicion while para que recorra la lista de registros y me permite visualizar cada uno de ellos de a uno por vez
                    {
                        Entidades.Sistema.Ciudad oCiudad = new Entidades.Sistema.Ciudad(); //creo un objeto de _TipoMovimiento ciudad, el oCiudad
                        oCiudad.CodLocalidad = Convert.ToInt32(drCiudades["Cod_Localidad"]);
                        oCiudad.Nombre = drCiudades["NombreLocalidad"].ToString(); //le indico el nombre de la columna del registro actual, que es NombreLocalidad en la bd (o tambien puede ser el indice "0", porque es la primera columna), y lo convierte a string y se lo asigna a oCiudad.Nombre, que es el "Nombre" que le di en la clase 
                        oProvincia.AgregarCiudad(oCiudad); //le indico que agregue el objeto oCiudad en el objeto oProvincia, donde el agregar ciudad es un metodo de la clase Provincia
                    }
                    colProvincias.Add(oProvincia); //todos los objetos los va guardando en el objeto coleccion provincias
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colProvincias; //devuelve la coleccion provincias
        }
    }
}
