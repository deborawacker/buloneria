﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Entidades.Sistema;

namespace Mapping.Sistema
{
    public static class MappingOrdenesCompras
    {

        //recuperar oc  -y uso try catch  -no uso transaccion
        public static List<OrdenCompra> RecuperarOrdenesCompras()
        {
            List<OrdenCompra> colOrdenCompra = new List<OrdenCompra>();
            try
            {
                SqlCommand cmdOrdenCompra = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdOrdenCompra.CommandType = System.Data.CommandType.StoredProcedure;
                cmdOrdenCompra.CommandText = "sp_RecuperarOrdenesCompras";
                cmdOrdenCompra.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drOrdenesCompras = cmdOrdenCompra.ExecuteReader();
                while (drOrdenesCompras.Read())
                {
                    OrdenCompra oOrdenCompra = new OrdenCompra();
                    oOrdenCompra.NroOrdenCompra = Convert.ToInt32(drOrdenesCompras["nroOrdenCompra"]);
                    oOrdenCompra.FechaEmision = Convert.ToDateTime(drOrdenesCompras["fechaEmision"]);
                    oOrdenCompra.FechaPlazo = Convert.ToDateTime(drOrdenesCompras["fechaPlazo"]);
                    oOrdenCompra.FechaHora = Convert.ToDateTime(drOrdenesCompras["fechaHora"]);
                    oOrdenCompra.Usuario = Convert.ToString(drOrdenesCompras["usuario"]);

                    #region OrdenCompra.Estado
                    string estado = drOrdenesCompras["estado"].ToString();
                    EstadoOrdenCompra estadoOC = EstadoOrdenCompra.EnCurso;
                    switch (estado)
                    {
                        case "FinConFalt":
                            estadoOC = Entidades.Sistema.EstadoOrdenCompra.FinConFalt;
                            break;
                        case "Pendiente":
                            estadoOC = Entidades.Sistema.EstadoOrdenCompra.Pendiente;
                            break;
                        case "Finalizado":
                            estadoOC = Entidades.Sistema.EstadoOrdenCompra.Finalizado;
                            break;
                        case "EnCurso":
                            estadoOC = Entidades.Sistema.EstadoOrdenCompra.EnCurso;
                            break;
                        default:
                            break;
                    }
                    oOrdenCompra.Estado = estadoOC;
                    #endregion

                    #region OrdenCompra.FormaPago
                    Entidades.Sistema.FormaPago oFormaPago = new Entidades.Sistema.FormaPago();
                    oFormaPago.TipoFormaPago = drOrdenesCompras["tipoFormaPago"].ToString();
                    oOrdenCompra.FormaPago = oFormaPago;
                    #endregion

                    #region OrdenCompra.Proveedor
                    Entidades.Sistema.Proveedor oProveedor = new Entidades.Sistema.Proveedor();
                    oProveedor.CodProveedor = Convert.ToInt32(drOrdenesCompras["codProv"]);
                    oProveedor.Celular = drOrdenesCompras["celular"].ToString();
                    oProveedor.CodPostal = Convert.ToInt32(drOrdenesCompras["codPostal"]);
                    oProveedor.Cuit = Convert.ToInt64(drOrdenesCompras["cuit"]);
                    oProveedor.Direccion = drOrdenesCompras["direccion"].ToString();
                    oProveedor.Email = drOrdenesCompras["email"].ToString();
                    oProveedor.Fax = drOrdenesCompras["fax"].ToString();
                    oProveedor.NombreFantasia = drOrdenesCompras["nomFantasia"].ToString();
                    oProveedor.RazonSocial = drOrdenesCompras["razonSocial"].ToString();
                    oProveedor.Telefono = drOrdenesCompras["telefono"].ToString();

                    #region Proveedor.TipoIva
                    Entidades.Sistema.TipoIva oTipoIva = new Entidades.Sistema.TipoIva();
                    oTipoIva.Descripcion = drOrdenesCompras["descripcion"].ToString();
                    oTipoIva.Nombre = drOrdenesCompras["tipoiva"].ToString();
                    oProveedor.TipoIva = oTipoIva;
                    #endregion

                    #region Proveedor.Ciudad
                    Entidades.Sistema.Ciudad oCiudad = new Entidades.Sistema.Ciudad();
                    oCiudad.Nombre = drOrdenesCompras["nombrelocalidad"].ToString();
                    oProveedor.Ciudad = oCiudad;
                    #endregion

                    #region Proveedor.Provincia
                    Entidades.Sistema.Provincia oProvincia = new Entidades.Sistema.Provincia();
                    oProvincia.Nombre = drOrdenesCompras["nombreprovincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    #endregion

                    oOrdenCompra.Proveedor = oProveedor;
                    #endregion

                    //aca traigo todas las lineas de articulos de una orden de compra
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_RecuperarLineasOC";
                    cmdLinea.Parameters.Add("@pNroOC", System.Data.SqlDbType.Int).Value = oOrdenCompra.NroOrdenCompra;
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drLineasArticulos = cmdLinea.ExecuteReader();
                    while (drLineasArticulos.Read())
                    {
                        LineaOrdenCompra oLinea = new LineaOrdenCompra();

                        #region LineaOrdenCompra.Articulo
                        Entidades.Sistema.Articulo oArticulo = new Entidades.Sistema.Articulo();

                        #region Articulo.GrupoArticulo
                        Entidades.Sistema.GrupoArticulo Grupo = new Entidades.Sistema.GrupoArticulo();
                        Grupo.CodGrupoArticulo = Convert.ToInt32(drLineasArticulos["CodGrupoArticulo"]);  //en el store "as..."
                        Grupo.Descripcion = drLineasArticulos["DescripcionGrupo"].ToString(); //en el store "as..."
                        oArticulo.Grupo = Grupo;
                        #endregion

                        oArticulo.IdArticulo = Convert.ToInt32(drLineasArticulos["IdArticulo"]);
                        oArticulo.CodArticulo = drLineasArticulos["CodArticulo"].ToString();
                        oArticulo.Descripcion = drLineasArticulos["DescripcionArticulo"].ToString();
                        oArticulo.CantMinima = Convert.ToInt32(drLineasArticulos["CantMinima"]);
                        oArticulo.CantActual = Convert.ToInt32(drLineasArticulos["cantActual"]);
                        oArticulo.PrecioVenta = Convert.ToInt32(drLineasArticulos["PrecioVenta"]);

                        #region LineaOrdenCompra.Articulo.Alicuota
                        Alicuota oAlicuota = new Alicuota();
                        oAlicuota.Valor = Convert.ToDecimal(drLineasArticulos["ValorAlicuota"]);
                        oArticulo.Alicuota = oAlicuota;
                        #endregion

                        #region Articulo.Proveedores

                        SqlCommand cmdProveedoresArticulo = new SqlCommand();
                        cmdProveedoresArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                        cmdProveedoresArticulo.CommandText = "sp_ConsultarProveedoresArticulo";
                        
                        cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                        cmdProveedoresArticulo.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = oArticulo.CodArticulo;
                        cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                        SqlDataReader drProveedoresArticulo = cmdProveedoresArticulo.ExecuteReader();
                        while (drProveedoresArticulo.Read()) //tengo que recorrer el proveedor
                        {
                            Proveedor oProveedorArticulo = new Proveedor();
                            oProveedorArticulo.Celular = drProveedoresArticulo["Celular"].ToString();
                            oProveedorArticulo.CodPostal = Convert.ToInt32(drProveedoresArticulo["CodPostal"]);
                            oProveedorArticulo.CodProveedor = Convert.ToInt32(drProveedoresArticulo["CodProveedor"]);
                            oProveedorArticulo.Cuit = Convert.ToInt64(drProveedoresArticulo["Cuit"]);
                            oProveedorArticulo.Direccion = drProveedoresArticulo["Direccion"].ToString();
                            oProveedorArticulo.Email = drProveedoresArticulo["Email"].ToString();
                            oProveedorArticulo.Fax = drProveedoresArticulo["Fax"].ToString();
                            oProveedorArticulo.NombreFantasia = drProveedoresArticulo["NombreFantasia"].ToString();
                            oProveedorArticulo.RazonSocial = drProveedoresArticulo["RazonSocial"].ToString();
                            oProveedorArticulo.Telefono = drProveedoresArticulo["Telefono"].ToString();

                            #region TipoIva

                            TipoIva oIva = new TipoIva();
                            oIva.Descripcion = drProveedoresArticulo["DescripcionTipoIva"].ToString();
                            oIva.Nombre = drProveedoresArticulo["NombreTipoIva"].ToString();
                            oProveedorArticulo.TipoIva = oIva;

                            #endregion

                            #region Ciudad

                            Ciudad oCiudadArticulo = new Ciudad();
                            oCiudadArticulo.Nombre = drProveedoresArticulo["NombreCiudad"].ToString();
                            Provincia oProvinciaArticulo = new Provincia();
                            oProvinciaArticulo.Nombre = drProveedoresArticulo["NombreProvincia"].ToString();
                            oProvinciaArticulo.AgregarCiudad(oCiudad);
                            oProveedorArticulo.Ciudad = oCiudadArticulo;

                            #endregion

                            oArticulo.AgregarProveedor(oProveedorArticulo);
                        }
                        #endregion

                        oLinea.Articulo = oArticulo;
                        #endregion

                        oLinea.NroLineaOrdenCompra = Convert.ToInt32(drLineasArticulos["NroLineaOrdenCompra"]);
                        oLinea.CantPedir = Convert.ToInt32(drLineasArticulos["CantPedir"]);
                        oLinea.CantRecibida = Convert.ToInt32(drLineasArticulos["CantRecibida"]);
                        oLinea.PrecioUnitario = Convert.ToInt32(drLineasArticulos["PrecioUnitario"]);
                        oOrdenCompra.AgregarLineaOrdenCompra(oLinea);
                    }
                    colOrdenCompra.Add(oOrdenCompra);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colOrdenCompra;  //recordar que le tengo que devolver la coleccion
        }

        //agregar oc -uso try catch -y uso transaccion
        public static int AgregarOrdenCompra(OrdenCompra orden)
        {
            int nroOrdenCompra = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_GenerarOrdenCompra";
                cmd.Parameters.Add("@fechaEmision", System.Data.SqlDbType.DateTime).Value = orden.FechaEmision;
                cmd.Parameters.Add("@fechaPlazo", System.Data.SqlDbType.DateTime).Value = orden.FechaPlazo;
                cmd.Parameters.Add("@formaPago", System.Data.SqlDbType.VarChar, 20).Value = orden.FormaPago.TipoFormaPago;
                cmd.Parameters.Add("@codProv", System.Data.SqlDbType.Int).Value = orden.Proveedor.CodProveedor;
                cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar, 10).Value = orden.Estado.ToString(); //el estado lo paso a string porque es numeracion.SI SE COMO EL ESTADO!!!
                cmd.Parameters.Add("@usuario", System.Data.SqlDbType.VarChar, 15).Value = orden.Usuario;
                cmd.Parameters.Add("@fechaHora", System.Data.SqlDbType.DateTime).Value = orden.FechaHora;

                //aca se genera el numero de la orden de compra, el mismo store procedure devuelve el scopeidentity (ultimo numero de compra autogenerado por la BD)
                nroOrdenCompra = Convert.ToInt32(cmd.ExecuteScalar());

                //el foreach es para agregar cada linea a la orden de compra
                foreach (LineaOrdenCompra oLinea in orden.LineasOrdenCompra)
                {
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_AgregarLinea";
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdLinea.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                    cmdLinea.Parameters.Add("@nroOrdenCompra", System.Data.SqlDbType.Int).Value = nroOrdenCompra;
                    cmdLinea.Parameters.Add("@cantRecibida", System.Data.SqlDbType.Int).Value = 0;//oLinea.CantRecibida;
                    cmdLinea.Parameters.Add("@codArticulo", System.Data.SqlDbType.Int).Value = oLinea.Articulo.IdArticulo;
                    cmdLinea.Parameters.Add("@cantPedir", System.Data.SqlDbType.Int).Value = oLinea.CantPedir;
                    cmdLinea.Parameters.Add("@precioUnitario", System.Data.SqlDbType.Int).Value = oLinea.PrecioUnitario;
                    //Devuelve el ultimo autonumerico de la linea insertada
                    int idLinea = Convert.ToInt32(cmdLinea.ExecuteScalar());
                    oLinea.NroLineaOrdenCompra = idLinea;
                }
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit(); //confirmo la transaccion                
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return nroOrdenCompra;
        }

        //aca el metodo anular que solo cambia el estado, uso try catch y transaccion
        public static void CambiarEstadoOrdenCompra(OrdenCompra orden)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_CambiarEstadoOrdenCompra";
                cmd.Parameters.Add("@pNroOrdenCompra", System.Data.SqlDbType.Int).Value = orden.NroOrdenCompra;
                cmd.Parameters.Add("@pEstado", System.Data.SqlDbType.VarChar, 10).Value = orden.Estado.ToString();  //al estado lo paso a string xq es una numeracion
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        //ver historial oc es un recuperar todas las ordenes de compras -uso try y catch pero NO transaccion
        public static List<OrdenCompra> RecuperarHistorial()
        {
            List<OrdenCompra> colOrdenCompra = new List<OrdenCompra>();
            try
            {
                SqlCommand cmdOrdenCompra = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdOrdenCompra.CommandType = System.Data.CommandType.StoredProcedure;
                cmdOrdenCompra.CommandText = "sp_RecuperarHistorialOrdenesCompras";
                cmdOrdenCompra.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drOrdenesCompras = cmdOrdenCompra.ExecuteReader();
                while (drOrdenesCompras.Read())
                {
                    OrdenCompra oOrdenCompra = new OrdenCompra();
                    oOrdenCompra.NroOrdenCompra = Convert.ToInt32(drOrdenesCompras["nroOrdenCompra"]);
                    oOrdenCompra.FechaEmision = Convert.ToDateTime(drOrdenesCompras["fechaEmision"]);
                    oOrdenCompra.FechaPlazo = Convert.ToDateTime(drOrdenesCompras["fechaPlazo"]);
                    string estado = drOrdenesCompras["estado"].ToString();
                    if (estado == Entidades.Sistema.EstadoOrdenCompra.FinConFalt.ToString()) oOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.FinConFalt;
                    if (estado == Entidades.Sistema.EstadoOrdenCompra.Finalizado.ToString()) oOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.Finalizado;
                    if (estado == Entidades.Sistema.EstadoOrdenCompra.EnCurso.ToString()) oOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.EnCurso;
                    //FORMA DE PAGO
                    Entidades.Sistema.FormaPago oFormaPago = new Entidades.Sistema.FormaPago();
                    oFormaPago.TipoFormaPago = drOrdenesCompras["tipoFormaPago"].ToString();
                    oOrdenCompra.FormaPago = oFormaPago;
                    //ACA TRAJE TODO EL PROVEEDOR, ES UN PROVEEDOR POR ORDEN DE COMPRA:
                    Entidades.Sistema.Proveedor oProveedor = new Entidades.Sistema.Proveedor();
                    oProveedor.CodProveedor = Convert.ToInt32(drOrdenesCompras["codProv"]);
                    oProveedor.Celular = drOrdenesCompras["celular"].ToString();
                    oProveedor.CodPostal = Convert.ToInt32(drOrdenesCompras["codPostal"]);
                    //oProveedor.CodProveedor = Convert.ToInt32(drOrdenesCompras["codProv"]);
                    oProveedor.Cuit = Convert.ToInt64(drOrdenesCompras["cuit"]);
                    oProveedor.Direccion = drOrdenesCompras["direccion"].ToString();
                    oProveedor.Email = drOrdenesCompras["email"].ToString();
                    oProveedor.Fax = drOrdenesCompras["fax"].ToString();
                    oProveedor.NombreFantasia = drOrdenesCompras["nomFantasia"].ToString();
                    oProveedor.RazonSocial = drOrdenesCompras["razonSocial"].ToString();
                    oProveedor.Telefono = drOrdenesCompras["telefono"].ToString();
                    Entidades.Sistema.TipoIva oTipoIva = new Entidades.Sistema.TipoIva();
                    oTipoIva.Descripcion = drOrdenesCompras["descripcion"].ToString();
                    oTipoIva.Nombre = drOrdenesCompras["tipoiva"].ToString();
                    Entidades.Sistema.Ciudad oCiudad = new Entidades.Sistema.Ciudad();
                    oCiudad.Nombre = drOrdenesCompras["nombrelocalidad"].ToString();
                    Entidades.Sistema.Provincia oProvincia = new Entidades.Sistema.Provincia();
                    oProvincia.Nombre = drOrdenesCompras["nombreprovincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    oProveedor.TipoIva = oTipoIva;
                    oProveedor.Ciudad = oCiudad;
                    oOrdenCompra.Proveedor = oProveedor;
                    //aca traigo todas las lineas de articulos de una orden de compra
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_RecuperarLineasOC";
                    cmdLinea.Parameters.Add("@nroOC", System.Data.SqlDbType.Int).Value = oOrdenCompra.NroOrdenCompra; //le paso como parametro el _nroOrdenCompra al stored procedure
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drLineasArticulos = cmdLinea.ExecuteReader();
                    while (drLineasArticulos.Read())
                    {
                        LineaOrdenCompra oLinea = new LineaOrdenCompra();
                        oLinea.CantPedir = Convert.ToInt32(drLineasArticulos["cantPedir"]);
                        oLinea.CantRecibida = Convert.ToInt32(drLineasArticulos["cantRecibida"]);
                        //oLinea.PrecioUnitario = Convert.ToInt32(drLineasArticulos["precioUnitario"]);
                        Entidades.Sistema.Articulo oArticulo = new Entidades.Sistema.Articulo();
                        Entidades.Sistema.GrupoArticulo Grupo = new Entidades.Sistema.GrupoArticulo();
                        Grupo.CodGrupoArticulo = Convert.ToInt32(drLineasArticulos["CodigoGrupoArticulo"]); //en el store "as..."
                        Grupo.Descripcion = drLineasArticulos["DescripcionGrupo"].ToString(); //en el store "as..."
                        oArticulo.Grupo = Grupo;
                        oArticulo.CodArticulo = drLineasArticulos["codigoArticulo"].ToString();
                        oArticulo.Descripcion = drLineasArticulos["descripcion"].ToString();
                        oArticulo.CantMinima = Convert.ToInt32(drLineasArticulos["cantMinima"]);
                        oArticulo.CantActual = Convert.ToInt32(drLineasArticulos["cantActual"]);
                        Entidades.Sistema.Alicuota oAlicuota = new Entidades.Sistema.Alicuota();
                        oAlicuota.Valor = Convert.ToDecimal(drLineasArticulos["alicuota"]);
                        oArticulo.Alicuota = oAlicuota;
                        oLinea.Articulo = oArticulo;  //le asigno a la propiedad Articulo el objeto oArticulo
                        oOrdenCompra.AgregarLineaOrdenCompra(oLinea);  //al objeto oOrdenCompra le agrego el objeto oLinea, con el metodo AgregarLineaOrdenCompra
                    }
                    colOrdenCompra.Add(oOrdenCompra);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colOrdenCompra;  //recordar que le tengo que devolver la coleccion
        }

        public static void ActualizarCantRecibidaLineasOrdenCompra(OrdenCompra orden)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();

                foreach (LineaOrdenCompra oLinea in orden.LineasOrdenCompra)
                {
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_ActualizarCantidadRecibidaLineasOrdenCompra";
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdLinea.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                    cmdLinea.Parameters.Add("@pNroLineaOrdenCompra", System.Data.SqlDbType.Int).Value = oLinea.NroLineaOrdenCompra;
                    cmdLinea.Parameters.Add("@pCantidadRecibida", System.Data.SqlDbType.Int).Value = oLinea.CantIngresada;
                    cmdLinea.ExecuteNonQuery();
                }
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit(); //confirmo la transaccion                
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }


        }
    }
}

#region sp_RecuperarLineasOC
//ALTER procedure [dbo].[sp_RecuperarLineasOC] @pNroOC int
//AS
//    SELECT nroLineaOrdenCompra			AS NroLineaOrdenCompra, 
//    cantPedir							AS CantPedir, 
//    cantRecibida						AS CantRecibida,
//    art.precioVenta						AS PrecioVenta,
//    art.descripcion						AS DescripcionArticulo,
//    art.cantMinima						AS CantMinima,
//    art.cantActual						AS CantActual,
//    art.codArticulo						AS IdArticulo,
//    art.codigoArticulo					AS CodArticulo,
//    alic.alicuota						AS ValorAlicuota,  
//    loc.precioUnitario					AS PrecioUnitario,
//    gart.codGrupoArticulo				AS CodGrupoArticulo, 
//    gart.descripcion					AS DescripcionGrupo
//    FROM lineasordenescompras loc 
//    INNER JOIN Articulos  art
//        ON loc.codarticulo = art.codarticulo 
//    INNER JOIN Alicuotas alic
//        ON art.id_alicuota = alic.id_alicuota 
//    INNER JOIN GruposArticulos gart
//        ON art.CodGrupoArticulo = gart.CodGrupoArticulo
//    WHERE nroOrdenCompra = @pNroOC
#endregion

#region sp_CambiarEstadoOrdenCompra
//ALTER PROCEDURE [dbo].[sp_CambiarEstadoOrdenCompra] 
//@pNroOrdenCompra int,
//@pEstado varchar(10)
//AS
//BEGIN

//    UPDATE ordenescompras 
//    SET estado=@pEstado 
//    WHERE nroOrdenCompra= @pNroOrdenCompra

//END
#endregion

#region sp_ActualizarCantidadRecibidaLineasOrdenCompra
//CREATE PROCEDURE sp_ActualizarCantidadRecibidaLineasOrdenCompra
//    -- Add the parameters for the stored procedure here
//    @pNroLineaOrdenCompra int, 
//    @pCantidadRecibida int
//AS
//BEGIN
//    -- Insert statements for procedure here
//    UPDATE lineasordenescompras
//    SET cantRecibida+=@pCantidadRecibida
//    WHERE nroLineaOrdenCompra=@pNroLineaOrdenCompra;
//END
//GO

#endregion