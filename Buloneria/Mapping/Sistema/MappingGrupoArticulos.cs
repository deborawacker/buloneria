﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Mapping.Sistema
{
    public static class MappingGrupoArticulos //defino la clase como estatica, y los metodos tambien tienen que ser estaticos, esto es porque esta clase de mapping solo hace metodos y no tiene atributos, no cambian de estado los atributos porque no los tiene
    {
        //metodo recuperar grupo articulo -uso try catch -no uso transaccion
        public static List<Entidades.Sistema.GrupoArticulo> RecuperarGruposArticulos()
        {
            List<Entidades.Sistema.GrupoArticulo> colGrupoArticulo = new List<Entidades.Sistema.GrupoArticulo>();  //aca defino la coleccion fuera del try
            try   //aca defino un try, pero no voy a tener una transaccion porque el recuperar es de solo lectura, es decir que no cambio nada 
            {
                SqlCommand cmdGrupoArticulo = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdGrupoArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                cmdGrupoArticulo.CommandText = "sp_ConsultarGruposArticulos";
                cmdGrupoArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drGrupoArticulo = cmdGrupoArticulo.ExecuteReader();
                while (drGrupoArticulo.Read())
                {
                    Entidades.Sistema.GrupoArticulo oGrupoArticulo = new Entidades.Sistema.GrupoArticulo();
                    oGrupoArticulo.CodGrupoArticulo = Convert.ToInt32(drGrupoArticulo["codGrupoArticulo"]); //convierte del registro de la bd al objeto de la clase, y de Tipo entero
                    oGrupoArticulo.Descripcion = drGrupoArticulo["descripcion"].ToString(); //convierte del registro de la bd al objeto de la clase, y Tipo string
                    
                    colGrupoArticulo.Add(oGrupoArticulo);
                }
                
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);   //aca indico que en la excepcion voy a registrar el error que ocurrio
            }
            finally
            { 
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colGrupoArticulo;  //recien cuando paso por todo lo anteriro, devuelvo la coleccion, aunque este vacia
        }
        
        //metodo agregar grupo articulo -uso try catch -y uso transaccion
        public static void agregarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoArticulo)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion(); //le indico que comienza la transaccion, a partir de este momento tiene que poner a prueba lo que hace con la base de datos
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.CommandText = "sp_AgregarGrupoArticulo";
                cmd.Parameters.Add("@grupoarticulo", System.Data.SqlDbType.VarChar, 10).Value = grupoArticulo.CodGrupoArticulo;
                cmd.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar, 50).Value = grupoArticulo.Descripcion;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion                
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();  //si encuentra un error en el anterior, tira todo atras
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();  // el finally se ejecuta siempre, y aca es donde desconecto
            }
        }

        //metodo modificar grupo articulo -uso try catch -y uso transaccion
        public static void modificarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoArticulo)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion(); //le indico que comienza la transaccion, a partir de este momento tiene que poner a prueba lo que hace con la base de datos
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_ModificarGrupoArticulo";
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.Parameters.Add("@grupoArticulo", System.Data.SqlDbType.VarChar, 10).Value = grupoArticulo.CodGrupoArticulo;
                cmd.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar, 50).Value = grupoArticulo.Descripcion;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();  //si encuentra un error en el anterior, tira todo atras
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }

        }

        //metodo eliminar grupo articulo -uso try catch -y uso transaccion
        public static void eliminarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoarticulo) //aca creo el metodo y le paso el parametro grupoarticulo de _TipoMovimiento grupoarticulo
        {
            try
            {
                SqlCommand cmd = new SqlCommand(); //aca creo un nuevo objeto cmd
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema"); //aca le doy la orden que conecte con "sistema" que lo tengo declarado en el app.config
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure; //le indico que voy a usar un stored procedure
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion(); //son los metodos para retornar la conexion, que estan dentro de la capa de servicios
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandText = "sp_EliminarGrupoArticulo"; //le digo cual es el stored procedure que voy a usar de la base de datos
                cmd.Parameters.Add("@grupoArticulo", System.Data.SqlDbType.VarChar, 10).Value = grupoarticulo.CodGrupoArticulo;
                cmd.ExecuteNonQuery(); //este es para indicar que ejecute y que no es una consulta
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion//le digo cual es el codgrupoarticulo de la base de datos y el grupoarticulo que le paso como parametro 
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar(); //desconectar de la bd
            }
        }
    }
}
