﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Sistema
{
    public static class MappingFormasPago
    {
        //metodo recuperar formas de pago
        public static List<Entidades.Sistema.FormaPago> RecuperarFormasPago() 
        {
            List<Entidades.Sistema.FormaPago> colFormasPago = new List<Entidades.Sistema.FormaPago>();
            try 
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection =  Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_RecuperarFormasPago";
                SqlDataReader drFormasPago = cmd.ExecuteReader();
                while (drFormasPago.Read())
                {
                    Entidades.Sistema.FormaPago oFormaPago = new Entidades.Sistema.FormaPago();
                    oFormaPago.TipoFormaPago = drFormasPago["tipoFormaPago"].ToString();
                    colFormasPago.Add(oFormaPago);
                }
            }
            catch(SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();            
            }
            return colFormasPago;        
        }        
    }
}
