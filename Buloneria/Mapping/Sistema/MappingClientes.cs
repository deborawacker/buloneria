﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Security.Permissions;
using System;
using Entidades.Sistema;

namespace Mapping.Sistema
{
    public static class MappingClientes
    {

        //metodo recuperar cliente -uso try catch -no uso transaccion
        public static List<Cliente> RecuperarClientes()
        {

            List<Cliente> colClientes = new List<Cliente>();
            try
            {
                SqlCommand cmdClientes = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdClientes.CommandType = System.Data.CommandType.StoredProcedure;
                cmdClientes.CommandText = "sp_ConsultarClientes";
                cmdClientes.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drClientes = cmdClientes.ExecuteReader();
                while (drClientes.Read())
                {
                    Cliente oCliente = new Cliente();
                    oCliente.Celular = drClientes["celular"].ToString(); //convierte del registro drClientes de la bd al objeto de la clase, y Tipo string
                    oCliente.CodPostal = Convert.ToInt32(drClientes["codPostal"]); //convierte del registro de la bd al objeto de la clase, y de Tipo entero
                    oCliente.CodCliente = Convert.ToInt32(drClientes["codCli"]);
                    oCliente.Cuit = Convert.ToInt64(drClientes["cuit"]);
                    oCliente.Direccion = drClientes["direccion"].ToString();
                    oCliente.Email = drClientes["email"].ToString();
                    oCliente.Fax = drClientes["fax"].ToString();
                    oCliente.NombreFantasia = drClientes["nomFantasia"].ToString();
                    oCliente.RazonSocial = drClientes["razonSocial"].ToString();
                    oCliente.Telefono = drClientes["telefono"].ToString();
                    Entidades.Sistema.TipoIva oTipoIva = new Entidades.Sistema.TipoIva();
                    oTipoIva.Descripcion = drClientes["descripcion"].ToString();
                    oTipoIva.Nombre = drClientes["nombreTipo"].ToString();
                    Entidades.Sistema.Ciudad oCiudad = new Entidades.Sistema.Ciudad();
                    oCiudad.Nombre = drClientes["nombrelocalidad"].ToString();
                    Entidades.Sistema.Provincia oProvincia = new Entidades.Sistema.Provincia();
                    oProvincia.Nombre = drClientes["nombreprovincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    oCliente.TipoIva = oTipoIva;
                    oCliente.Ciudad = oCiudad;
                    colClientes.Add(oCliente);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colClientes;
        }

        //metodo agregar cliente -uso try catch -y uso transaccion
        public static int AgregarCliente(Cliente cliente)  //aca el metodo devuelve un int porque devuelve el autonumerico que se genera del codigo de cliente
        {
            int codigocliente = 0;  //aca tengo que declarar la variable fuera del try, y tambien inicializarla para poder usarla dentro del try
            try
            {
                SqlCommand cmd = new SqlCommand(); //con todo esto conecto a la base de datos
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;  //aca le indico que va a ser un _TipoMovimiento stored procedure el que uso en la base de datos
                cmd.CommandText = "sp_AgregarCliente";  //el nombre del stored procedure de la base de datos
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.Parameters.Add("@cuit", System.Data.SqlDbType.BigInt).Value = cliente.Cuit;
                cmd.Parameters.Add("@razonsocial", System.Data.SqlDbType.VarChar, 50).Value = cliente.RazonSocial;
                cmd.Parameters.Add("@nomfantasia", System.Data.SqlDbType.VarChar, 50).Value = cliente.NombreFantasia;
                cmd.Parameters.Add("@direccion", System.Data.SqlDbType.VarChar, 100).Value = cliente.Direccion;
                cmd.Parameters.Add("@nombreciudad", System.Data.SqlDbType.VarChar, 50).Value = cliente.Ciudad.Nombre;
                cmd.Parameters.Add("@nombreprov", System.Data.SqlDbType.VarChar, 50).Value = cliente.Ciudad.Provincia.Nombre;
                cmd.Parameters.Add("@codigopostal", System.Data.SqlDbType.Int).Value = cliente.CodPostal;
                cmd.Parameters.Add("@telefono", System.Data.SqlDbType.VarChar, 20).Value = cliente.Telefono;
                cmd.Parameters.Add("@fax", System.Data.SqlDbType.VarChar, 20).Value = cliente.Fax;
                cmd.Parameters.Add("@celular", System.Data.SqlDbType.VarChar, 20).Value = cliente.Celular;
                cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 100).Value = cliente.Email;
                cmd.Parameters.Add("@tipoiva", System.Data.SqlDbType.VarChar, 20).Value = cliente.TipoIva.Nombre;
                codigocliente = (int)cmd.ExecuteScalar(); //declara una variable codigocliente de Tipo entero,y le guarda el autonumerico que se crea cuando se inserta el cli (con executescalar se inserta y se devuelve todo junto,lo hace el mismo metodo)
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit(); //con el commit confirmo la transaccion
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return codigocliente;  //devuelve el codigo generado automaticamente, al final de todo
        }


        public static void ModificarCliente(Cliente cliente)  
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion(); 
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_ModificarCliente";
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.Parameters.Add("@CodCliente", System.Data.SqlDbType.Int).Value = cliente.CodCliente;
                cmd.Parameters.Add("@Cuit", System.Data.SqlDbType.BigInt).Value = cliente.Cuit;
                cmd.Parameters.Add("@RazonSocial", System.Data.SqlDbType.VarChar, 50).Value = cliente.RazonSocial;
                cmd.Parameters.Add("@NombreFantasia", System.Data.SqlDbType.VarChar, 50).Value = cliente.NombreFantasia;
                cmd.Parameters.Add("@Direccion", System.Data.SqlDbType.VarChar, 100).Value = cliente.Direccion;
                cmd.Parameters.Add("@NombreCiudad", System.Data.SqlDbType.VarChar, 50).Value = cliente.Ciudad.Nombre;
                cmd.Parameters.Add("@NombreProvincia", System.Data.SqlDbType.VarChar, 50).Value = cliente.Ciudad.Provincia.Nombre;
                cmd.Parameters.Add("@CodPostal", System.Data.SqlDbType.Int).Value = cliente.CodPostal;
                cmd.Parameters.Add("@Telefono", System.Data.SqlDbType.VarChar, 20).Value = cliente.Telefono;
                cmd.Parameters.Add("@Fax", System.Data.SqlDbType.VarChar, 20).Value = cliente.Fax;
                cmd.Parameters.Add("@Celular", System.Data.SqlDbType.VarChar, 20).Value = cliente.Celular;
                cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100).Value = cliente.Email;
                cmd.Parameters.Add("@TipoIva", System.Data.SqlDbType.VarChar, 20).Value = cliente.TipoIva.Nombre;
                cmd.ExecuteNonQuery();  //solo ejecuta el stored procedure, no devuelve ningun escalar
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
                throw ex;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        //TODO: Revisar Stores
        public static void EliminarCliente(Cliente cliente)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandText = "sp_EliminarCliente";
                cmd.Parameters.Add("@CodCliente", System.Data.SqlDbType.Int).Value = cliente.CodCliente;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();

                throw ex;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }


        public static Cliente BuscarCliente(int codClienteBuscado)
        {
            //HACK: hacer un store propio que lea por codigo
            return (Cliente)RecuperarClientes().Where(p => p.CodCliente == codClienteBuscado).ToList().FirstOrDefault();
        }
    }
}
