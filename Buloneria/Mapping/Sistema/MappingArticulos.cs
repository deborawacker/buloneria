﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Entidades.Sistema;

namespace Mapping.Sistema
{
    public static class MappingArticulos //clase y metodos static, esto es porque esta clase de mapping solo hace metodos y no tiene atributos
    {
        #region Const
        private const String sp_UpdateArticulo = "sp_UpdateArticulo";
        #endregion

        //metodo recuperar  -uso try catch -no uso transaccion
        public static List<Articulo> RecuperarArticulos()
        {

            List<Articulo> coleccionArticulo = new List<Articulo>();
            try
            {
                SqlCommand cmdArticulos = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdArticulos.CommandType = System.Data.CommandType.StoredProcedure;
                cmdArticulos.CommandText = "sp_ConsultarArticulos";
                cmdArticulos.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drArticulos = cmdArticulos.ExecuteReader();
                while (drArticulos.Read())
                {

                    Articulo oArticulo = new Articulo();
                    oArticulo.IdArticulo = Convert.ToInt32(drArticulos["IdArticulo"]);
                    oArticulo.Descripcion = drArticulos["Descripcion"].ToString();
                    oArticulo.CantMinima = Convert.ToInt32(drArticulos["CantMinima"]);
                    oArticulo.CantActual = Convert.ToInt32(drArticulos["CantActual"]);
                    oArticulo.CodArticulo = drArticulos["CodArticulo"].ToString();
                    oArticulo.PrecioVenta = Convert.ToInt32(drArticulos["PrecioVenta"]);


                    #region GrupoArticulo

                    GrupoArticulo oGrupoArticulo = new GrupoArticulo();
                    oGrupoArticulo.CodGrupoArticulo = Convert.ToInt32(drArticulos["CodGrupoArticulo"]);
                    oGrupoArticulo.Descripcion = drArticulos["DescripcionGrupo"].ToString();
                    oArticulo.Grupo = oGrupoArticulo;

                    #endregion

                    #region Alicuota

                    Alicuota oAlicuota = new Alicuota();
                    oAlicuota.Valor = Convert.ToDecimal(drArticulos["ValorAlicuota"]);
                    oArticulo.Alicuota = oAlicuota;

                    #endregion

                    #region Articulo.Proveedor

                    SqlCommand cmdProveedoresArticulo = new SqlCommand();
                    cmdProveedoresArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdProveedoresArticulo.CommandText = "sp_ConsultarProveedoresArticulo";
                    cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdProveedoresArticulo.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = oArticulo.CodArticulo;
                    cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drProveedoresArticulo = cmdProveedoresArticulo.ExecuteReader();

                    while (drProveedoresArticulo.Read())
                    {
                        Proveedor oProveedor = new Proveedor();

                        #region Datos proveedor

                        oProveedor.CodProveedor = Convert.ToInt32(drProveedoresArticulo["CodProveedor"]);
                        oProveedor.Cuit = Convert.ToInt64(drProveedoresArticulo["Cuit"]);
                        oProveedor.RazonSocial = drProveedoresArticulo["RazonSocial"].ToString();
                        oProveedor.NombreFantasia = drProveedoresArticulo["NombreFantasia"].ToString();
                        oProveedor.Direccion = drProveedoresArticulo["Direccion"].ToString();
                        oProveedor.CodPostal = Convert.ToInt32(drProveedoresArticulo["CodPostal"]);
                        oProveedor.Telefono = drProveedoresArticulo["Telefono"].ToString();
                        oProveedor.Fax = drProveedoresArticulo["Fax"].ToString();
                        oProveedor.Celular = drProveedoresArticulo["Celular"].ToString();
                        oProveedor.Email = drProveedoresArticulo["Email"].ToString();

                        #endregion

                        #region TipoIva

                        TipoIva oIva = new TipoIva();
                        oIva.Descripcion = drProveedoresArticulo["DescripcionTipoIva"].ToString();
                        oIva.Nombre = drProveedoresArticulo["NombreTipoIva"].ToString();
                        oProveedor.TipoIva = oIva;

                        #endregion

                        #region Ciudad

                        Ciudad oCiudad = new Ciudad();
                        oCiudad.Nombre = drProveedoresArticulo["NombreCiudad"].ToString();
                        Provincia oProvincia = new Provincia();
                        oProvincia.Nombre = drProveedoresArticulo["NombreProvincia"].ToString();
                        oProvincia.AgregarCiudad(oCiudad);
                        oProveedor.Ciudad = oCiudad;

                        #endregion

                        oArticulo.AgregarProveedor(oProveedor);
                    }

                    #endregion

                    coleccionArticulo.Add(oArticulo);
                }

            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return coleccionArticulo;
        }

        //metodo agregar articulo-uso try catch -y uso transaccion
        public static void AgregarArticulo(Articulo articulo)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(); //con todo esto conecto a la base de datos
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;  //aca le indico que va a ser un _TipoMovimiento stored procedure el que uso en la base de datos
                cmd.CommandText = "sp_AgregarArticulo";  //el nombre del stored procedure de la base de datos
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)

                #region Parameters

                cmd.Parameters.Add("@pDescripcion", System.Data.SqlDbType.VarChar, 50).Value = articulo.Descripcion;
                cmd.Parameters.Add("@pCodGrupoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.Grupo.CodGrupoArticulo;
                cmd.Parameters.Add("@pCantMinima", System.Data.SqlDbType.Int).Value = articulo.CantMinima;
                cmd.Parameters.Add("@pCantActual", System.Data.SqlDbType.Int).Value = articulo.CantActual;
                cmd.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                cmd.Parameters.Add("@pValorAlicuota", System.Data.SqlDbType.Decimal).Value = articulo.Alicuota.Valor;
                cmd.Parameters.Add("@pPrecioVenta", System.Data.SqlDbType.Int).Value = articulo.PrecioVenta;

                #endregion

                cmd.ExecuteNonQuery();

                foreach (Entidades.Sistema.Proveedor proveedor in articulo.Proveedores)
                {
                    SqlCommand cmdProveedoresArticulo = new SqlCommand();
                    cmdProveedoresArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdProveedoresArticulo.CommandText = "sp_AgregarArticuloProveedor";
                    cmdProveedoresArticulo.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                    cmdProveedoresArticulo.Parameters.Add("@pCodProveedor", System.Data.SqlDbType.Int).Value = proveedor.CodProveedor;
                    cmdProveedoresArticulo.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                    cmdProveedoresArticulo.ExecuteNonQuery();
                }

                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion                        
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        public static void EliminarArticulo(Articulo articulo)
        {
            try
            {
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                SqlCommand cmd = new SqlCommand();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_EliminarArticulo";
                cmd.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (Exception e)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
                throw e;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }


        public static void Update(Articulo articulo)
        {
            SqlTransaction transaction = null;
            
            try
            {
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();

                transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();

                SqlCommand cmd = new SqlCommand();
                cmd.Transaction = transaction;
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = sp_UpdateArticulo;

                #region Parameters
                cmd.Parameters.Add("@pCodArticulo", System.Data.SqlDbType.Int).Value = articulo.IdArticulo;
                cmd.Parameters.Add("@pDescripcion", System.Data.SqlDbType.VarChar, 50).Value = articulo.Descripcion;
                cmd.Parameters.Add("@pCodGrupoArticulo", System.Data.SqlDbType.Int).Value = articulo.Grupo.CodGrupoArticulo;
                cmd.Parameters.Add("@pCantMinima", System.Data.SqlDbType.Int).Value = articulo.CantMinima;
                cmd.Parameters.Add("@pCantActual", System.Data.SqlDbType.Int).Value = articulo.CantActual;
                cmd.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                //TODO: por ahora precioVenta es entero pero para cambiar a decimal hay que borrar la tabla articulos, antes deberia estar el ABM articulos
                cmd.Parameters.Add("@pPrecioVenta", System.Data.SqlDbType.Int).Value = articulo.PrecioVenta;
                #endregion

                cmd.ExecuteNonQuery();


                SqlCommand cmdeap = new SqlCommand();
                cmdeap.Transaction = transaction;
                cmdeap.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmdeap.CommandType = System.Data.CommandType.StoredProcedure;
                cmdeap.CommandText = "sp_EliminarArticuloProveedor";
                cmdeap.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                cmdeap.ExecuteNonQuery();

                foreach (Proveedor proveedor in articulo.Proveedores)
                {
                    SqlCommand cmdProveedoresArticulo = new SqlCommand();
                    cmdProveedoresArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdProveedoresArticulo.CommandText = "sp_AgregarArticuloProveedor";
                    cmdProveedoresArticulo.Transaction = transaction;
                    cmdProveedoresArticulo.Parameters.Add("@pCodProveedor", System.Data.SqlDbType.Int).Value = proveedor.CodProveedor;
                    cmdProveedoresArticulo.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = articulo.CodArticulo;
                    cmdProveedoresArticulo.ExecuteNonQuery();
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                if (transaction != null) transaction.Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }
    }
}

#region StoredProcedures

#region sp_ConsultarArticulos
//ALTER PROCEDURE [dbo].[sp_ConsultarArticulos]
//AS
//BEGIN
//    SELECT 
//        codArticulo					AS IdArticulo,
//        art.descripcion				AS Descripcion,
//        art.codGrupoArticulo		AS CodGrupoArticulo,
//        gart.descripcion			AS DescripcionGrupo,
//        cantMinima					AS CantMinima,
//        cantActual					AS CantActual,
//        codigoArticulo				AS CodArticulo,
//        precioVenta					AS PrecioVenta,
//        alic.alicuota				AS ValorAlicuota
//    FROM Articulos art 
//    INNER JOIN GruposArticulos gart
//        ON art.codGrupoArticulo = gart.codGrupoArticulo
//    INNER JOIN Alicuotas alic
//        ON art.id_alicuota=alic.id_alicuota
//END
#endregion

#region sp_UpdateArticulo
//ALTER PROCEDURE [dbo].[sp_UpdateArticulo] 
//    -- Add the parameters for the stored procedure here
//@pCodArticulo INT,
//@pDescripcion VARCHAR(50),
//@pCodGrupoArticulo INT,
//@pCantMinima INT,
//@pCantActual INT,
//@pCodigoArticulo VARCHAR(10),
//@pPrecioVenta INT
//AS
//BEGIN
//    UPDATE Articulos
//    SET descripcion = @pDescripcion,
//        codGrupoArticulo = @pCodGrupoArticulo,
//        cantMinima = @pCantMinima,
//        cantActual = @pCantActual,
//        codigoArticulo = @pCodigoArticulo,
//        precioVenta = @pPrecioVenta 
//    WHERE codArticulo = @pCodArticulo
//END 
#endregion

#region EliminarArticulo
//ALTER procedure [dbo].[sp_EliminarArticuloProveedor] 
//@pCodigoArticulo varchar (10)
//AS 
//DELETE FROM ArticulosProveedores 
//WHERE codArticulo = (SELECT codArticulo FROM Articulos WHERE codigoArticulo = @pCodigoArticulo)
#endregion

#region sp_ConsultarProveedoresArticulo
//ALTER PROCEDURE [dbo].[sp_ConsultarProveedoresArticulo] 
//@pCodigoArticulo VARCHAR(10)
//AS
//    SELECT	proveedores.codprov			AS CodProveedor,
//            cuit						AS Cuit,
//            razonsocial					AS RazonSocial,
//            nomfantasia					AS NombreFantasia,
//            direccion					AS Direccion,
//            codpostal					AS CodPostal,
//            telefono					AS Telefono,
//            fax							AS Fax,
//            celular						AS Celular,
//            email						AS Email,
//            localidades.nombrelocalidad AS NombreCiudad,
//            provincias.nombreprovincia	NombreProvincia,
//            tiposiva.nombretipo			AS NombreTipoIva,
//            tiposiva.descripcion		AS DescripcionTipoIva
//    FROM ArticulosProveedores
//    INNER JOIN proveedores 
//        ON articulosproveedores.codprov = proveedores.codprov
//    INNER JOIN localidades 
//        ON proveedores.ciudad = localidades.Cod_Localidad
//    INNER JOIN Provincias 
//        ON Localidades.cod_Provincia = provincias.cod_provincia
//    INNER JOIN tiposiva 
//        ON proveedores.tipoiva = tiposiva.nombretipo
//    INNER JOIN articulos 
//        ON articulosproveedores.codarticulo = articulos.codarticulo
//WHERE codigoArticulo = @pCodigoArticulo
#endregion

#endregion
