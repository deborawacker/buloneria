﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Permissions;
using System;

namespace Mapping.Sistema
{
    public static class MappingCuentasCorrientes
    {
        //metodo recuperar cuentas corrientes -try catch -no transaccion
        public static List<Entidades.Sistema.CuentaCorriente> RecuperarCuentasCorrientes() 
        { 
            List<Entidades.Sistema.CuentaCorriente> colCtaCte = new List<Entidades.Sistema.CuentaCorriente>();
            try
            {
                SqlCommand cmdCtaCte = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdCtaCte.CommandType = System.Data.CommandType.StoredProcedure;
                cmdCtaCte.CommandText = "sp_RecuperarCuentasCorrientes";
                cmdCtaCte.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drCtaCte = cmdCtaCte.ExecuteReader();
                while (drCtaCte.Read())
                {
                    Entidades.Sistema.CuentaCorriente oCtaCte = new Entidades.Sistema.CuentaCorriente();
                    oCtaCte.NroCtaCte = Convert.ToInt32(drCtaCte["nroctacte"]);
                   
                    //traigo el cliente
                    Entidades.Sistema.Cliente oCliente = new Entidades.Sistema.Cliente();
                    oCliente.CodCliente = Convert.ToInt32(drCtaCte["codCli"]);
                    oCliente.Celular = drCtaCte["celular"].ToString();
                    oCliente.CodPostal = Convert.ToInt32(drCtaCte["codPostal"]);
                    oCliente.Cuit = Convert.ToInt64(drCtaCte["cuit"]);
                    oCliente.Direccion = drCtaCte["direccion"].ToString();
                    oCliente.Email = drCtaCte["email"].ToString();
                    oCliente.Fax = drCtaCte["fax"].ToString();
                    oCliente.NombreFantasia = drCtaCte["nomFantasia"].ToString();
                    oCliente.RazonSocial = drCtaCte["razonSocial"].ToString();
                    oCliente.Telefono = drCtaCte["telefono"].ToString();                    
                    oCtaCte.Cliente = oCliente;
                    //traigo todos los movimientos de una cuenta corriente
                    SqlCommand cmdMov = new SqlCommand();
                    cmdMov.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdMov.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdMov.CommandText = "sp_RecuperarMovimientosCtasCtes";
                    cmdMov.Parameters.Add("@codCli", System.Data.SqlDbType.Int).Value = oCliente.CodCliente;
                    SqlDataReader drMovCtaCte = cmdMov.ExecuteReader();
                    while (drMovCtaCte.Read())
                    {
                        Entidades.Sistema.MovimientoCtaCte oMovCtaCte = new Entidades.Sistema.MovimientoCtaCte();
                        oMovCtaCte.Fecha = Convert.ToDateTime(drMovCtaCte["fecha"]);
                        oMovCtaCte.Monto = Convert.ToInt32(drMovCtaCte["monto"]);                        
                        //traigo el concepto
                        Entidades.Sistema.Concepto oConcepto = new Entidades.Sistema.Concepto();
                        oConcepto.Descripcion = drMovCtaCte["descripcion"].ToString();
                        oConcepto.Concepto1 = drMovCtaCte["concepto"].ToString();
                        //traigo el tipo movimiento (enumeracion)
                        string tipoMovimiento = drMovCtaCte["tipoMovimiento"].ToString();
                        if (tipoMovimiento == Entidades.Sistema.TipoMovimiento.Debe.ToString()) oConcepto.TipoMovimiento = Entidades.Sistema.TipoMovimiento.Debe;
                        if (tipoMovimiento == Entidades.Sistema.TipoMovimiento.Haber.ToString()) oConcepto.TipoMovimiento = Entidades.Sistema.TipoMovimiento.Haber;
                        //agrego el concepto al movimiento
                        oMovCtaCte.Concepto = oConcepto;

                        oMovCtaCte.NroMovimiento = Convert.ToInt32(drMovCtaCte["nromovimiento"]);
                        oCtaCte.Saldo = Convert.ToInt32(drMovCtaCte["saldo"]);
                        oCtaCte.AgregarMovimiento(oMovCtaCte);
                    }
                    
                    colCtaCte.Add(oCtaCte);
                }
            
            }
            catch(SqlException ex)
            {
               Servicios.EventManager.RegistarErrores(ex);
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colCtaCte;
        }

        //metodo modificar cuenta corriente -try catch -y transaccion
        public static void ModificarCuentaCorriente(Entidades.Sistema.CuentaCorriente oCtaCte)
        {
            try
            {                
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                
                SqlCommand cmdElim = new SqlCommand();
                cmdElim.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmdElim.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmdElim.CommandType = System.Data.CommandType.StoredProcedure;
                cmdElim.CommandText = "sp_EliminarMovCtaCte";
                cmdElim.Parameters.Add("@nroCtaCte", System.Data.SqlDbType.Int).Value = oCtaCte.NroCtaCte;
                cmdElim.ExecuteNonQuery();

                SqlCommand cmdMovCtaCte = new SqlCommand();
                cmdMovCtaCte.CommandType = System.Data.CommandType.StoredProcedure;
                cmdMovCtaCte.CommandText = "sp_AgregarMovCtaCte";
                cmdMovCtaCte.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmdMovCtaCte.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();

                int saldo = 0;
                foreach (Entidades.Sistema.MovimientoCtaCte movCtaCte in oCtaCte.MovimientosCtaCte)
                {
                    if(movCtaCte.Concepto.TipoMovimiento == Entidades.Sistema.TipoMovimiento.Debe)
                    {
                        saldo += movCtaCte.Monto;
                    }
                    else
                    {
                        saldo -= movCtaCte.Monto;
                    }
                    cmdMovCtaCte.Parameters.Add("@nroCtaCte", System.Data.SqlDbType.Int).Value = oCtaCte.NroCtaCte;
                    cmdMovCtaCte.Parameters.Add("@nromovimiento", System.Data.SqlDbType.Int).Value = movCtaCte.NroMovimiento;
                    cmdMovCtaCte.Parameters.Add("@fecha", System.Data.SqlDbType.DateTime).Value = movCtaCte.Fecha;
                    cmdMovCtaCte.Parameters.Add("@monto", System.Data.SqlDbType.Int).Value = movCtaCte.Monto;                    
                    cmdMovCtaCte.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar,20).Value = movCtaCte.Concepto.Descripcion;
                    cmdMovCtaCte.Parameters.Add("@saldo", System.Data.SqlDbType.Int).Value = saldo;                    

                    cmdMovCtaCte.ExecuteNonQuery();
                    cmdMovCtaCte.Parameters.Clear();
                }
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch(SqlException ex)
            {                 
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally 
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            
        }       
    }
}
