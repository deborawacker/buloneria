﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Mapping.Sistema
{
    public static class MappingInformes
    {
        //metodo recuperar artículos con faltantes de stock:
        public static DataTable RecuperarArticulosFaltStock() //este metodo trae un tipo de datos datatable, es toda la tabla que trae de la bd
        {
            DataTable dt = new DataTable();  //dt es mi datatable
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_RecuperarArticulosFaltStock";
                SqlDataReader drArtFaltStock = cmd.ExecuteReader();
                dt.Load(drArtFaltStock); //en el load le asigno al dt el drartfaltstock               
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return dt; //devuelvo el datatable
        }

        //metodo recuperar articulos mas vendidos:
        public static DataTable RecuperarArtMasVendidos(DateTime fechadesde,DateTime fechahasta) //este metodo trae un tipo de datos datatable, es toda la tabla que trae de la bd
        {
            DataTable dt = new DataTable();  //dt es mi datatable
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_ConsultarArtMasVendidos";
                cmd.Parameters.Add("@fechadesde", System.Data.SqlDbType.DateTime).Value=fechadesde;
                cmd.Parameters.Add("@fechahasta",System.Data.SqlDbType.DateTime).Value=fechahasta;
                SqlDataReader drArtMasVend = cmd.ExecuteReader();                
                dt.Load(drArtMasVend); //en el load le asigno al dt el drartmasvend
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return dt; //devuelvo el datatable
        }

        //metodo recuperar montos de compras por mes de clientes:
        public static DataTable RecuperarMontosComprasMesClientes(DateTime fechadesde,DateTime fechahasta) //este metodo trae un tipo de datos datatable, es toda la tabla que trae de la bd
        {
            DataTable dt = new DataTable(); //dt es mi datatable
            try 
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_RecuperarMontosComprasMesClientes";
                cmd.Parameters.Add("@fechadesde", System.Data.SqlDbType.DateTime).Value=fechadesde;
                cmd.Parameters.Add("@fechahasta",System.Data.SqlDbType.DateTime).Value=fechahasta;
                SqlDataReader drMontosComprasMesCli = cmd.ExecuteReader();
                dt.Load(drMontosComprasMesCli); //en el load le asigno al dt el drmontoscomprasmescli                
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return dt; //devuelvo el datatable        
        }

        //metodo recuperar precios entre proveedores de un articulo:
        public static DataTable RecuperarPreciosComparativosArt(string codigoArticulo) //este metodo trae un tipo de datos datatable, es toda la tabla que trae de la bd
        {
            DataTable dt = new DataTable(); //dt es mi datatable
            try 
            {                
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_recuperarPreciosComparativosArt";
                cmd.Parameters.Add("@codigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = codigoArticulo;
                SqlDataReader drPreCompArt = cmd.ExecuteReader();
                dt.Load(drPreCompArt);
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return dt; //devuelvo el datatable   
        
        }

        


    }
}
