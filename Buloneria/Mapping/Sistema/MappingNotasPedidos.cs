﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Entidades.Sistema;

namespace Mapping.Sistema
{
    public class MappingNotasPedidos
    {

        #region Const
        private const String sp_RecuperarLineasNP = "sp_RecuperarLineasNP";

        private const String sp_ActualizarCantidadEntregadaLineasNotaPedido = "sp_ActualizarCantidadEntregadaLineasNotaPedido";

        private const String pNroLineaNotaPedido = "@pNroLineaNotaPedido";

        private const String pCantidadEntregada = "@pCantidadEntregada";
        #endregion

        //recuperar notas de pedidos  -uso try catch  -no uso transaccion
        public static List<NotaPedido> RecuperarNotasPedidos()
        {
            List<NotaPedido> colNotaPedido = new List<NotaPedido>();
            try
            {
                SqlCommand cmdNotaPedido = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdNotaPedido.CommandType = System.Data.CommandType.StoredProcedure;
                cmdNotaPedido.CommandText = "sp_RecuperarNotasPedidos";
                cmdNotaPedido.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drNotasPedidos = cmdNotaPedido.ExecuteReader();
                while (drNotasPedidos.Read())
                {
                    NotaPedido oNotaPedido = new NotaPedido();
                    oNotaPedido.NroNotaPedido = Convert.ToInt32(drNotasPedidos["NroNotaPedido"]);
                    oNotaPedido.FechaEmision = Convert.ToDateTime(drNotasPedidos["FechaEmision"]);

                    #region Estado
                    Int64 id = Convert.ToInt64(drNotasPedidos["Id_EstadoNotaPedido"]);
                    oNotaPedido.EstadoNotaPedido = EstadoNotaPedidoFactory.Crear(id);

                    oNotaPedido.EstadoNotaPedido.Id = id;
                    oNotaPedido.EstadoNotaPedido.Descripcion = drNotasPedidos["Descripcion_EstadoNotaPedido"].ToString();
                    #endregion

                    #region FormaPago
                    FormaPago oFormaPago = new FormaPago();
                    oFormaPago.TipoFormaPago = drNotasPedidos["TipoFormaPago"].ToString();
                    oNotaPedido.FormaPago = oFormaPago;
                    #endregion

                    #region Cliente
                    Cliente oCliente = new Cliente();
                    oCliente.CodCliente = Convert.ToInt32(drNotasPedidos["CodCliente"]);
                    oCliente.Celular = drNotasPedidos["Celular"].ToString();
                    oCliente.CodPostal = Convert.ToInt32(drNotasPedidos["CodPostal"]);
                    oCliente.Cuit = Convert.ToInt64(drNotasPedidos["Cuit"]);
                    oCliente.Direccion = drNotasPedidos["Direccion"].ToString();
                    oCliente.Email = drNotasPedidos["Email"].ToString();
                    oCliente.Fax = drNotasPedidos["Fax"].ToString();
                    oCliente.NombreFantasia = drNotasPedidos["NombreFantasia"].ToString();
                    oCliente.RazonSocial = drNotasPedidos["RazonSocial"].ToString();
                    oCliente.Telefono = drNotasPedidos["Telefono"].ToString();

                    #region Cliente.TipoIva
                    TipoIva oTipoIva = new TipoIva();
                    oTipoIva.Descripcion = drNotasPedidos["Descripcion_TipoIva"].ToString();
                    oTipoIva.Nombre = drNotasPedidos["Nombre_TipoIva"].ToString();
                    oCliente.TipoIva = oTipoIva;
                    #endregion

                    #region Cliente.Ciudad

                    Ciudad oCiudad = new Ciudad();
                    oCiudad.Nombre = drNotasPedidos["Nombre_Localidad"].ToString();

                    #region Ciudad.Provincia
                    Provincia oProvincia = new Provincia();
                    oProvincia.Nombre = drNotasPedidos["Nombre_Provincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    oCliente.Ciudad = oCiudad;
                    #endregion

                    #endregion

                    #endregion

                    oNotaPedido.Cliente = oCliente;

                    //aca traigo todas las lineas de articulos de una nota de pedido
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = sp_RecuperarLineasNP;
                    cmdLinea.Parameters.Add("@NroNotaPedido", System.Data.SqlDbType.Int).Value = oNotaPedido.NroNotaPedido; //le paso como parametro el NroNP al stored procedure

                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drLineasArticulos = cmdLinea.ExecuteReader();
                    while (drLineasArticulos.Read())
                    {
                        try
                        {

                            LineaNotaPedido oLinea = new LineaNotaPedido();
                            oLinea.NroLineaNotaPedido = Convert.ToInt32(drLineasArticulos["ID_LineaNotaPedido"]);
                            oLinea.CantPedida = Convert.ToInt32(drLineasArticulos["CantPedida"]);
                            oLinea.PrecioUnitario = Convert.ToInt32(drLineasArticulos["PrecioUnitario"]);
                            oLinea.CantEntregada = Convert.ToInt32(drLineasArticulos["CantidadEntregada"]);


                            #region LineaNotaPedido.Articulo
                            Articulo oArticulo = new Articulo();
                            oArticulo.IdArticulo = Convert.ToInt32(drLineasArticulos["IdArticulo"]);
                            oArticulo.CodArticulo = drLineasArticulos["CodigoArticulo"].ToString();
                            oArticulo.Descripcion = drLineasArticulos["Descripcion_Articulo"].ToString();
                            oArticulo.CantMinima = Convert.ToInt32(drLineasArticulos["CantidadMinima"]);
                            oArticulo.CantActual = Convert.ToInt32(drLineasArticulos["CantidadActual"]);
                            oArticulo.PrecioVenta = Convert.ToInt32(drLineasArticulos["PrecioVenta"]);

                            #region Articulo.Alicuota

                            Alicuota oAlicuota = new Alicuota();
                            oAlicuota.Valor = Convert.ToDecimal(drLineasArticulos["Valor_Alicuota"]);
                            oArticulo.Alicuota = oAlicuota;

                            #endregion

                            #region Articulo.Grupo
                            GrupoArticulo Grupo = new GrupoArticulo();
                            Grupo.CodGrupoArticulo = Convert.ToInt32(drLineasArticulos["CodigoGrupoArticulo"]); //porque en la bd el "as..."
                            Grupo.NombreGrupo = drLineasArticulos["GrupoArticulo"].ToString();
                            Grupo.Descripcion = drLineasArticulos["Descripcion_GrupoArticulo"].ToString(); //porque en la bd el "as..."
                            oArticulo.Grupo = Grupo;
                            #endregion

                            #region Articulo.Proveedores

                            SqlCommand cmdProveedoresArticulo = new SqlCommand();
                            cmdProveedoresArticulo.CommandType = System.Data.CommandType.StoredProcedure;
                            cmdProveedoresArticulo.CommandText = "sp_ConsultarProveedoresArticulo";

                            cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                            cmdProveedoresArticulo.Parameters.Add("@pCodigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = oArticulo.CodArticulo;
                            cmdProveedoresArticulo.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                            SqlDataReader drProveedoresArticulo = cmdProveedoresArticulo.ExecuteReader();
                            while (drProveedoresArticulo.Read()) //tengo que recorrer el proveedor
                            {
                                Proveedor oProveedorArticulo = new Proveedor();
                                oProveedorArticulo.Celular = drProveedoresArticulo["Celular"].ToString();
                                oProveedorArticulo.CodPostal = Convert.ToInt32(drProveedoresArticulo["CodPostal"]);
                                oProveedorArticulo.CodProveedor = Convert.ToInt32(drProveedoresArticulo["CodProveedor"]);
                                oProveedorArticulo.Cuit = Convert.ToInt64(drProveedoresArticulo["Cuit"]);
                                oProveedorArticulo.Direccion = drProveedoresArticulo["Direccion"].ToString();
                                oProveedorArticulo.Email = drProveedoresArticulo["Email"].ToString();
                                oProveedorArticulo.Fax = drProveedoresArticulo["Fax"].ToString();
                                oProveedorArticulo.NombreFantasia = drProveedoresArticulo["NombreFantasia"].ToString();
                                oProveedorArticulo.RazonSocial = drProveedoresArticulo["RazonSocial"].ToString();
                                oProveedorArticulo.Telefono = drProveedoresArticulo["Telefono"].ToString();

                                #region TipoIva

                                TipoIva oIva = new TipoIva();
                                oIva.Descripcion = drProveedoresArticulo["DescripcionTipoIva"].ToString();
                                oIva.Nombre = drProveedoresArticulo["NombreTipoIva"].ToString();
                                oProveedorArticulo.TipoIva = oIva;

                                #endregion

                                #region Ciudad

                                Ciudad oCiudadArticulo = new Ciudad();
                                oCiudadArticulo.Nombre = drProveedoresArticulo["NombreCiudad"].ToString();
                                Provincia oProvinciaArticulo = new Provincia();
                                oProvinciaArticulo.Nombre = drProveedoresArticulo["NombreProvincia"].ToString();
                                oProvinciaArticulo.AgregarCiudad(oCiudad);
                                oProveedorArticulo.Ciudad = oCiudadArticulo;

                                #endregion

                                oArticulo.AgregarProveedor(oProveedorArticulo);
                            }
                            #endregion

                            #endregion

                            oLinea.Articulo = oArticulo;  //le asigno a la propiedad Articulo el objeto oArticulo
                            oNotaPedido.AgregarLineaNotaPedido(oLinea);  //al objeto onotapedido le agrego el objeto oLinea, con el metodo AgregarLineaNotaPedido



                        }
                        catch (SqlException ex)
                        {

                            throw ex;
                        }
                    }
                    colNotaPedido.Add(oNotaPedido);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colNotaPedido;  //recordar que le tengo que devolver la coleccion
        }

        //agregar nota de pedido -uso try catch -y uso transaccion
        public static int AgregarNotaPedido(NotaPedido np)
        {
            int nroNotaPedido = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_GenerarNotaPedido";
                cmd.Parameters.Add("@fechaEmision", System.Data.SqlDbType.DateTime).Value = np.FechaEmision;
                cmd.Parameters.Add("@tipoFormaPago", System.Data.SqlDbType.VarChar, 20).Value = np.FormaPago.TipoFormaPago;
                cmd.Parameters.Add("@codCli", System.Data.SqlDbType.Int).Value = np.Cliente.CodCliente;
                cmd.Parameters.Add("@idUsuario", System.Data.SqlDbType.Int).Value = np.Usuario.IdUsuario;
                nroNotaPedido = Convert.ToInt32(cmd.ExecuteScalar());

                //el foreach es para agregar cada linea a la nota pedido
                foreach (LineaNotaPedido oLinea in np.LineasNotaPedido)
                {
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_AgregarLineaNotaPedido";
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdLinea.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                    cmdLinea.Parameters.Add("@nroNotaPedido", System.Data.SqlDbType.Int).Value = nroNotaPedido;
                    cmdLinea.Parameters.Add("@codigoArticulo", System.Data.SqlDbType.VarChar, 10).Value = oLinea.Articulo.CodArticulo;
                    cmdLinea.Parameters.Add("@cantidad", System.Data.SqlDbType.Int).Value = oLinea.CantPedida;
                    cmdLinea.Parameters.Add("@precioUnitario", System.Data.SqlDbType.Int).Value = oLinea.PrecioUnitario;
                    cmdLinea.ExecuteNonQuery();
                }
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit(); //confirmo la transaccion                
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return nroNotaPedido;
        }

        //eliminar nota pedido 
        public static void EliminarNotaPedido(NotaPedido np)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_EliminarNotaPedido";
                cmd.Parameters.Add("@nroNotaPedido", System.Data.SqlDbType.Int).Value = np.NroNotaPedido;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        //ver historial nota de pedido es un recuperar todas las notas de pedidos -uso try y catch pero NO transaccion
        public static List<NotaPedido> RecuperarHistorialNotasPedidos()
        {
            List<NotaPedido> colNotaPedido = new List<NotaPedido>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_RecuperarHistorialNotasPedidos";
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drNP = cmd.ExecuteReader();
                while (drNP.Read())
                {
                    NotaPedido oNP = new NotaPedido();
                    oNP.NroNotaPedido = Convert.ToInt32(drNP["nroNotaPedido"]);
                    oNP.FechaEmision = Convert.ToDateTime(drNP["fechaEmision"]);
                    //FORMA DE PAGO
                    FormaPago oFormaPago = new FormaPago();
                    oFormaPago.TipoFormaPago = drNP["tipoFormaPago"].ToString();
                    oNP.FormaPago = oFormaPago;
                    //cliente, un cliente por cada np
                    Cliente oCliente = new Cliente();
                    oCliente.CodCliente = Convert.ToInt32(drNP["codCli"]);
                    oCliente.Celular = drNP["celular"].ToString();
                    oCliente.CodPostal = Convert.ToInt32(drNP["codPostal"]);
                    oCliente.Cuit = Convert.ToInt64(drNP["cuit"]);
                    oCliente.Direccion = drNP["direccion"].ToString();
                    oCliente.Email = drNP["email"].ToString();
                    oCliente.Fax = drNP["fax"].ToString();
                    oCliente.NombreFantasia = drNP["nomFantasia"].ToString();
                    oCliente.RazonSocial = drNP["razonSocial"].ToString();
                    oCliente.Telefono = drNP["telefono"].ToString();
                    TipoIva oTipoIva = new TipoIva();
                    oTipoIva.Descripcion = drNP["descripcion"].ToString();
                    oTipoIva.Nombre = drNP["tipoiva"].ToString();
                    Ciudad oCiudad = new Ciudad();
                    oCiudad.Nombre = drNP["nombrelocalidad"].ToString();
                    Provincia oProvincia = new Provincia();
                    oProvincia.Nombre = drNP["nombreprovincia"].ToString();
                    oProvincia.AgregarCiudad(oCiudad);
                    oCliente.TipoIva = oTipoIva;
                    oCliente.Ciudad = oCiudad;
                    oNP.Cliente = oCliente;
                    //aca traigo todas las lineas de articulos de una NP
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdLinea.CommandText = "sp_RecuperarLineasNP";
                    cmdLinea.Parameters.Add("@nroNP", System.Data.SqlDbType.Int).Value = oNP.NroNotaPedido; //le paso como parametro el nronp al stored procedure
                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drLineasArticulos = cmdLinea.ExecuteReader();
                    while (drLineasArticulos.Read())
                    {
                        LineaNotaPedido oLinea = new LineaNotaPedido();
                        oLinea.CantPedida = Convert.ToInt32(drLineasArticulos["cantidad"]);
                        oLinea.PrecioUnitario = Convert.ToInt32(drLineasArticulos["precioUnitario"]);
                        Articulo oArticulo = new Articulo();
                        GrupoArticulo Grupo = new GrupoArticulo();
                        Grupo.CodGrupoArticulo = Convert.ToInt32(drLineasArticulos["CodigoGrupoArticulo"]); //porque en la bd el "as..."
                        Grupo.Descripcion = drLineasArticulos["DescripcionGrupo"].ToString(); //porque en la bd el "as..."
                        oArticulo.Grupo = Grupo;
                        oArticulo.CodArticulo = drLineasArticulos["codigoArticulo"].ToString();
                        oArticulo.Descripcion = drLineasArticulos["descripcion"].ToString();
                        oArticulo.CantMinima = Convert.ToInt32(drLineasArticulos["cantMinima"]);
                        oArticulo.CantActual = Convert.ToInt32(drLineasArticulos["cantActual"]);
                        Alicuota oAlicuota = new Alicuota();
                        oAlicuota.Valor = Convert.ToDecimal(drLineasArticulos["alicuota"]);
                        oArticulo.Alicuota = oAlicuota;
                        oLinea.Articulo = oArticulo;  //le asigno a la propiedad Articulo el objeto oArticulo
                        oNP.AgregarLineaNotaPedido(oLinea);  //al objeto ono le agrego el objeto oLinea, con el metodo AgregarLineaNotaPedido
                    }
                    colNotaPedido.Add(oNP);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colNotaPedido;  //recordar que le tengo que devolver la coleccion
        }

        public static void ActualizarEstadoNotaPedido(NotaPedido notaPedido)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_CambiarEstadoNotaPedido";
                cmd.Parameters.Add("@nroNotaPedido", System.Data.SqlDbType.Int).Value = notaPedido.NroNotaPedido;
                cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar, 10).Value = notaPedido.EstadoNotaPedido.Id;
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }

        public static void ActualizarCantidadEntregadaLineasNotaPedido(NotaPedido notaPedido)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();

                foreach (LineaNotaPedido oLineaNP in notaPedido.LineasNotaPedido)
                {
                    SqlCommand cmdLinea = new SqlCommand();
                    cmdLinea.CommandType = System.Data.CommandType.StoredProcedure;

                    cmdLinea.CommandText = sp_ActualizarCantidadEntregadaLineasNotaPedido;

                    cmdLinea.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    cmdLinea.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();

                    cmdLinea.Parameters.Add(pNroLineaNotaPedido, System.Data.SqlDbType.Int).Value = oLineaNP.NroLineaNotaPedido;
                    cmdLinea.Parameters.Add(pCantidadEntregada, System.Data.SqlDbType.Int).Value = oLineaNP.CantEntregada;

                    cmdLinea.ExecuteNonQuery();
                }
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit(); //confirmo la transaccion                
            }
            catch (SqlException ex)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }
    }
}

#region Stored Procedures

#region sp_RecuperarLineasNP
//ALTER procedure [dbo].[sp_RecuperarLineasNP]
//@NroNP int
//as
//select  lnp.nroLineaNotaPedido	AS 'ID_LineaNotaPedido',
//        cantidad,
//        precioUnitario,
//        art.codArticulo,
//        art.codigoArticulo,
//        art.descripcion,
//        art.cantMinima,
//        art.cantActual, 
//        alic.alicuota, 
//        cantEntregada			AS 'CantidadEntregada',
//        gart.codGrupoArticulo	as 'CodigoGrupoArticulo', 
//        gart.descripcion		as 'DescripcionGrupo',
//        gart.grupoArticulo		as 'GrupoArticulo' 
//from LineaNotaPedido lnp
//inner join Articulos art 
//    on lnp.codarticulo = art.codarticulo 
//inner join Alicuotas alic 
//    on art.id_alicuota = alic.id_alicuota 
//inner join GruposArticulos gart 
//    on art.CodGrupoArticulo = gart.CodGrupoArticulo
//where nroNotaPedido = @NroNP
#endregion

#endregion


