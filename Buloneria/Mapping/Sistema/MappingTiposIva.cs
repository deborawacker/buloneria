﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Sistema
{
    public static class MappingTiposIva
    {
        //voy a definir un metodo recuperar recuperar _TipoMovimiento iva -uso try catch -no uso transaccion
        public static List<Entidades.Sistema.TipoIva> RecuperarTiposIva()
        {
            List<Entidades.Sistema.TipoIva> colTiposIva = new List<Entidades.Sistema.TipoIva>();
            try
            {
                SqlCommand cmdTiposIva = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdTiposIva.CommandText = "sp_ConsultarTiposIVA";
                cmdTiposIva.CommandType = System.Data.CommandType.StoredProcedure;
                cmdTiposIva.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drTiposIva = cmdTiposIva.ExecuteReader();
                while (drTiposIva.Read())
                {
                    Entidades.Sistema.TipoIva oTipoIva = new Entidades.Sistema.TipoIva();
                    oTipoIva.Nombre = drTiposIva["nombreTipo"].ToString();
                    oTipoIva.Descripcion = drTiposIva["descripcion"].ToString();
                    colTiposIva.Add(oTipoIva);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colTiposIva;
        }
    }
}
