﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Security.Permissions;
using System;

namespace Mapping.Sistema
{
    public static class MappingConceptos
    {
        
        //metodo recuperar conceptos -try catch -no transaccion
        public static List<Entidades.Sistema.Concepto> RecuperarConceptos()
        {
            List<Entidades.Sistema.Concepto> colConceptos = new List<Entidades.Sistema.Concepto>();
            try
            {
                SqlCommand cmdConceptos = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdConceptos.CommandType = System.Data.CommandType.StoredProcedure;
                cmdConceptos.CommandText = "sp_RecuperarConceptos";
                cmdConceptos.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drConceptos = cmdConceptos.ExecuteReader();
                while (drConceptos.Read())
                {
                    Entidades.Sistema.Concepto oConcepto = new Entidades.Sistema.Concepto();
                    oConcepto.Concepto1 = drConceptos["concepto"].ToString();
                    oConcepto.Descripcion = drConceptos["descripcion"].ToString();
                    string oTipoMov = drConceptos["tipoMovimiento"].ToString();
                    if (oTipoMov == Entidades.Sistema.TipoMovimiento.Debe.ToString()) oConcepto.TipoMovimiento = Entidades.Sistema.TipoMovimiento.Debe;
                    if (oTipoMov == Entidades.Sistema.TipoMovimiento.Haber.ToString()) oConcepto.TipoMovimiento = Entidades.Sistema.TipoMovimiento.Haber;
                    colConceptos.Add(oConcepto);                    
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colConceptos;
        }

        //metodo agregar concepto -uso try catch y transaccion
        public static void AgregarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion(); //le indico que comienza la transaccion, a partir de este momento tiene que poner a prueba lo que hace con la base de datos
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();  //le paso los datos de la transaccion y se lo asigno al objeto cmd (el que hace realmente la manipulacion en la bd)
                cmd.CommandText = "sp_AgregarConcepto";
                cmd.Parameters.Add("@concepto", System.Data.SqlDbType.VarChar, 20).Value = oConcepto.Concepto1;
                cmd.Parameters.Add("@descripcion", System.Data.SqlDbType.VarChar, 20).Value = oConcepto.Descripcion;
                cmd.Parameters.Add("@tipoMovimiento", System.Data.SqlDbType.VarChar, 20).Value = oConcepto.TipoMovimiento;                
                cmd.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion                
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();  //si encuentra un error en el anterior, tira todo atras
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();  // el finally se ejecuta siempre, y aca es donde desconecto
            }
        
        }

        //metodo eliminar concepto -uso try catch y transaccion
        public static void EliminarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(); 
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema"); //aca le doy la orden que conecte con "sistema" que lo tengo declarado en el app.config
                Servicios.Conexion.ObtenerInstancia().ComenzarTransaccion();
                cmd.CommandType = System.Data.CommandType.StoredProcedure; //le indico que voy a usar un stored procedure
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion(); //son los metodos para retornar la conexion, que estan dentro de la capa de servicios
                cmd.Transaction = Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion();
                cmd.CommandText = "sp_EliminarConcepto";
                cmd.Parameters.Add("@concepto", System.Data.SqlDbType.VarChar, 20).Value = oConcepto.Concepto1;                
                cmd.ExecuteNonQuery(); //este es para indicar que ejecute y que no es una consulta
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Commit();  //con el commit confirmo la transaccion//le digo cual es el codgrupoarticulo de la base de datos y el grupoarticulo que le paso como parametro 
            }
            catch (SqlException)
            {
                Servicios.Conexion.ObtenerInstancia().RetornarSqlTransaccion().Rollback();
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar(); //desconectar de la bd
            }
        }
    }
}
