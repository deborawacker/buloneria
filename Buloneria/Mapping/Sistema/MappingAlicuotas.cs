﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Sistema
{
    public static class MappingAlicuotas
    {
        //voy a definir un metodo recuperar recuperar alicuotas -uso try catch -no uso transaccion
        public static List<Entidades.Sistema.Alicuota> RecuperarAlicuotas()
        {
            List<Entidades.Sistema.Alicuota> colAlicuotas = new List<Entidades.Sistema.Alicuota>();
            try
            {
                SqlCommand cmdAlicuotas = new SqlCommand();
                Servicios.Conexion.ObtenerInstancia().Conectar("Sistema");
                cmdAlicuotas.CommandText = "sp_consultaralicuotas";
                cmdAlicuotas.CommandType = System.Data.CommandType.StoredProcedure;
                cmdAlicuotas.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drAlicuotas = cmdAlicuotas.ExecuteReader();
                while (drAlicuotas.Read())
                {
                    Entidades.Sistema.Alicuota oAlicuota = new Entidades.Sistema.Alicuota();
                    oAlicuota.Valor = Convert.ToDecimal(drAlicuotas["alicuota"]);
                    colAlicuotas.Add(oAlicuota);
                }
            }
            catch (SqlException ex)
            {
                Servicios.EventManager.RegistarErrores(ex);
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
            return colAlicuotas;
        }

    }
}
