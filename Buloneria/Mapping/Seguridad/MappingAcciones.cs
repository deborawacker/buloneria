﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Seguridad
{
    public static class MappingAcciones
    {
        public static List<Entidades.Seguridad.Accion> RecuperarAccionesPerfiles(Entidades.Seguridad.Perfil oPerfil)
        {
            List<Entidades.Seguridad.Accion> ColAcciones = new List<Entidades.Seguridad.Accion>();

            SqlCommand cmdAcciones = new SqlCommand();
            cmdAcciones.CommandText = "sp_ConsultarAccionesPerfiles";
            cmdAcciones.CommandType = System.Data.CommandType.StoredProcedure;
            cmdAcciones.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = oPerfil.IdPerfil;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drAcciones = cmdAcciones.ExecuteReader();
             
            while (drAcciones.Read())
            {
                Entidades.Seguridad.Accion oAccion = new Entidades.Seguridad.Accion();
                oAccion.IdAccion = Convert.ToInt32(drAcciones["idAccion"]);
                oAccion.NombreAccion = drAcciones["nombreAccion"].ToString();
                oAccion.DescripcionAccion = drAcciones["descripcion"].ToString();
                 
                ColAcciones.Add(oAccion);

            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return ColAcciones;
        }

        public static List<Entidades.Seguridad.Accion> RecuperarAcciones()
        {
            List<Entidades.Seguridad.Accion> ColAcciones = new List<Entidades.Seguridad.Accion>();

            SqlCommand cmdAcciones = new SqlCommand();
            cmdAcciones.CommandText = "sp_RecuperarAcciones";
            cmdAcciones.CommandType = System.Data.CommandType.StoredProcedure; 
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drAcciones = cmdAcciones.ExecuteReader();

            while (drAcciones.Read())
            {
                Entidades.Seguridad.Accion oAccion = new Entidades.Seguridad.Accion();
                oAccion.IdAccion = Convert.ToInt32(drAcciones["idAccion"]);
                oAccion.NombreAccion = drAcciones["nombreAccion"].ToString();
                oAccion.DescripcionAccion = drAcciones["descripcion"].ToString();

                ColAcciones.Add(oAccion);

            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return ColAcciones;
        }

        public static void AsignarAccionesAPerfil(Entidades.Seguridad.Perfil oPerfil, List<Entidades.Seguridad.Accion> accionesAAgregar)
        {
            //ELIMINAR ACCIONES YA ASIGNADAS Y ASIGNAR NUEVAMENTE LAS ACCIONES DEL PERFIL 15/08
            //Eliminar acciones de perfil
            SqlCommand cmdEliminarAcciones = new SqlCommand();
            cmdEliminarAcciones.CommandText = "sp_EliminarAccionesPerfil";
            cmdEliminarAcciones.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdEliminarAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdEliminarAcciones.Parameters.Add("@IdPerfil", System.Data.SqlDbType.Int).Value = oPerfil.IdPerfil;
            int rta = cmdEliminarAcciones.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();

            //Agregar acciones
            foreach(Entidades.Seguridad.Accion accionAAgregar in accionesAAgregar)
            {
                SqlCommand cmdAgregarAcciones = new SqlCommand();
                cmdAgregarAcciones.CommandText = "sp_AgregarAccion";
                cmdAgregarAcciones.CommandType = System.Data.CommandType.StoredProcedure;
                Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
                cmdAgregarAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmdAgregarAcciones.Parameters.Add("@IdPerfil", System.Data.SqlDbType.Int).Value = oPerfil.IdPerfil;
                cmdAgregarAcciones.Parameters.Add("@IdAccion", System.Data.SqlDbType.Int).Value = accionAAgregar.IdAccion;
                cmdAgregarAcciones.ExecuteNonQuery();
                Servicios.Conexion.ObtenerInstancia().Desconectar();

            }
           

        }
    }
}
