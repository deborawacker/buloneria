﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Seguridad
{
    public static class MappingPerfiles
    {
        public static List<Entidades.Seguridad.Perfil> RecuperarPerfiles()
        {
            SqlCommand cmdPerfiles = new SqlCommand();
            cmdPerfiles.CommandText = "sp_ConsultarPerfiles";
            cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drPerfiles = cmdPerfiles.ExecuteReader();

            List<Entidades.Seguridad.Perfil> ColPerfiles = new List<Entidades.Seguridad.Perfil>();

            while (drPerfiles.Read())
            {
                Entidades.Seguridad.Perfil oPerfil = new Entidades.Seguridad.Perfil();
                oPerfil.IdPerfil = Convert.ToInt32(drPerfiles["idPerfil"]);
                oPerfil.NombrePerfil = drPerfiles["nombrePerfil"].ToString();
                oPerfil.DescripcionPerfil = drPerfiles["descripcion"].ToString();

                List<Entidades.Seguridad.Accion> ColAcciones = new List<Entidades.Seguridad.Accion>();

                SqlCommand cmdAcciones = new SqlCommand();
                cmdAcciones.CommandText = "sp_ConsultarAccionesPerfiles";
                cmdAcciones.CommandType = System.Data.CommandType.StoredProcedure;
                cmdAcciones.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = oPerfil.IdPerfil;
                cmdAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drAcciones = cmdAcciones.ExecuteReader();

                while (drAcciones.Read())
                {
                    Entidades.Seguridad.Accion oAccion = new Entidades.Seguridad.Accion();
                    oAccion.IdAccion = Convert.ToInt32(drAcciones["idAccion"]);
                    oAccion.NombreAccion = drAcciones["nombreAccion"].ToString();
                    oAccion.DescripcionAccion = drAcciones["descripcion"].ToString();

                    ColAcciones.Add(oAccion);

                }
                oPerfil.ColAcciones = ColAcciones;

                ColPerfiles.Add(oPerfil);

            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return ColPerfiles;
        }

        public static List<Entidades.Seguridad.Perfil> RecuperarPerfilesUsuario(int idUsuario)
        {
            SqlCommand cmdPerfiles = new SqlCommand();
            cmdPerfiles.CommandText = "sp_ConsultarPerfilesUsuarios";
            cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
            cmdPerfiles.Parameters.Add("@idUsuario", System.Data.SqlDbType.Int).Value = idUsuario;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drPerfiles = cmdPerfiles.ExecuteReader();

            List<Entidades.Seguridad.Perfil> ColPerfiles = new List<Entidades.Seguridad.Perfil>();

            while (drPerfiles.Read())
            {
                Entidades.Seguridad.Perfil oPerfil = new Entidades.Seguridad.Perfil();
                oPerfil.IdPerfil = Convert.ToInt32(drPerfiles["idPerfil"]);
                oPerfil.NombrePerfil = drPerfiles["nombrePerfil"].ToString();
                oPerfil.DescripcionPerfil = drPerfiles["descripcion"].ToString();


                oPerfil.ColAcciones = MappingAcciones.RecuperarAccionesPerfiles(oPerfil);

                ColPerfiles.Add(oPerfil);

            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return ColPerfiles;
        }

        /*public static bool AgregarPerfil(Entidades.Seguridad.Perfil oPerfil)
        {
            SqlCommand cmdPerfiles = new SqlCommand();
            cmdPerfiles.CommandText = "sp_AgregarPerfil";
            cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdPerfiles.Parameters.Add("@NombreGrupo", System.Data.SqlDbType.VarChar, 30).Value = oPerfil.Grupo.Nombre;
            cmdPerfiles.Parameters.Add("@NombreFormulario", System.Data.SqlDbType.VarChar, 150).Value = oPerfil.Formulario.Nombre;
            cmdPerfiles.Parameters.Add("@TipoPermiso", System.Data.SqlDbType.VarChar, 20).Value = oPerfil.Permiso.Tipo;
            int rta = cmdPerfiles.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (rta >= 1) return true; else return false;
        }*/
       /* public static bool EliminarPerfil(Entidades.Seguridad.Perfil oPerfil)
        {
            SqlCommand cmdPerfiles = new SqlCommand();
            cmdPerfiles.CommandText = "sp_EliminarPerfil";
            cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdPerfiles.Parameters.Add("@NombreGrupo", System.Data.SqlDbType.VarChar, 30).Value = oPerfil.Grupo.Nombre;
            cmdPerfiles.Parameters.Add("@NombreFormulario", System.Data.SqlDbType.VarChar, 150).Value = oPerfil.Formulario.Nombre;
            cmdPerfiles.Parameters.Add("@TipoPermiso", System.Data.SqlDbType.VarChar, 20).Value = oPerfil.Permiso.Tipo;
            int rta = cmdPerfiles.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (rta >= 1) return true; else return false;
        }*/

       
    }
}
