﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
namespace Mapping.Seguridad
{
    public static class MappingUsuarios
    {
        public static List<Entidades.Seguridad.Usuario> RecuperarUsuarios()
        {
            List<Entidades.Seguridad.Usuario> ColUsuarios = new List<Entidades.Seguridad.Usuario>(); 

            SqlCommand cmdUsuarios = new SqlCommand();
            cmdUsuarios.CommandText = "sp_ConsultarUsuarios";
            cmdUsuarios.CommandType = System.Data.CommandType.StoredProcedure;
            cmdUsuarios.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            SqlDataReader drUsuarios = cmdUsuarios.ExecuteReader();

            while (drUsuarios.Read())
            {
                
                Entidades.Seguridad.Usuario oUsuario = new Entidades.Seguridad.Usuario();
                oUsuario.NombreYApellido = drUsuarios["nombreApellido"].ToString();
                oUsuario.Clave = drUsuarios["clave"].ToString();
                //oUsuario.CambiarContraseña(null,drUsuarios["Contraseña"].ToString());
                oUsuario.Email = drUsuarios["email"].ToString();
                oUsuario.IdUsuario =  Convert.ToInt32(drUsuarios["idUsuario"]);
                oUsuario.NombreUsuario = drUsuarios["nombreUsuario"].ToString();

                SqlCommand cmdPerfiles = new SqlCommand();
                cmdPerfiles.CommandText = "sp_ConsultarPerfilesUsuarios";
                cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
                cmdPerfiles.Parameters.Add("@idUsuario", System.Data.SqlDbType.Int).Value = oUsuario.IdUsuario;
                cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                SqlDataReader drPerfiles = cmdPerfiles.ExecuteReader();

                List<Entidades.Seguridad.Perfil> ColPerfiles = new List<Entidades.Seguridad.Perfil>();

                while (drPerfiles.Read())
                {
                    Entidades.Seguridad.Perfil oPerfil = new Entidades.Seguridad.Perfil();
                    oPerfil.IdPerfil = Convert.ToInt32(drPerfiles["idPerfil"]);
                    oPerfil.NombrePerfil = drPerfiles["nombrePerfil"].ToString();
                    oPerfil.DescripcionPerfil = drPerfiles["descripcion"].ToString();

                    List<Entidades.Seguridad.Accion> ColAcciones = new List<Entidades.Seguridad.Accion>();

                    SqlCommand cmdAcciones = new SqlCommand();
                    cmdAcciones.CommandText = "sp_ConsultarAccionesPerfiles";
                    cmdAcciones.CommandType = System.Data.CommandType.StoredProcedure;
                    cmdAcciones.Parameters.Add("@idPerfil", System.Data.SqlDbType.Int).Value = oPerfil.IdPerfil;
                    cmdAcciones.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                    SqlDataReader drAcciones = cmdAcciones.ExecuteReader();

                    while (drAcciones.Read())
                    {
                        Entidades.Seguridad.Accion oAccion = new Entidades.Seguridad.Accion();
                        oAccion.IdAccion = Convert.ToInt32(drAcciones["idAccion"]);
                        oAccion.NombreAccion = drAcciones["nombreAccion"].ToString();
                        oAccion.DescripcionAccion = drAcciones["descripcion"].ToString();

                        ColAcciones.Add(oAccion);

                    }
                    oPerfil.ColAcciones=ColAcciones;

                    ColPerfiles.Add(oPerfil);

                }
                oUsuario.ColPerfiles = ColPerfiles;

                ColUsuarios.Add(oUsuario);
            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return ColUsuarios;
        }

        public static bool GuardarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            SqlCommand cmdUsuario = new SqlCommand();
            cmdUsuario.CommandText = "sp_AgregarUsuario";
            cmdUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad"); 
            cmdUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();  
            cmdUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 50).Value = oUsuario.NombreUsuario;
            cmdUsuario.Parameters.Add("@Clave", System.Data.SqlDbType.VarChar, 20).Value = oUsuario.Clave;
            cmdUsuario.Parameters.Add("@NombreApellido", System.Data.SqlDbType.VarChar, 50).Value = oUsuario.NombreYApellido;
            cmdUsuario.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 50).Value = oUsuario.Email;
            int idAutogenerado = Convert.ToInt32(cmdUsuario.ExecuteScalar());
            oUsuario.IdUsuario = idAutogenerado;
             
            List<Entidades.Seguridad.Perfil> _colPerfiles = oUsuario.ColPerfiles;

            foreach (Entidades.Seguridad.Perfil perfil in _colPerfiles)
            {
                SqlCommand cmdPerfiles = new SqlCommand();
                cmdPerfiles.CommandText = "sp_AgregarPerfilUsuario";
                cmdPerfiles.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmdPerfiles.CommandType = System.Data.CommandType.StoredProcedure;
                cmdPerfiles.Parameters.Add("@IdUsuario", System.Data.SqlDbType.Int).Value = oUsuario.IdUsuario;
                cmdPerfiles.Parameters.Add("@IdPerfil", System.Data.SqlDbType.Int).Value = perfil.IdPerfil;
                 
                SqlDataReader drPerfiles = cmdPerfiles.ExecuteReader(); 
            
            }

            Servicios.Conexion.ObtenerInstancia().Desconectar();


            
            if (idAutogenerado != 0)
            {
                return true;
            }
            else return false;
        }

       public static bool ModificarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            SqlCommand cmdUsuario = new SqlCommand();
            cmdUsuario.CommandText = "sp_CambiarClaveUsuario";
            cmdUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdUsuario.Parameters.Add("@IdUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.IdUsuario; 
            cmdUsuario.Parameters.Add("@Clave", System.Data.SqlDbType.VarChar, 100).Value=oUsuario.Clave;
          
            int Rta = cmdUsuario.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (Rta != 0)
            {
                return true;
            }
            else return false;
        }

        public static bool EliminarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            SqlCommand cmdUsuario = new SqlCommand();
            cmdUsuario.CommandText = "sp_EliminarUsuario";
            cmdUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.NombreUsuario;
            int Rta = cmdUsuario.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (Rta != 0)
            {
                return true;
            }
            else return false;
        }

        public static bool HabilitarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            SqlCommand cmdUsuario = new SqlCommand();
            cmdUsuario.CommandText = "sp_HabilitarUsuario";
            cmdUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.NombreUsuario;
            int Rta = cmdUsuario.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (Rta != 0)
            {
                return true;
            }
            else return false;
        }

        public static bool InhabilitarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            SqlCommand cmdUsuario = new SqlCommand();
            cmdUsuario.CommandText = "sp_InhabilitarUsuario";
            cmdUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Seguridad");
            cmdUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.NombreUsuario;
            int Rta = cmdUsuario.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            if (Rta != 0)
            {
                return true;
            }
            else return false;
        }
        /*private static bool AgregarGruposUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            SqlCommand cmdGrupoUsuario=new SqlCommand();
            cmdGrupoUsuario.CommandText="sp_AgregarGrupoUsuario";
            cmdGrupoUsuario.CommandType= System.Data.CommandType.StoredProcedure;
            cmdGrupoUsuario.Connection=Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            int rta=0;
            foreach(Entidades.Seguridad.Grupo oGrupo in oUsuario.Grupos)
            {
                cmdGrupoUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.NombreUsuario;
                cmdGrupoUsuario.Parameters.Add("@NombreGrupo", System.Data.SqlDbType.VarChar, 30).Value = oGrupo.Nombre;
                rta = rta+ cmdGrupoUsuario.ExecuteNonQuery();
                cmdGrupoUsuario.Parameters.Clear();
            }
            
            if (rta != 0) return true; else return false;
        }*/
        private static void EliminarGrupoUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            SqlCommand cmdElimGrupoUsuario = new SqlCommand();
            cmdElimGrupoUsuario.CommandText = "sp_EliminarGruposUsuario";
            cmdElimGrupoUsuario.CommandType = System.Data.CommandType.StoredProcedure;
            cmdElimGrupoUsuario.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmdElimGrupoUsuario.Parameters.Add("@NombreUsuario", System.Data.SqlDbType.VarChar, 30).Value = oUsuario.NombreUsuario;
            cmdElimGrupoUsuario.ExecuteNonQuery();
        }
    }
}



/*
 [sp_AgregarUsuario] 
(@NombreUsuario varchar(30), 
@Clave varchar(100), 
@NombreApellido varchar(50), 
@Email varchar(50))
as 
insert into usuarios (nombreUsuario, clave, nombreApellido, email)
values(@NombreUsuario, @Clave, @NombreApellido, @Email)
 
  
 
 CREATE PROCEDURE sp_CambiarClaveUsuario
@IdUsuario int,
@Clave varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE usuarios 
	SET clave=@Clave
	WHERE idUsuario=@IdUsuario
END
 */