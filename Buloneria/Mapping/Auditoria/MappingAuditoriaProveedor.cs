﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace Mapping.Auditoria
{
    public static class MappingAuditoriaProveedor
    {
        public static List<Entidades.Auditoria.Proveedor> RecuperarAuditoriaProveedor()
        {
            List<Entidades.Auditoria.Proveedor> _DatosAuditoriaProveedor = new List<Entidades.Auditoria.Proveedor>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_ConsultarRegistrosProveedor";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drAuditProveedor = cmd.ExecuteReader();
            while (drAuditProveedor.Read())
            {
                Entidades.Auditoria.Proveedor oProveedor = new Entidades.Auditoria.Proveedor();
                 
                oProveedor.Celular = drAuditProveedor["celular"].ToString();  
                oProveedor.CodPostal = Convert.ToInt32(drAuditProveedor["codPostal"]);  
                oProveedor.CodProveedor = Convert.ToInt32(drAuditProveedor["codProv"]);
                oProveedor.Cuit = Convert.ToInt64(drAuditProveedor["cuit"]);
                oProveedor.Direccion = drAuditProveedor["direccion"].ToString();
                oProveedor.Email = drAuditProveedor["email"].ToString();
                oProveedor.Fax = drAuditProveedor["fax"].ToString();
                oProveedor.NombreFantasia = drAuditProveedor["nomFantasia"].ToString();
                oProveedor.RazonSocial = drAuditProveedor["razonSocial"].ToString();
                oProveedor.Telefono = drAuditProveedor["telefono"].ToString();
                oProveedor.TipoIva = drAuditProveedor["tipoIva"].ToString();
                oProveedor.Ciudad = Convert.ToInt32(drAuditProveedor["ciudad"]);
                oProveedor.FechaHora = Convert.ToDateTime(drAuditProveedor["fecha"]);
                oProveedor.Accion = drAuditProveedor["accion"].ToString();
                oProveedor.Usuario = drAuditProveedor["usuario"].ToString();
                _DatosAuditoriaProveedor.Add(oProveedor);
            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return _DatosAuditoriaProveedor;
        }
        public static void AgregarRegistro(Entidades.Auditoria.Proveedor oProvAuditoria)
        {
            try
            {
                Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_RegistrarProveedor";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
                cmd.Parameters.Add("@CodProveedor", System.Data.SqlDbType.Int).Value = oProvAuditoria.CodProveedor;
                cmd.Parameters.Add("@Cuit", System.Data.SqlDbType.BigInt).Value = oProvAuditoria.Cuit;
                cmd.Parameters.Add("@RazonSocial", System.Data.SqlDbType.VarChar, 50).Value = oProvAuditoria.RazonSocial;
                cmd.Parameters.Add("@NombreFantasia", System.Data.SqlDbType.VarChar, 50).Value =
                    oProvAuditoria.NombreFantasia;
                cmd.Parameters.Add("@Direccion", System.Data.SqlDbType.VarChar, 100).Value = oProvAuditoria.Direccion;
                cmd.Parameters.Add("@Ciudad", System.Data.SqlDbType.Int).Value = oProvAuditoria.Ciudad;
                cmd.Parameters.Add("@CodPostal", System.Data.SqlDbType.Int).Value = oProvAuditoria.CodPostal;
                cmd.Parameters.Add("@Telefono", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.Telefono;
                cmd.Parameters.Add("@Fax", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.Fax;
                cmd.Parameters.Add("@Celular", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.Celular;
                cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100).Value = oProvAuditoria.Email;
                cmd.Parameters.Add("@TipoIva", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.TipoIva;
                cmd.Parameters.Add("@Fecha", System.Data.SqlDbType.DateTime).Value = oProvAuditoria.FechaHora;
                cmd.Parameters.Add("@Accion", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.Accion;
                cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar, 20).Value = oProvAuditoria.Usuario;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
            }
        }
    }
}
