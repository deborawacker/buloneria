﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Auditoria
{
    public class MappingAuditoriaOC
    {
        public static List<Entidades.Auditoria.AuditoriaOrdenCompra> RecuperarAuditoriaOC()
        {
            List<Entidades.Auditoria.AuditoriaOrdenCompra> _AuditoriaOC = new List<Entidades.Auditoria.AuditoriaOrdenCompra>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_ConsultarRegistrosOC";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drAuOC = cmd.ExecuteReader();
            while (drAuOC.Read())
            {
                Entidades.Auditoria.AuditoriaOrdenCompra oAuditoriaOrdenCompra = new Entidades.Auditoria.AuditoriaOrdenCompra();
               
                oAuditoriaOrdenCompra.Usuario = drAuOC["usuario"].ToString();

                if (drAuOC["estado"].ToString() == Entidades.Sistema.EstadoOrdenCompra.FinConFalt.ToString())
                    oAuditoriaOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.FinConFalt;
                if (drAuOC["estado"].ToString() == Entidades.Sistema.EstadoOrdenCompra.Finalizado.ToString())
                    oAuditoriaOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.Finalizado;
                if (drAuOC["estado"].ToString() == Entidades.Sistema.EstadoOrdenCompra.EnCurso.ToString())
                    oAuditoriaOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.EnCurso;
                if (drAuOC["estado"].ToString() == Entidades.Sistema.EstadoOrdenCompra.Pendiente.ToString())
                    oAuditoriaOrdenCompra.Estado = Entidades.Sistema.EstadoOrdenCompra.Pendiente;
                
                oAuditoriaOrdenCompra.NroOrdenCompra = Convert.ToInt32(drAuOC["nroOrdenCompra"]);
                oAuditoriaOrdenCompra.FechaRegistro = Convert.ToDateTime(drAuOC["fechaRegistro"]);
                _AuditoriaOC.Add(oAuditoriaOrdenCompra);
                
            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return _AuditoriaOC;
        }
        public static void AgregarRegistro(Entidades.Auditoria.AuditoriaOrdenCompra oAuditoria)
        {
            Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_RegistrarCambioEstadoOrdenCompra";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmd.Parameters.Add("@FechaHora", System.Data.SqlDbType.DateTime).Value = oAuditoria.FechaRegistro;
            cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar, 50).Value = oAuditoria.Usuario;
            cmd.Parameters.Add("@nroOC", System.Data.SqlDbType.Int).Value = oAuditoria.NroOrdenCompra;
            cmd.Parameters.Add("@estado", System.Data.SqlDbType.VarChar, 10).Value = oAuditoria.Estado;
            cmd.ExecuteNonQuery();
            Servicios.Conexion.ObtenerInstancia().Desconectar();            
        }
    }
}
