﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Mapping.Auditoria
{
    public static class MappingAuditoriaLogInOut
    {
        public static List<Entidades.Auditoria.LogInOut> RecuperarLogInOut()
        {
            List<Entidades.Auditoria.LogInOut> _LogInOut = new List<Entidades.Auditoria.LogInOut>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_ConsultarRegistros";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            SqlDataReader drAuLogInOut = cmd.ExecuteReader();
            while (drAuLogInOut.Read())
            {
                Entidades.Auditoria.LogInOut oLogInOut = new Entidades.Auditoria.LogInOut();
                Entidades.Seguridad.Usuario oUsuario = new Entidades.Seguridad.Usuario();
                oUsuario.NombreUsuario = drAuLogInOut["Usuario"].ToString();
                oLogInOut.Usuario = oUsuario.NombreUsuario;
                if (drAuLogInOut["Operacion"].ToString() == Entidades.Auditoria.TipoInicioSesion.Ingreso.ToString())
                    oLogInOut.Operacion = Entidades.Auditoria.TipoInicioSesion.Ingreso;
                else oLogInOut.Operacion = Entidades.Auditoria.TipoInicioSesion.Egreso;
                oLogInOut.NombreEquipo = drAuLogInOut["Equipo"].ToString();
                oLogInOut.FechaHora = Convert.ToDateTime(drAuLogInOut["FechaHora"]);
                _LogInOut.Add(oLogInOut);
            }
            Servicios.Conexion.ObtenerInstancia().Desconectar();
            return _LogInOut;
        }
        public static bool AgregarRegistro(Entidades.Auditoria.LogInOut oAuditoria)
        {
            Servicios.Conexion.ObtenerInstancia().Conectar("Auditoria");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_RegistrarLogInOut";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Connection = Servicios.Conexion.ObtenerInstancia().RetornarConexion();
            cmd.Parameters.Add("@FechaHora", System.Data.SqlDbType.DateTime).Value = oAuditoria.FechaHora;
            cmd.Parameters.Add("@Equipo", System.Data.SqlDbType.VarChar, 50).Value = oAuditoria.NombreEquipo;
            cmd.Parameters.Add("@Operacion", System.Data.SqlDbType.VarChar, 50).Value = oAuditoria.Operacion;
            cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar, 50).Value = oAuditoria.Usuario;
            if (cmd.ExecuteNonQuery() != -1)
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
                return true;
            }
            else
            {
                Servicios.Conexion.ObtenerInstancia().Desconectar();
                return false;
            }
         }
    }
}
