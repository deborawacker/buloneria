﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;

namespace Modelo.Auditoria
{
    public class CatalogoAuditoriaProveedor
    {
        public List<Proveedor> ColeccionAuditoriaProveedor { get; set; } 

        #region Singleton
        private static CatalogoAuditoriaProveedor _Instancia;

        public static CatalogoAuditoriaProveedor ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoAuditoriaProveedor();
            return _Instancia;
        }

        private CatalogoAuditoriaProveedor()
        {

        } 
        #endregion

        public List<Proveedor> RecuperarAuditoriaProveedor()
        {
            return Mapping.Auditoria.MappingAuditoriaProveedor.RecuperarAuditoriaProveedor();
        }

        public void AgregarRegistro(Proveedor oAuditoriaProveedor)
        {
            Mapping.Auditoria.MappingAuditoriaProveedor.AgregarRegistro(oAuditoriaProveedor);
        } 

        public IEnumerable<Proveedor> RecuperarListadoPorCriterio(ProveedorCriterio criterio)
        {
            IEnumerable<Proveedor> colAuditProv = this.RecuperarAuditoriaProveedor();

            if (!string.IsNullOrEmpty(criterio.Usuario))
            {
                colAuditProv = colAuditProv.Where(loc => loc.Usuario.ToUpper().Contains(criterio.Usuario.ToUpper()));
            }

            if (!string.IsNullOrEmpty(criterio.RazonSocial))
            {
                colAuditProv = colAuditProv.Where(loc => loc.RazonSocial.ToUpper().Contains(criterio.RazonSocial.ToUpper()));
            }

            return colAuditProv;
        }
    }
}
