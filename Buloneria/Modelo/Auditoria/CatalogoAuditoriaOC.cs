﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Auditoria;

namespace Modelo.Auditoria
{
    public class CatalogoAuditoriaOC
    {

        #region Singleton
        private static CatalogoAuditoriaOC _Instancia;

        public static CatalogoAuditoriaOC ObtenerInstancia()
        {
            return _Instancia ?? (_Instancia = new CatalogoAuditoriaOC());
        }

        private CatalogoAuditoriaOC()
        {
            ColeccionAuditoriaOC = this.RecuperarAuditoriaOC();
        } 
        #endregion

        private List<AuditoriaOrdenCompra> ColeccionAuditoriaOC { get; set; }

        public void AgregarRegistro(AuditoriaOrdenCompra oAuditoriaOrdenCompra)
        {
            Mapping.Auditoria.MappingAuditoriaOC.AgregarRegistro(oAuditoriaOrdenCompra);
        }

        public List<AuditoriaOrdenCompra> RecuperarAuditoriaOC()
        {
            return Mapping.Auditoria.MappingAuditoriaOC.RecuperarAuditoriaOC();
        }
         
        public void ActualizarColeccion()
        {
            _Instancia = null;
            _Instancia = new CatalogoAuditoriaOC();
        }

        public List<AuditoriaOrdenCompra> RecuperarHistorialOC(int nroOC)
        {
            return this.ColeccionAuditoriaOC.Where(auoc => auoc.NroOrdenCompra == nroOC).ToList();
        }

        public IEnumerable<AuditoriaOrdenCompra> RecuperarListadoPorCriterio(AuditoriaOrdenCompraCriterio criterio)
        {
            IEnumerable<AuditoriaOrdenCompra> colAuditOC = this.RecuperarAuditoriaOC();

            if (!string.IsNullOrEmpty(criterio.Usuario))
            {
                colAuditOC = colAuditOC.Where(loc => loc.Usuario.Contains(criterio.Usuario));
            }

            if (!string.IsNullOrEmpty(criterio.NroOrdenCompra))
            {
                colAuditOC = colAuditOC.Where(loc => loc.NroOrdenCompra.ToString().Contains(criterio.NroOrdenCompra));
            }

            return colAuditOC;

        }
    }
}
