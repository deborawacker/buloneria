﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Entidades.Auditoria;

namespace Modelo.Auditoria
{
    public class CatalogoAuditoriaLogInOut
    {

        #region Singleton
        private static CatalogoAuditoriaLogInOut _Instancia;

        public static CatalogoAuditoriaLogInOut ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new CatalogoAuditoriaLogInOut();
            return _Instancia;
        }
        #endregion

        #region Properties

        public List<LogInOut> ColeccionLogInOut { get; set; }

        #endregion

        #region Constructor
        private CatalogoAuditoriaLogInOut()
        {
            ColeccionLogInOut = this.RecuperarLogInOut();
        }
        #endregion

        public void AgregarRegistro(LogInOut oAuditoria)
        {
            if (Mapping.Auditoria.MappingAuditoriaLogInOut.AgregarRegistro(oAuditoria))
            {
                ColeccionLogInOut.Add(oAuditoria);
            }
        }

        public List<LogInOut> RecuperarLogInOut()
        {
            return Mapping.Auditoria.MappingAuditoriaLogInOut.RecuperarLogInOut();
        }


        public void ActualizarColeccion()
        {
            _Instancia = null;
            _Instancia = new CatalogoAuditoriaLogInOut();
        }

        public IEnumerable<LogInOut> RecuperarLogInOutPorCriterio(LoginOutCriterio criterio)
        {
            IEnumerable<LogInOut> colLoginOut = this.RecuperarLogInOut();

            if (!string.IsNullOrEmpty(criterio.Usuario))
            {
                colLoginOut = colLoginOut.Where(lo => lo.Usuario.Contains(criterio.Usuario));
            }


            if (!criterio.TraerIngresos)
            {
                colLoginOut = colLoginOut.Where(lo => lo.Operacion.Equals(TipoInicioSesion.Egreso));
            }

            if (!criterio.TraerEgresos)
            {
                colLoginOut = colLoginOut.Where(lo => lo.Operacion.Equals(TipoInicioSesion.Ingreso));
            }

            return colLoginOut;

            //this.RecuperarLogInOut()
            //    .Where(
            //        lo => lo.Usuario.Contains(criterio.Usuario)
            //            && (criterio.TraerIngresos && lo.Operacion.Equals(TipoInicioSesion.Ingreso))
            //            && (criterio.TraerEgresos && lo.Operacion.Equals(TipoInicioSesion.Egreso))
            //          ).ToList();
        }
    }
}
