﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Seguridad
{
    public class CatalogoAcciones
    {
         private static CatalogoAcciones Instancia = null;
        private List<Entidades.Seguridad.Accion> ColeccionAcciones = null;

        private CatalogoAcciones()
        {
            if (ColeccionAcciones == null)
            {
                ColeccionAcciones = Mapping.Seguridad.MappingAcciones.RecuperarAcciones();
            }
        }
        public List<Entidades.Seguridad.Accion> RecuperarAcciones()
        {
            return ColeccionAcciones;
        }

        public static CatalogoAcciones ObtenerInstancia()
        {
            if (Instancia == null) Instancia = new CatalogoAcciones();
            return Instancia;       
        }
        public void AsignarAccionesAPerfil(Entidades.Seguridad.Perfil perfil, List<Entidades.Seguridad.Accion> accionesAAgregar)
        {
            //Primero actualizo a nivel objeto y luego a nivel base de datos
            perfil.ColAcciones = accionesAAgregar;
            Mapping.Seguridad.MappingAcciones.AsignarAccionesAPerfil(perfil, accionesAAgregar);
        }
    }
}
