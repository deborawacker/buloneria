﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Modelo.Seguridad
{   //Facade de Seguridad
    public static class Facade
    {
        public static bool Agregar(object objeto)
        {
            bool Rta;
            switch (objeto.GetType().Name)
            {
                case "Usuario":
                    Entidades.Seguridad.Usuario usuario = (Entidades.Seguridad.Usuario)objeto;
                    Rta = CatalogoUsuarios.ObtenerInstancia().GuardarUsuario(usuario);
                    break;
                /*case "Grupo":
                    Entidades.Seguridad.Grupo grupo = (Entidades.Seguridad.Grupo)objeto;
                    Rta = CatalogoGrupos.ObtenerInstancia().AgregarGrupo(grupo);
                    break;*/
                case "Perfil":
                    Entidades.Seguridad.Perfil perfil = (Entidades.Seguridad.Perfil)objeto;
                    Rta = CatalogoPerfiles.ObtenerInstancia().AgregarPerfil(perfil);
                    break;
                default:
                    Rta = false;
                    break;
            }
            return Rta;
        }

        public static bool Modificar(object objeto)
        {
            bool Rta;
            switch (objeto.GetType().Name)
            {
                case "Usuario":
                    Entidades.Seguridad.Usuario usuario = (Entidades.Seguridad.Usuario)objeto;
                    Rta = CatalogoUsuarios.ObtenerInstancia().ModificarUsuario(usuario);
                    break;
                /*case "Grupo":
                    Entidades.Seguridad.Grupo grupo = (Entidades.Seguridad.Grupo)objeto;
                    Rta = CatalogoGrupos.ObtenerInstancia().ModificarGrupo(grupo);
                    break;*/
                default:
                    Rta = false;
                    break;
            }
            return Rta;
        }

        public static bool Eliminar(object objeto)
        {
            bool Rta;
            switch (objeto.GetType().Name)
            {
                case "Usuario":
                    Entidades.Seguridad.Usuario usuario = (Entidades.Seguridad.Usuario)objeto;
                    Rta = CatalogoUsuarios.ObtenerInstancia().EliminarUsuario(usuario);
                    break;
                /*case "Grupo":
                    Entidades.Seguridad.Grupo grupo = (Entidades.Seguridad.Grupo)objeto;
                    Rta = CatalogoGrupos.ObtenerInstancia().EliminarGrupo(grupo);
                    break;*/
                case "Perfil":
                    Entidades.Seguridad.Perfil perfil = (Entidades.Seguridad.Perfil)objeto;
                    Rta = CatalogoPerfiles.ObtenerInstancia().EliminarPerfil(perfil);
                    break;
                default:
                    Rta = false;
                    break;
            }
            return Rta;
        }

        public static object Recuperar(string parametro)
        {
            object Lista;
            switch (parametro) 
            {
                case "Usuarios":
                    Lista = CatalogoUsuarios.ObtenerInstancia().RecuperarUsuarios();
                    break;
                /*case "Grupos":
                    Lista = CatalogoGrupos.ObtenerInstancia().RecuperarGrupos();
                    break;*/
                case "Perfiles":
                    Lista = CatalogoPerfiles.ObtenerInstancia().RecuperarPerfiles();
                    break;
                /*case "Formularios":
                    Lista = CatalogoFormularios.ObtenerInstancia().RecuperarFormularios();
                    break;*/
                case "Acciones":
                    Lista = CatalogoAcciones.ObtenerInstancia().RecuperarAcciones();
                    break;
                default:
                    Lista = false;
                    break;
            }
            return Lista;
        }

        public static object Buscar(object objeto) 
        {
            
            switch (objeto.GetType().Name.ToString()) 
            {
                /*case "Grupo":
                    Entidades.Seguridad.Grupo grupo = (Entidades.Seguridad.Grupo)objeto;
                    return CatalogoGrupos.ObtenerInstancia().BuscarGrupo(grupo);*/
                    
                case "Perfil":
                    Entidades.Seguridad.Perfil perfil = (Entidades.Seguridad.Perfil)objeto;
                     return CatalogoPerfiles.ObtenerInstancia().BuscarPerfil(perfil);
                    
                case "Usuario":
                    Entidades.Seguridad.Usuario usuario = (Entidades.Seguridad.Usuario)objeto;
                    return CatalogoUsuarios.ObtenerInstancia().BuscarUsuario(usuario.NombreUsuario);
    
                default:
                    string ousuario = (string)objeto;
                    return CatalogoUsuarios.ObtenerInstancia().BuscarUsuario(ousuario);
    
                                        
            }            
        }
    }
}
