﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Modelo.Seguridad
{
    class CatalogoUsuarios
    {
        private static CatalogoUsuarios Instancia = null;
        //private List<Entidades.Seguridad.Usuario> _ColeccionUsuarios = null;
        public List<Entidades.Seguridad.Usuario> ColeccionUsuarios 
        {
            get
            {
                return Mapping.Seguridad.MappingUsuarios.RecuperarUsuarios();
            }
            //set
            //{ 
            
            //}
        }
        
        private CatalogoUsuarios()
        {
           /*if (ColeccionUsuarios == null)
            {
                ColeccionUsuarios = Mapping.Seguridad.MappingUsuarios.RecuperarUsuarios();
            }*/
        }


        public static CatalogoUsuarios ObtenerInstancia()
        {
            if (Instancia == null) Instancia = new CatalogoUsuarios();
            return Instancia;       
        }

        public Entidades.Seguridad.Usuario RecuperarUsuario(string usuario, string pass)
        {
            return ColeccionUsuarios.Find(delegate(Entidades.Seguridad.Usuario oUsu) { return oUsu.NombreUsuario == usuario && oUsu.Clave == pass; });
        }

        public ReadOnlyCollection<Entidades.Seguridad.Usuario> RecuperarUsuarios() 
        {
            return ColeccionUsuarios.AsReadOnly();
           // return ColeccionUsuarios.AsReadOnly();
        }

        public Entidades.Seguridad.Usuario BuscarUsuario(string oUsuario) 
        {
            return ColeccionUsuarios.Find(delegate(Entidades.Seguridad.Usuario usuario) { return usuario.NombreUsuario == oUsuario; });
        }

        public bool GuardarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            if (Mapping.Seguridad.MappingUsuarios.GuardarUsuario(oUsuario))
            {
                ColeccionUsuarios.Add(oUsuario);
                return true;
            }
            else return false;
        }

        public bool ModificarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            if (Mapping.Seguridad.MappingUsuarios.ModificarUsuario(oUsuario))
            {
                ColeccionUsuarios.Remove(ColeccionUsuarios.Find(delegate(Entidades.Seguridad.Usuario usuario) { return usuario.NombreUsuario == oUsuario.NombreUsuario; }));
                ColeccionUsuarios.Add(oUsuario);
                return true;
            }
            else
            {
                return false;
            }
              
        }

        public bool EliminarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            if(Mapping.Seguridad.MappingUsuarios.EliminarUsuario(oUsuario))
            {
                ColeccionUsuarios.Remove(oUsuario);
                return true;
            }
            else return false;
        }

        public bool HabilitarUsuario(Entidades.Seguridad.Usuario oUsuario) 
        {
            if (Mapping.Seguridad.MappingUsuarios.HabilitarUsuario(oUsuario))
            {
                ColeccionUsuarios.Remove(ColeccionUsuarios.Find(delegate(Entidades.Seguridad.Usuario usuario) { return usuario.NombreUsuario == oUsuario.NombreUsuario; }));
                ColeccionUsuarios.Add(oUsuario);
                return true;
            }
            else return false;
        }

        public bool InhabilitarUsuario(Entidades.Seguridad.Usuario oUsuario)
        {
            if (Mapping.Seguridad.MappingUsuarios.InhabilitarUsuario(oUsuario))
            {
                ColeccionUsuarios.Remove(ColeccionUsuarios.Find(delegate(Entidades.Seguridad.Usuario usuario) { return usuario.NombreUsuario == oUsuario.NombreUsuario; }));
                ColeccionUsuarios.Add(oUsuario);
                return true;
            }
            else return false;
        }

        public ReadOnlyCollection<Entidades.Seguridad.Usuario> Filtrar(string Parametro) 
        {
            return ColeccionUsuarios.FindAll(delegate(Entidades.Seguridad.Usuario usuario) { return usuario.NombreUsuario.ToLower().Contains(Parametro.ToLower()) || usuario.NombreYApellido.ToLower().Contains(Parametro.ToLower()); }).AsReadOnly();
        }
    }
}
