﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Modelo.Seguridad
{
    public class CatalogoPerfiles
    {
        private static CatalogoPerfiles _Instancia;
        private List<Entidades.Seguridad.Perfil> ColeccionPerfiles;
        public static CatalogoPerfiles ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoPerfiles();
            return _Instancia;
        }
        private CatalogoPerfiles()
        {
            if(ColeccionPerfiles == null) 
                ColeccionPerfiles = Mapping.Seguridad.MappingPerfiles.RecuperarPerfiles();
        }

        public bool AgregarPerfil(Entidades.Seguridad.Perfil perfil)
        {
           /* if(Mapping.Seguridad.MappingPerfiles.AgregarPerfil(perfil))
            {
                ColeccionPerfiles.Add(perfil);
                return true;
            }
            else*/ return false;
        }
        public bool EliminarPerfil(Entidades.Seguridad.Perfil perfil)
        {
            /*if (Mapping.Seguridad.MappingPerfiles.EliminarPerfil(perfil))
            {
                ColeccionPerfiles.Remove(perfil);
                return true;
            }
            else
            {*/
                return false;
            /*}*/
        }
        public bool BuscarPerfil(Entidades.Seguridad.Perfil perfil)
        {
            //if (ColeccionPerfiles.Find(delegate(Entidades.Seguridad.Perfil oPerfil) { return oPerfil.Permiso.Tipo == perfil.Permiso.Tipo && oPerfil.Grupo.Nombre == perfil.Grupo.Nombre && oPerfil.Formulario.Nombre == perfil.Formulario.Nombre; }) == null)
                return false;
            //else return true;
        }

        public List<Entidades.Seguridad.Perfil> RecuperarPerfiles()
        {
            return ColeccionPerfiles;
        }
        /*public ReadOnlyCollection<Entidades.Seguridad.Perfil> RecuperarPerfilesXGrupo(ReadOnlyCollection<Entidades.Seguridad.Grupo> Grupos)
        {
            List<Entidades.Seguridad.Perfil> _perfiles = new List<Entidades.Seguridad.Perfil>();
            foreach(Entidades.Seguridad.Grupo oGrupo in Grupos)
                {
                //agrega a la coleccion _perfiles los perfiles que se encuentran para el oGrupo///
                    _perfiles.AddRange(ColeccionPerfiles.FindAll(delegate(Entidades.Seguridad.Perfil oPerfil) { return oPerfil.Grupo.Nombre == oGrupo.Nombre; }));
                }
            return _perfiles.AsReadOnly();
        }*/

        
    }
}
