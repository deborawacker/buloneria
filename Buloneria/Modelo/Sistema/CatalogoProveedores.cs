﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoProveedores
    {
        #region Singleton

        private static CatalogoProveedores _Instancia; 

        private CatalogoProveedores()
        {
        }

        public static CatalogoProveedores ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new CatalogoProveedores();
            return _Instancia;
        }

        #endregion

        #region Members

        public List<Proveedor> ColeccionProveedores
        {
            get
            {
                return this.RecuperarProveedores();
            }
        }

        #endregion

        #region Methods

        public List<Proveedor> RecuperarProveedores()
        {
            return Mapping.Sistema.MappingProveedores.RecuperarProveedores();
        }

        public List<Proveedor> FiltrarProveedores(string criteriofiltrado)
        {
            string criterio = criteriofiltrado.ToLower();

            return ColeccionProveedores.FindAll(delegate(Proveedor proveedor) { return proveedor.CodProveedor.ToString().ToLower().Contains(criterio) || proveedor.RazonSocial.ToLower().Contains(criterio) || proveedor.Direccion.ToLower().Contains(criterio) || proveedor.Ciudad.Nombre.ToLower().Contains(criterio); });
        }

        public int AgregarProveedor(Proveedor proveedor)
        {
            proveedor.Validate();

            proveedor.CodProveedor = Mapping.Sistema.MappingProveedores.AgregarProveedor(proveedor);  

            return proveedor.CodProveedor;
        }

        public bool ModificarProveedor(Proveedor proveedor)
        {
            proveedor.Validate();

            return Mapping.Sistema.MappingProveedores.ModificarProveedor(proveedor);  
        }

        public bool EliminarProveedor(Proveedor proveedor)   
        {
            Mapping.Sistema.MappingProveedores.EliminarProveedor(proveedor);

            return ColeccionProveedores.Remove(proveedor);  
        }

        public bool ExisteCuit(Proveedor proveedor)
        {
            return ColeccionProveedores.Any(p => p.Cuit == proveedor.Cuit);
        }

        public Proveedor BuscarProveedor(int codProveedor)
        {
            return Mapping.Sistema.MappingProveedores.BuscarProveedor(codProveedor);
        }

        #endregion
    }
}
