﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema //dentro del modelo voy a crear los catalogos
{
    public class CatalogoProvincias
    {
        private List<Entidades.Sistema.Provincia> _ColeccionProvincias; //voy a crear un lista que se llama _ColeccionProvincias
        private static CatalogoProvincias _Instancia; //creo una instancia de _TipoMovimiento catalogoprovincias

        private CatalogoProvincias() //llamo al constructor y tiene que ser privado en singleton
        {
            _ColeccionProvincias = Mapping.Sistema.MappingProvincias.RecuperarProvincias(); //cuando lo crea voy a traer todas las provincias que recupere en el mapping, las trae a memoria
        }

        public static CatalogoProvincias ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoProvincias();
            return _Instancia;
                  
        }

        //metodo recuperar provincias
        public List<Entidades.Sistema.Provincia> RecuperarProvincias() //y ahora la coleccion esta en memoria, necesito que me la devuelva cuando me la pida la controladora
        {
            return _ColeccionProvincias; //aca devuelve la coleccion
        }
    }
}
