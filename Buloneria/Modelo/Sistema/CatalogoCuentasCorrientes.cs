﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoCuentasCorrientes
    {
        private List<Entidades.Sistema.CuentaCorriente> _ColeccionCtasCtes;
        private static CatalogoCuentasCorrientes _Instancia;
        
        //constructor
        private CatalogoCuentasCorrientes()  
        {
            _ColeccionCtasCtes = Mapping.Sistema.MappingCuentasCorrientes.RecuperarCuentasCorrientes();
        }

        public static CatalogoCuentasCorrientes ObtenerInstancia() 
        {
            if (_Instancia == null) _Instancia = new CatalogoCuentasCorrientes();
            return _Instancia; 
        }

        //metodo buscar cuenta corriente
        public Entidades.Sistema.CuentaCorriente BuscarCuentaCorriente(Entidades.Sistema.Cliente oCliente)
        {
            Entidades.Sistema.CuentaCorriente oCtaCte = _ColeccionCtasCtes.Find(cta => cta.Cliente.CodCliente == oCliente.CodCliente);            
            return oCtaCte;
        }

        //metodo modificar cuenta corriente
        public bool ModificarCuentaCorriente(Entidades.Sistema.CuentaCorriente oCtaCte)
        {
            Mapping.Sistema.MappingCuentasCorrientes.ModificarCuentaCorriente(oCtaCte);
            Entidades.Sistema.CuentaCorriente cuenta = _ColeccionCtasCtes.Find(cta => cta.NroCtaCte == oCtaCte.NroCtaCte);
            bool resultado = _ColeccionCtasCtes.Remove(cuenta);  //tengo que primero eliminar el objeto cuenta encontrado
            _ColeccionCtasCtes.Add(oCtaCte); //y luego agrego el objeto oCtaCte modificado (esto porque no existe el "update" como en sql)
            return resultado;
        }      
      
        //metodo recuperar cuentas corrientes
        public List<Entidades.Sistema.CuentaCorriente> RecuperarCuentasCorrientes() 
        {
            return _ColeccionCtasCtes;
        }
    }
}
