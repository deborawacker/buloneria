﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoGruposArticulos
    {
        private List<Entidades.Sistema.GrupoArticulo> _ColeccionGruposArticulos;
        private static CatalogoGruposArticulos _Instancia;

        //constructor
        private CatalogoGruposArticulos()  //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {
            _ColeccionGruposArticulos = Mapping.Sistema.MappingGrupoArticulos.RecuperarGruposArticulos(); //en la coleccion voy a llamar a la capa mapping y al metodo dentro del mapping recuperargruposarticulos()
        }

        public static CatalogoGruposArticulos ObtenerInstancia()
        { 
            if (_Instancia==null) _Instancia = new CatalogoGruposArticulos();
            return _Instancia;
        }

        //metodo recuperar grupos de articulos del modelo
        public List<Entidades.Sistema.GrupoArticulo> RecuperarGruposArticulos()
        {
            return _ColeccionGruposArticulos;
        }

        //metodo agregar grupos de articulos del modelo
        public void AgregarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoarticulo)
        {
            Mapping.Sistema.MappingGrupoArticulos.agregarGrupoArticulo(grupoarticulo);  //llamo al mapping y dentro del mapping al metodo agregargrupoarticulo, y le mando el grupoarticulo como parametro
            _ColeccionGruposArticulos.Add(grupoarticulo); //le agrego a la coleccion de grupos de articulos el grupoarticulo que le pase
        }

        //metodo eliminar grupos de articulos del modelo
        public bool EliminarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoarticulo)
        {
            Mapping.Sistema.MappingGrupoArticulos.eliminarGrupoArticulo(grupoarticulo);  //aca llamo al mapping, dentro del mapping al metodo eliminargrupoarticulo y le mando el grupoarticulo como parametro
            return _ColeccionGruposArticulos.Remove(grupoarticulo); //aca elimino de la coleccion el grupoarticulo que le pase
        }

        //metodo modificar grupos de articulos del modelo
        public bool ModificarGrupoArticulo(Entidades.Sistema.GrupoArticulo grupoarticulo)
        {
            Mapping.Sistema.MappingGrupoArticulos.modificarGrupoArticulo(grupoarticulo);
            bool resultado = _ColeccionGruposArticulos.Remove(grupoarticulo);
            _ColeccionGruposArticulos.Add(grupoarticulo);
            return resultado;
        }

        //metodo filtrar grupos de articulos del modelo
        public List<Entidades.Sistema.GrupoArticulo> FiltrarGruposArticulos(string criterio) 
        {
            string criterioFiltrado = criterio.ToLower();
            return _ColeccionGruposArticulos.FindAll(delegate(Entidades.Sistema.GrupoArticulo grupoarticulo) { return grupoarticulo.CodGrupoArticulo.ToString().ToLower().Contains(criterio) || grupoarticulo.Descripcion.ToLower().Contains(criterio); });            
        }

        //metodo buscar grupo articulo
        public bool BuscarGrupoArticulo(Entidades.Sistema.GrupoArticulo oGrupoArticulo)
        {
            foreach (Entidades.Sistema.GrupoArticulo grupoart in _ColeccionGruposArticulos)
            {
                if (grupoart.CodGrupoArticulo == oGrupoArticulo.CodGrupoArticulo) return true;
            }
            return false;
        }




    }
}
