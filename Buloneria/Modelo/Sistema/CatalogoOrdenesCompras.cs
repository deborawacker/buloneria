﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema;

namespace Modelo.Sistema
{
    public class CatalogoOrdenesCompras
    {

        #region Singleton
        private static CatalogoOrdenesCompras _Instancia;

        //constructor
        private CatalogoOrdenesCompras()  //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {

        }

        public static CatalogoOrdenesCompras ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoOrdenesCompras();  //si la instancia es null, que cree el objeto
            return _Instancia;  //y sino, no lo crea y devuelve el objeto existente      
        }
        #endregion

        public List<OrdenCompra> ColeccionOrdenesCompra
        {
            get
            {
                return Mapping.Sistema.MappingOrdenesCompras.RecuperarOrdenesCompras(); ;
            }

        }

        //metodo recuperar OC, recupera de la BD
        public List<OrdenCompra> RecuperarOrdenesCompras()
        {
            return Mapping.Sistema.MappingOrdenesCompras.RecuperarOrdenesCompras();
        }

        public List<OrdenCompra> FiltrarOrdenesCompras(string criteriofiltrado)
        {
            string criterio = criteriofiltrado.ToLower();
            return ColeccionOrdenesCompra.FindAll(
                orden =>
                    orden.NroOrdenCompra.ToString().ToLower().Contains(criterio) ||
                    orden.Proveedor.RazonSocial.ToLower().Contains(criterio) ||
                    orden.FechaEmision.ToShortDateString().Contains(criterio) ||
                    orden.Estado.ToString().ToLower().Contains(criterio.ToLower()));
        }

        public void CambiarEstadoOrdenCompra(OrdenCompra orden)
        {
            Mapping.Sistema.MappingOrdenesCompras.CambiarEstadoOrdenCompra(orden);
        }

        //metodo recuperar historial OC
        public List<OrdenCompra> RecuperarHistorial()
        {
            ColeccionOrdenesCompra.AddRange(Mapping.Sistema.MappingOrdenesCompras.RecuperarHistorial());
            return ColeccionOrdenesCompra;
        }

        //metodo agregar OC
        public int AgregarOrdenCompra(OrdenCompra orden)
        {
            int nroOrden = Mapping.Sistema.MappingOrdenesCompras.AgregarOrdenCompra(orden);
            orden.NroOrdenCompra = nroOrden;
            //ColeccionOrdenesCompra.Add(orden);
            return nroOrden;
        }

        public OrdenCompra BuscarOrdenCompra(int nroOrdenCompra)
        {
            return ColeccionOrdenesCompra.Find(delegate(OrdenCompra ordenCompra) { return ordenCompra.NroOrdenCompra == nroOrdenCompra; });
        }

        public void ActualizarIngresoMercaderia(OrdenCompra orden)
        {
            //Cambia el estado a la OC, actualiza la cantidad recibida de LOC y actualiza el stock de Articulos
            this.CambiarEstadoOrdenCompra(orden);
            this.ActualizarCantidadRecibidaLineasOrdenCompra(orden);
            this.ActualizarStockArticulos(orden);
        }

        private void ActualizarStockArticulos(OrdenCompra orden)
        {
            foreach (LineaOrdenCompra loc in orden.LineasOrdenCompra)
            {
                //Actualiza la cantidad actual de la linea
                loc.Articulo.CantActual += loc.CantIngresada;

                CatalogoArticulos.ObtenerInstancia().Update(loc.Articulo);
            }
        }

        public void ActualizarCantidadRecibidaLineasOrdenCompra(OrdenCompra orden)
        {
            Mapping.Sistema.MappingOrdenesCompras.ActualizarCantRecibidaLineasOrdenCompra(orden);
        }

        /// <summary>
        /// Busca una orden de compra por numero en una determinada lista de Ordenes de compra.
        /// Este método se utiliza para ejemplificar el uso del Patron Iterator
        /// </summary>
        /// <param name="nroOCBuscado"></param>
        /// <param name="_listaOC"></param>
        /// <returns></returns>
        private OrdenCompra BuscarOrdenCompraPorNroEn(int nroOCBuscado, List<OrdenCompra> _listaOC)
        {

            OrdenCompra ocBuscada = null;

            //Patron Iterator
            Collection collection = new Collection();

            int i = 0;
            foreach (object oc in _listaOC)
            {
                collection[i] = oc;
                i++;
            }

            Iterator iterator = new Iterator(collection);

            //Inicializacion, condicion, accion
            for (object item = iterator.First(); !iterator.HasNext; item = iterator.Next())
            {
                OrdenCompra oc = (OrdenCompra)item;
                if (oc.NroOrdenCompra == nroOCBuscado)
                {
                    ocBuscada = oc;
                }
            }

            return ocBuscada;
        }

        public OrdenCompra BuscarOrdenCompraPendientesOEnCursoPorNro(int nroOrdenCompra)
        {
            var ordenesComprasEnCursoYPendientes = this.RecuperarOrdenesComprasEnCursoYPendientes();

            return this.BuscarOrdenCompraPorNroEn(nroOrdenCompra, ordenesComprasEnCursoYPendientes);
        }

        public List<OrdenCompra> RecuperarOrdenesComprasEnCursoYPendientes()
        {
            return
                this.RecuperarOrdenesCompras().Where(
                    oc => oc.Estado == EstadoOrdenCompra.EnCurso || oc.Estado == EstadoOrdenCompra.Pendiente).ToList();
        }

        public List<OrdenCompra> BuscarOrdenCompraPendientesOEnCursoPorProveedor(Proveedor proveedor)
        {
            return
                this.RecuperarOrdenesComprasEnCursoYPendientes()
                    .Where(oc => oc.Proveedor.CodProveedor == proveedor.CodProveedor)
                    .ToList();
        }
    }

}
