﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema.Reportes;
using Entidades.Sistema;

namespace Modelo.Sistema
{
    public class CatalogoClientes
    {
        #region Singleton

        private static CatalogoClientes _Instancia;

        //constructor
        private CatalogoClientes()
        {

        }

        public static CatalogoClientes ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new CatalogoClientes();
            return _Instancia;
        }

        #endregion

        #region Methods

        public List<Cliente> RecuperarClientes()
        {
            return Mapping.Sistema.MappingClientes.RecuperarClientes();
        }

        public List<Cliente> FiltrarClientesPorCodigoORazonSocial(string codigo, string razonSocial)
        {
            return Mapping.Sistema.MappingClientes.RecuperarClientes()
                            .Where(c => c.RazonSocial.Contains(razonSocial) && c.CodCliente.ToString().Contains(codigo)).ToList();

        }
        public List<ArticulosPedidosXClientes> FiltrarClientesPorArticulosVendidos(string articulo, string razonSocial, DateTime desde, DateTime hasta)
        {
            return Mapping.Sistema.MappingArticulosPedidosXClientes.RecuperarArticulosPedidosXClientes(articulo, razonSocial, desde, hasta);
        }

        public List<Cliente> FiltrarClientes(string criteriofiltrado)
        {
            string criterio = criteriofiltrado.ToLower();
            return Mapping.Sistema.MappingClientes.RecuperarClientes().FindAll(delegate(Cliente cliente) { return cliente.CodCliente.ToString().ToLower().Contains(criterio) || cliente.RazonSocial.ToLower().Contains(criterio) || cliente.Direccion.ToLower().Contains(criterio) || cliente.Ciudad.Nombre.ToLower().Contains(criterio); });
        }

        public int AgregarCliente(Cliente cliente)
        {
            int nroCliente = Mapping.Sistema.MappingClientes.AgregarCliente(cliente);  //aca se comunica con el mapping y la base, le pide el autonumerico que creo la base
            cliente.CodCliente = nroCliente;
            return nroCliente;
        }

        public void ModificarCliente(Cliente cliente)
        {
            Mapping.Sistema.MappingClientes.ModificarCliente(cliente);
        }

        public void EliminarCliente(Cliente cliente)
        {
            Mapping.Sistema.MappingClientes.EliminarCliente(cliente);
        }

        public bool BuscarCuit(Cliente cliente)
        {
            Cliente oCliente = Mapping.Sistema.MappingClientes.RecuperarClientes().Find(clie => clie.Cuit == cliente.Cuit);
            if (oCliente == null)
                return false;
            else
                return true;
        }


        public Cliente BuscarClientePorCUIT(Int64 nroCuitClienteSeleccionado)
        {
            return Mapping.Sistema.MappingClientes.RecuperarClientes().Find(clie => clie.Cuit == nroCuitClienteSeleccionado);
        }

        public Cliente BuscarCliente(int codClienteBuscado)
        {
            return Mapping.Sistema.MappingClientes.BuscarCliente(codClienteBuscado);
        }

        #endregion

        public void GuardarCliente(Cliente cliente)
        {
            cliente.Validate();

            if (cliente.CodCliente != 0)
            {
                this.ModificarCliente(cliente);
            }
            else
            {
                this.AgregarCliente(cliente);
            }
        }
    }
}
