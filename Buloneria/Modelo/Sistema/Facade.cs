﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Entidades.Auditoria;
using Entidades.Sistema;
using Modelo.Auditoria;

namespace Modelo.Sistema
{
    public static class Facade
    {
        public static int Agregar(object objeto)
        {
            int Rta = 0;
            //LLAMAR A CATALOGO OC
            switch (objeto.GetType().Name)
            {
                case "OrdenCompra":
                    OrdenCompra ordenCompra = (OrdenCompra)objeto;
                    Rta = CatalogoOrdenesCompras.ObtenerInstancia().AgregarOrdenCompra(ordenCompra);
                    break;
                case "AuditoriaOrdenCompra":
                    AuditoriaOrdenCompra auditoriaOrdenCompra = (AuditoriaOrdenCompra) objeto;
                    CatalogoAuditoriaOC.ObtenerInstancia().AgregarRegistro(auditoriaOrdenCompra);
                    break;
                
                default:
                    Rta = 0;
                    break;
            }
            return Rta;
        }

        

        public static object Recuperar(string parametro)
        {
            object Lista;
            switch (parametro)
            {
                case "FormaPago":
                    Lista = CatalogoFormasPago.ObtenerInstancia().RecuperarFormasPago();
                    break;
                case "Proveedor":
                    Lista = CatalogoProveedores.ObtenerInstancia().RecuperarProveedores();
                    break; 
                default:
                    Lista = false;
                    break;
            }
            return Lista;
        }

        public static object Recuperar(string parametro, object objeto)
        {
            object Lista = null;
            switch (parametro)
            {
                case "Articulo":
                    Entidades.Sistema.Proveedor proveedor = (Entidades.Sistema.Proveedor)objeto;
                    Lista = CatalogoArticulos.ObtenerInstancia().RecuperarArticulosProveedor(proveedor);
                    break; 


            }
            return Lista;
        }

        public static object Buscar(int codigo, string tipo)
        {
            object entidad = null;
            switch (tipo)
            {
                case "Proveedor":
                    entidad = CatalogoProveedores.ObtenerInstancia().BuscarProveedor(codigo);
                    break;
                case "Articulo":
                    entidad = CatalogoArticulos.ObtenerInstancia().BuscarArticulo(codigo);
                    break;
            }
            return entidad;
        }
    }
}