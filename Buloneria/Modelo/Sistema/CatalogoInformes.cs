﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Modelo.Sistema
{
    public class CatalogoInformes
    {
        private static CatalogoInformes _Instancia;
        private DataTable _ColArticulosMasVendidos; //coleccion de tipo datatable
        private DataTable _ColArticulosFaltStock; //coleccion de tipo datatable
        private DataTable _ColMontosComprasMesCli; //coleccion de tipo datatable
        private DataTable _ColPreCompArt; //coleccion de tipo datatable

        //constructor
        private CatalogoInformes()  //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {
            _ColArticulosMasVendidos = new DataTable();
            _ColArticulosFaltStock = new DataTable();
            _ColMontosComprasMesCli = new DataTable();
            _ColPreCompArt = new DataTable();            
        }

        public static CatalogoInformes ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoInformes();  //si la instancia es null, que cree el objeto
            return _Instancia; //y sino, no lo crea y devuelve el objeto existente
        }


        //metodo recuperar artículos con faltantes de stock, de tipo datatable
        public DataTable RecuperarArticulosFaltStock()
        {
            return _ColArticulosFaltStock = Mapping.Sistema.MappingInformes.RecuperarArticulosFaltStock();
        }

        //metodo recuperar articulos mas vendidos, de tipo datatable
        public DataTable RecuperarArtMasVendidos(DateTime fechadesde, DateTime fechahasta)
        {
            return _ColArticulosMasVendidos = Mapping.Sistema.MappingInformes.RecuperarArtMasVendidos(fechadesde, fechahasta);
        }
        
        //metodo recuperar montos de compras por mes de clientes, de tipo datatable
        public DataTable RecuperarMontosComprasMesClientes(DateTime fechadesde, DateTime fechahasta)
        {
            return _ColMontosComprasMesCli = Mapping.Sistema.MappingInformes.RecuperarMontosComprasMesClientes(fechadesde, fechahasta);
        }

        //metodo recuperar precios entre proveedores de un articulo, de tipo datatable
        public DataTable RecuperarPreciosComparativosArt(string codigoArticulo)
        {
            return _ColPreCompArt = Mapping.Sistema.MappingInformes.RecuperarPreciosComparativosArt(codigoArticulo);
        }
    }
}
