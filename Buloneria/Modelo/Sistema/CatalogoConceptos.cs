﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoConceptos
    {
        private List<Entidades.Sistema.Concepto> _colConceptos;
        private static CatalogoConceptos _instancia;
       
        //constructor
        private CatalogoConceptos()
        {
            _colConceptos = Mapping.Sistema.MappingConceptos.RecuperarConceptos();
        }

        public static CatalogoConceptos ObtenerInstancia()
        {
            if (_instancia == null) _instancia = new CatalogoConceptos(); //si la instancia es null, que cree el objeto
            return _instancia; //y sino, no lo crea y devuelve el objeto existente
        }
        
        //metodo recuperar conceptos
        public List<Entidades.Sistema.Concepto> RecuperarConceptos()
        {
            return _colConceptos;
        }

        //metodo buscar concepto
        public Entidades.Sistema.Concepto BuscarConcepto(Entidades.Sistema.Concepto concepto)
        {
            Entidades.Sistema.Concepto oConcepto = _colConceptos.Find(cpto => cpto.Concepto1== concepto.Concepto1);
            return oConcepto;            
        }

        //metodo agregar concepto
        public void AgregarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            Mapping.Sistema.MappingConceptos.AgregarConcepto(oConcepto);
            _colConceptos.Add(oConcepto);
        }

        //metodo eliminar concepto
        public bool EliminarConcepto(Entidades.Sistema.Concepto oConcepto)
        {
            Mapping.Sistema.MappingConceptos.EliminarConcepto(oConcepto);
            return _colConceptos.Remove(oConcepto);
        }

    }
}
