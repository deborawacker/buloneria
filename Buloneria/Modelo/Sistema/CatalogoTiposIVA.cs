﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema;

namespace Modelo.Sistema
{
    public class CatalogoTiposIVA
    {
        #region Singleton

        private static CatalogoTiposIVA _Instancia;

        private CatalogoTiposIVA()
        {
        }

        public static CatalogoTiposIVA ObtenerInstancia()
        {
            return _Instancia ?? (_Instancia = new CatalogoTiposIVA());
        }

        #endregion

        private List<TipoIva> ColeccionTiposIVA
        {
            get { return this.RecuperarTiposIva(); }
        }
         
        public List<TipoIva> RecuperarTiposIva()
        {
            return Mapping.Sistema.MappingTiposIva.RecuperarTiposIva();
        }

        //metodo recuperar tipos iva proveedores
        public List<TipoIva> RecuperarTiposIvaProveedores()
        {
            List<TipoIva> tiposivaproveedores = ColeccionTiposIVA;
            TipoIva iva = ColeccionTiposIVA.Find(delegate (TipoIva tipoiva) {return tipoiva.Nombre.Contains("Exento");});
            tiposivaproveedores.Remove(iva);
            return tiposivaproveedores;
        }
    }
}
