﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoNotasPedidos
    {
        // aca declaro la coleccion _colNotasPedidos de _TipoMovimiento list<entidades.sistema.notapedido>
        //private List<Entidades.Sistema.NotaPedido> _colNotasPedidos;

        private static CatalogoNotasPedidos _Instancia;

        public List<NotaPedido> ColNotasPedidos
        {
            get
            {
                return Mapping.Sistema.MappingNotasPedidos.RecuperarNotasPedidos(); 
            }
        }
        

        //constructor
        private CatalogoNotasPedidos()  //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {
        }

        public static CatalogoNotasPedidos ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new CatalogoNotasPedidos();  //si la instancia es null, que cree el objeto
            return _Instancia;  //y sino, no lo crea y devuelve el objeto existente      
        }

        //metodo recuperar NP
        public List<Entidades.Sistema.NotaPedido> RecuperarNotasPedidos()
        {
            return Mapping.Sistema.MappingNotasPedidos.RecuperarNotasPedidos();
        }

        //metodo flitrar NP
        public List<Entidades.Sistema.NotaPedido> FiltrarNotasPedidos(string criteriofiltrado)
        {
            string criterio = criteriofiltrado.ToLower();
            return ColNotasPedidos.FindAll(delegate(Entidades.Sistema.NotaPedido np) { return np.NroNotaPedido.ToString().ToLower().Contains(criterio) || np.Cliente.RazonSocial.ToLower().Contains(criterio) || np.FechaEmision.ToShortDateString().Contains(criterio); });
        }

        //metodo modificar NP
        public void ModificarNotaPedido(Entidades.Sistema.NotaPedido np)
        {
            ColNotasPedidos.Remove(np); //elimino np de la coleccion
            ColNotasPedidos.Add(np);   //y agrego la nueva np modificada a la coleccion         
        }

        //metodo recuperar historial NP
        public List<Entidades.Sistema.NotaPedido> RecuperarHistorialNotasPedidos()
        {
            ColNotasPedidos.AddRange(Mapping.Sistema.MappingNotasPedidos.RecuperarHistorialNotasPedidos());
            return ColNotasPedidos;
        }

        //metodo agregar NP
        public int AgregarNotaPedido(Entidades.Sistema.NotaPedido np)
        {
            int nroNP = Mapping.Sistema.MappingNotasPedidos.AgregarNotaPedido(np);
            np.NroNotaPedido = nroNP;
            ColNotasPedidos.Add(np);
            return nroNP;
        }

        //metodo buscar NP
        public bool BuscarNotaPedido(Entidades.Sistema.NotaPedido oNP)
        {
            foreach (Entidades.Sistema.NotaPedido np in ColNotasPedidos)
            {
                if (np.NroNotaPedido == oNP.NroNotaPedido) return true; //si encuentra la np, que devuelva true                
            }
            return false; //si no la encontro, que devuelva false
        }

        //metodo eliminar NP
        public bool EliminarNotaPedido(Entidades.Sistema.NotaPedido np)
        {
            try
            {
                Mapping.Sistema.MappingNotasPedidos.EliminarNotaPedido(np);  //aca llamo al mapping, dentro del mapping al metodo eliminargrupoarticulo y le mando np como parametro
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<NotaPedido> BuscarNotaPedidoPorCriterio(NotaPedidoCriterio npCriterio)
        {
            var colNotasPedidos = this.ColNotasPedidos;
            return colNotasPedidos
                .Where(np => 
                    (npCriterio.CodCliente>0  && np.Cliente.CodCliente == npCriterio.CodCliente)
                    || (npCriterio.NroNotaPedido>0 && np.NroNotaPedido == npCriterio.NroNotaPedido)
                    || (!String.IsNullOrEmpty(npCriterio.RazonSocial) && np.Cliente.RazonSocial.ToLower().Contains(npCriterio.RazonSocial.ToLower()))
                   
                    )
                .Select(np=>np)
                .ToList()
                //HACK: deberia no restringir los estados
                .Where(np=>np.EstadoNotaPedido.Id == EstadoNotaPedidoParameters.EstadoNotaPedidoEnCurso || np.EstadoNotaPedido.Id == EstadoNotaPedidoParameters.EstadoNotaPedidoPendienteEntrega)
                .ToList();
        }

        public NotaPedido BuscarNotasDePedidoPorNro(int nroNotaPedidoBuscada)
        {
            return this.ColNotasPedidos.FirstOrDefault(np => np.NroNotaPedido == nroNotaPedidoBuscada);
        } 
        public void ActualizarEstadoNotaPedido(Entidades.Sistema.NotaPedido notaPedido)
        {
            Mapping.Sistema.MappingNotasPedidos.ActualizarEstadoNotaPedido(notaPedido);
        }

        public void ActualizarCantidadEntregadaLineasNotaPedido(Entidades.Sistema.NotaPedido notaPedido)
        {
            Mapping.Sistema.MappingNotasPedidos.ActualizarCantidadEntregadaLineasNotaPedido(notaPedido);
        }
    }
}
