﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoFormasPago
    {
        private List<Entidades.Sistema.FormaPago> _ColeccionFormasPago;  //creo una coleccion de _TipoMovimiento List<entidades.sistema.formapago>

        private static CatalogoFormasPago _Instancia; //creo instancia de _TipoMovimiento catalogoformaspago de forma privada y static

        //constructor
        private CatalogoFormasPago() //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {
            _ColeccionFormasPago = Mapping.Sistema.MappingFormasPago.RecuperarFormasPago();
        }

        public static CatalogoFormasPago ObtenerInstancia()
        { 
            if (_Instancia==null) _Instancia = new CatalogoFormasPago();
            return _Instancia;        
        }

        //metodo recuperar formas de pago 
        public List<Entidades.Sistema.FormaPago> RecuperarFormasPago()
        {
            return _ColeccionFormasPago;
        }



    }
}
