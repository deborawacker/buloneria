﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoAlicuotas
    {
        private List<Entidades.Sistema.Alicuota> _ColeccionAlicuotas;

        private static CatalogoAlicuotas _Instancia; //creo _instancia de _TipoMovimiento catalogoalicuotas de forma privada y static

        //constructor
        private CatalogoAlicuotas() //el constructor tiene que ser privado en singleton para que la clase cree la instancia al objeto
        {
            _ColeccionAlicuotas = Mapping.Sistema.MappingAlicuotas.RecuperarAlicuotas();
        }

        public static CatalogoAlicuotas ObtenerInstancia() 
        { 
            if(_Instancia==null) _Instancia = new CatalogoAlicuotas();
            return _Instancia;
        
        }

        //metodo recuperar 
        public List<Entidades.Sistema.Alicuota> RecuperarAlicuotas()
        {
            return _ColeccionAlicuotas;
        }    
    }
}
