﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Sistema
{
    public class CatalogoArticulos
    {
        //private List<Articulo> _ColArticulos;
        private static CatalogoArticulos _Instancia;

        //constructor
        private CatalogoArticulos()
        {

        }

        public static CatalogoArticulos ObtenerInstancia()
        {
            if (_Instancia == null)
                _Instancia = new CatalogoArticulos();

            return _Instancia;
        }

        //metodo recuperar articulos
        public List<Articulo> RecuperarArticulos()
        {
            return Mapping.Sistema.MappingArticulos.RecuperarArticulos();
        }

        //metodo agregar articulo
        public void AgregarArticulo(Articulo articulo)  //el agregar articulo no devuelve nada (es void) lo agrega solamente (el codigo no es autonumerico asi que no tiene que devolverlo)
        {
            articulo.Validate();
            
            Mapping.Sistema.MappingArticulos.AgregarArticulo(articulo);
        }

        //metodo eliminar articulo
        public void EliminarArticulo(Articulo articulo)
        {
            Mapping.Sistema.MappingArticulos.EliminarArticulo(articulo);  //aca llama a la capa mapping, al metodo eliminar articulo, para que elimine el articulo de la base de datos
        }

        //modificar modificar articulo
        /*public bool ModificarArticulo(Articulo articulo)
        {
            Mapping.Sistema.MappingArticulos.modificarArticulo(articulo);
            Articulo articulito = _ColArticulos.Find(art => art.CodArticulo == articulo.CodArticulo);
            bool resultado = _ColArticulos.Remove(articulito);
            _ColArticulos.Add(articulo);
            return resultado;
        }*/

        //metodo filtrar articulos
        public List<Articulo> FiltrarArticulos(string criterio)
        {
            string criterioFiltrado = criterio.ToLower();
            return this.RecuperarArticulos().FindAll(delegate(Articulo articulo) { return articulo.CodArticulo.ToString().ToLower().Contains(criterioFiltrado) || articulo.Descripcion.ToLower().Contains(criterioFiltrado); });
        }

        //metodo buscar articulo
        public bool BuscarArticulo(Articulo articulo)
        {
            Articulo oArticulo = this.RecuperarArticulos().Find(art => art.CodArticulo == articulo.CodArticulo); //pregunta y compara si el art.codarticulo es igual al articulo.codarticulo
            if (oArticulo == null) return false; //si guarda null, significa que no son iguales los codarticulo, devuelve false
            else return true; //si son iguales los codarticulo, guarda ese codarticulo, y devuelve true
        }

        //metodo recuperar articulos de un proveedor
        public List<Articulo> RecuperarArticulosProveedor(Entidades.Sistema.Proveedor proveedor)
        {
            List<Articulo> colArticulosProveedor = new List<Articulo>();
            foreach (Articulo articulo in this.RecuperarArticulos())
            {
                foreach (Entidades.Sistema.Proveedor oproveedor in articulo.Proveedores)
                {
                    if (proveedor.CodProveedor == oproveedor.CodProveedor) colArticulosProveedor.Add(articulo);
                }
            }
            return colArticulosProveedor;
        }

        //metodo buscar articulos de un grupo especifico pasado por parametro
        public bool BuscarArticulosGrupo(Entidades.Sistema.GrupoArticulo grupo)
        {
            Articulo articulo = new Articulo();
            articulo = this.RecuperarArticulos().Find(oArticulo => oArticulo.Grupo.CodGrupoArticulo == grupo.CodGrupoArticulo);
            if (articulo == null) return false; //devuelve falso en el articulo esta vacio, es decir si no encontro ningun codgrupoarticulo igual
            else return true; //devuelve true si en el articulo hay algo, es decir que encontro codgrupoarticulo
        }

        //metodo recuperar articulos de un grupo especifico pasado por parametro
        public List<Articulo> RecuperarArticulosGrupo(Entidades.Sistema.GrupoArticulo grupo)
        {
            List<Articulo> colArticulosGrupo = new List<Articulo>();

            foreach (Articulo articulo in this.RecuperarArticulos())
            {
                if (grupo.CodGrupoArticulo == articulo.Grupo.CodGrupoArticulo) colArticulosGrupo.Add(articulo);  //aca comparo que el codigo del grupoarticulo que recibi como parametro sea igual al codigo del grupoarticulo del articulo, y si encuentra lo agrega a la coleccion de articulosgrupo                
            }
            return colArticulosGrupo;
        }

        /// <summary>
        /// Invoca a la BD para modificar un articulo
        /// </summary>
        /// <param name="articulo"></param>
        /// <returns></returns>
        public void Update(Articulo articulo)
        {
            articulo.Validate();

            Mapping.Sistema.MappingArticulos.Update(articulo);
        }

        /// <summary>
        /// Invoca a la BD para modificar una lista de articulos
        /// </summary>
        /// <param name="listaArticulos"></param>
        /// <returns></returns>
        public Boolean Update(List<Articulo> listaArticulos)
        {
            foreach (var articulo in listaArticulos)
            {
                Mapping.Sistema.MappingArticulos.Update(articulo);
            }
            return true;
        }

        //[Obsolete("Para hacer esto existe el método Update")]
        //public bool ModificarStockArticulo(Articulo articulo, int cantidad)
        //{

        //    //Articulo articulito = _ColArticulos.Find(art => art.CodArticulo == articulo.CodArticulo);
        //    // bool resultado = _ColArticulos.Remove(articulito);
        //    //_ColArticulos.Add(articulo);
        //    //Mapping.Sistema.MappingArticulos.modificarStockArticulo(articulo, cantidad);
        //    return true;
        //}

        public List<Articulo> FiltrarArticulosPorCodigoDescripcion(string codigo, string descripcion)
        {
            return this.RecuperarArticulos().Where(a => a.CodArticulo.Equals(codigo) || a.Descripcion.Equals(descripcion) || (!String.IsNullOrEmpty(descripcion) && a.Descripcion.ToLower().Contains(descripcion.ToLower()))).ToList();
        }

        public Articulo BuscarArticuloPorCodigo(string codArticulo)
        {
            return (Articulo)this.RecuperarArticulos().FirstOrDefault(a => a.CodArticulo.Equals(codArticulo));
        }

        public Articulo BuscarArticuloPorCriterio(ArticuloCriterio criterio)
        {
            return this.RecuperarArticulos().FirstOrDefault(a => a.CodArticulo == criterio.CodigoArticulo);
        }

        public Articulo BuscarArticulo(int idArticuloBuscado)
        {
            //HACK: hacer un método que busque por idarticulo a traves de un store
            return (Articulo)this.RecuperarArticulos().Where(a => a.IdArticulo == idArticuloBuscado).ToList().FirstOrDefault();
        }
    }
}
