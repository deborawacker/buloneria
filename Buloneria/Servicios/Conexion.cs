﻿using System;
using System.Configuration;
using System.Data.SqlClient;
[assembly: CLSCompliant(true)]
namespace Servicios
{
    public class Conexion
    {
        private static Conexion _Instancia;
        private SqlConnection _Conexion;
        private SqlTransaction _Transaccion;

        public static Conexion ObtenerInstancia()
        {
            if (_Instancia == null) _Instancia = new Conexion();
            return _Instancia;
        }
        private Conexion()
        {
            _Conexion = new SqlConnection();
        }
        public void Conectar(string conexion)
        {
            foreach (System.Configuration.ConnectionStringSettings item in ConfigurationManager.ConnectionStrings)
            {
                if (item.Name == conexion)
                {
                    _Conexion.ConnectionString = item.ConnectionString;
                    _Conexion.Open();
                    break;
                }
            }
        }
        public void Desconectar()
        {
            _Conexion.Close();
        }

        public SqlConnection RetornarConexion()
        {
            return _Conexion;
        }
        public void ComenzarTransaccion()
        {
            _Transaccion = _Conexion.BeginTransaction();
        }
        public SqlTransaction RetornarSqlTransaccion()
        {
            return _Transaccion;
        }
    }
}
