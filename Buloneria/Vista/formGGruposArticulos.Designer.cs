﻿namespace Vista
{
    partial class formGGruposArticulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.dgvGruposArticulos = new System.Windows.Forms.DataGridView();
            this.colCodigoGrupoArt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescGrupoArticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposArticulos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(12, 12);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 47);
            this.btnAgregar.TabIndex = 0;
            this.btnAgregar.Tag = "Alta";
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(93, 12);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 47);
            this.btnModificar.TabIndex = 1;
            this.btnModificar.Tag = "Modificacion";
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(174, 12);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 47);
            this.btnConsultar.TabIndex = 2;
            this.btnConsultar.Tag = "Consulta";
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(255, 12);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 47);
            this.btnEliminar.TabIndex = 3;
            this.btnEliminar.Tag = "Baja";
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Buscar:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(385, 26);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(376, 20);
            this.txtBuscar.TabIndex = 5;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // dgvGruposArticulos
            // 
            this.dgvGruposArticulos.AllowUserToAddRows = false;
            this.dgvGruposArticulos.AllowUserToDeleteRows = false;
            this.dgvGruposArticulos.AllowUserToResizeColumns = false;
            this.dgvGruposArticulos.AllowUserToResizeRows = false;
            this.dgvGruposArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGruposArticulos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCodigoGrupoArt,
            this.colDescGrupoArticulo});
            this.dgvGruposArticulos.Location = new System.Drawing.Point(13, 65);
            this.dgvGruposArticulos.MultiSelect = false;
            this.dgvGruposArticulos.Name = "dgvGruposArticulos";
            this.dgvGruposArticulos.ReadOnly = true;
            this.dgvGruposArticulos.RowHeadersVisible = false;
            this.dgvGruposArticulos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGruposArticulos.ShowCellErrors = false;
            this.dgvGruposArticulos.ShowCellToolTips = false;
            this.dgvGruposArticulos.ShowEditingIcon = false;
            this.dgvGruposArticulos.ShowRowErrors = false;
            this.dgvGruposArticulos.Size = new System.Drawing.Size(748, 418);
            this.dgvGruposArticulos.TabIndex = 6;
            // 
            // colCodigoGrupoArt
            // 
            this.colCodigoGrupoArt.DataPropertyName = "codGrupoArticulo";
            this.colCodigoGrupoArt.HeaderText = "Cod Grupo Articulo";
            this.colCodigoGrupoArt.Name = "colCodigoGrupoArt";
            this.colCodigoGrupoArt.ReadOnly = true;
            // 
            // colDescGrupoArticulo
            // 
            this.colDescGrupoArticulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDescGrupoArticulo.DataPropertyName = "descripcion";
            this.colDescGrupoArticulo.HeaderText = "Grupo Articulo";
            this.colDescGrupoArticulo.Name = "colDescGrupoArticulo";
            this.colDescGrupoArticulo.ReadOnly = true;
            // 
            // formGGruposArticulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 495);
            this.Controls.Add(this.dgvGruposArticulos);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnAgregar);
            this.Name = "formGGruposArticulos";
            this.Tag = "GruposArticulos";
            this.Text = "Gestionar Grupos de Articulos";
            this.Load += new System.EventHandler(this.formGGruposArticulos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGruposArticulos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridView dgvGruposArticulos;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCodigoGrupoArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescGrupoArticulo;
    }
}