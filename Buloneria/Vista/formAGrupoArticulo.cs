﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAGrupoArticulo : Form
    {

        private string _operacion;

        public formAGrupoArticulo(string operacion)  //en operacion voy a guardar si es agregar, modificar, eliminar o consultar
        {
            _operacion = operacion;
            InitializeComponent();
        }
        
        Entidades.Sistema.GrupoArticulo _GrupoArticulo;        
       
        public formAGrupoArticulo(Entidades.Sistema.GrupoArticulo ogrupoArticulo, string operacion) //constructor sobrecargado, para que el formagrupoarticulo se comporte de distintas formas
        {
            InitializeComponent();
            _operacion = operacion;
            _GrupoArticulo = ogrupoArticulo;
            if (operacion == "Modificar") PrepararFormModificar();
            if (operacion == "Eliminar") PrepararFormEliminar();
            if (operacion == "Consultar") PrepararFormConsultar();
        }

        private void PrepararFormConsultar()
        {
            this.Text = "Consultar Grupo de Articulos";
            this.txtCodigo.Text = _GrupoArticulo.CodGrupoArticulo;
            this.txtDescripcion.Text = _GrupoArticulo.Descripcion;
            this.btnAceptar.Visible = false;
            this.txtCodigo.Enabled = false;
            this.txtDescripcion.Enabled = false;
            this.btnCancelar.Text = "Cerrar";
        }

        private void PrepararFormEliminar()
        {
            this.Text = "Eliminar Grupo de Articulos";
            this.txtCodigo.Text = _GrupoArticulo.CodGrupoArticulo;
            this.txtDescripcion.Text = _GrupoArticulo.Descripcion;
            this.txtCodigo.Enabled = false;
            this.txtDescripcion.Enabled = false;
        }

        private void PrepararFormModificar()
        {
            this.Text = "Modificar Grupo de Articulos";
            this.txtCodigo.Text = _GrupoArticulo.CodGrupoArticulo;
            this.txtDescripcion.Text = _GrupoArticulo.Descripcion;
            this.txtCodigo.Enabled = false;
        }

        //evento click del aceptar, como es un solo form con distintos comportamientos, voy armando el if para cada uno
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                if (_operacion == "Agregar")
                {
                    Entidades.Sistema.GrupoArticulo oGrupoArticulo = new Entidades.Sistema.GrupoArticulo();
                    oGrupoArticulo.CodGrupoArticulo = txtCodigo.Text;
                    oGrupoArticulo.Descripcion = txtDescripcion.Text;

                    if (Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().AgregarGrupoArticulo(oGrupoArticulo) == true)
                    {
                        MessageBox.Show("Grupo de artículo agregado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Grupo de artículo existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCodigo.Focus();
                        return;
                    }
                }
                if (_operacion == "Modificar")
                {                    
                    _GrupoArticulo.CodGrupoArticulo = txtCodigo.Text;
                    _GrupoArticulo.Descripcion = txtDescripcion.Text;

                    if (Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().ModificarGrupoArticulo(_GrupoArticulo) == true)
                    {
                        MessageBox.Show("Grupo de articulo modificado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else 
                    {
                        MessageBox.Show("Grupo de articulo existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtDescripcion.Focus();
                        return;
                    }                
                }
                
                if (_operacion == "Eliminar")
                {
                    _GrupoArticulo.CodGrupoArticulo = txtCodigo.Text;
                    _GrupoArticulo.Descripcion = txtDescripcion.Text;

                    DialogResult respuesta = MessageBox.Show("Desea eliminar el grupo de articulo?", "Eliminar", MessageBoxButtons.YesNo);
                    if (respuesta == System.Windows.Forms.DialogResult.Yes)
                    { 
                        bool resultado = Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().EliminarGrupoArticulo(_GrupoArticulo);
                        if (resultado) //si el resultado que devuelve es true significa que lo pudo eliminar
                        {
                            MessageBox.Show("El grupo de articulo se elimino exitosamente.","Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close(); 
                        }
                        else //si el resultado es false significa que no lo pudo eliminar, porque ese grupo de articulo tiene un articulo asociado
                        {
                            MessageBox.Show("Ha ocurrido un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }
                    }            
                }                                    
            }
            else MessageBox.Show("Complete todos los datos");
        }

        //metodo validar datos
        private bool ValidarDatos()
        {
            if (txtCodigo.Text == null) return false;
            if (txtCodigo.Text == "") return false;
            if (txtDescripcion.Text == null) return false;
            if (txtDescripcion.Text == "") return false;
            return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formAGrupoArticulo_Load(object sender, EventArgs e)
        {

        }        
    }
}
