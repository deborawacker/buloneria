﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGArticulos : Form
    {
        public formGArticulos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();
            this.dgvArticulos.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }
                        
        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                        foreach (Control control in this.Controls)
                            if (control is Button)
                            {
                                Button boton = (Button)control;
                                if (boton.Tag != null)
                                {
                                    if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                                }
                            }
                }
            }
        }

        private BindingSource bsArticulos;

        private void formGArticulos_Load(object sender, EventArgs e)
        {
            this.bsArticulos = new BindingSource();
            CargarGrilla();
        }
        
        private void CargarGrilla()
        {
            bsArticulos.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarArticulos();
            dgvArticulos.DataSource = null;
            dgvArticulos.DataSource = bsArticulos;
        }

        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            bsArticulos.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().FiltrarArticulos(txtBuscar.Text);
            dgvArticulos.DataSource = null;
            dgvArticulos.DataSource = bsArticulos;
        }

        private void ActualizarGrilla()
        {
            dgvArticulos.DataSource = null;
            CargarGrilla();
        }   

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            formAArticulo formaarticulo = new formAArticulo("Agregar");
            formaarticulo.FormClosed += new FormClosedEventHandler(formaarticulo_FormClosed);
            formaarticulo.Show();
        }

        void formaarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            formAArticulo formmarticulo = new formAArticulo((Entidades.Sistema.Articulo)this.bsArticulos.Current,"Modificar");
            formmarticulo.FormClosed += new FormClosedEventHandler(formmarticulo_FormClosed);
            formmarticulo.Show();
        }

        void formmarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formAArticulo formcarticulo = new formAArticulo((Entidades.Sistema.Articulo)this.bsArticulos.Current, "Consultar");
            formcarticulo.FormClosed += new FormClosedEventHandler(formcarticulo_FormClosed);
            formcarticulo.Show();
        }

        void formcarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            formAArticulo formearticulo = new formAArticulo((Entidades.Sistema.Articulo)this.bsArticulos.Current, "Eliminar");
            formearticulo.FormClosed += new FormClosedEventHandler(formearticulo_FormClosed);
            formearticulo.Show();
        }

        void formearticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }          
    }
}
