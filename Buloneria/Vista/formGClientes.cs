﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGClientes : Form
    {
        public formGClientes(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();
            this.dgvClientes.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

        private BindingSource bsClientes;

        private void formGClientes_Load(object sender, EventArgs e)
        {
            this.bsClientes = new BindingSource();
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            Controladora.Sistema.ControladoraCUGestionarClientes oCtrlGClientes = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia();
            bsClientes.DataSource = oCtrlGClientes.RecuperarClientes();
            dgvClientes.DataSource = bsClientes;
        }
        
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            formACliente formacliente = new formACliente();
            formacliente.FormClosed += new FormClosedEventHandler(formacliente_FormClosed); //aca le digo que cuando se cierre el form se actualice grilla
            formacliente.Show();
        }

        void formacliente_FormClosed(object sender, FormClosedEventArgs e)  //evento, cuando se cierra el form se actualiza grilla
        {
            this.ActualizarGrilla();
        }
           
        private void btnModificar_Click(object sender, EventArgs e)
        {
            formMCliente formmcliente = new formMCliente((Entidades.Sistema.Cliente)this.bsClientes.Current);
            formmcliente.FormClosed += new FormClosedEventHandler(formmcliente_FormClosed);
            formmcliente.Show();
        }

        void formmcliente_FormClosed(object sender, FormClosedEventArgs e)
        {
            ActualizarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formCCliente formccliente = new formCCliente((Entidades.Sistema.Cliente)this.bsClientes.Current);
            formccliente.FormClosed += new FormClosedEventHandler(formccliente_FormClosed);
            formccliente.Show();
        }

        void formccliente_FormClosed(object sender, FormClosedEventArgs e)
        {
            ActualizarGrilla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            formECliente formecliente = new formECliente((Entidades.Sistema.Cliente)this.bsClientes.Current);
            formecliente.FormClosed += new FormClosedEventHandler(formecliente_FormClosed);
            formecliente.Show();
        }

        void formecliente_FormClosed(object sender, FormClosedEventArgs e)
        {
            ActualizarGrilla();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            bsClientes.DataSource = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().FiltrarClientes(txtBuscar.Text);
            dgvClientes.DataSource = null;
            dgvClientes.DataSource = bsClientes;
        }

        private void ActualizarGrilla()
        {
            dgvClientes.DataSource = null;
            CargarGrilla();
        }

        

        
    }
}
