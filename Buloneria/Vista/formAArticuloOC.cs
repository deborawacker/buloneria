﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAArticuloOC : Form
    {
        Entidades.Sistema.Articulo _articulo;
        public formAArticuloOC(Entidades.Sistema.Articulo articulo)
        {
            _articulo = articulo;
            InitializeComponent();
            this.txtDescripcion.Text = _articulo.Descripcion;
            this.txtCantidad.Text = _articulo.CantActual.ToString();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.No;
            this.Close();
        }
        Entidades.Sistema.LineaOrdenCompra olinea;
        private void btnAceptar_Click(object sender, EventArgs e)
        {            
            if (ValidarDatos()) 
            {
                olinea = new Entidades.Sistema.LineaOrdenCompra();
                olinea.Articulo = _articulo;
                olinea.CantPedir = Convert.ToInt32(this.txtCantidadPedir.Text);
                olinea.PrecioUnitario = Convert.ToInt32(this.txtPrecioUnitario.Text);                
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {
            if (txtCantidadPedir.Text == null) return false;
            if (txtPrecioUnitario.Text == null) return false;
            if (txtCantidadPedir.Text == "") return false;
            if (txtPrecioUnitario.Text == "") return false;
            return true;
        }

        internal Entidades.Sistema.LineaOrdenCompra RetornarLineaOC()
        {
            return olinea;
        }

        private void formAArticuloOC_Load(object sender, EventArgs e)
        {

        }
    }
}
