﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGestionarNP : Form
    {
        public formGestionarNP(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();        
            this.dgvNotasPedidos.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }


        private BindingSource bsNotasPedidos;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bsNotasPedidos.DataSource = Controladora.Sistema.ControladoraCUGestionarNP.ObtenerInstancia().FiltrarNotasPedidos(txtBuscar.Text);
            dgvNotasPedidos.DataSource = null;
            dgvNotasPedidos.DataSource = bsNotasPedidos;
        }

        private void fromGestionarNP_Load(object sender, EventArgs e)
        {
            this.bsNotasPedidos = new BindingSource();
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            bsNotasPedidos.DataSource = Controladora.Sistema.ControladoraCUGestionarNP.ObtenerInstancia().RecuperarNotasPedidos();
            dgvNotasPedidos.DataSource = bsNotasPedidos;
        }

        private void ActualizarGrilla()
        {
            dgvNotasPedidos.DataSource = null;
            CargarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formConsultarNP formconsultarnp = new formConsultarNP((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current); //le paso al form consultarnp el objeto actual seleccionado en la grilla
            formconsultarnp.FormClosed += new FormClosedEventHandler(formconsultarnp_FormClosed);
            formconsultarnp.Show();
        }

        void formconsultarnp_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }
                
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            formEliminarNP formeliminarnp = new formEliminarNP((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current); //le paso al form eliminarnp el objeto actual seleccionado en la grilla
            formeliminarnp.FormClosed += new FormClosedEventHandler(formeliminarnp_FormClosed);
            formeliminarnp.Show();           
        }

        void formeliminarnp_FormClosed(object sender, FormClosedEventArgs e) 
        { 
            ActualizarGrilla();
        }

        private void btnVerHistorial_Click_1(object sender, EventArgs e)
        {
            bsNotasPedidos.DataSource =null;
            bsNotasPedidos.DataSource = Controladora.Sistema.ControladoraCUGestionarNP.ObtenerInstancia().RecuperarHistorialNotasPedidos();
            bsNotasPedidos.ResetBindings(false);
        }

        private void dgvNotasPedidos_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

            foreach (DataGridViewRow item in this.dgvNotasPedidos.Rows)
            {
                Entidades.Sistema.NotaPedido NP = (Entidades.Sistema.NotaPedido)dgvNotasPedidos.Rows[item.Index].DataBoundItem;
                item.Cells["colCli"].Value = NP.Cliente.NombreFantasia;
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            //Calculo Total, subtotal y montoiva
            decimal subtotaliva = 0;
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaNotaPedido linea in ((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).LineasNotaPedido)
            {
                int subtotallinea = linea.PrecioUnitario * linea.Cantidad;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;
            }
            decimal total = subtotal + subtotaliva;
        



            Microsoft.Reporting.WinForms.ReportParameter[] Parametros = new Microsoft.Reporting.WinForms.ReportParameter[7];  //instancio un arreglo de tamano 7
            Parametros[0] = new Microsoft.Reporting.WinForms.ReportParameter("Subtotal", subtotal.ToString());
            Parametros[1] = new Microsoft.Reporting.WinForms.ReportParameter("RazonSocial",((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).Cliente.RazonSocial );
            Parametros[2] = new Microsoft.Reporting.WinForms.ReportParameter("NumeroNota", ((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).NroNotaPedido.ToString());
            Parametros[3] = new Microsoft.Reporting.WinForms.ReportParameter("FechaEmision", ((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).FechaEmision.ToString());
            Parametros[4] = new Microsoft.Reporting.WinForms.ReportParameter("FormaPago", ((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).FormaPago.ToString());
            Parametros[5] = new Microsoft.Reporting.WinForms.ReportParameter("MontoIva", subtotaliva.ToString());
            Parametros[6] = new Microsoft.Reporting.WinForms.ReportParameter("Total", total.ToString());

            BindingSource bsLineas = new BindingSource();
            List<Entidades.Sistema.LineaNotaPedido> ListaNotas = new List<Entidades.Sistema.LineaNotaPedido>();
            ListaNotas = ((Entidades.Sistema.NotaPedido)bsNotasPedidos.Current).LineasNotaPedido;
            bsLineas.DataSource = ListaNotas;
            Reportes.FormReportes formReportes = new Reportes.FormReportes("NotaPedido", bsLineas, Parametros);
            formReportes.Show();



        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {
            if (txtBuscar.Text == "Filtrar por Número de Nota, Fecha de Emisión o Razón Social...")
            {
                txtBuscar.Text = "";
            }
       }

        private void txtBuscar_Leave(object sender, EventArgs e)
        {
            if (txtBuscar.Text == "")
            {
                txtBuscar.Font = new Font(txtBuscar.Font, FontStyle.Italic);
                txtBuscar.ForeColor = System.Drawing.Color.DarkGray;
                txtBuscar.Text = "Filtrar por Número de Nota, Fecha de Emisión o Razón Social...";
            }
        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            Filtrar();
        }

        private void Filtrar()
        {
            List<Entidades.Sistema.NotaPedido> NotasFiltradas;
            NotasFiltradas = Controladora.Sistema.ControladoraCUGestionarNP.ObtenerInstancia().FiltrarNotasPedidos(txtBuscar.Text);
       
            bsNotasPedidos = new BindingSource();
            bsNotasPedidos.DataSource = NotasFiltradas;
            dgvNotasPedidos.AutoGenerateColumns = false;
            dgvNotasPedidos.DataSource = bsNotasPedidos;
        }

        private void txtBuscar_Enter(object sender, EventArgs e)
        {
            if (txtBuscar.Text == "Filtrar por Número de Nota, Fecha de Emisión o Razón Social...")
            {
                txtBuscar.Font = new Font(txtBuscar.Font, FontStyle.Regular);
                txtBuscar.ForeColor = System.Drawing.Color.Black;
                txtBuscar.Text = "";
            }
        }        
    }
}
