﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formReporteCompPreArt : Form
    {
        public formReporteCompPreArt(BindingSource bs)
        {
            List<PreciosComparativos> precios = new List<PreciosComparativos>();
            InitializeComponent();
            foreach (DataRow item in ((DataTable)bs.DataSource).Rows)
            {
                PreciosComparativos oprecio = new PreciosComparativos();
                oprecio.Precio = Convert.ToInt32(item.ItemArray[0]);
                oprecio.Fecha = Convert.ToDateTime(item.ItemArray[1]);
                oprecio.Razonsocial = item.ItemArray[2].ToString();
                precios.Add(oprecio);
            }
            this.PreciosComparativosBindingSource.DataSource = precios;
        }

        private void formReporteCompPreArt_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
    public class PreciosComparativos
    {
        private int _precio;

        public int Precio
        {
            get { return _precio; }
            set { _precio = value; }
        }
        private DateTime _fecha;

        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
        private string _razonsocial;

        public string Razonsocial
        {
            get { return _razonsocial; }
            set { _razonsocial = value; }
        }
    }
}
