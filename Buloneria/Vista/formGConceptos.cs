﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formGConceptos : Form
    {
        public formGConceptos()
        {
            InitializeComponent();            
            this.dgvConceptos.AutoGenerateColumns = false;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            formAConcepto formaconcepto = new formAConcepto();
            formaconcepto.FormClosed += new FormClosedEventHandler(formaconcepto_FormClosed);
            formaconcepto.Show();
        }

        void formaconcepto_FormClosed(object sender, FormClosedEventArgs e)
        {
            ActualizarGrilla();
        }

        private BindingSource bsConcep;
        private void formGConceptos_Load(object sender, EventArgs e)
        {
            this.bsConcep = new BindingSource();
            CargarGrilla();

        }

        private void CargarGrilla()
        {
            bsConcep.DataSource = Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().RecuperarConceptos();
            dgvConceptos.DataSource = null;
            dgvConceptos.DataSource = bsConcep;
        }

        private void ActualizarGrilla()
        {
            dgvConceptos.DataSource = null;
            CargarGrilla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Entidades.Sistema.Concepto oConcepto = (Entidades.Sistema.Concepto)bsConcep.Current;
            Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().EliminarConcepto(oConcepto);
            ActualizarGrilla();
        }

        

      
    }
}
