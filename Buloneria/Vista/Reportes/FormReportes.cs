﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
namespace Vista.Reportes
{
    public partial class FormReportes : Form
    {
        public FormReportes(string reporte, BindingSource binding, ReportParameter[] Parametros=null)
        {
            InitializeComponent();
            Microsoft.Reporting.WinForms.ReportDataSource oReporte = new Microsoft.Reporting.WinForms.ReportDataSource();
            switch (reporte)
            {
                case "OC":
                    oReporte.Name = "ConjuntoDatosOrdenCompra";
                    oReporte.Value = binding;
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "Vista.Reportes.ReporteOrdenCompra.rdlc";
                    reportViewer1.LocalReport.SetParameters(Parametros);
                    this.reportViewer1.LocalReport.DataSources.Add(oReporte);
                    break;
                case "ResumenCuenta":
                    oReporte.Name = "Movimientos";
                    oReporte.Value = binding;
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "Vista.Reportes.ReporteCtaCte.rdlc";
                    reportViewer1.LocalReport.SetParameters(Parametros);
                    this.reportViewer1.LocalReport.DataSources.Add(oReporte);
                    break;
                case "NotaPedido":
                    oReporte.Name = "LineasNP";
                    oReporte.Value = binding;
                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "Vista.Reportes.ReporteNotaPedido.rdlc";
                    reportViewer1.LocalReport.SetParameters(Parametros);
                    this.reportViewer1.LocalReport.DataSources.Add(oReporte);
                    break;
            }
        }

        private void FormReportes_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
    }
}
