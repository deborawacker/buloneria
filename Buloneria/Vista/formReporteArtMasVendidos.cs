﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formReporteArtMasVendidos : Form
    {
        public formReporteArtMasVendidos(BindingSource bs)
        {
            List<ArtMasVendidos> art = new List<ArtMasVendidos>();
            InitializeComponent();
            foreach (DataRow item in ((DataTable)bs.DataSource).Rows)
            {
                ArtMasVendidos artmasven = new ArtMasVendidos();
                artmasven.Cantvend = Convert.ToInt32(item.ItemArray[0]);
                artmasven.Desc = item.ItemArray[1].ToString();
                artmasven.Codigoart= item.ItemArray[2].ToString();
                artmasven.Fecha = Convert.ToDateTime(item.ItemArray[3].ToString());
                art.Add(artmasven);
            }
            this.ArtMasVendidosBindingSource.DataSource = art;
        }

        private void formReporteArtMasVendidos_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
    public class ArtMasVendidos
    {
        private int _cantvend;

        public int Cantvend
        {
            get { return _cantvend; }
            set { _cantvend = value; }
        }
        private string _desc;

        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }
        private string _codigoart;

        public string Codigoart
        {
            get { return _codigoart; }
            set { _codigoart = value; }
        }
        private DateTime _fecha;

        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
    }
}
