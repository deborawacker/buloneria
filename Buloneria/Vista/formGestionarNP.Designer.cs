﻿namespace Vista
{
    partial class formGestionarNP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnVerHistorial = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.dgvNotasPedidos = new System.Windows.Forms.DataGridView();
            this.colNroNP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotasPedidos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(12, 12);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 47);
            this.btnConsultar.TabIndex = 0;
            this.btnConsultar.Tag = "Consulta";
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(93, 12);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 47);
            this.btnImprimir.TabIndex = 1;
            this.btnImprimir.Tag = "Imprimir";
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnVerHistorial
            // 
            this.btnVerHistorial.Location = new System.Drawing.Point(174, 12);
            this.btnVerHistorial.Name = "btnVerHistorial";
            this.btnVerHistorial.Size = new System.Drawing.Size(75, 47);
            this.btnVerHistorial.TabIndex = 2;
            this.btnVerHistorial.Tag = "VerHistorial";
            this.btnVerHistorial.Text = "Ver Historial";
            this.btnVerHistorial.UseVisualStyleBackColor = true;
            this.btnVerHistorial.Click += new System.EventHandler(this.btnVerHistorial_Click_1);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(255, 12);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 47);
            this.btnEliminar.TabIndex = 3;
            this.btnEliminar.Tag = "Baja";
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Location = new System.Drawing.Point(337, 36);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(43, 13);
            this.lblBuscar.TabIndex = 4;
            this.lblBuscar.Text = "Buscar:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.ForeColor = System.Drawing.Color.DarkGray;
            this.txtBuscar.Location = new System.Drawing.Point(386, 29);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(381, 20);
            this.txtBuscar.TabIndex = 5;
            this.txtBuscar.Text = "Filtrar por Número de Nota, Fecha de Emisión o Razón Social...";
            this.txtBuscar.Click += new System.EventHandler(this.txtBuscar_Click);
            this.txtBuscar.Enter += new System.EventHandler(this.txtBuscar_Enter);
            this.txtBuscar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtBuscar_KeyUp);
            this.txtBuscar.Leave += new System.EventHandler(this.txtBuscar_Leave);
            // 
            // dgvNotasPedidos
            // 
            this.dgvNotasPedidos.AllowUserToAddRows = false;
            this.dgvNotasPedidos.AllowUserToDeleteRows = false;
            this.dgvNotasPedidos.AllowUserToResizeColumns = false;
            this.dgvNotasPedidos.AllowUserToResizeRows = false;
            this.dgvNotasPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNotasPedidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNroNP,
            this.colCli,
            this.colEmision});
            this.dgvNotasPedidos.Location = new System.Drawing.Point(12, 65);
            this.dgvNotasPedidos.MultiSelect = false;
            this.dgvNotasPedidos.Name = "dgvNotasPedidos";
            this.dgvNotasPedidos.ReadOnly = true;
            this.dgvNotasPedidos.RowHeadersVisible = false;
            this.dgvNotasPedidos.ShowCellErrors = false;
            this.dgvNotasPedidos.ShowCellToolTips = false;
            this.dgvNotasPedidos.ShowEditingIcon = false;
            this.dgvNotasPedidos.ShowRowErrors = false;
            this.dgvNotasPedidos.Size = new System.Drawing.Size(754, 409);
            this.dgvNotasPedidos.TabIndex = 6;
            this.dgvNotasPedidos.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvNotasPedidos_DataBindingComplete);
            // 
            // colNroNP
            // 
            this.colNroNP.DataPropertyName = "NroNotaPedido";
            this.colNroNP.HeaderText = "Nro. NP";
            this.colNroNP.Name = "colNroNP";
            this.colNroNP.ReadOnly = true;
            // 
            // colCli
            // 
            this.colCli.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colCli.HeaderText = "Cliente";
            this.colCli.Name = "colCli";
            this.colCli.ReadOnly = true;
            // 
            // colEmision
            // 
            this.colEmision.DataPropertyName = "FechaEmision";
            this.colEmision.HeaderText = "Emision";
            this.colEmision.Name = "colEmision";
            this.colEmision.ReadOnly = true;
            this.colEmision.Width = 200;
            // 
            // formGestionarNP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 486);
            this.Controls.Add(this.dgvNotasPedidos);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnVerHistorial);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnConsultar);
            this.Name = "formGestionarNP";
            this.Tag = "NotasPedidos";
            this.Text = "Gestionar Notas de Pedidos";
            this.Load += new System.EventHandler(this.fromGestionarNP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNotasPedidos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnVerHistorial;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridView dgvNotasPedidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNroNP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmision;
    }
}