﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAConcepto : Form
    {
        public formAConcepto()
        {
            InitializeComponent();
            cmbTipoMov.DataSource = Enum.GetValues(typeof(Entidades.Sistema.TipoMovimiento)); //aca traigo al combo los valores que tiene mi tipo de movimiento
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                Entidades.Sistema.Concepto oConcepto = new Entidades.Sistema.Concepto();
                oConcepto.Concepto1 = txtConcepto.Text;
                oConcepto.Descripcion = txtDescripcion.Text;
                oConcepto.TipoMovimiento = (Entidades.Sistema.TipoMovimiento)cmbTipoMov.SelectedItem;
                
                if(Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().AgregarConcepto(oConcepto) == true)
                {
                    MessageBox.Show("Concepto agregado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Concepto existente","ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtConcepto.Focus();
                    return;
                }        
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {
            if (txtConcepto.Text == null) return false;
            if (txtDescripcion.Text == null) return false;
            if (txtConcepto.Text == "") return false;
            if (txtDescripcion.Text == "") return false;
            return true;
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
