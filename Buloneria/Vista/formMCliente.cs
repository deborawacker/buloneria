﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formMCliente : Form
    {
        private Entidades.Sistema.Cliente _cliente;

        public formMCliente(Entidades.Sistema.Cliente cliente) //aca paso como parametro del form gestionar clientes
        {
            _cliente = cliente;
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formMCliente_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _cliente.Celular;
            txtCodigo.Text = _cliente.CodCliente.ToString();
            txtCodigoPostal.Text = _cliente.CodPostal.ToString();
            txtCuit.Text = _cliente.Cuit.ToString();
            txtDireccion.Text = _cliente.Direccion;
            txtEmail.Text = _cliente.Email;
            txtFax.Text = _cliente.Fax;
            txtNombreFantasia.Text = _cliente.NombreFantasia;
            txtRazonSocial.Text = _cliente.RazonSocial;
            txtTelefono.Text = _cliente.Telefono;
            cmbCiudad.DataSource = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarProvincias();
            cmbCiudad.DisplayMember = "Ciudades.Nombre";
            cmbCiudad.Text = _cliente.Ciudad.Nombre;
            cmbProvincia.DataSource = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarProvincias();
            cmbProvincia.DisplayMember = "Nombre";
            cmbProvincia.Text = _cliente.Ciudad.Provincia.Nombre;
            cmbTipoIva.DataSource = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarTiposIva();
            cmbTipoIva.DisplayMember = "Nombre";
            cmbTipoIva.Text = _cliente.TipoIva.Nombre;        
            
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                _cliente.Celular = txtCelular.Text;
                _cliente.Ciudad = (Entidades.Sistema.Ciudad)cmbCiudad.SelectedItem;
                _cliente.CodPostal = Convert.ToInt32(txtCodigoPostal.Text);
                _cliente.Cuit = Convert.ToInt64(txtCuit.Text);
                _cliente.Direccion = txtDireccion.Text;
                _cliente.Email = txtEmail.Text;
                _cliente.Fax = txtFax.Text;
                _cliente.NombreFantasia = txtNombreFantasia.Text;
                _cliente.RazonSocial = txtRazonSocial.Text;
                _cliente.Telefono = txtTelefono.Text;
                _cliente.TipoIva = (Entidades.Sistema.TipoIva)cmbTipoIva.SelectedItem;

                if (Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().ModificarCliente(_cliente) == true)
                {
                    MessageBox.Show("Cliente modificado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }   
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {
            if (txtCodigoPostal.Text == null) return false;
            if (txtCuit.Text == null) return false;
            if (txtDireccion.Text == null) return false;
            if (txtNombreFantasia.Text == null) return false;
            if (txtRazonSocial.Text == null) return false;
            if (txtTelefono.Text == null) return false;
            if (txtCodigoPostal.Text == "") return false;
            if (txtCuit.Text == "") return false;
            if (txtDireccion.Text == "") return false;
            if (txtNombreFantasia.Text == "") return false;
            if (txtRazonSocial.Text == "") return false;
            if (txtTelefono.Text == "") return false;
            return true;
        }
    }
}
