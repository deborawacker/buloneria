﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formInformes : Form
    {
        public formInformes(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();        
            
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

       

        private void btnArticulos_Click(object sender, EventArgs e)
        {
            btnGenRepFaltantes.Enabled = true;
            btnGenRepArt.Enabled = true;
            btnGenRepMontos.Enabled = false;
            btnGenRepPrecios.Enabled = false;
            
            dgvInformesGerenciales.DataSource = null;
            gbInfArticulos.Visible = true;
            gbInfClientes.Visible = false;
            gbInfProveedores.Visible = false;
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            btnGenRepFaltantes.Enabled = false;
            btnGenRepArt.Enabled = false;
            btnGenRepMontos.Enabled = true;
            btnGenRepPrecios.Enabled = false;

            dgvInformesGerenciales.DataSource = null;
            gbInfClientes.Visible = true;
            gbInfClientes.Location = new Point(149,3);
            gbInfArticulos.Visible = false;
            gbInfProveedores.Visible = false;
        }

        private void formInformes_Load(object sender, EventArgs e)
        {
            gbInfArticulos.Visible = false;
            gbInfClientes.Visible = false;
            gbInfProveedores.Visible = false;   
            
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            btnGenRepFaltantes.Enabled = false;
            btnGenRepArt.Enabled = false;
            btnGenRepMontos.Enabled = false;
            btnGenRepPrecios.Enabled = true;

            dgvInformesGerenciales.DataSource = null;
            gbInfProveedores.Visible = true;
            gbInfProveedores.Location = new Point(149, 3);
            gbInfArticulos.Visible = false;
            gbInfClientes.Visible = false;
            cmbSeleccionarArt.DataSource = Controladora.Sistema.ControladoraCUInformes.ObtenerInstancia().RecuperarArticulos();
            cmbSeleccionarArt.DisplayMember = "Descripcion";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //boton mostrar articulos con faltantes de stock
        private BindingSource bsArtFaltStock; //creo un bindingsource
        private void btnMostrarArtStock_Click(object sender, EventArgs e)
        {
            btnGenRepArt.Enabled = false;
            btnGenRepFaltantes.Enabled = true;

            bsArtFaltStock = new BindingSource();
            bsArtFaltStock.DataSource = Controladora.Sistema.ControladoraCUInformes.ObtenerInstancia().RecuperarArticulosFaltStock();
            dgvInformesGerenciales.DataSource = null;
            dgvInformesGerenciales.DataSource = bsArtFaltStock; //cargo grilla
        }

        //boton mostrar periodo de articulos mas vendidos
        private BindingSource bsArtMasVendidos; //creo un bindingsource
        private void btnMostrarPeriodoArt_Click(object sender, EventArgs e)
        {
            btnGenRepArt.Enabled = true;
            btnGenRepFaltantes.Enabled = false;
            
            bsArtMasVendidos = new BindingSource();
            bsArtMasVendidos.DataSource = Controladora.Sistema.ControladoraCUInformes.ObtenerInstancia().RecuperarArtMasVendidos(dtpFechaDesdeArt.Value,dtpFechaHastaArt.Value);
            dgvInformesGerenciales.DataSource = null;
            dgvInformesGerenciales.DataSource = bsArtMasVendidos; //cargo grilla           
            
        }

       
        //boton mostrar periodo de montos compras mes clientes
        private BindingSource bsMontoMesCli; //creo un bindingsource
        private void btnMostrarPeriodoCli_Click(object sender, EventArgs e)
        {
            bsMontoMesCli = new BindingSource();
            bsMontoMesCli.DataSource = Controladora.Sistema.ControladoraCUInformes.ObtenerInstancia().RecuperarMontosComprasMesClientes(dtpFechaDesdeCli.Value, dtpFechaHastaCli.Value);
            dgvInformesGerenciales.DataSource = null;
            dgvInformesGerenciales.DataSource = bsMontoMesCli; //cargo grilla            
        }

        //boton mostrar precios articulo
        private BindingSource bsPreCompArt;
        private void btnMostrarPreciosArt_Click(object sender, EventArgs e)
        {
            bsPreCompArt = new BindingSource();
            Entidades.Sistema.Articulo oart = (Entidades.Sistema.Articulo)cmbSeleccionarArt.SelectedItem; //guardo el articulo seleccionado
            bsPreCompArt.DataSource = Controladora.Sistema.ControladoraCUInformes.ObtenerInstancia().RecuperarPreciosComparativosArt(oart.CodArticulo); //y le mando el articulo.codarticulo que es el varchar
            dgvInformesGerenciales.DataSource = null;
            dgvInformesGerenciales.DataSource = bsPreCompArt; //cargo grilla   
        }      

        private void btnGenRepPrecios_Click(object sender, EventArgs e)
        {
            if (dgvInformesGerenciales.RowCount == 0)
            {
                MessageBox.Show("GRILLA VACIA: Debe seleccionar un informe.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                formReporteCompPreArt oForm = new formReporteCompPreArt(bsPreCompArt);
                oForm.Show();
            }
        }

        private void btnGenRepArt_Click(object sender, EventArgs e)
        {
            if (dgvInformesGerenciales.RowCount == 0)
            {
                MessageBox.Show("GRILLA VACIA: Debe seleccionar un informe.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                formReporteArtMasVendidos oForm = new formReporteArtMasVendidos(bsArtMasVendidos);
                oForm.Show();
            }
        }

        private void btnGenRepFaltantes_Click(object sender, EventArgs e)
        {
            if (dgvInformesGerenciales.RowCount == 0) 
            {
                MessageBox.Show("GRILLA VACIA: Debe seleccionar un informe.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                formReporteFaltanteStock oForm = new formReporteFaltanteStock(bsArtFaltStock);
                oForm.Show();
            }
        }

        private void btnGenRepMontos_Click(object sender, EventArgs e)
        {
            if (dgvInformesGerenciales.RowCount == 0)
            {
                MessageBox.Show("GRILLA VACIA: Debe seleccionar un informe.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                formReporteMontoCompraCli oForm = new formReporteMontoCompraCli(bsMontoMesCli);
                oForm.Show();
            }
        }

    }
}

