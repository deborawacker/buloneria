﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAArticulo : Form
    {
        private string _operacion;
       
        public formAArticulo(string operacion)
        {
            _operacion = operacion;
            InitializeComponent();
        }

        Entidades.Sistema.Articulo _Articulo;

        public formAArticulo(Entidades.Sistema.Articulo oArticulo, string operacion)  //constructor recargado
        {
            InitializeComponent();
            _Articulo = oArticulo;
            _operacion = operacion;
            if (operacion == "Modificar") PrepararFormModificar();
            if (operacion == "Consultar") PrepararFormConsultar();
            if (operacion == "Eliminar") PrepararFormEliminar();
        }

        private void PrepararFormModificar()
        {
            this.Text = "Modificar Articulo";
            this.txtCodigo.Text = _Articulo.CodArticulo;
            this.txtDescripcion.Text = _Articulo.Descripcion;
            this.txtCantidadMinima.Text = _Articulo.CantMinima.ToString();
            this.txtCantidadActual.Text = _Articulo.CantActual.ToString();
            this.txtPrecioVenta.Text = _Articulo.PrecioVenta.ToString();
            this.txtCodigo.Enabled = false;
            this.txtCantidadActual.Enabled = false;
            this.cmbIva.Enabled = false;
            this.cmbGrupoArticulos.Enabled = false;
            foreach (object item in _Articulo.Proveedores)
            {
                lstProveedoresSeleccionados.Items.Add(item);
                lstProveedoresSeleccionados.DisplayMember = "RazonSocial";

            }
        }

        private void PrepararFormConsultar() 
        {
            this.Text = "Consultar Articulo";
            this.txtCodigo.Text = _Articulo.CodArticulo;
            this.txtDescripcion.Text = _Articulo.Descripcion;
            this.txtCantidadMinima.Text = _Articulo.CantMinima.ToString();
            this.txtCantidadActual.Text = _Articulo.CantActual.ToString();
            this.txtPrecioVenta.Text = _Articulo.PrecioVenta.ToString();
            this.txtCodigo.Enabled = false;
            this.txtDescripcion.Enabled = false;
            this.txtCantidadMinima.Enabled = false;
            this.txtCantidadActual.Enabled = false;
            this.txtPrecioVenta.Enabled = false;
            this.cmbGrupoArticulos.Enabled = false;
            this.cmbIva.Enabled = false;
            this.cmbProveedor.Enabled = false;
            this.lstProveedoresSeleccionados.Enabled = false;
            this.btnAgregarProveedor.Enabled = false;
            this.btnQuitarProveedor.Enabled = false;            
            foreach (object item in _Articulo.Proveedores)
            {
                lstProveedoresSeleccionados.Items.Add(item);
                lstProveedoresSeleccionados.DisplayMember = "RazonSocial";
            }
            this.btnAceptar.Visible = false;
            this.btnCancelar.Text = "Cerrar";
            this.btnAgregarProveedor.Enabled = false;
            this.btnQuitarProveedor.Enabled = false;
        }

        private void PrepararFormEliminar()
        {
            this.Text = "Eliminar Articulo";
            this.txtCodigo.Text = _Articulo.CodArticulo;
            this.txtDescripcion.Text = _Articulo.Descripcion;
            this.txtCantidadMinima.Text = _Articulo.CantMinima.ToString();
            this.txtCantidadActual.Text = _Articulo.CantActual.ToString();
            this.txtPrecioVenta.Text = _Articulo.PrecioVenta.ToString();
            this.txtCodigo.Enabled = false;
            this.txtDescripcion.Enabled = false;
            this.txtCantidadMinima.Enabled = false;
            this.txtCantidadActual.Enabled = false;
            this.txtPrecioVenta.Enabled = false;
            this.cmbGrupoArticulos.Enabled = false;
            this.cmbIva.Enabled = false;
            this.cmbProveedor.Enabled = false;
            this.lstProveedoresSeleccionados.Enabled = false;
            this.btnAgregarProveedor.Enabled = false;
            this.btnQuitarProveedor.Enabled = false;            
            foreach (object item in _Articulo.Proveedores)
            {
                lstProveedoresSeleccionados.Items.Add(item);
                lstProveedoresSeleccionados.DisplayMember = "RazonSocial";
            }
        }

        List<Entidades.Sistema.Alicuota> alicuota;
        List<Entidades.Sistema.GrupoArticulo> grupoarticulo;
        List<Entidades.Sistema.Proveedor> proveedor;

        //evento load, cargo combos
        private void formAArticulo_Load(object sender, EventArgs e)
        {            
            alicuota = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarAlicuotas();
            grupoarticulo = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarGruposArticulos();
            proveedor = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarProveedores();
            CargarCombos();
        }

        //creo metodo cargar combos
        private void CargarCombos()
        {
            cmbGrupoArticulos.DataSource = grupoarticulo; //data source es el origen de los datos
            cmbGrupoArticulos.DisplayMember = "Valor"; //display member muestra en el combo "Valor", que es el atributo de la clase grupoarticulos
            cmbIva.DataSource = alicuota;
            cmbIva.DisplayMember = "Valor";
            cmbProveedor.DataSource = proveedor;
            cmbProveedor.DisplayMember = "RazonSocial";
        }
         
        //evento click del aceptar, paso todo lo que hay en los text
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                if (_operacion == "Agregar")
                {
                    Entidades.Sistema.Articulo oArticulo = new Entidades.Sistema.Articulo();
                    oArticulo.CantActual = 0; // Convert.ToInt32(txtCantidadActual.Text); //convierto a entero lo que hay en el txt porque cantactual es entero
                    oArticulo.CantMinima = Convert.ToInt32(txtCantidadMinima.Text);
                    oArticulo.PrecioVenta = Convert.ToInt32(txtPrecioVenta.Text);
                    oArticulo.CodArticulo = txtCodigo.Text; //lo que esta en el txt es string y lo que esta en codarticulo es string
                    oArticulo.Descripcion = txtDescripcion.Text;
                    oArticulo.Alicuota = (Entidades.Sistema.Alicuota)cmbIva.SelectedItem;
                    oArticulo.Grupo = (Entidades.Sistema.GrupoArticulo)cmbGrupoArticulos.SelectedItem;
                    foreach (object item in lstProveedoresSeleccionados.Items)  //aca le digo que para cada item del listbox...
                    {
                        oArticulo.AgregarProveedor((Entidades.Sistema.Proveedor)item);  //...le agregue el proveedor al articulo, que esta guardado en item, y como es de _TipoMovimiento object lo tengo que castear: "entidades.sistema.proveedor", le digo que sea de _TipoMovimiento proveedor
                    }
                    if (Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().AgregarArticulo(oArticulo) == true)
                    {
                        MessageBox.Show("Artículo agregado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Artículo existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCodigo.Focus();
                        return;
                    }
                }
                if (_operacion == "Modificar")
                {
                    _Articulo.CodArticulo = txtCodigo.Text;
                    _Articulo.Descripcion = txtDescripcion.Text;
                    _Articulo.CantMinima = Convert.ToInt32(txtCantidadMinima.Text);
                    _Articulo.CantActual = Convert.ToInt32(txtCantidadActual.Text);
                    _Articulo.PrecioVenta = Convert.ToInt32(txtPrecioVenta.Text);
                    cmbGrupoArticulos.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarGruposArticulos();
                    cmbGrupoArticulos.DisplayMember = "descripcion";
                    cmbIva.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarAlicuotas();
                    cmbIva.DisplayMember = "alicuota";
                    cmbProveedor.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarProveedores();
                    cmbProveedor.DisplayMember = "razonSocial";
                    if (Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().ModificarArticulo(_Articulo) == true)
                    {
                        MessageBox.Show("Articulo modificado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Articulo existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtDescripcion.Focus();
                        return;
                    }
                }
                if (_operacion == "Eliminar")
                {
                    _Articulo.CodArticulo = txtCodigo.Text;
                    _Articulo.Descripcion = txtDescripcion.Text;
                    _Articulo.CantMinima = Convert.ToInt32(txtCantidadMinima.Text);
                    _Articulo.CantActual = Convert.ToInt32(txtCantidadActual.Text);
                    _Articulo.PrecioVenta = Convert.ToInt32(txtPrecioVenta.Text);
                    cmbGrupoArticulos.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarGruposArticulos();
                    cmbGrupoArticulos.DisplayMember = "descripcion";
                    cmbIva.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarAlicuotas();
                    cmbIva.DisplayMember = "alicuota";
                    cmbProveedor.DataSource = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarProveedores();
                    cmbProveedor.DisplayMember = "razonSocial";

                    DialogResult respuesta = MessageBox.Show("Desea eliminar el articulo?", "Eliminar", MessageBoxButtons.YesNo);
                    if (respuesta == System.Windows.Forms.DialogResult.Yes)
                    {
                        bool resultado = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().EliminarArticulo(_Articulo);
                        if (resultado)
                        {
                            MessageBox.Show("El articulo se elimino exitosamente.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Ha ocurrido un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }
                    }
                }
            }
            else MessageBox.Show("Complete todos los datos");
        }

        //metodo validar datos
        private bool ValidarDatos()
        {
            if (txtCodigo.Text == null) return false;
            if (txtDescripcion.Text == null) return false;
            if (txtCantidadMinima.Text == null) return false;          
            if (txtPrecioVenta.Text == null) return false;
            if (txtCodigo.Text == "") return false;
            if (txtDescripcion.Text == "") return false;
            if (txtCantidadMinima.Text == "") return false;          
            if (txtPrecioVenta.Text == "") return false;
            return true;
        }

        //evento click del boton agregar proveedor
        private void btnAgregarProveedor_Click(object sender, EventArgs e)
        {
            if (_operacion != "Agregar") _Articulo.AgregarProveedor((Entidades.Sistema.Proveedor)cmbProveedor.SelectedItem);
                lstProveedoresSeleccionados.DisplayMember = "RazonSocial";
                lstProveedoresSeleccionados.Items.Add(cmbProveedor.SelectedItem); //aca le digo que agregue al listbox lo que esta seleccionado en el item del combo
            
        }

        private void btnQuitarProveedor_Click(object sender, EventArgs e)
        {
            if (_operacion != "Agregar") _Articulo.EliminarProveedor((Entidades.Sistema.Proveedor)cmbProveedor.SelectedItem);
            lstProveedoresSeleccionados.Items.Remove(lstProveedoresSeleccionados.SelectedItem);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }   
    }
}
