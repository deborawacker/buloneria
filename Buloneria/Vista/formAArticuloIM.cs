﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAArticuloIM : Form
    {

        Entidades.Sistema.LineaOrdenCompra _linea;
        Entidades.Sistema.FaltanteDeOrdenCompra _faltante;
        public formAArticuloIM(Entidades.Sistema.LineaOrdenCompra linea)
        {
            _linea = linea;
            InitializeComponent();
            this.txtDescripcion.Text = _linea.Articulo.Descripcion;
            this.txtCantidad.Text = _linea.CantPedir.ToString();
            this.txtPrecioUnitario.Text = _linea.PrecioUnitario.ToString();
        }

        public formAArticuloIM(Entidades.Sistema.LineaOrdenCompra linea, Entidades.Sistema.FaltanteDeOrdenCompra faltante)
        {
            _faltante = faltante;
            _linea = linea;
            InitializeComponent();
            this.txtDescripcion.Text = _linea.Articulo.Descripcion;
            this.txtCantidad.Text = _linea.CantPedir.ToString();
            this.txtPrecioUnitario.Text = _linea.PrecioUnitario.ToString();
        }

                
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

        Entidades.Sistema.Faltante artfalt;
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                if (_linea.CantPedir >= Convert.ToInt32(this.txtCantidadRecibida.Text)) //comparo la cantpedir de la lineaoc con la cantrecibida
                {
                    if (_faltante == null)
                    {
                        artfalt = new Entidades.Sistema.Faltante(); //aca creo mi articulofaltante llamado artfalt
                        artfalt.LineaOrdenCompra = _linea;
                        artfalt.CantFaltante = (_linea.CantPedir - Convert.ToInt32(this.txtCantidadRecibida.Text)); //aca le agrego a mi cantfaltante del artfalt la diferencia entre cantpedir me la recibida
                        _linea.Articulo.CantActual = _linea.Articulo.CantActual + Convert.ToInt32(this.txtCantidadRecibida.Text);

                        Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().ModificarArticulo(_linea.Articulo);
                        this.Close();
                        DialogResult = System.Windows.Forms.DialogResult.Yes;
                    }
                    else
                    {
                        Entidades.Sistema.Faltante oartfaltante = _faltante.ColFaltantes.Find(x => x.LineaOrdenCompra.Articulo.CodArticulo == _linea.Articulo.CodArticulo);
                        if (oartfaltante != null)
                        {
                            if (oartfaltante.CantFaltante < Convert.ToInt32(this.txtCantidadRecibida.Text))
                            {
                                MessageBox.Show("La cantidad ingresada no puede ser mayor a la cantidad faltante");
                            }
                            else
                            {
                                artfalt = oartfaltante;
                                artfalt.CantFaltante = (artfalt.CantFaltante - Convert.ToInt32(this.txtCantidadRecibida.Text));
                                _linea.Articulo.CantActual = _linea.Articulo.CantActual + Convert.ToInt32(this.txtCantidadRecibida.Text);

                                Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().ModificarArticulo(_linea.Articulo);
                                this.Close();
                                DialogResult = System.Windows.Forms.DialogResult.Yes;
                    
                            }
                        }
                        else
                        {
                            artfalt = new Entidades.Sistema.Faltante();
                            artfalt.CantFaltante = (_linea.CantPedir - Convert.ToInt32(this.txtCantidadRecibida.Text) );
                            _linea.Articulo.CantActual = _linea.Articulo.CantActual + Convert.ToInt32(this.txtCantidadRecibida.Text);
                            artfalt.LineaOrdenCompra = _linea;
                            _faltante.AgregarFaltante(artfalt);
                            Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().ModificarArticulo(_linea.Articulo);
                            this.Close();
                            DialogResult = System.Windows.Forms.DialogResult.Yes;
                        }
                    }
                    
                }
                else
                {
                    MessageBox.Show("La cantidad ingresada del articulo no puede ser mayor a la pedida en la orden de compra.");
                    this.Close();
                    DialogResult = System.Windows.Forms.DialogResult.No;
                }
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {
            if (txtCantidadRecibida.Text == null) return false;
            if (txtCantidadRecibida.Text == "") return false;
            else return true;
        }

        internal Entidades.Sistema.Articulo RetornarArticulo()
        {
            return _linea.Articulo;
        }
        internal int RetornarCantidadEntregada()
        {
            return Convert.ToInt32(this.txtCantidadRecibida.Text);
        }
        internal Entidades.Sistema.Faltante RetornarArticuloFaltante() //aca devuelvo mi articulo faltante
        {
            return artfalt;
        }
        private void formAArticuloIM_Load(object sender, EventArgs e)
        {

        }
    }
}
