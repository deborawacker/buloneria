﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
  
    public partial class formCProveedor : Form
    {
        private Entidades.Sistema.Proveedor _proveedor;
        public formCProveedor(Entidades.Sistema.Proveedor proveedor)
        {
            _proveedor = proveedor;
            InitializeComponent();
        }

        private void formCProveedor_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _proveedor.Celular;
            txtCodigo.Text = _proveedor.CodProveedor.ToString();
            txtCodigoPostal.Text = _proveedor.CodPostal.ToString();
            txtCuit.Text = _proveedor.Cuit.ToString();
            txtDireccion.Text = _proveedor.Direccion;
            txtEmail.Text = _proveedor.Email;
            txtFax.Text = _proveedor.Fax;
            txtNombreFantasia.Text = _proveedor.NombreFantasia;
            txtRazonSocial.Text = _proveedor.RazonSocial;
            txtTelefono.Text = _proveedor.Telefono;
            cmbProvincia.Text = _proveedor.Ciudad.Provincia.Nombre;
            cmbCiudad.Text = _proveedor.Ciudad.Nombre;
            cmbTipoIva.Text = _proveedor.TipoIva.Nombre;       

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   
    }
}

