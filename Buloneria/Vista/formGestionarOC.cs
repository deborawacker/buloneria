﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGestionarOC : Form
    {
        private Entidades.Seguridad.Usuario oUsuario;

        public formGestionarOC(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles,Entidades.Seguridad.Usuario usuario)
        {
            oUsuario = usuario;
            InitializeComponent();
            this.dgvOrdenesCompras.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

        private BindingSource bsOrdenesCompras;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bsOrdenesCompras.DataSource = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().FiltrarOrdenesCompras(txtBuscar.Text);
            dgvOrdenesCompras.DataSource = null;
            dgvOrdenesCompras.DataSource = bsOrdenesCompras;
        }       

        private void btnImprimir_Click(object sender, EventArgs e) //le voy pasando los parametros de la grilla seleccionada, para que arme el reporte
        {
            decimal totalsiniva = 0;
            foreach (Entidades.Sistema.LineaOrdenCompra item in ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).LineasOrdenCompra)
            {
                totalsiniva = totalsiniva + (item.CantPedir * item.PrecioUnitario); //aca caluco el total sin iva que lo voy a pasar como parametro
            }
            
            Microsoft.Reporting.WinForms.ReportParameter[] Parametros = new Microsoft.Reporting.WinForms.ReportParameter[7];  //instancio un arreglo de tamano 7
            Parametros[0] = new Microsoft.Reporting.WinForms.ReportParameter("Numero", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).NroOrdenCompra.ToString());
            Parametros[1] = new Microsoft.Reporting.WinForms.ReportParameter("RazonSocial", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).Proveedor.RazonSocial);
            Parametros[2] = new Microsoft.Reporting.WinForms.ReportParameter("FormaPago", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).FormaPago.TipoFormaPago);
            Parametros[3] = new Microsoft.Reporting.WinForms.ReportParameter("Estado", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).Estado.ToString());
            Parametros[4] = new Microsoft.Reporting.WinForms.ReportParameter("FechaEmision", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).FechaEmision.ToShortDateString()); 
            Parametros[5] = new Microsoft.Reporting.WinForms.ReportParameter("FechaPlazo", ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).FechaPlazo.ToShortDateString());
            Parametros[6] = new Microsoft.Reporting.WinForms.ReportParameter("total", totalsiniva.ToString());  //este es el total sin iva, se lo paso como parametro


            BindingSource bsLineas = new BindingSource();
            List<Entidades.Sistema.LineaOrdenCompra> ListaLineas = new List<Entidades.Sistema.LineaOrdenCompra>();
            ListaLineas = ((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current).LineasOrdenCompra;
            bsLineas.DataSource = ListaLineas;
            Reportes.FormReportes formReportes = new Reportes.FormReportes("OC", bsLineas, Parametros);
            formReportes.Show();
        }

        private void formGestionarOC_Load(object sender, EventArgs e)
        {
            this.bsOrdenesCompras = new BindingSource();
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            bsOrdenesCompras.DataSource = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarOrdenesCompras();
            dgvOrdenesCompras.DataSource = bsOrdenesCompras;
        }

        private void ActualizarGrilla()
        {
            dgvOrdenesCompras.DataSource = null;
            CargarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formConsultarOC formcoc = new formConsultarOC((Entidades.Sistema.OrdenCompra)bsOrdenesCompras.Current); //le paso el objeto actual seleccionado en la grilla
            formcoc.FormClosed += new FormClosedEventHandler(formcoc_FormClosed);
            formcoc.Show();
        }

        void formcoc_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        Entidades.Sistema.OrdenCompra orden;
        private void btnAnular_Click(object sender, EventArgs e)
        {
            orden = (Entidades.Sistema.OrdenCompra)dgvOrdenesCompras.CurrentRow.DataBoundItem;          
            bool respuesta = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().FinalizarConFaltante(orden,oUsuario);
            if (respuesta == true)
            {
                this.ActualizarGrilla();
            }
            else MessageBox.Show("No puede anular la orden de compra. Verifique que no esté previamente anulada o finalizada", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);            
        }

        private void btnVerHistorial_Click(object sender, EventArgs e)
        {
            bsOrdenesCompras.DataSource =null;
            bsOrdenesCompras.DataSource = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarHistorialOrdenesCompras();
            bsOrdenesCompras.ResetBindings(false);
        }

        private void dgvOrdenesCompras_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
