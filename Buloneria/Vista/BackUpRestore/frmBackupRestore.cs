﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace Vista.BackupRestore
{
    public partial class FrmBackupRestore : Form
    {
       public FrmBackupRestore()
        {
            InitializeComponent();
            this.CenterToScreen();
            this.cbBaseDatos.SelectedIndex = 0;
        }      


        private void btnBackup_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                Thread.Sleep(100);

                if (Controladora.BackupRestore.CtrlBackupRestore.ObtenerInstancia().GuardarBackup(cbBaseDatos.SelectedItem.ToString()))
                {
                    MessageBox.Show("El backup de la base de datos  se realizó con éxito", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            this.Close();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                Thread.Sleep(100);

                openFileDialogRestore.InitialDirectory = "C:\\Backup Buloneria";
                openFileDialogRestore.Filter = "Todo (*.*)|*.*|BAK (*.BAK)|*.BAK|MDF (*.mdf)|*.mdf|LDF (*.ldf)|*.ldf";
                openFileDialogRestore.FilterIndex = 1;
                openFileDialogRestore.FileName = "";
                openFileDialogRestore.Title = "Buloneria! :: Restore";
                openFileDialogRestore.RestoreDirectory = true;

                if (openFileDialogRestore.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string Ruta = openFileDialogRestore.FileName.ToString();
                        if (Controladora.BackupRestore.CtrlBackupRestore.ObtenerInstancia().Restore( cbBaseDatos.SelectedItem.ToString()))
                        {
                            MessageBox.Show("El restore de la base de datos se realizó con éxito. \n   Debe reiniciar la aplicación para visualizar los cambios", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        this.Close();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("No es un backup válido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
