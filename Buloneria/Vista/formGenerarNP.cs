﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGenerarNP : Form
    {
        public formGenerarNP(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();
            this.dgvArticulosDisponibles.AutoGenerateColumns = false;
            this.dgvArticulosSolicitados.AutoGenerateColumns = false;
            txtNumeroNotaPedido.Enabled = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

        List<Entidades.Sistema.FormaPago> formapago;
        List<Entidades.Sistema.Cliente> cliente;
        List<Entidades.Sistema.GrupoArticulo> grupoarticulo;        
        BindingSource bsArticulosGrupo;
        
        //evento load del form, tengo que recuperar y cargar combos
        private void formGenerarNP_Load(object sender, EventArgs e)
        {
            //deshabilito los botones agregar art,quitar art,aceptar
            btnAceptar.Enabled = false;
            btnAgregarArticulo.Enabled = false;
            btnQuitarArticulo.Enabled = false;
            
            formapago = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarFormasPago();
            cliente = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarClientes();
            grupoarticulo = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarGruposArticulos();           
            CargarCombos();
            bsArticulosGrupo = new BindingSource();
        }

        //creo el metodo cargarcombos
        private void CargarCombos()
        {
            cmbFormaPago.DataSource = formapago;
            cmbFormaPago.DisplayMember = "TipoFormaPago";
            cmbCliente.DataSource = cliente;
            cmbCliente.DisplayMember = "RazonSocial";
            cmbGrupoArticulos.DataSource = grupoarticulo;
            cmbGrupoArticulos.DisplayMember = "grupoArticulo";
        }

        //aca se carga la _fecha de hoy
        private void dtpFechaEmision_ValueChanged(object sender, EventArgs e)
        {            
        }

        private void btnSeleccionarCliente_Click(object sender, EventArgs e)
        {
            //deshabilito los combos forma de pago y cliente para que no pueda cambiarlos, y tambien el boton seleccionar cliente
            cmbCliente.Enabled = false;
            cmbFormaPago.Enabled = false;
            btnSeleccionarCliente.Enabled = false;
            onotapedido = new Entidades.Sistema.NotaPedido();
            onotapedido.FechaEmision = this.dtpFechaEmision.Value;
            onotapedido.FormaPago = (Entidades.Sistema.FormaPago)this.cmbFormaPago.SelectedItem;
            onotapedido.Cliente = (Entidades.Sistema.Cliente)this.cmbCliente.SelectedItem;            

        }

        Entidades.Sistema.NotaPedido onotapedido;
        private void btnSeleccionarGrupo_Click(object sender, EventArgs e)
        {            
            //habilito botones de agregar articulo, quitar articulo y aceptar, y deshabilito combos clientes y forma de pago y boton seleccionar cliente
            btnAgregarArticulo.Enabled = true;
            btnQuitarArticulo.Enabled = true;
            btnAceptar.Enabled = true;
            cmbCliente.Enabled = false;
            btnSeleccionarCliente.Enabled = false;
            cmbFormaPago.Enabled = false;

            bsArticulosGrupo.DataSource = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarArticulosGrupo((Entidades.Sistema.GrupoArticulo)cmbGrupoArticulos.SelectedItem); //el origen de articulos va a ser los articulos recuperados del grupo seleccionado
            CargarGrilla();
        }       

        private void CargarGrilla()
        {
            dgvArticulosDisponibles.DataSource = null;
            dgvArticulosDisponibles.DataSource = bsArticulosGrupo;
        }
                
        private void btnAgregarArticulo_Click_1(object sender, EventArgs e)
        {            
            formAArticuloNP oform = new formAArticuloNP((Entidades.Sistema.Articulo)this.bsArticulosGrupo.Current);
            DialogResult dr = oform.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.Yes) onotapedido.AgregarLineaNotaPedido(oform.RetornarLineaNP());
            dgvArticulosSolicitados.DataSource = null;
            dgvArticulosDisponibles.AutoGenerateColumns = false;
            dgvArticulosSolicitados.DataSource = onotapedido.LineasNotaPedido;
            CalcularTotales();
        }

        private void CalcularTotales()
        {
            decimal subtotaliva = 0;
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaNotaPedido linea in onotapedido.LineasNotaPedido)
            {
                int subtotallinea = linea.PrecioUnitario * linea.Cantidad;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;

            }
            txtSubtotalFinal.Text = subtotal.ToString();
            txtMontoIvaFinal.Text = subtotaliva.ToString();
            decimal total = subtotal + subtotaliva;
            txtTotalFinal.Text = total.ToString();

        }
        private void btnQuitarArticulo_Click_1(object sender, EventArgs e)
        {
            onotapedido.QuitarLineaNotaPedido((Entidades.Sistema.LineaNotaPedido)dgvArticulosSolicitados.CurrentRow.DataBoundItem);
            dgvArticulosSolicitados.DataSource = null;
            dgvArticulosSolicitados.DataSource = onotapedido.LineasNotaPedido;
            CalcularTotales();
        }
      
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click_1(object sender, EventArgs e)
        {
            if (onotapedido == null)
            {
                MessageBox.Show("");
            }
            else if (onotapedido.LineasNotaPedido.Count != 0)
            {
               
                bool resultado = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().AgregarNotaPedido(onotapedido);
                if (resultado) MessageBox.Show("Nota de pedido generada exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("Ha ocurrido un error.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            else MessageBox.Show("Ingrese productos a la NP", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        void dgvArticulosSolicitados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvArticulosSolicitados.Rows)
            {
                item.Cells["colSubTotal"].Value = Convert.ToInt32(item.Cells["colCantidadPedir"].Value) * Convert.ToInt32(item.Cells["colPrecio"].Value);
            }
        }

        private void dgvArticulosSolicitados_DataBindingComplete_1(object sender, DataGridViewBindingCompleteEventArgs e)
        {
           
            foreach (DataGridViewRow fila in this.dgvArticulosSolicitados.Rows)
            {
                object objeto = fila.DataBoundItem;
                Entidades.Sistema.LineaNotaPedido oLin = (Entidades.Sistema.LineaNotaPedido)objeto;
                fila.Cells["Articulo"].Value = oLin.Articulo.Descripcion;
                fila.Cells["colPrecioVenta"].Value = oLin.PrecioUnitario.ToString();
                fila.Cells["colSubtotal"].Value = (oLin.PrecioUnitario * oLin.Cantidad).ToString();
               
            }
            
        }        
    }
}
