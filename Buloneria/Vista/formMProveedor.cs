﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formMProveedor : Form
    {
        private Entidades.Sistema.Proveedor _proveedor;
        public formMProveedor(Entidades.Sistema.Proveedor proveedor)
        {
            _proveedor = proveedor;
            InitializeComponent();
        }

       

        private void formMProveedor_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _proveedor.Celular;
            txtCodigo.Text = _proveedor.CodProveedor.ToString();
            txtCodigoPostal.Text = _proveedor.CodPostal.ToString();
            txtCuit.Text = _proveedor.Cuit.ToString();
            txtDireccion.Text = _proveedor.Direccion;
            txtEmail.Text = _proveedor.Email;
            txtFax.Text = _proveedor.Fax;
            txtNombreFantasia.Text = _proveedor.NombreFantasia;
            txtRazonSocial.Text = _proveedor.RazonSocial;
            txtTelefono.Text = _proveedor.Telefono;
            cmbProvincia.DataSource = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
            cmbProvincia.DisplayMember = "Nombre";
            cmbProvincia.Text = _proveedor.Ciudad.Provincia.Nombre;
            cmbCiudad.DataSource = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
            cmbCiudad.DisplayMember = "Ciudades.Nombre";
            cmbCiudad.Text = _proveedor.Ciudad.Nombre;
            cmbTipoIva.DataSource = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva();
            cmbTipoIva.DisplayMember = "Nombre";
            cmbTipoIva.Text = _proveedor.TipoIva.Nombre;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos()) //aca por defecto es true
            {
                
                _proveedor.Celular = txtCelular.Text;
                _proveedor.Ciudad = (Entidades.Sistema.Ciudad)cmbCiudad.SelectedItem;
                _proveedor.CodPostal = Convert.ToInt32(txtCodigoPostal.Text);
                _proveedor.Cuit = Convert.ToInt64(txtCuit.Text);
                _proveedor.Direccion = txtDireccion.Text;
                _proveedor.Email = txtEmail.Text;
                _proveedor.Fax = txtFax.Text;
                _proveedor.NombreFantasia = txtNombreFantasia.Text;
                _proveedor.RazonSocial = txtRazonSocial.Text;
                _proveedor.Telefono = txtTelefono.Text;
                _proveedor.TipoIva = (Entidades.Sistema.TipoIva)cmbTipoIva.SelectedItem;

                if (Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().ModificarProveedor(_proveedor) == true)
                {
                    MessageBox.Show("Proveedor modificado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }               
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {            
            if (txtCodigoPostal.Text == null) return false;
            if (txtCuit.Text == null) return false;
            if (txtDireccion.Text == null) return false;
            if (txtNombreFantasia.Text == null) return false;
            if (txtRazonSocial.Text == null) return false;
            if (txtTelefono.Text == null) return false;
            if (txtCodigoPostal.Text == "") return false;
            if (txtCuit.Text == "") return false;
            if (txtDireccion.Text == "") return false;
            if (txtNombreFantasia.Text == "") return false;
            if (txtRazonSocial.Text == "") return false;
            if (txtTelefono.Text == "") return false;
            return true;
        }
    }
}
