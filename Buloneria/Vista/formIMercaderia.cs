﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formIMercaderia : Form
    {
        private Entidades.Sistema.FaltanteDeOrdenCompra Faltante; //creo mi objeto faltante de tipo faltante de oc y lo instancio en el load
        private Entidades.Seguridad.Usuario usuario;
        public formIMercaderia(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles, Entidades.Seguridad.Usuario usu)
        {
            usuario = usu;
            InitializeComponent();
            this.dgvArticulosAgregados.AutoGenerateColumns = false;
            this.dgvArticulosOC.AutoGenerateColumns = false;
            this.dgvArticulosFaltantes.AutoGenerateColumns = false;
            this.dgvArticulosOC.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosOC_DataBindingComplete);
            this.dgvArticulosAgregados.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosAgregados_DataBindingComplete);           
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

        void dgvArticulosAgregados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvArticulosAgregados.Rows)
            {
      //          item.Cells["colSubTotal"].Value = Convert.ToInt32(item.Cells["colCantidadPedir"].Value) * Convert.ToInt32(item.Cells["colPrecio"].Value);
            }
        }

        void dgvArticulosOC_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvArticulosOC.Rows)
            {
                item.Cells["colSubtotal"].Value = Convert.ToInt32(item.Cells["colCantPedir"].Value) * Convert.ToInt32(item.Cells["colPrecioUnitario"].Value);
            }
        }    

        private void btnCancelar(object sender, EventArgs e)
        {
            this.Close();
        }

        List<Entidades.Sistema.Proveedor> proveedores;
        //evento load del form, recupero y cargo combo proveedor
        private void formIMercaderia_Load(object sender, EventArgs e)
        {
            //que me deshabilite los botones de aceptar,seleccionar orden,agregar art y quitar articulo, y el combo selec orden
            btnAceptar.Enabled = false;
            //btnSeleccionarOrden.Enabled = false;
            btnAgregarArticulo.Enabled = false;
            btnQuitarArticulo.Enabled = false;
            //cmbOrdenCompras.Enabled = false;
            dgvArticulosFaltantes.DataBindingComplete += dgvArticulosFaltantes_DataBindingComplete;
            Faltante = new Entidades.Sistema.FaltanteDeOrdenCompra(); //instancio mi objeto faltante
            proveedores = Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().RecuperarProveedores();
            CargarComboProveedor();
            bsArticulosOC = new BindingSource();
        }

        void dgvArticulosFaltantes_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if(dgvArticulosFaltantes.Rows.Count != 0)
            {
                foreach (DataGridViewRow item in dgvArticulosFaltantes.Rows)
                {
                    Entidades.Sistema.Faltante art = (Entidades.Sistema.Faltante)item.DataBoundItem;
                    item.Cells["colDesArt"].Value = art.LineaOrdenCompra.Articulo.Descripcion;
                }   
            }
        }

        //creo metodo cargar combo prov
        private void CargarComboProveedor() 
        {
            /*cmbOrdenCompras.DataSource = null;
            cmbProveedor.DataSource = proveedores;
            cmbProveedor.DisplayMember = "razonSocial";*/
        }

        List<Entidades.Sistema.OrdenCompra> ordenes;
        //en el evento click del boton seleccionar proveedor, tengo que recuperar las ordenes de compras, que van a tener estado en curso y pendiente, y son de ese prov
        private void btnSeleccionarProveedor_Click(object sender, EventArgs e)
        {
            //que me habilite el boton seleccionar orden y el combo
            /*btnSeleccionarOrden.Enabled = true;
            cmbOrdenCompras.Enabled=true;*/

            //ordenes = Controladora.Sistema.ControladoraCUIngresarMercaderia.ObtenerInstancia().RecuperarOrdenesCompras((Entidades.Sistema.Proveedor)cmbProveedor.SelectedItem);
            CargarComboOrden();
        }

        //creo metodo cargar combo orden, con las ordenes de estado en curso y pendientes del prov seleccionado en el combo prov
        private void CargarComboOrden()
        {
            /*cmbOrdenCompras.DataSource = null;
            cmbOrdenCompras.DataSource = ordenes;
            cmbOrdenCompras.DisplayMember = "nroOrdenCompra";*/
        }

        BindingSource bsArticulosOC;
        private void CargarGrilla()
        {
           /* if (cmbOrdenCompras.Items.Count != 0)
            {
                dtpFechaRecepcion.Enabled = false;
                txtNumeroRemito.Enabled = false;
                dtpFechaRemito.Enabled = false;
                cmbProveedor.Enabled = false;
                btnSeleccionarProveedor.Enabled = false;
                cmbOrdenCompras.Enabled = false;
                btnSeleccionarOrden.Enabled = false;
            
                bsArticulosOC.DataSource = ((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).LineasOrdenCompra;
                Faltante.NroOrdenCompra = ((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).NroOrdenCompra;
                dgvArticulosOC.DataSource = null;
                dgvArticulosOC.DataSource = bsArticulosOC;
                CalcularTotales();
                bsartagregados = new BindingSource();
                if (((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).Estado == Entidades.Sistema.EstadoOrdenCompra.Pendiente)
                { 
                    //si el estado es pendiente que busque el faltante de la orden de compra
                    Entidades.Sistema.FaltanteDeOrdenCompra oFaltante = Controladora.Sistema.ControladoraCUIngresarMercaderia.ObtenerInstancia().Buscar(((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).NroOrdenCompra);
                    if (oFaltante != null)
                    {
                        Faltante = oFaltante; //que en Faltante guarde el oFaltante que encontro
                        dgvArticulosFaltantes.DataSource = Faltante.ArticulosFaltantes; //y llene la grilla dgvarticulosfaltantes con los Faltante.ArticulosFaltantes
                    }                    
                } //y si el estado es en curso no llena la grilla dgvArticulosFaltantes, y sigue su transcurso normal
            }
            else MessageBox.Show("No hay ordenes de compra para ingresar");    */
        }
        
        private void btnSeleccionarOrden_Click(object sender, EventArgs e)
        {
            //que me habilite los botones de aceptar,agregar art y quitar art
            btnAceptar.Enabled = true;
            btnAgregarArticulo.Enabled = true;
            btnQuitarArticulo.Enabled = true;

            CargarGrilla();
        }
        private void CalcularTotales()
        {
            decimal subtotaliva = 0;
            int subtotal = 0;
            /*foreach (Entidades.Sistema.LineaOrdenCompra linea in ((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).LineasOrdenCompra)
            {
                int subtotallinea = linea.PrecioUnitario * linea.CantPedir;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;

            }*/
            /*txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = subtotaliva.ToString();
            decimal total = subtotal + subtotaliva;
            txtTotal.Text = total.ToString();*/

        }
        
        private BindingSource bsartagregados;
        private decimal subtotalfinal, montoivafinal, totalfinal;
        private void btnAgregarArticulo_Click(object sender, EventArgs e)
        {
            formAArticuloIM oform;
            Entidades.Sistema.LineaOrdenCompra loc = (Entidades.Sistema.LineaOrdenCompra)this.bsArticulosOC.Current;
            //Entidades.Sistema.OrdenCompra oc = (Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem;
            DialogResult dr;
            if (Faltante.ColFaltantes.Count == 0)
            {
                oform = new formAArticuloIM(loc);
               dr = oform.ShowDialog();
            }
            else
            {
                oform = new formAArticuloIM(loc,Faltante);
                dr = oform.ShowDialog();
            }
            
           if (dr == System.Windows.Forms.DialogResult.Yes)
            { 
                Entidades.Sistema.Articulo oart = oform.RetornarArticulo();
                int cantent = oform.RetornarCantidadEntregada();
                Entidades.Sistema.Faltante artfaltante = oform.RetornarArticuloFaltante(); //aca recupero el artfalt del form aarticuloIM
                
                Faltante.AgregarFaltante(artfaltante); //aca le agrego el articulofaltante a la coleccion de faltantes
                dgvArticulosFaltantes.DataSource = null;
                dgvArticulosFaltantes.DataSource = Faltante.ColFaltantes; //aca lleno la grilla de articulosfaltantes
                bsartagregados.Add(oart);
                dgvArticulosAgregados.DataSource = bsartagregados;
                int indice = bsartagregados.IndexOf(oart);
                this.dgvArticulosAgregados.Rows[indice].Cells["colCantIngresada"].Value = cantent;    
                subtotalfinal = (subtotalfinal + (loc.PrecioUnitario * cantent));
                montoivafinal = (montoivafinal + ((loc.PrecioUnitario * cantent) * (loc.Articulo.Alicuota.Valor / 100))); 
                /*this.txtSubtotalFinal.Text = subtotalfinal.ToString();
                this.txtMontoIvaFinal.Text = montoivafinal.ToString();
                totalfinal = subtotalfinal + montoivafinal; 
                this.txtTotalFinal.Text = totalfinal.ToString();*/
            }            
            else dgvArticulosAgregados.DataSource = null;     
          
            CalcularTotales();           
        }
                
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (dgvArticulosAgregados.RowCount != dgvArticulosOC.RowCount)
            {
                MessageBox.Show("No ha ingresado toda la mercaderia pedida en la orden de compra.");
                foreach (DataGridViewRow item in dgvArticulosOC.Rows)
                {
                    Entidades.Sistema.LineaOrdenCompra oli = (Entidades.Sistema.LineaOrdenCompra)item.DataBoundItem;
                    List<Entidades.Sistema.Articulo> oart = new List<Entidades.Sistema.Articulo>();
                    foreach (DataGridViewRow item2 in dgvArticulosAgregados.Rows)
                    {
                         Entidades.Sistema.Articulo articulo = (Entidades.Sistema.Articulo)item2.DataBoundItem;
                         oart.Add(articulo);
                    }
                                        
                    Entidades.Sistema.Articulo oartexist = oart.Find(x => x.CodArticulo== oli.Articulo.CodArticulo);
                    if (oartexist == null)
                    {
                        Entidades.Sistema.LineaOrdenCompra olinfaltanteagergaraarticulosfaltantes = (Entidades.Sistema.LineaOrdenCompra)item.DataBoundItem;
                        Entidades.Sistema.Faltante oArt = new Entidades.Sistema.Faltante();
                        oArt.LineaOrdenCompra = olinfaltanteagergaraarticulosfaltantes;
                        oArt.CantFaltante = olinfaltanteagergaraarticulosfaltantes.CantPedir;
                        Faltante.AgregarFaltante(oArt);
                    }
                    


                }
            }
            if (dgvArticulosAgregados.RowCount != 0)
            {
                
                
                   // bool rta = Controladora.Sistema.ControladoraCUIngresarMercaderia.ObtenerInstancia().IngresarMercaderia((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem, usuario,Faltante);
                    MessageBox.Show("Mercaderia ingresada exitosamente");
                
                
            }
            else MessageBox.Show("Ingrese productos a la OC", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Close();
        }

   
        private void btnQuitarArticulo_Click(object sender, EventArgs e)
        {
            int nroCant = Convert.ToInt32(dgvArticulosAgregados.CurrentRow.Cells["colCantIngresada"].Value);
            Entidades.Sistema.Articulo Articuloquitado = ((Entidades.Sistema.Articulo)dgvArticulosAgregados.CurrentRow.DataBoundItem);
            bsartagregados.Remove(Articuloquitado);
        
            /*decimal totalfinal = Convert.ToDecimal(this.txtTotalFinal.Text);
            decimal subtotafinal = Convert.ToDecimal(this.txtSubtotalFinal.Text);
            decimal montoivafinal = Convert.ToDecimal(this.txtMontoIvaFinal.Text); */           
            foreach (Entidades.Sistema.LineaOrdenCompra item in bsArticulosOC.List)
	        {
		        if(item.Articulo.CodArticulo == Articuloquitado.CodArticulo)
                {
                    
                    int nro = item.Articulo.CantActual - nroCant;
                    //subtotafinal = subtotafinal - (item.PrecioUnitario* nroCant);
                    montoivafinal = montoivafinal - (((item.PrecioUnitario )* Articuloquitado.Alicuota.Valor /100)*nroCant);                    
                    break;
                }
	        }
            
            /*txtSubtotalFinal.Text = subtotafinal.ToString();
            txtMontoIvaFinal.Text = montoivafinal.ToString();
            txtTotalFinal.Text = (montoivafinal+subtotafinal).ToString(); */  
        
        }

        private void dgvArticulosAgregados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmbProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //en el evento selectedindexchanged del combo prov,cada vez que se carga o se cambia el combo,que me deshabilite el combo oc y los botones seleccionar orden, aceptar, agregar y quitar art           
            /*cmbOrdenCompras.Enabled = false;
            btnSeleccionarOrden.Enabled = false;*/
            btnAgregarArticulo.Enabled = false;
            btnQuitarArticulo.Enabled = false;
            btnAceptar.Enabled = false;           
        }

        private void cmbOrdenCompras_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            //en el evento selectedindexchange del combo orden compra, si el estado de la oc es pendiente
            if (((Entidades.Sistema.OrdenCompra)cmbOrdenCompras.SelectedItem).Estado == Entidades.Sistema.EstadoOrdenCompra.Pendiente)
            {
                txtEstado.Text = "Pendiente"; //que diga en el textbox estado pendiente
            }
            else //y si el estado de la oc es en curso que diga en curso
            {
                txtEstado.Text = "En Curso";
            }
            */
        }

        private void btnCancelar1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
