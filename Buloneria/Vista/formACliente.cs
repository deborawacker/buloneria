﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formACliente : Form
    {
        public formACliente()
        {
            InitializeComponent();
        }

        List<Entidades.Sistema.TipoIva> tipoiva; 
        List<Entidades.Sistema.Provincia> provincia;

        //en el evento load tengo que cargar todos los combos
        private void formACli_Load(object sender, EventArgs e)
        {
            tipoiva = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarTiposIva(); 
            provincia = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarProvincias(); 
            CargarCombos(); //aca llamo al metodo
        }

        //creo el metodo cargar combos
        private void CargarCombos()  
        {
            cmbTipoIva.DataSource = tipoiva; //data source es el origen de los datos
            cmbTipoIva.DisplayMember = "Nombre"; //display member para que muestre en el combo el "nombre", que es el atributo de la clase tipoiva
            cmbProvincia.DataSource = provincia; //data source es el origen de los datos
            cmbProvincia.DisplayMember = "Nombre"; //display member para que muestre en el combo el "nombre", que es el atributo de la clase provincia
            cmbCiudad.DataSource = provincia;
            cmbCiudad.DisplayMember = "Ciudades.Nombre"; 
        }

        //evento click del boton aceptar, paso todos los datos del form
        private void btnAceptar_Click(object sender, EventArgs e) 
        {
            if (ValidarDatos())
            {
                Entidades.Sistema.Cliente oCliente = new Entidades.Sistema.Cliente();
                oCliente.Celular = txtCelular.Text; //aca le paso lo que hay en txtcelular, la propiedad text es de Tipo string como el celular
                oCliente.Ciudad = (Entidades.Sistema.Ciudad)cmbCiudad.SelectedItem; //aca convierto lo que esta seleccionado en el combo, a mi _TipoMovimiento de dato entidades.sistema.ciudad
                oCliente.CodPostal = Convert.ToInt32(txtCodigoPostal.Text);
                oCliente.Cuit = Convert.ToInt64(txtCuit.Text);
                oCliente.Direccion = txtDireccion.Text;
                oCliente.Email = txtEmail.Text;
                oCliente.Fax = txtFax.Text;
                oCliente.NombreFantasia = txtNombreFantasia.Text;
                oCliente.RazonSocial = txtRazonSocial.Text;
                oCliente.Telefono = txtTelefono.Text;
                oCliente.TipoIva = (Entidades.Sistema.TipoIva)cmbTipoIva.SelectedItem;

                if (Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().AgregarCliente(oCliente) == true)
                {
                    Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().AgregarCliente(oCliente);
                    MessageBox.Show("Cliente agregado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Cliente existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCuit.Focus();
                    return;
                }

            }
            else MessageBox.Show("Complete todos los datos");
            
        }

        //metodo validar datos
        private bool ValidarDatos()
        {
            if (txtCelular.Text == null) return false;            
            if (txtCodigoPostal.Text == null) return false;
            if (txtCuit.Text == null) return false;
            if (txtDireccion.Text == null) return false;            
            if (txtNombreFantasia.Text == null) return false;
            if (txtRazonSocial.Text == null) return false;
            if (txtTelefono.Text == null) return false;                        
            if (txtCodigoPostal.Text == "") return false;
            if (txtCuit.Text == "") return false;
            if (txtDireccion.Text == "") return false;                        
            if (txtNombreFantasia.Text == "") return false;
            if (txtRazonSocial.Text == "") return false;
            if (txtTelefono.Text == "") return false;
            
            return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
