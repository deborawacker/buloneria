﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formVisualizarMercFaltante : Form
    {
        
       
        public formVisualizarMercFaltante(Entidades.Sistema.OrdenCompra oc) 
        {
            InitializeComponent();
            this.dgvArticulosFaltantes.AutoGenerateColumns = false;
            dgvArticulosFaltantes.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosFaltantes_DataBindingComplete);
           // cmbNroOrdenCompra.DataSource = (Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarMercaderiaFaltanteOrdenesCompras(oc)).ArticulosFaltantes;
            
            cmbNroOrdenCompra.Items.Add(oc);
            cmbNroOrdenCompra.DisplayMember = "NroOrdenCompra";
            cmbNroOrdenCompra.Refresh();
            txtEstado.Text = oc.Estado.ToString();
           // this.cmbNroOrdenCompra.Enabled = false;
            this.cmbNroOrdenCompra.SelectedIndex = 0;
            Entidades.Sistema.FaltanteDeOrdenCompra oFalt = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarMercaderiaFaltanteOrdenesCompras(oc);
            if (oFalt != null)
            {
                dgvArticulosFaltantes.DataSource = null;
                dgvArticulosFaltantes.DataSource = oFalt.ColFaltantes;
            }
        }

        public formVisualizarMercFaltante()
        {
            InitializeComponent();
            this.dgvArticulosFaltantes.AutoGenerateColumns = false;
            dgvArticulosFaltantes.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosFaltantes_DataBindingComplete);
            cmbNroOrdenCompra.DataSource = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarOrdenesCompraConFaltantes();
            cmbNroOrdenCompra.DisplayMember = "NroOrdenCompra";
            txtEstado.Text = ((Entidades.Sistema.OrdenCompra)cmbNroOrdenCompra.SelectedItem).Estado.ToString();            
        }


        private void txtCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbNroOrdenCompra_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEstado.Text = ((Entidades.Sistema.OrdenCompra)cmbNroOrdenCompra.SelectedItem).Estado.ToString();
            Entidades.Sistema.FaltanteDeOrdenCompra oFalt = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarMercaderiaFaltanteOrdenesCompras((Entidades.Sistema.OrdenCompra)cmbNroOrdenCompra.SelectedItem);
            if (oFalt != null)
            {
                dgvArticulosFaltantes.DataSource = null;
                dgvArticulosFaltantes.DataSource = oFalt.ColFaltantes;
            }
        }

        void dgvArticulosFaltantes_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (dgvArticulosFaltantes.Rows.Count != 0)
            {
                foreach (DataGridViewRow item in dgvArticulosFaltantes.Rows)
                {
                    Entidades.Sistema.Faltante art = (Entidades.Sistema.Faltante)item.DataBoundItem;
                    item.Cells["colDescripcion"].Value = art.LineaOrdenCompra.Articulo.Descripcion;
                    item.Cells["colCantFalt"].Value = art.CantFaltante;
                }
            }
        }

      

    }
}
