﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGProveedores : Form
    {
        public formGProveedores(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();
            this.dgvProveedores.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }


        private BindingSource bsProveedores; //creo un bsprveedores
        
        private void formGProveedores_Load(object sender, EventArgs e)
        {
            this.bsProveedores = new BindingSource();
            CargarGrilla(); //llamo al metodo cargargrilla
        }

        private void CargarGrilla()
        {
            Controladora.Sistema.ControladoraCUGestionarProveedores octrlGProveedores = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia(); //obtenerinstancia de singleton,declaro e instancio
            bsProveedores.DataSource = octrlGProveedores.RecuperarProveedores(); //en el bsproveedores voy a guardar lo que recupera del metodo recuperarproveedores, es decir todos los proveedores
            dgvProveedores.DataSource = bsProveedores; //el metodo cargargrilla asigna al datagridview todos los proveedores
        }
    
        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            bsProveedores.DataSource = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().FiltrarProveedores(txtBuscar.Text);
            dgvProveedores.DataSource = null;
            dgvProveedores.DataSource = bsProveedores;    
        }

        private void ActualizarGrilla()
        {
            dgvProveedores.DataSource = null;
            CargarGrilla();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            formMProveedor formmproveedor = new formMProveedor((Entidades.Sistema.Proveedor)bsProveedores.Current); //aca le paso el objeto proveedor que esta seleccionado en la grilla (bs es binding source)
            formmproveedor.FormClosed += new FormClosedEventHandler(formmproveedor_FormClosed);
            formmproveedor.Show();
        }

        void formmproveedor_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formCProveedor formcproveedor = new formCProveedor((Entidades.Sistema.Proveedor)bsProveedores.Current); //aca le paso el objeto proveedor que esta seleccionado en la grilla (bs es binding source)
            formcproveedor.FormClosed += new FormClosedEventHandler(formcproveedor_FormClosed);
            formcproveedor.Show();
        }

        void formcproveedor_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            formEProveedor formeproveedor = new formEProveedor((Entidades.Sistema.Proveedor)bsProveedores.Current); //aca le paso el objeto proveedor que esta seleccionado en la grilla (bs es binding source);
            formeproveedor.Show();
            formeproveedor.FormClosed += new FormClosedEventHandler(formeproveedor_FormClosed); //aca le digo que cuando se cierra el form eliminar, se actualice la grilla
            
        }

        void formeproveedor_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            formAProveedor oAgregarProveedor = new formAProveedor();
            oAgregarProveedor.FormClosed += new FormClosedEventHandler(oAgregarProveedor_FormClosed); //aca le digo que cuando se cierre el form se actualice grilla
            oAgregarProveedor.Show();          
        }
        
        void oAgregarProveedor_FormClosed(object sender, FormClosedEventArgs e)  //evento, cuando se cierra el form se actualiza grilla
        {
            this.ActualizarGrilla();
        }

    }
}
