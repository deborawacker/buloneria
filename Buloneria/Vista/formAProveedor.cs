﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAProveedor : Form
    {
        public formAProveedor()
        {
            InitializeComponent();
        }

        List<Entidades.Sistema.TipoIva> tipoiva;
        List<Entidades.Sistema.Provincia> provincia;

        //en el evento load tengo que cargar todos los combos
        private void formAProveedor_Load(object sender, EventArgs e)
        {            
            tipoiva = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva();  //aca llamo a la capa controladora, y le pido que ejecute el metodo recuperar _TipoMovimiento iva de la controladoraCUGProveedores
            provincia = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
            CargarCombos(); //llamo al metodo      
            
        } 

        //aca creo el metodo cargar combos
        private void CargarCombos()
        {
            cmbTipoIva.DataSource = tipoiva; //data source es el origen de los datos
            cmbTipoIva.DisplayMember = "Nombre"; //display member para que muestre en el combo el "nombre", que es el atributo de la clase tipoiva
            cmbProvincia.DataSource = provincia;
            cmbProvincia.DisplayMember = "Nombre";
            cmbCiudad.DataSource = provincia;
            cmbCiudad.DisplayMember = "Ciudades.Nombre";
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                Entidades.Sistema.Proveedor oProveedor = new Entidades.Sistema.Proveedor();
                oProveedor.Celular = txtCelular.Text; //aca le paso lo que hay en txtcelular, la propiedad text es de _TipoMovimiento string como el celular
                oProveedor.Ciudad = (Entidades.Sistema.Ciudad)cmbCiudad.SelectedItem; //"casteo" aca convierto lo que esta seleccionado en el combo, a mi _TipoMovimiento de dato entidades.sistema.ciudad
                oProveedor.CodPostal = Convert.ToInt32(txtCodigoPostal.Text);
                oProveedor.Cuit = Convert.ToInt64(txtCuit.Text);
                oProveedor.Direccion = txtDireccion.Text;
                oProveedor.Email = txtEmail.Text;
                oProveedor.Fax = txtFax.Text;
                oProveedor.NombreFantasia = txtNombreFantasia.Text;
                oProveedor.RazonSocial = txtRazonSocial.Text;
                oProveedor.Telefono = txtTelefono.Text;
                oProveedor.TipoIva = (Entidades.Sistema.TipoIva)cmbTipoIva.SelectedItem;
                
                if (Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().AgregarProveedor(oProveedor) == true)
                {
                    Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().AgregarProveedor(oProveedor);
                    MessageBox.Show("Proveedor agregado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Proveedor existente", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCuit.Focus();
                    return;
                }
            }
            else MessageBox.Show("Complete todos los datos");
        }

        //metodo validar datos
        private bool ValidarDatos()
        {
            if (txtCodigoPostal.Text == null) return false;
            if (txtCuit.Text == null) return false;
            if (txtDireccion.Text == null) return false;
            if (txtNombreFantasia.Text == null) return false;
            if (txtRazonSocial.Text == null) return false;
            if (txtTelefono.Text == null) return false;
            if (txtCodigoPostal.Text == "") return false;
            if (txtCuit.Text == "") return false;
            if (txtDireccion.Text == "") return false;
            if (txtNombreFantasia.Text == "") return false;
            if (txtRazonSocial.Text == "") return false;
            if (txtTelefono.Text == "") return false;
            return true;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }   
    }
}
