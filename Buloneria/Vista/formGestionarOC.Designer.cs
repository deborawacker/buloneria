﻿namespace Vista
{
    partial class formGestionarOC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvOrdenesCompras = new System.Windows.Forms.DataGridView();
            this.colNroOrdenCompra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFechaPlazo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFormaPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnVerHistorial = new System.Windows.Forms.Button();
            this.btnAnular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrdenesCompras)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvOrdenesCompras
            // 
            this.dgvOrdenesCompras.AllowUserToAddRows = false;
            this.dgvOrdenesCompras.AllowUserToDeleteRows = false;
            this.dgvOrdenesCompras.AllowUserToResizeColumns = false;
            this.dgvOrdenesCompras.AllowUserToResizeRows = false;
            this.dgvOrdenesCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrdenesCompras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNroOrdenCompra,
            this.colProveedor,
            this.colFechaEmision,
            this.colFechaPlazo,
            this.colFormaPago,
            this.colEstado});
            this.dgvOrdenesCompras.Location = new System.Drawing.Point(12, 65);
            this.dgvOrdenesCompras.MultiSelect = false;
            this.dgvOrdenesCompras.Name = "dgvOrdenesCompras";
            this.dgvOrdenesCompras.ReadOnly = true;
            this.dgvOrdenesCompras.RowHeadersVisible = false;
            this.dgvOrdenesCompras.ShowCellErrors = false;
            this.dgvOrdenesCompras.ShowCellToolTips = false;
            this.dgvOrdenesCompras.ShowEditingIcon = false;
            this.dgvOrdenesCompras.ShowRowErrors = false;
            this.dgvOrdenesCompras.Size = new System.Drawing.Size(754, 409);
            this.dgvOrdenesCompras.TabIndex = 0;
            this.dgvOrdenesCompras.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrdenesCompras_CellContentClick);
            // 
            // colNroOrdenCompra
            // 
            this.colNroOrdenCompra.DataPropertyName = "NroOrdenCompra";
            this.colNroOrdenCompra.HeaderText = "N° OC";
            this.colNroOrdenCompra.Name = "colNroOrdenCompra";
            this.colNroOrdenCompra.ReadOnly = true;
            // 
            // colProveedor
            // 
            this.colProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colProveedor.DataPropertyName = "Proveedor";
            this.colProveedor.HeaderText = "Proveedor";
            this.colProveedor.Name = "colProveedor";
            this.colProveedor.ReadOnly = true;
            // 
            // colFechaEmision
            // 
            this.colFechaEmision.DataPropertyName = "FechaEmision";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.colFechaEmision.DefaultCellStyle = dataGridViewCellStyle1;
            this.colFechaEmision.HeaderText = "Emision";
            this.colFechaEmision.Name = "colFechaEmision";
            this.colFechaEmision.ReadOnly = true;
            // 
            // colFechaPlazo
            // 
            this.colFechaPlazo.DataPropertyName = "FechaPlazo";
            dataGridViewCellStyle2.Format = "d";
            this.colFechaPlazo.DefaultCellStyle = dataGridViewCellStyle2;
            this.colFechaPlazo.HeaderText = "Entrega";
            this.colFechaPlazo.Name = "colFechaPlazo";
            this.colFechaPlazo.ReadOnly = true;
            // 
            // colFormaPago
            // 
            this.colFormaPago.DataPropertyName = "FormaPago";
            this.colFormaPago.HeaderText = "Forma Pago";
            this.colFormaPago.Name = "colFormaPago";
            this.colFormaPago.ReadOnly = true;
            // 
            // colEstado
            // 
            this.colEstado.DataPropertyName = "Estado";
            this.colEstado.HeaderText = "Estado OC";
            this.colEstado.Name = "colEstado";
            this.colEstado.ReadOnly = true;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(12, 12);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 47);
            this.btnConsultar.TabIndex = 1;
            this.btnConsultar.Tag = "Consulta";
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(93, 12);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 47);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Tag = "Imprimir";
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnVerHistorial
            // 
            this.btnVerHistorial.Location = new System.Drawing.Point(174, 12);
            this.btnVerHistorial.Name = "btnVerHistorial";
            this.btnVerHistorial.Size = new System.Drawing.Size(75, 47);
            this.btnVerHistorial.TabIndex = 3;
            this.btnVerHistorial.Tag = "VerHistorial";
            this.btnVerHistorial.Text = "Ver Historial";
            this.btnVerHistorial.UseVisualStyleBackColor = true;
            this.btnVerHistorial.Click += new System.EventHandler(this.btnVerHistorial_Click);
            // 
            // btnAnular
            // 
            this.btnAnular.Location = new System.Drawing.Point(255, 12);
            this.btnAnular.Name = "btnAnular";
            this.btnAnular.Size = new System.Drawing.Size(75, 47);
            this.btnAnular.TabIndex = 4;
            this.btnAnular.Tag = "Anular";
            this.btnAnular.Text = "Anular";
            this.btnAnular.UseVisualStyleBackColor = true;
            this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Buscar:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(385, 23);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(381, 20);
            this.txtBuscar.TabIndex = 6;
            this.txtBuscar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // formGestionarOC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 486);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAnular);
            this.Controls.Add(this.btnVerHistorial);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.dgvOrdenesCompras);
            this.Name = "formGestionarOC";
            this.Tag = "OrdenesCompras";
            this.Text = "Gestionar Ordenes de Compras";
            this.Load += new System.EventHandler(this.formGestionarOC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrdenesCompras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvOrdenesCompras;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnVerHistorial;
        private System.Windows.Forms.Button btnAnular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNroOrdenCompra;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFechaPlazo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFormaPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEstado;
    }
}