﻿namespace Vista
{
    partial class formInformes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbInfArticulos = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnMostrarPeriodoArt = new System.Windows.Forms.Button();
            this.btnMostrarArtStock = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFechaHastaArt = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaDesdeArt = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnArticulos = new System.Windows.Forms.Button();
            this.btnClientes = new System.Windows.Forms.Button();
            this.btnProveedores = new System.Windows.Forms.Button();
            this.dgvInformesGerenciales = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.gbInfClientes = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpFechaHastaCli = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaDesdeCli = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.btnMostrarPeriodoCli = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.gbInfProveedores = new System.Windows.Forms.GroupBox();
            this.btnMostrarPreciosArt = new System.Windows.Forms.Button();
            this.cmbSeleccionarArt = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.btnGenRepFaltantes = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnGenRepArt = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.btnGenRepMontos = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.btnGenRepPrecios = new System.Windows.Forms.Button();
            this.gbInfArticulos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInformesGerenciales)).BeginInit();
            this.gbInfClientes.SuspendLayout();
            this.gbInfProveedores.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbInfArticulos
            // 
            this.gbInfArticulos.Controls.Add(this.label7);
            this.gbInfArticulos.Controls.Add(this.btnMostrarPeriodoArt);
            this.gbInfArticulos.Controls.Add(this.btnMostrarArtStock);
            this.gbInfArticulos.Controls.Add(this.label5);
            this.gbInfArticulos.Controls.Add(this.label4);
            this.gbInfArticulos.Controls.Add(this.dtpFechaHastaArt);
            this.gbInfArticulos.Controls.Add(this.dtpFechaDesdeArt);
            this.gbInfArticulos.Controls.Add(this.label3);
            this.gbInfArticulos.Controls.Add(this.label2);
            this.gbInfArticulos.Location = new System.Drawing.Point(149, 3);
            this.gbInfArticulos.Name = "gbInfArticulos";
            this.gbInfArticulos.Size = new System.Drawing.Size(313, 210);
            this.gbInfArticulos.TabIndex = 0;
            this.gbInfArticulos.TabStop = false;
            this.gbInfArticulos.Text = "Informes sobre articulos";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Seleccionar periodo:";
            // 
            // btnMostrarPeriodoArt
            // 
            this.btnMostrarPeriodoArt.Location = new System.Drawing.Point(200, 173);
            this.btnMostrarPeriodoArt.Name = "btnMostrarPeriodoArt";
            this.btnMostrarPeriodoArt.Size = new System.Drawing.Size(96, 23);
            this.btnMostrarPeriodoArt.TabIndex = 8;
            this.btnMostrarPeriodoArt.Text = "Mostrar Periodo";
            this.btnMostrarPeriodoArt.UseVisualStyleBackColor = true;
            this.btnMostrarPeriodoArt.Click += new System.EventHandler(this.btnMostrarPeriodoArt_Click);
            // 
            // btnMostrarArtStock
            // 
            this.btnMostrarArtStock.Location = new System.Drawing.Point(221, 21);
            this.btnMostrarArtStock.Name = "btnMostrarArtStock";
            this.btnMostrarArtStock.Size = new System.Drawing.Size(75, 23);
            this.btnMostrarArtStock.TabIndex = 6;
            this.btnMostrarArtStock.Text = "Mostrar";
            this.btnMostrarArtStock.UseVisualStyleBackColor = true;
            this.btnMostrarArtStock.Click += new System.EventHandler(this.btnMostrarArtStock_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha Hasta:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Fecha Desde:";
            // 
            // dtpFechaHastaArt
            // 
            this.dtpFechaHastaArt.Location = new System.Drawing.Point(96, 147);
            this.dtpFechaHastaArt.Name = "dtpFechaHastaArt";
            this.dtpFechaHastaArt.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaHastaArt.TabIndex = 3;
            // 
            // dtpFechaDesdeArt
            // 
            this.dtpFechaDesdeArt.Location = new System.Drawing.Point(96, 121);
            this.dtpFechaDesdeArt.Name = "dtpFechaDesdeArt";
            this.dtpFechaDesdeArt.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaDesdeArt.TabIndex = 2;
            this.dtpFechaDesdeArt.Value = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Listado de artículos más vendidos:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Listado de artículos con faltantes de stock:";
            // 
            // btnArticulos
            // 
            this.btnArticulos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnArticulos.Location = new System.Drawing.Point(18, 46);
            this.btnArticulos.Name = "btnArticulos";
            this.btnArticulos.Size = new System.Drawing.Size(119, 46);
            this.btnArticulos.TabIndex = 3;
            this.btnArticulos.Text = "Artículos";
            this.btnArticulos.UseVisualStyleBackColor = true;
            this.btnArticulos.Click += new System.EventHandler(this.btnArticulos_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnClientes.Location = new System.Drawing.Point(18, 104);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Size = new System.Drawing.Size(119, 46);
            this.btnClientes.TabIndex = 4;
            this.btnClientes.Text = "Clientes";
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.btnClientes_Click);
            // 
            // btnProveedores
            // 
            this.btnProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnProveedores.Location = new System.Drawing.Point(18, 164);
            this.btnProveedores.Name = "btnProveedores";
            this.btnProveedores.Size = new System.Drawing.Size(119, 46);
            this.btnProveedores.TabIndex = 5;
            this.btnProveedores.Text = "Proveedores";
            this.btnProveedores.UseVisualStyleBackColor = true;
            this.btnProveedores.Click += new System.EventHandler(this.btnProveedores_Click);
            // 
            // dgvInformesGerenciales
            // 
            this.dgvInformesGerenciales.AllowUserToAddRows = false;
            this.dgvInformesGerenciales.AllowUserToDeleteRows = false;
            this.dgvInformesGerenciales.AllowUserToResizeColumns = false;
            this.dgvInformesGerenciales.AllowUserToResizeRows = false;
            this.dgvInformesGerenciales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInformesGerenciales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInformesGerenciales.Location = new System.Drawing.Point(18, 256);
            this.dgvInformesGerenciales.MultiSelect = false;
            this.dgvInformesGerenciales.Name = "dgvInformesGerenciales";
            this.dgvInformesGerenciales.ReadOnly = true;
            this.dgvInformesGerenciales.RowHeadersVisible = false;
            this.dgvInformesGerenciales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInformesGerenciales.ShowCellErrors = false;
            this.dgvInformesGerenciales.ShowCellToolTips = false;
            this.dgvInformesGerenciales.ShowEditingIcon = false;
            this.dgvInformesGerenciales.ShowRowErrors = false;
            this.dgvInformesGerenciales.Size = new System.Drawing.Size(633, 299);
            this.dgvInformesGerenciales.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "SELECCIONAR";
            // 
            // gbInfClientes
            // 
            this.gbInfClientes.Controls.Add(this.label11);
            this.gbInfClientes.Controls.Add(this.label10);
            this.gbInfClientes.Controls.Add(this.dtpFechaHastaCli);
            this.gbInfClientes.Controls.Add(this.dtpFechaDesdeCli);
            this.gbInfClientes.Controls.Add(this.label9);
            this.gbInfClientes.Controls.Add(this.btnMostrarPeriodoCli);
            this.gbInfClientes.Controls.Add(this.label6);
            this.gbInfClientes.Location = new System.Drawing.Point(698, 12);
            this.gbInfClientes.Name = "gbInfClientes";
            this.gbInfClientes.Size = new System.Drawing.Size(372, 158);
            this.gbInfClientes.TabIndex = 8;
            this.gbInfClientes.TabStop = false;
            this.gbInfClientes.Text = "Informes sobre clientes";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Fecha Hasta:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(73, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Fecha Desde:";
            // 
            // dtpFechaHastaCli
            // 
            this.dtpFechaHastaCli.Location = new System.Drawing.Point(153, 94);
            this.dtpFechaHastaCli.Name = "dtpFechaHastaCli";
            this.dtpFechaHastaCli.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaHastaCli.TabIndex = 7;
            // 
            // dtpFechaDesdeCli
            // 
            this.dtpFechaDesdeCli.Location = new System.Drawing.Point(153, 68);
            this.dtpFechaDesdeCli.Name = "dtpFechaDesdeCli";
            this.dtpFechaDesdeCli.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaDesdeCli.TabIndex = 6;
            this.dtpFechaDesdeCli.Value = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(73, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Seleccionar periodo:";
            // 
            // btnMostrarPeriodoCli
            // 
            this.btnMostrarPeriodoCli.Location = new System.Drawing.Point(257, 120);
            this.btnMostrarPeriodoCli.Name = "btnMostrarPeriodoCli";
            this.btnMostrarPeriodoCli.Size = new System.Drawing.Size(96, 23);
            this.btnMostrarPeriodoCli.TabIndex = 4;
            this.btnMostrarPeriodoCli.Text = "Mostrar Periodo";
            this.btnMostrarPeriodoCli.UseVisualStyleBackColor = true;
            this.btnMostrarPeriodoCli.Click += new System.EventHandler(this.btnMostrarPeriodoCli_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(248, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Listado de montos de compras por mes de clientes:";
            // 
            // gbInfProveedores
            // 
            this.gbInfProveedores.Controls.Add(this.btnMostrarPreciosArt);
            this.gbInfProveedores.Controls.Add(this.cmbSeleccionarArt);
            this.gbInfProveedores.Controls.Add(this.label13);
            this.gbInfProveedores.Controls.Add(this.label12);
            this.gbInfProveedores.Location = new System.Drawing.Point(698, 240);
            this.gbInfProveedores.Name = "gbInfProveedores";
            this.gbInfProveedores.Size = new System.Drawing.Size(372, 118);
            this.gbInfProveedores.TabIndex = 9;
            this.gbInfProveedores.TabStop = false;
            this.gbInfProveedores.Text = "Informes sobre proveedores";
            // 
            // btnMostrarPreciosArt
            // 
            this.btnMostrarPreciosArt.Location = new System.Drawing.Point(283, 55);
            this.btnMostrarPreciosArt.Name = "btnMostrarPreciosArt";
            this.btnMostrarPreciosArt.Size = new System.Drawing.Size(75, 23);
            this.btnMostrarPreciosArt.TabIndex = 3;
            this.btnMostrarPreciosArt.Text = "Mostrar";
            this.btnMostrarPreciosArt.UseVisualStyleBackColor = true;
            this.btnMostrarPreciosArt.Click += new System.EventHandler(this.btnMostrarPreciosArt_Click);
            // 
            // cmbSeleccionarArt
            // 
            this.cmbSeleccionarArt.FormattingEnabled = true;
            this.cmbSeleccionarArt.Location = new System.Drawing.Point(117, 57);
            this.cmbSeleccionarArt.Name = "cmbSeleccionarArt";
            this.cmbSeleccionarArt.Size = new System.Drawing.Size(160, 21);
            this.cmbSeleccionarArt.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Seleccionar artículo:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(246, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Listado comparativo de precios entre proveedores:";
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnSalir.Location = new System.Drawing.Point(532, 561);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(119, 31);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(125, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = " TIPO DE INFORME:";
            // 
            // btnGenRepFaltantes
            // 
            this.btnGenRepFaltantes.Enabled = false;
            this.btnGenRepFaltantes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenRepFaltantes.Location = new System.Drawing.Point(532, 24);
            this.btnGenRepFaltantes.Name = "btnGenRepFaltantes";
            this.btnGenRepFaltantes.Size = new System.Drawing.Size(117, 23);
            this.btnGenRepFaltantes.TabIndex = 12;
            this.btnGenRepFaltantes.Text = "Generar Reporte";
            this.btnGenRepFaltantes.UseVisualStyleBackColor = true;
            this.btnGenRepFaltantes.Click += new System.EventHandler(this.btnGenRepFaltantes_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(529, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Faltantes de stock:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(531, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Artículos más vendidos:";
            // 
            // btnGenRepArt
            // 
            this.btnGenRepArt.Enabled = false;
            this.btnGenRepArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenRepArt.Location = new System.Drawing.Point(532, 77);
            this.btnGenRepArt.Name = "btnGenRepArt";
            this.btnGenRepArt.Size = new System.Drawing.Size(117, 23);
            this.btnGenRepArt.TabIndex = 15;
            this.btnGenRepArt.Text = "Generar Reporte";
            this.btnGenRepArt.UseVisualStyleBackColor = true;
            this.btnGenRepArt.Click += new System.EventHandler(this.btnGenRepArt_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(531, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Montos compras clientes:";
            // 
            // btnGenRepMontos
            // 
            this.btnGenRepMontos.Enabled = false;
            this.btnGenRepMontos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenRepMontos.Location = new System.Drawing.Point(532, 132);
            this.btnGenRepMontos.Name = "btnGenRepMontos";
            this.btnGenRepMontos.Size = new System.Drawing.Size(117, 23);
            this.btnGenRepMontos.TabIndex = 17;
            this.btnGenRepMontos.Text = "Generar Reporte";
            this.btnGenRepMontos.UseVisualStyleBackColor = true;
            this.btnGenRepMontos.Click += new System.EventHandler(this.btnGenRepMontos_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(531, 171);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Precios para artículo:";
            // 
            // btnGenRepPrecios
            // 
            this.btnGenRepPrecios.Enabled = false;
            this.btnGenRepPrecios.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenRepPrecios.Location = new System.Drawing.Point(534, 187);
            this.btnGenRepPrecios.Name = "btnGenRepPrecios";
            this.btnGenRepPrecios.Size = new System.Drawing.Size(117, 23);
            this.btnGenRepPrecios.TabIndex = 19;
            this.btnGenRepPrecios.Text = "Generar Reporte";
            this.btnGenRepPrecios.UseVisualStyleBackColor = true;
            this.btnGenRepPrecios.Click += new System.EventHandler(this.btnGenRepPrecios_Click);
            // 
            // formInformes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 593);
            this.Controls.Add(this.btnGenRepPrecios);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnGenRepMontos);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnGenRepArt);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnGenRepFaltantes);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.gbInfProveedores);
            this.Controls.Add(this.gbInfClientes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvInformesGerenciales);
            this.Controls.Add(this.btnProveedores);
            this.Controls.Add(this.btnClientes);
            this.Controls.Add(this.btnArticulos);
            this.Controls.Add(this.gbInfArticulos);
            this.Name = "formInformes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "Informes Gerenciales";
            this.Text = "Informes Gerenciales";
            this.Load += new System.EventHandler(this.formInformes_Load);
            this.gbInfArticulos.ResumeLayout(false);
            this.gbInfArticulos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInformesGerenciales)).EndInit();
            this.gbInfClientes.ResumeLayout(false);
            this.gbInfClientes.PerformLayout();
            this.gbInfProveedores.ResumeLayout(false);
            this.gbInfProveedores.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbInfArticulos;
        private System.Windows.Forms.Button btnMostrarPeriodoArt;
        private System.Windows.Forms.Button btnMostrarArtStock;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFechaHastaArt;
        private System.Windows.Forms.DateTimePicker dtpFechaDesdeArt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnArticulos;
        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.Button btnProveedores;
        private System.Windows.Forms.DataGridView dgvInformesGerenciales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbInfClientes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpFechaHastaCli;
        private System.Windows.Forms.DateTimePicker dtpFechaDesdeCli;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnMostrarPeriodoCli;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gbInfProveedores;
        private System.Windows.Forms.Button btnMostrarPreciosArt;
        private System.Windows.Forms.ComboBox cmbSeleccionarArt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnGenRepFaltantes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnGenRepArt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnGenRepMontos;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnGenRepPrecios;
    }
}