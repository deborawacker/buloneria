﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formEProveedor : Form
    {
        Entidades.Sistema.Proveedor _proveedor;
        public formEProveedor(Entidades.Sistema.Proveedor proveedor)  //aca paso como parametro del form gestionar proveedor
        {
            _proveedor = proveedor;
            InitializeComponent();
        }

        private void formEProveedor_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _proveedor.Celular;
            txtCodigo.Text = _proveedor.CodProveedor.ToString();
            txtCodigoPostal.Text = _proveedor.CodPostal.ToString();
            txtCuit.Text = _proveedor.Cuit.ToString();
            txtDireccion.Text = _proveedor.Direccion;
            txtEmail.Text = _proveedor.Email;
            txtFax.Text = _proveedor.Fax;
            txtNombreFantasia.Text = _proveedor.NombreFantasia;
            txtRazonSocial.Text = _proveedor.RazonSocial;
            txtTelefono.Text = _proveedor.Telefono;
            cmbProvincia.Text = _proveedor.Ciudad.Nombre;
            cmbCiudad.Text = _proveedor.Ciudad.Nombre;
            cmbTipoIva.Text = _proveedor.TipoIva.Nombre;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
           DialogResult respuesta = MessageBox.Show("Desea eliminar el proveedor?", "Eliminar", MessageBoxButtons.YesNo);
           if (respuesta == System.Windows.Forms.DialogResult.Yes)
           {
               bool resultado = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().EliminarProveedor(_proveedor);
               if (resultado)
               {
                   MessageBox.Show("El proveedor se elimino exitosamente.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   this.Close();
               }
               else
               {
                   MessageBox.Show("Ha ocurrido un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   this.Close();
               }
           }
        }

        
    }
}
