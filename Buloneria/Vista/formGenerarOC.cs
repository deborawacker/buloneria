﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGenerarOC : Form
    {
        
        Entidades.Sistema.Cuidador _cuidador;
        Entidades.Seguridad.Usuario usu;
        public formGenerarOC(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles, Entidades.Seguridad.Usuario usuario)
        {
            usu = usuario;
            _cuidador = new Entidades.Sistema.Cuidador();
            InitializeComponent();
            dtpFechaPlazo.Value = dtpFechaEmision.Value.AddDays(15);
            this.dgvArticulosAgregados.AutoGenerateColumns = false;
            this.dgvArticulosProveedor.AutoGenerateColumns = false;
            dgvArticulosAgregados.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosAgregados_DataBindingComplete);
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }

        void dgvArticulosAgregados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvArticulosAgregados.Rows)
            {
                item.Cells["colSubTotal"].Value = Convert.ToInt32(item.Cells["colCantidadPedir"].Value) * Convert.ToInt32(item.Cells["colPrecio"].Value);
            }
        }

        List<Entidades.Sistema.FormaPago> formapago;
        List<Entidades.Sistema.Proveedor> proveedor;
        BindingSource bsArticulosProveedor;
        //evento load del form, tengo que recuperar y cargar combos
        private void formGenerarOC_Load(object sender, EventArgs e)
        {
            //cuando se abre form que esten todos los botones deshabilitados excepto seleccionar prov y cancelar
            btnAceptar.Enabled = false;
            btnAgregarArticulo.Enabled = false;
            btnQuitarArticulo.Enabled = false;
            btnDeshacer.Enabled = false;

            formapago = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().RecuperarFormasPago();
            proveedor = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().RecuperarProveedores();
            CargarCombos();
            bsArticulosProveedor = new BindingSource();
        }

        //creo el metodo cargarcombos
        private void CargarCombos()
        {
            cmbFormaPago.DataSource = formapago;
            cmbFormaPago.DisplayMember = "TipoFormaPago";
            cmbProveedor.DataSource = proveedor;
            cmbProveedor.DisplayMember = "RazonSocial";
        }

        //aca se carga la _fecha de hoy
        private void dtpFechaEmision_ValueChanged(object sender, EventArgs e)
        {
            dtpFechaPlazo.Value = dtpFechaEmision.Value.AddDays(15);
        }

        private void btnSeleccionarProveedor_Click(object sender, EventArgs e)
        {   
            //cuando hago click en seleccionar prov se habilitan el resto de botones
            btnAgregarArticulo.Enabled = true;
            btnQuitarArticulo.Enabled = true;
            btnDeshacer.Enabled = true;
            btnAceptar.Enabled = true;

            bsArticulosProveedor.DataSource = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().RecuperarArticulosProveedor((Entidades.Sistema.Proveedor)cmbProveedor.SelectedItem);
            CargarGrilla();
            oordencompra = new Entidades.Sistema.OrdenCompra();
            oordencompra.FechaEmision = this.dtpFechaEmision.Value;
            oordencompra.FechaPlazo = this.dtpFechaPlazo.Value;
            oordencompra.FormaPago = (Entidades.Sistema.FormaPago)this.cmbFormaPago.SelectedItem;
            oordencompra.Proveedor = (Entidades.Sistema.Proveedor)this.cmbProveedor.SelectedItem;
            cmbProveedor.Enabled = false;
            cmbFormaPago.Enabled = false;
            btnSeleccionarProveedor.Enabled = false;
            _cuidador.AgregarMemento(oordencompra.CrearMemento());
        }

        private void CargarGrilla()
        {
            dgvArticulosProveedor.DataSource = null;
            dgvArticulosProveedor.DataSource = bsArticulosProveedor;
        }

        Entidades.Sistema.OrdenCompra oordencompra;
        private void btnAgregarArticulo_Click(object sender, EventArgs e)
        {
            _cuidador.AgregarMemento(oordencompra.CrearMemento());
            formAArticuloOC oform = new formAArticuloOC((Entidades.Sistema.Articulo)this.bsArticulosProveedor.Current);
            DialogResult dr = oform.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.Yes) oordencompra.AgregarLineaOrdenCompra(oform.RetornarLineaOC());
            dgvArticulosAgregados.DataSource = null;
            dgvArticulosAgregados.DataSource = oordencompra.LineasOrdenCompra;
            CalcularTotales();           
        }

        private void CalcularTotales()
        {
            decimal subtotaliva = 0;
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaOrdenCompra linea in oordencompra.LineasOrdenCompra)
            {
                int subtotallinea = linea.PrecioUnitario * linea.CantPedir;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;

            }
            txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = subtotaliva.ToString();
            decimal total = subtotal + subtotaliva;
            txtTotal.Text = total.ToString();

        }
        private void btnQuitarArticulo_Click(object sender, EventArgs e)
        {
            _cuidador.AgregarMemento(oordencompra.CrearMemento());
            oordencompra.QuitarLineaOrdenCompra((Entidades.Sistema.LineaOrdenCompra)dgvArticulosAgregados.CurrentRow.DataBoundItem);
            dgvArticulosAgregados.DataSource = null;
            dgvArticulosAgregados.DataSource = oordencompra.LineasOrdenCompra;
            CalcularTotales();
        }

        private void btnDeshacer_Click(object sender, EventArgs e)
        {
            if (oordencompra.RestablecerMememnto(_cuidador.RecuperarEstado()))
            {
                dgvArticulosAgregados.DataSource = null;
                dgvArticulosAgregados.DataSource = oordencompra.LineasOrdenCompra;
                CalcularTotales();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (oordencompra == null)
            {
                MessageBox.Show("");
            }
            else if (oordencompra.LineasOrdenCompra.Count != 0 )
            {
                bool resultado = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().AgregarOrdenCompra(oordencompra,  usu);
                if (resultado) MessageBox.Show("Orden de compra generada exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("Ha ocurrido un error.", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
            else MessageBox.Show("Ingrese productos a la OC", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
