﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formAArticuloNP : Form
    {
        public formAArticuloNP()
        {
            InitializeComponent();
        }      

        Entidades.Sistema.Articulo _articulo;
        public formAArticuloNP(Entidades.Sistema.Articulo articulo)
        {
            _articulo = articulo;
            InitializeComponent();
            this.txtDescripcion.Text = _articulo.Descripcion;
            this.txtCantidad.Text = _articulo.CantActual.ToString();
            this.txtPrecioUnitario.Text = _articulo.PrecioVenta.ToString();
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.No;
            this.Close();
        }
        Entidades.Sistema.LineaNotaPedido olinea;
        private void btnAceptar_Click_1(object sender, EventArgs e)
        {            
            if (ValidarDatos()) 
            {
                olinea = new Entidades.Sistema.LineaNotaPedido();
                olinea.Articulo = _articulo;
                olinea.Cantidad = Convert.ToInt32(this.txtCantidadPedida.Text);
                olinea.PrecioUnitario = olinea.Articulo.PrecioVenta;
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;                
            }
            else MessageBox.Show("Complete todos los datos");
        }

        private bool ValidarDatos()
        {
            if (txtCantidadPedida == null) return false;           
            if (txtCantidadPedida.Text == "") return false;
            return true;
        }

        internal Entidades.Sistema.LineaNotaPedido RetornarLineaNP()
        {
            return olinea;
        }

        private void formAArticuloNP_Load(object sender, EventArgs e)
        {

        }     
    
    }
}
