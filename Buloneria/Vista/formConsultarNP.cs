﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formConsultarNP : Form
    {
        private Entidades.Sistema.NotaPedido _np;
        
        public formConsultarNP(Entidades.Sistema.NotaPedido notapedido)
        {
            _np = notapedido;
            InitializeComponent();
          
            this.dgvArticulosAgregados.AutoGenerateColumns = false;
            this.dgvArticulosAgregados.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosAgregados_DataBindingComplete);
        }

        void dgvArticulosAgregados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
       


            foreach (DataGridViewRow fila in this.dgvArticulosAgregados.Rows)
            {
                object objeto = fila.DataBoundItem;
                Entidades.Sistema.LineaNotaPedido oLin = (Entidades.Sistema.LineaNotaPedido)objeto;
                fila.Cells["Articulo"].Value = oLin.Articulo.Descripcion;
                fila.Cells["colPrecioVenta"].Value = oLin.PrecioUnitario.ToString();
                fila.Cells["colSubtotal"].Value = (oLin.PrecioUnitario * oLin.Cantidad).ToString();

            }






        }

        private void formConsultarNP_Load(object sender, EventArgs e)
        {            
            this.cmbFormaPago.DropDownStyle = ComboBoxStyle.Simple;
            this.cmbCliente.DropDownStyle = ComboBoxStyle.Simple;
            txtNumeroNotaPedido.Text = _np.NroNotaPedido.ToString();
            dtpFechaEmision.Text = _np.FechaEmision.ToString();                        
            cmbFormaPago.Text = _np.FormaPago.ToString();
            cmbCliente.Text = _np.Cliente.RazonSocial.ToString();
            this.cmbFormaPago.Enabled = false;           
            dtpFechaEmision.Enabled = false;            
            this.dgvArticulosAgregados.DataSource = _np.LineasNotaPedido;
            decimal subtotaliva = 0; 
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaNotaPedido linea in _np.LineasNotaPedido)
            {
                int subtotallinea = linea.PrecioUnitario * linea.Cantidad;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;
            }
            txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = subtotaliva.ToString();
            decimal total = subtotal + subtotaliva;
            txtTotal.Text = total.ToString();
        }
        
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     

    }
}
