﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista
{
    public partial class formGGruposArticulos : Form
    {
        public formGGruposArticulos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            InitializeComponent();
            this.dgvGruposArticulos.AutoGenerateColumns = false;
            CargarPermisos(Perfiles);
        }

        private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            foreach (Entidades.Seguridad.Perfil perfil in Perfiles)
            {
                if (this.Tag.ToString() == perfil.Formulario.Nombre)
                {
                    foreach (Control control in this.Controls)
                        if (control is Button)
                        {
                            Button boton = (Button)control;
                            if (boton.Tag != null)
                            {
                                if (perfil.Permiso.Tipo == boton.Tag.ToString()) boton.Enabled = true;
                            }
                        }
                }
            }
        }


        private BindingSource bsGruposArticulos;
        private void formGGruposArticulos_Load(object sender, EventArgs e)
        {
            this.bsGruposArticulos = new BindingSource();
            CargarGrilla();
        }

        private void CargarGrilla()
        {
            bsGruposArticulos.DataSource = Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
            dgvGruposArticulos.DataSource = bsGruposArticulos;
        }     

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            bsGruposArticulos.DataSource = Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().FiltrarGruposArticulos(txtBuscar.Text);
            dgvGruposArticulos.DataSource = null;
            dgvGruposArticulos.DataSource = bsGruposArticulos;
        }

        private void ActualizarGrilla()
        {
            dgvGruposArticulos.DataSource = null;
            CargarGrilla();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            formAGrupoArticulo formagrupoarticulo = new formAGrupoArticulo("Agregar");
            formagrupoarticulo.FormClosed += new FormClosedEventHandler(formagrupoarticulo_FormClosed);
            formagrupoarticulo.Show();
        }

        void formagrupoarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            formAGrupoArticulo formmgrupoarticulo = new formAGrupoArticulo((Entidades.Sistema.GrupoArticulo)bsGruposArticulos.Current , "Modificar");
            formmgrupoarticulo.FormClosed += new FormClosedEventHandler(formmgrupoarticulo_FormClosed);
            formmgrupoarticulo.Show();
        }

        void formmgrupoarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            formAGrupoArticulo formcgrupoarticulo = new formAGrupoArticulo((Entidades.Sistema.GrupoArticulo)bsGruposArticulos.Current, "Consultar");
            formcgrupoarticulo.FormClosed += new FormClosedEventHandler(formcgrupoarticulo_FormClosed);
            formcgrupoarticulo.Show();
        }

        void formcgrupoarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            formAGrupoArticulo formegrupoarticulo = new formAGrupoArticulo((Entidades.Sistema.GrupoArticulo)bsGruposArticulos.Current, "Eliminar");
            formegrupoarticulo.FormClosed += new FormClosedEventHandler(formegrupoarticulo_FormClosed);
            formegrupoarticulo.Show();
        }

        void formegrupoarticulo_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.ActualizarGrilla();
        }



      
    }
}
