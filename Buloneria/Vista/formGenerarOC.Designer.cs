﻿namespace Vista
{
    partial class formGenerarOC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumeroOrdenCompra = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbProveedor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvArticulosAgregados = new System.Windows.Forms.DataGridView();
            this.colCantidadPedir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colArticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregarArticulo = new System.Windows.Forms.Button();
            this.btnQuitarArticulo = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSubtotal = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMontoIva = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.dgvArticulosProveedor = new System.Windows.Forms.DataGridView();
            this.colGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCodArticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantMinima = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantActual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAlicuota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSeleccionarProveedor = new System.Windows.Forms.Button();
            this.dtpFechaEmision = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaPlazo = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.btnDeshacer = new System.Windows.Forms.Button();
            this.btnArticuloProveedor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosAgregados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosProveedor)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(430, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bulonería Wacker";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(370, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bv. Seguí 2131 - 2000 Rosario - Santa Fe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Número Orden de Compra:";
            // 
            // txtNumeroOrdenCompra
            // 
            this.txtNumeroOrdenCompra.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtNumeroOrdenCompra.Enabled = false;
            this.txtNumeroOrdenCompra.Location = new System.Drawing.Point(160, 14);
            this.txtNumeroOrdenCompra.Name = "txtNumeroOrdenCompra";
            this.txtNumeroOrdenCompra.Size = new System.Drawing.Size(155, 20);
            this.txtNumeroOrdenCompra.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Fecha de Emisión:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(70, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Fecha de Plazo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Forma de Pago:";
            // 
            // cmbProveedor
            // 
            this.cmbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProveedor.FormattingEnabled = true;
            this.cmbProveedor.Location = new System.Drawing.Point(160, 119);
            this.cmbProveedor.Name = "cmbProveedor";
            this.cmbProveedor.Size = new System.Drawing.Size(155, 21);
            this.cmbProveedor.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(95, 127);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Proveedor:";
            // 
            // dgvArticulosAgregados
            // 
            this.dgvArticulosAgregados.AllowUserToAddRows = false;
            this.dgvArticulosAgregados.AllowUserToDeleteRows = false;
            this.dgvArticulosAgregados.AllowUserToResizeColumns = false;
            this.dgvArticulosAgregados.AllowUserToResizeRows = false;
            this.dgvArticulosAgregados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosAgregados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCantidadPedir,
            this.colArticulo,
            this.colPrecio,
            this.colSubTotal});
            this.dgvArticulosAgregados.Location = new System.Drawing.Point(24, 408);
            this.dgvArticulosAgregados.MultiSelect = false;
            this.dgvArticulosAgregados.Name = "dgvArticulosAgregados";
            this.dgvArticulosAgregados.ReadOnly = true;
            this.dgvArticulosAgregados.RowHeadersVisible = false;
            this.dgvArticulosAgregados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosAgregados.ShowCellErrors = false;
            this.dgvArticulosAgregados.ShowCellToolTips = false;
            this.dgvArticulosAgregados.ShowEditingIcon = false;
            this.dgvArticulosAgregados.ShowRowErrors = false;
            this.dgvArticulosAgregados.Size = new System.Drawing.Size(636, 149);
            this.dgvArticulosAgregados.TabIndex = 17;
            // 
            // colCantidadPedir
            // 
            this.colCantidadPedir.DataPropertyName = "CantPedir";
            this.colCantidadPedir.HeaderText = "Cantidad ";
            this.colCantidadPedir.Name = "colCantidadPedir";
            this.colCantidadPedir.ReadOnly = true;
            // 
            // colArticulo
            // 
            this.colArticulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colArticulo.DataPropertyName = "Articulo";
            this.colArticulo.HeaderText = "Articulo";
            this.colArticulo.Name = "colArticulo";
            this.colArticulo.ReadOnly = true;
            // 
            // colPrecio
            // 
            this.colPrecio.DataPropertyName = "PrecioUnitario";
            this.colPrecio.HeaderText = "Precio Unitario";
            this.colPrecio.Name = "colPrecio";
            this.colPrecio.ReadOnly = true;
            // 
            // colSubTotal
            // 
            this.colSubTotal.HeaderText = "SubTotal";
            this.colSubTotal.Name = "colSubTotal";
            this.colSubTotal.ReadOnly = true;
            // 
            // btnAgregarArticulo
            // 
            this.btnAgregarArticulo.Location = new System.Drawing.Point(24, 332);
            this.btnAgregarArticulo.Name = "btnAgregarArticulo";
            this.btnAgregarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnAgregarArticulo.TabIndex = 5;
            this.btnAgregarArticulo.Text = "Agregar Artículo";
            this.btnAgregarArticulo.UseVisualStyleBackColor = true;
            this.btnAgregarArticulo.Click += new System.EventHandler(this.btnAgregarArticulo_Click);
            // 
            // btnQuitarArticulo
            // 
            this.btnQuitarArticulo.Location = new System.Drawing.Point(105, 332);
            this.btnQuitarArticulo.Name = "btnQuitarArticulo";
            this.btnQuitarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnQuitarArticulo.TabIndex = 6;
            this.btnQuitarArticulo.Text = "Quitar Artículo";
            this.btnQuitarArticulo.UseVisualStyleBackColor = true;
            this.btnQuitarArticulo.Click += new System.EventHandler(this.btnQuitarArticulo_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(504, 570);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Subtotal:";
            // 
            // txtSubtotal
            // 
            this.txtSubtotal.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtSubtotal.Location = new System.Drawing.Point(559, 563);
            this.txtSubtotal.Name = "txtSubtotal";
            this.txtSubtotal.Size = new System.Drawing.Size(100, 20);
            this.txtSubtotal.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(469, 596);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Monto de I.V.A.:";
            // 
            // txtMontoIva
            // 
            this.txtMontoIva.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtMontoIva.Location = new System.Drawing.Point(559, 589);
            this.txtMontoIva.Name = "txtMontoIva";
            this.txtMontoIva.Size = new System.Drawing.Size(100, 20);
            this.txtMontoIva.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(519, 622);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Total:";
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtTotal.Location = new System.Drawing.Point(559, 615);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 25;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.Location = new System.Drawing.Point(23, 596);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(98, 41);
            this.btnAceptar.TabIndex = 8;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(127, 596);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(98, 41);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // dgvArticulosProveedor
            // 
            this.dgvArticulosProveedor.AllowUserToAddRows = false;
            this.dgvArticulosProveedor.AllowUserToDeleteRows = false;
            this.dgvArticulosProveedor.AllowUserToResizeColumns = false;
            this.dgvArticulosProveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosProveedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGrupo,
            this.colCodArticulo,
            this.colDescripcion,
            this.colCantMinima,
            this.colCantActual,
            this.colAlicuota});
            this.dgvArticulosProveedor.Location = new System.Drawing.Point(24, 177);
            this.dgvArticulosProveedor.MultiSelect = false;
            this.dgvArticulosProveedor.Name = "dgvArticulosProveedor";
            this.dgvArticulosProveedor.ReadOnly = true;
            this.dgvArticulosProveedor.RowHeadersVisible = false;
            this.dgvArticulosProveedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosProveedor.ShowCellErrors = false;
            this.dgvArticulosProveedor.ShowCellToolTips = false;
            this.dgvArticulosProveedor.ShowEditingIcon = false;
            this.dgvArticulosProveedor.ShowRowErrors = false;
            this.dgvArticulosProveedor.Size = new System.Drawing.Size(636, 149);
            this.dgvArticulosProveedor.TabIndex = 28;
            // 
            // colGrupo
            // 
            this.colGrupo.DataPropertyName = "Grupo";
            this.colGrupo.HeaderText = "Grupo";
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.ReadOnly = true;
            // 
            // colCodArticulo
            // 
            this.colCodArticulo.DataPropertyName = "codArticulo";
            this.colCodArticulo.HeaderText = "Codigo Articulo";
            this.colCodArticulo.Name = "colCodArticulo";
            this.colCodArticulo.ReadOnly = true;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDescripcion.DataPropertyName = "descripcion";
            this.colDescripcion.HeaderText = "Descripcion";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            // 
            // colCantMinima
            // 
            this.colCantMinima.DataPropertyName = "cantMinima";
            this.colCantMinima.HeaderText = "Cantidad Minima";
            this.colCantMinima.Name = "colCantMinima";
            this.colCantMinima.ReadOnly = true;
            // 
            // colCantActual
            // 
            this.colCantActual.DataPropertyName = "cantActual";
            this.colCantActual.HeaderText = "Cantidad Actual";
            this.colCantActual.Name = "colCantActual";
            this.colCantActual.ReadOnly = true;
            // 
            // colAlicuota
            // 
            this.colAlicuota.DataPropertyName = "Alicuota";
            this.colAlicuota.HeaderText = "Alicuota";
            this.colAlicuota.Name = "colAlicuota";
            this.colAlicuota.ReadOnly = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(246, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Artículos Disponibles del Proveedor Seleccionado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 392);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(212, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Artículos Agregados a la Orden de Compra:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(431, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 16);
            this.label3.TabIndex = 33;
            this.label3.Text = "TeleFax: 0341-4620890";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // btnSeleccionarProveedor
            // 
            this.btnSeleccionarProveedor.Location = new System.Drawing.Point(321, 117);
            this.btnSeleccionarProveedor.Name = "btnSeleccionarProveedor";
            this.btnSeleccionarProveedor.Size = new System.Drawing.Size(128, 23);
            this.btnSeleccionarProveedor.TabIndex = 4;
            this.btnSeleccionarProveedor.Text = "Seleccionar Proveedor";
            this.btnSeleccionarProveedor.UseVisualStyleBackColor = true;
            this.btnSeleccionarProveedor.Click += new System.EventHandler(this.btnSeleccionarProveedor_Click);
            // 
            // dtpFechaEmision
            // 
            this.dtpFechaEmision.Location = new System.Drawing.Point(160, 40);
            this.dtpFechaEmision.Name = "dtpFechaEmision";
            this.dtpFechaEmision.Size = new System.Drawing.Size(155, 20);
            this.dtpFechaEmision.TabIndex = 0;
            this.dtpFechaEmision.ValueChanged += new System.EventHandler(this.dtpFechaEmision_ValueChanged);
            // 
            // dtpFechaPlazo
            // 
            this.dtpFechaPlazo.Location = new System.Drawing.Point(160, 66);
            this.dtpFechaPlazo.Name = "dtpFechaPlazo";
            this.dtpFechaPlazo.Size = new System.Drawing.Size(155, 20);
            this.dtpFechaPlazo.TabIndex = 1;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(160, 92);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(155, 21);
            this.cmbFormaPago.TabIndex = 2;
            // 
            // btnDeshacer
            // 
            this.btnDeshacer.Location = new System.Drawing.Point(186, 332);
            this.btnDeshacer.Name = "btnDeshacer";
            this.btnDeshacer.Size = new System.Drawing.Size(75, 48);
            this.btnDeshacer.TabIndex = 7;
            this.btnDeshacer.Text = "Deshacer";
            this.btnDeshacer.UseVisualStyleBackColor = true;
            this.btnDeshacer.Click += new System.EventHandler(this.btnDeshacer_Click);
            // 
            // btnArticuloProveedor
            // 
            this.btnArticuloProveedor.Location = new System.Drawing.Point(508, 151);
            this.btnArticuloProveedor.Name = "btnArticuloProveedor";
            this.btnArticuloProveedor.Size = new System.Drawing.Size(152, 23);
            this.btnArticuloProveedor.TabIndex = 34;
            this.btnArticuloProveedor.Text = "Agregar Artículo Proveedor";
            this.btnArticuloProveedor.UseVisualStyleBackColor = true;
            // 
            // formGenerarOC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 646);
            this.Controls.Add(this.btnArticuloProveedor);
            this.Controls.Add(this.btnDeshacer);
            this.Controls.Add(this.cmbFormaPago);
            this.Controls.Add(this.dtpFechaPlazo);
            this.Controls.Add(this.dtpFechaEmision);
            this.Controls.Add(this.btnSeleccionarProveedor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dgvArticulosProveedor);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtMontoIva);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtSubtotal);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnQuitarArticulo);
            this.Controls.Add(this.btnAgregarArticulo);
            this.Controls.Add(this.dgvArticulosAgregados);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cmbProveedor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNumeroOrdenCompra);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "formGenerarOC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "Alta";
            this.Text = "Generar Orden de Compra";
            this.Load += new System.EventHandler(this.formGenerarOC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosAgregados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosProveedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumeroOrdenCompra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbProveedor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvArticulosAgregados;
        private System.Windows.Forms.Button btnAgregarArticulo;
        private System.Windows.Forms.Button btnQuitarArticulo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSubtotal;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMontoIva;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.DataGridView dgvArticulosProveedor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSeleccionarProveedor;
        private System.Windows.Forms.DateTimePicker dtpFechaEmision;
        private System.Windows.Forms.DateTimePicker dtpFechaPlazo;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Button btnDeshacer;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantidadPedir;
        private System.Windows.Forms.DataGridViewTextBoxColumn colArticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrecio;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSubTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCodArticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantMinima;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantActual;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAlicuota;
        private System.Windows.Forms.Button btnArticuloProveedor;
    }
}