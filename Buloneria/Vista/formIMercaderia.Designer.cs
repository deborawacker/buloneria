﻿namespace Vista
{
    partial class formIMercaderia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.txtNumeroRemito = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvArticulosOC = new System.Windows.Forms.DataGridView();
            this.colCantPedir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colArticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregarArticulo = new System.Windows.Forms.Button();
            this.btnQuitarArticulo = new System.Windows.Forms.Button();
            this.dgvArticulosAgregados = new System.Windows.Forms.DataGridView();
            this.colcodArticulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coldescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantIngresada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpFechaRecepcion = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaRemito = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.dgvArticulosFaltantes = new System.Windows.Forms.DataGridView();
            this.colDesArt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantFalt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNroOrdenCompra = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosAgregados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFaltantes)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Número del Remito:";
            // 
            // txtNumeroRemito
            // 
            this.txtNumeroRemito.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumeroRemito.Location = new System.Drawing.Point(131, 36);
            this.txtNumeroRemito.Name = "txtNumeroRemito";
            this.txtNumeroRemito.Size = new System.Drawing.Size(155, 20);
            this.txtNumeroRemito.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Fecha del Remito:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Fecha de Recepción:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 382);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Artículos Agregados al Ingreso de Mercadería:";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnAceptar.Location = new System.Drawing.Point(452, 641);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(98, 41);
            this.btnAceptar.TabIndex = 9;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar1
            // 
            this.btnCancelar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnCancelar1.Location = new System.Drawing.Point(556, 641);
            this.btnCancelar1.Name = "btnCancelar1";
            this.btnCancelar1.Size = new System.Drawing.Size(98, 41);
            this.btnCancelar1.TabIndex = 10;
            this.btnCancelar1.Text = "Cancelar";
            this.btnCancelar1.UseVisualStyleBackColor = true;
            this.btnCancelar1.Click += new System.EventHandler(this.btnCancelar1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(425, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(175, 24);
            this.label12.TabIndex = 40;
            this.label12.Text = "Bulonería Wacker";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(364, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(289, 16);
            this.label13.TabIndex = 41;
            this.label13.Text = "Bv. Seguí 2131 - 2000 Rosario - Santa Fe";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(426, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(166, 16);
            this.label14.TabIndex = 42;
            this.label14.Text = "TeleFax: 0341-4620890";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(232, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Artículos de la Orden de Compra Seleccionada:";
            // 
            // dgvArticulosOC
            // 
            this.dgvArticulosOC.AllowUserToAddRows = false;
            this.dgvArticulosOC.AllowUserToDeleteRows = false;
            this.dgvArticulosOC.AllowUserToResizeColumns = false;
            this.dgvArticulosOC.AllowUserToResizeRows = false;
            this.dgvArticulosOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosOC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCantPedir,
            this.colArticulo});
            this.dgvArticulosOC.Location = new System.Drawing.Point(15, 189);
            this.dgvArticulosOC.MultiSelect = false;
            this.dgvArticulosOC.Name = "dgvArticulosOC";
            this.dgvArticulosOC.ReadOnly = true;
            this.dgvArticulosOC.RowHeadersVisible = false;
            this.dgvArticulosOC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosOC.ShowCellErrors = false;
            this.dgvArticulosOC.ShowCellToolTips = false;
            this.dgvArticulosOC.ShowEditingIcon = false;
            this.dgvArticulosOC.ShowRowErrors = false;
            this.dgvArticulosOC.Size = new System.Drawing.Size(636, 136);
            this.dgvArticulosOC.TabIndex = 46;
            // 
            // colCantPedir
            // 
            this.colCantPedir.DataPropertyName = "CantPedir";
            this.colCantPedir.HeaderText = "Cantidad ";
            this.colCantPedir.Name = "colCantPedir";
            this.colCantPedir.ReadOnly = true;
            // 
            // colArticulo
            // 
            this.colArticulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colArticulo.DataPropertyName = "Articulo";
            this.colArticulo.HeaderText = "Articulo";
            this.colArticulo.Name = "colArticulo";
            this.colArticulo.ReadOnly = true;
            // 
            // btnAgregarArticulo
            // 
            this.btnAgregarArticulo.Location = new System.Drawing.Point(15, 331);
            this.btnAgregarArticulo.Name = "btnAgregarArticulo";
            this.btnAgregarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnAgregarArticulo.TabIndex = 7;
            this.btnAgregarArticulo.Text = "Agregar Artículo";
            this.btnAgregarArticulo.UseVisualStyleBackColor = true;
            this.btnAgregarArticulo.Click += new System.EventHandler(this.btnAgregarArticulo_Click);
            // 
            // btnQuitarArticulo
            // 
            this.btnQuitarArticulo.Location = new System.Drawing.Point(96, 331);
            this.btnQuitarArticulo.Name = "btnQuitarArticulo";
            this.btnQuitarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnQuitarArticulo.TabIndex = 8;
            this.btnQuitarArticulo.Text = "Quitar Artículo";
            this.btnQuitarArticulo.UseVisualStyleBackColor = true;
            this.btnQuitarArticulo.Click += new System.EventHandler(this.btnQuitarArticulo_Click);
            // 
            // dgvArticulosAgregados
            // 
            this.dgvArticulosAgregados.AllowUserToAddRows = false;
            this.dgvArticulosAgregados.AllowUserToDeleteRows = false;
            this.dgvArticulosAgregados.AllowUserToResizeColumns = false;
            this.dgvArticulosAgregados.AllowUserToResizeRows = false;
            this.dgvArticulosAgregados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosAgregados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colcodArticulo,
            this.coldescripcion,
            this.colCantIngresada});
            this.dgvArticulosAgregados.Location = new System.Drawing.Point(18, 398);
            this.dgvArticulosAgregados.MultiSelect = false;
            this.dgvArticulosAgregados.Name = "dgvArticulosAgregados";
            this.dgvArticulosAgregados.ReadOnly = true;
            this.dgvArticulosAgregados.RowHeadersVisible = false;
            this.dgvArticulosAgregados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosAgregados.ShowCellErrors = false;
            this.dgvArticulosAgregados.ShowCellToolTips = false;
            this.dgvArticulosAgregados.ShowEditingIcon = false;
            this.dgvArticulosAgregados.ShowRowErrors = false;
            this.dgvArticulosAgregados.Size = new System.Drawing.Size(636, 129);
            this.dgvArticulosAgregados.TabIndex = 24;
            this.dgvArticulosAgregados.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArticulosAgregados_CellContentClick);
            // 
            // colcodArticulo
            // 
            this.colcodArticulo.DataPropertyName = "CodArticulo";
            this.colcodArticulo.HeaderText = "Cod Articulo";
            this.colcodArticulo.Name = "colcodArticulo";
            this.colcodArticulo.ReadOnly = true;
            // 
            // coldescripcion
            // 
            this.coldescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.coldescripcion.DataPropertyName = "Descripcion";
            this.coldescripcion.HeaderText = "Descripcion";
            this.coldescripcion.Name = "coldescripcion";
            this.coldescripcion.ReadOnly = true;
            // 
            // colCantIngresada
            // 
            this.colCantIngresada.HeaderText = "Cant Ingresada";
            this.colCantIngresada.Name = "colCantIngresada";
            this.colCantIngresada.ReadOnly = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 13);
            this.label15.TabIndex = 51;
            this.label15.Text = "Número Orden Compra:";
            // 
            // dtpFechaRecepcion
            // 
            this.dtpFechaRecepcion.Location = new System.Drawing.Point(131, 10);
            this.dtpFechaRecepcion.Name = "dtpFechaRecepcion";
            this.dtpFechaRecepcion.Size = new System.Drawing.Size(155, 20);
            this.dtpFechaRecepcion.TabIndex = 0;
            // 
            // dtpFechaRemito
            // 
            this.dtpFechaRemito.Location = new System.Drawing.Point(131, 62);
            this.dtpFechaRemito.Name = "dtpFechaRemito";
            this.dtpFechaRemito.Size = new System.Drawing.Size(155, 20);
            this.dtpFechaRemito.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 559);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 65;
            this.label3.Text = "Articulos Faltantes:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(394, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 66;
            this.label7.Text = "Estado Orden Compra:";
            // 
            // txtEstado
            // 
            this.txtEstado.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtEstado.Enabled = false;
            this.txtEstado.Location = new System.Drawing.Point(514, 143);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(137, 20);
            this.txtEstado.TabIndex = 67;
            // 
            // dgvArticulosFaltantes
            // 
            this.dgvArticulosFaltantes.AllowUserToAddRows = false;
            this.dgvArticulosFaltantes.AllowUserToDeleteRows = false;
            this.dgvArticulosFaltantes.AllowUserToResizeColumns = false;
            this.dgvArticulosFaltantes.AllowUserToResizeRows = false;
            this.dgvArticulosFaltantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosFaltantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDesArt,
            this.colCantFalt});
            this.dgvArticulosFaltantes.Location = new System.Drawing.Point(18, 575);
            this.dgvArticulosFaltantes.MultiSelect = false;
            this.dgvArticulosFaltantes.Name = "dgvArticulosFaltantes";
            this.dgvArticulosFaltantes.ReadOnly = true;
            this.dgvArticulosFaltantes.RowHeadersVisible = false;
            this.dgvArticulosFaltantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosFaltantes.ShowCellErrors = false;
            this.dgvArticulosFaltantes.ShowCellToolTips = false;
            this.dgvArticulosFaltantes.ShowEditingIcon = false;
            this.dgvArticulosFaltantes.ShowRowErrors = false;
            this.dgvArticulosFaltantes.Size = new System.Drawing.Size(335, 104);
            this.dgvArticulosFaltantes.TabIndex = 68;
            // 
            // colDesArt
            // 
            this.colDesArt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDesArt.DataPropertyName = "ArticulosFaltantes.LineaOrdenCompra.Articulo.Descripcion";
            this.colDesArt.HeaderText = "Descripcion";
            this.colDesArt.Name = "colDesArt";
            this.colDesArt.ReadOnly = true;
            // 
            // colCantFalt
            // 
            this.colCantFalt.DataPropertyName = "CantFaltante";
            this.colCantFalt.HeaderText = "Cant Faltante";
            this.colCantFalt.Name = "colCantFalt";
            this.colCantFalt.ReadOnly = true;
            // 
            // txtNroOrdenCompra
            // 
            this.txtNroOrdenCompra.Location = new System.Drawing.Point(151, 88);
            this.txtNroOrdenCompra.Name = "txtNroOrdenCompra";
            this.txtNroOrdenCompra.Size = new System.Drawing.Size(135, 20);
            this.txtNroOrdenCompra.TabIndex = 69;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(292, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 70;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 71;
            this.label4.Text = "Proveedor:";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox1.Location = new System.Drawing.Point(151, 113);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 72;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 73;
            this.label5.Text = "Fecha Orden Compra:";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox2.Location = new System.Drawing.Point(151, 139);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 74;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(177, 331);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 48);
            this.button2.TabIndex = 75;
            this.button2.Text = "Deshacer";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // formIMercaderia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 691);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtNroOrdenCompra);
            this.Controls.Add(this.dgvArticulosFaltantes);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFechaRemito);
            this.Controls.Add(this.dtpFechaRecepcion);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnQuitarArticulo);
            this.Controls.Add(this.btnAgregarArticulo);
            this.Controls.Add(this.dgvArticulosOC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnCancelar1);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvArticulosAgregados);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtNumeroRemito);
            this.Controls.Add(this.label9);
            this.Name = "formIMercaderia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "Ata";
            this.Text = "Ingresar Mercadería";
            this.Load += new System.EventHandler(this.formIMercaderia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosAgregados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFaltantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNumeroRemito;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvArticulosOC;
        private System.Windows.Forms.Button btnAgregarArticulo;
        private System.Windows.Forms.Button btnQuitarArticulo;
        private System.Windows.Forms.DataGridView dgvArticulosAgregados;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtpFechaRecepcion;
        private System.Windows.Forms.DateTimePicker dtpFechaRemito;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.DataGridView dgvArticulosFaltantes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantFalt;
        private System.Windows.Forms.TextBox txtNroOrdenCompra;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantPedir;
        private System.Windows.Forms.DataGridViewTextBoxColumn colArticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colcodArticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn coldescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantIngresada;
        private System.Windows.Forms.Button button2;

        public System.EventHandler label12_Click { get; set; }
    }
}