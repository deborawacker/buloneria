﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formRegistrarPagoCliente : Form
    {
        public formRegistrarPagoCliente()
        {
            InitializeComponent();
        }

        List<Entidades.Sistema.Cliente> clientes;
        private void formRegistrarPagoCliente_Load(object sender, EventArgs e)
        {
            clientes = Controladora.Sistema.ControladoraCURegistrarPagoCliente.ObtenerInstancia().RecuperarClientes();
            cmbCliente.DataSource = clientes;
            cmbCliente.DisplayMember = "razonSocial";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
   

        private void cmbCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            Entidades.Sistema.Cliente ocliente = (Entidades.Sistema.Cliente)cmbCliente.SelectedItem;
            txtCuit.Text = ocliente.Cuit.ToString();
            txtDireccion.Text = ocliente.Direccion;
            txtCiudad.Text = ocliente.Ciudad.Nombre;
            txtProvincia.Text = ocliente.Ciudad.Provincia.Nombre;
            txtTelefono.Text = ocliente.Telefono;
            txtFax.Text = ocliente.Fax;
            txtEmail.Text = ocliente.Email;
            txtSaldoCliente.Text = Controladora.Sistema.ControladoraCURegistrarPagoCliente.ObtenerInstancia().RecuperarSaldoCliente(ocliente).ToString();
            
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {             
                Entidades.Sistema.MovimientoCtaCte oMov = new Entidades.Sistema.MovimientoCtaCte();
                oMov.Monto=Convert.ToInt32(txtMontoAPagar.Text);
                oMov.Fecha = dtpFechaRecibo.Value;
                
                if (Controladora.Sistema.ControladoraCURegistrarPagoCliente.ObtenerInstancia().RegistrarPago(oMov, (Entidades.Sistema.Cliente)cmbCliente.SelectedItem))
                {
                    MessageBox.Show("Pago registrado exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else MessageBox.Show("Complete todos los datos");

        }

        private bool ValidarDatos()
        {
            if (txtMontoAPagar.Text == "") return false;
            if (txtMontoAPagar.Text == null) return false;
            return true;

        }

        
    }
}
