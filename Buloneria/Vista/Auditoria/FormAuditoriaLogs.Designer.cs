﻿namespace Vista.Auditoria
{
    partial class FormAuditoriaLogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAuditoriaLogs = new System.Windows.Forms.DataGridView();
            this.colFechaHora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOperacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEquipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lnklblActualizar = new System.Windows.Forms.LinkLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbPageLoginout = new System.Windows.Forms.TabPage();
            this.tbPageOC = new System.Windows.Forms.TabPage();
            this.dgvAuditoriaOC = new System.Windows.Forms.DataGridView();
            this.colFecHora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUsu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNroOC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoriaLogs)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tbPageLoginout.SuspendLayout();
            this.tbPageOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoriaOC)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAuditoriaLogs
            // 
            this.dgvAuditoriaLogs.AllowUserToAddRows = false;
            this.dgvAuditoriaLogs.AllowUserToDeleteRows = false;
            this.dgvAuditoriaLogs.AllowUserToResizeColumns = false;
            this.dgvAuditoriaLogs.AllowUserToResizeRows = false;
            this.dgvAuditoriaLogs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAuditoriaLogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAuditoriaLogs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFechaHora,
            this.colUsuario,
            this.colOperacion,
            this.colEquipo});
            this.dgvAuditoriaLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAuditoriaLogs.Location = new System.Drawing.Point(3, 3);
            this.dgvAuditoriaLogs.MultiSelect = false;
            this.dgvAuditoriaLogs.Name = "dgvAuditoriaLogs";
            this.dgvAuditoriaLogs.ReadOnly = true;
            this.dgvAuditoriaLogs.RowHeadersVisible = false;
            this.dgvAuditoriaLogs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAuditoriaLogs.ShowCellErrors = false;
            this.dgvAuditoriaLogs.ShowCellToolTips = false;
            this.dgvAuditoriaLogs.ShowEditingIcon = false;
            this.dgvAuditoriaLogs.ShowRowErrors = false;
            this.dgvAuditoriaLogs.Size = new System.Drawing.Size(612, 445);
            this.dgvAuditoriaLogs.TabIndex = 2;
            // 
            // colFechaHora
            // 
            this.colFechaHora.DataPropertyName = "FechaHora";
            this.colFechaHora.HeaderText = "Fecha - Hora";
            this.colFechaHora.Name = "colFechaHora";
            this.colFechaHora.ReadOnly = true;
            // 
            // colUsuario
            // 
            this.colUsuario.DataPropertyName = "Usuario";
            this.colUsuario.HeaderText = "Usuario";
            this.colUsuario.Name = "colUsuario";
            this.colUsuario.ReadOnly = true;
            // 
            // colOperacion
            // 
            this.colOperacion.DataPropertyName = "Operacion";
            this.colOperacion.HeaderText = "Operación";
            this.colOperacion.Name = "colOperacion";
            this.colOperacion.ReadOnly = true;
            // 
            // colEquipo
            // 
            this.colEquipo.DataPropertyName = "NombreEquipo";
            this.colEquipo.HeaderText = "Equipo";
            this.colEquipo.Name = "colEquipo";
            this.colEquipo.ReadOnly = true;
            // 
            // lnklblActualizar
            // 
            this.lnklblActualizar.AutoSize = true;
            this.lnklblActualizar.Location = new System.Drawing.Point(12, 9);
            this.lnklblActualizar.Name = "lnklblActualizar";
            this.lnklblActualizar.Size = new System.Drawing.Size(53, 13);
            this.lnklblActualizar.TabIndex = 3;
            this.lnklblActualizar.TabStop = true;
            this.lnklblActualizar.Text = "Actualizar";
            this.lnklblActualizar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblActualizar_LinkClicked);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbPageLoginout);
            this.tabControl1.Controls.Add(this.tbPageOC);
            this.tabControl1.Location = new System.Drawing.Point(13, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(626, 477);
            this.tabControl1.TabIndex = 4;
            // 
            // tbPageLoginout
            // 
            this.tbPageLoginout.Controls.Add(this.dgvAuditoriaLogs);
            this.tbPageLoginout.Location = new System.Drawing.Point(4, 22);
            this.tbPageLoginout.Name = "tbPageLoginout";
            this.tbPageLoginout.Padding = new System.Windows.Forms.Padding(3);
            this.tbPageLoginout.Size = new System.Drawing.Size(618, 451);
            this.tbPageLoginout.TabIndex = 0;
            this.tbPageLoginout.Text = "Log In - Log Out";
            this.tbPageLoginout.UseVisualStyleBackColor = true;
            // 
            // tbPageOC
            // 
            this.tbPageOC.Controls.Add(this.dgvAuditoriaOC);
            this.tbPageOC.Location = new System.Drawing.Point(4, 22);
            this.tbPageOC.Name = "tbPageOC";
            this.tbPageOC.Padding = new System.Windows.Forms.Padding(3);
            this.tbPageOC.Size = new System.Drawing.Size(618, 451);
            this.tbPageOC.TabIndex = 1;
            this.tbPageOC.Text = "Orden de Compra";
            this.tbPageOC.UseVisualStyleBackColor = true;
            // 
            // dgvAuditoriaOC
            // 
            this.dgvAuditoriaOC.AllowUserToAddRows = false;
            this.dgvAuditoriaOC.AllowUserToDeleteRows = false;
            this.dgvAuditoriaOC.AllowUserToResizeColumns = false;
            this.dgvAuditoriaOC.AllowUserToResizeRows = false;
            this.dgvAuditoriaOC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAuditoriaOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAuditoriaOC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFecHora,
            this.colUsu,
            this.colNroOC,
            this.colEstado});
            this.dgvAuditoriaOC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAuditoriaOC.Location = new System.Drawing.Point(3, 3);
            this.dgvAuditoriaOC.MultiSelect = false;
            this.dgvAuditoriaOC.Name = "dgvAuditoriaOC";
            this.dgvAuditoriaOC.ReadOnly = true;
            this.dgvAuditoriaOC.RowHeadersVisible = false;
            this.dgvAuditoriaOC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAuditoriaOC.ShowCellErrors = false;
            this.dgvAuditoriaOC.ShowCellToolTips = false;
            this.dgvAuditoriaOC.ShowEditingIcon = false;
            this.dgvAuditoriaOC.ShowRowErrors = false;
            this.dgvAuditoriaOC.Size = new System.Drawing.Size(612, 445);
            this.dgvAuditoriaOC.TabIndex = 0;
            // 
            // colFecHora
            // 
            this.colFecHora.DataPropertyName = "FechaHora";
            this.colFecHora.HeaderText = "Fecha - Hora";
            this.colFecHora.Name = "colFecHora";
            this.colFecHora.ReadOnly = true;
            // 
            // colUsu
            // 
            this.colUsu.DataPropertyName = "Usuario";
            this.colUsu.HeaderText = "Usuario";
            this.colUsu.Name = "colUsu";
            this.colUsu.ReadOnly = true;
            // 
            // colNroOC
            // 
            this.colNroOC.DataPropertyName = "NroOrdenCompra";
            this.colNroOC.HeaderText = "Nro OC";
            this.colNroOC.Name = "colNroOC";
            this.colNroOC.ReadOnly = true;
            // 
            // colEstado
            // 
            this.colEstado.DataPropertyName = "Estado";
            this.colEstado.HeaderText = "Estado";
            this.colEstado.Name = "colEstado";
            this.colEstado.ReadOnly = true;
            // 
            // FormAuditoriaLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 515);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lnklblActualizar);
            this.Name = "FormAuditoriaLogs";
            this.Text = "Auditoria";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoriaLogs)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tbPageLoginout.ResumeLayout(false);
            this.tbPageOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuditoriaOC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAuditoriaLogs;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFechaHora;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOperacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEquipo;
        private System.Windows.Forms.LinkLabel lnklblActualizar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbPageLoginout;
        private System.Windows.Forms.TabPage tbPageOC;
        private System.Windows.Forms.DataGridView dgvAuditoriaOC;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFecHora;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUsu;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNroOC;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEstado;
    }
}