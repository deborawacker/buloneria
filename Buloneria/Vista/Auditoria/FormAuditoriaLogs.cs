﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Vista.Auditoria
{
    public partial class FormAuditoriaLogs : Form
    {
        public FormAuditoriaLogs()
        {
            InitializeComponent();
            this.bsAuditoriaIngresosEgresos = new BindingSource();
            this.bsAuditoriaEstadosOC = new BindingSource();
            this.CargarGrilla();
        }

        private BindingSource bsAuditoriaIngresosEgresos;
        private BindingSource bsAuditoriaEstadosOC;
    
        //en el cargar grilla voy a cargar las dos grillas de auditoria login y estadoOC
        private void CargarGrilla()
        {
            this.bsAuditoriaIngresosEgresos.DataSource = Controladora.Auditoria.ControladoraCUVisualizarListadosAuditoriaLog.ObtenerInstancia().RecuperarListadosIngresosEgresos();
            this.dgvAuditoriaLogs.AutoGenerateColumns = false;
            dgvAuditoriaLogs.DataSource = this.bsAuditoriaIngresosEgresos;

            this.bsAuditoriaEstadosOC.DataSource = Controladora.Auditoria.ControladoraCUVisualizarListadoAuditoriaOC.ObtenerInstancia().RecuperarListadosEstados();
            this.dgvAuditoriaOC.AutoGenerateColumns = false;
            dgvAuditoriaOC.DataSource = this.bsAuditoriaEstadosOC;
        }

        private void lnklblActualizar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.dgvAuditoriaLogs.DataSource = null;
            this.bsAuditoriaIngresosEgresos.DataSource = Controladora.Auditoria.ControladoraCUVisualizarListadosAuditoriaLog.ObtenerInstancia().Actualizar();
            this.dgvAuditoriaLogs.AutoGenerateColumns = false;
            dgvAuditoriaLogs.DataSource = this.bsAuditoriaIngresosEgresos;

            this.dgvAuditoriaOC.DataSource = null;
            this.bsAuditoriaEstadosOC.DataSource = Controladora.Auditoria.ControladoraCUVisualizarListadoAuditoriaOC.ObtenerInstancia().Actualizar();
            this.dgvAuditoriaOC.AutoGenerateColumns = false;
            dgvAuditoriaOC.DataSource = this.bsAuditoriaEstadosOC;
        }

      

    }
}
