﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formVCtaCteCliente : Form
    {
        public formVCtaCteCliente()
        {
            InitializeComponent();
            this.dtpFechaDesde.Value = DateTime.Now.Subtract(new TimeSpan(60,0,0,0));
            this.dgvMovimientosCliente.AutoGenerateColumns = false;
        }

        List<Entidades.Sistema.Cliente> clientes;

        //en el metodo load cargo combo clientes
        private void formVCtaCteCliente_Load(object sender, EventArgs e)
        {
            clientes = Controladora.Sistema.ControladoraCUVisCtaCteCliente.ObtenerInstancia().RecuperarClientes();
            cmbCliente.DataSource = clientes;
            cmbCliente.DisplayMember = "razonSocial";
        }      

        //en el evento selectedindexchanged del combo, cada vez que se carga o se cambia el combo, directamente se muestra el cliente seleccionado
        private void cmbCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            Entidades.Sistema.Cliente ocliente = (Entidades.Sistema.Cliente)cmbCliente.SelectedItem;
            txtCuit.Text = ocliente.Cuit.ToString();
            txtCiudad.Text = ocliente.Ciudad.Nombre;
            txtDireccion.Text = ocliente.Direccion;
            txtEmail.Text = ocliente.Email;
            txtFax.Text = ocliente.Fax;
            txtProvincia.Text = ocliente.Ciudad.Provincia.Nombre;
            txtTelefono.Text = ocliente.Telefono;
        }

        Entidades.Sistema.CuentaCorriente ctactefiltrada;
        private void btnMostrar_Click(object sender, EventArgs e)
        {
            Entidades.Sistema.Cliente ocliente = (Entidades.Sistema.Cliente)cmbCliente.SelectedItem;
            ctactefiltrada = Controladora.Sistema.ControladoraCUVisCtaCteCliente.ObtenerInstancia().BuscarCuentaCorriente(ocliente,dtpFechaDesde.Value,dtpFechaHasta.Value);
            dgvMovimientosCliente.DataSource = ctactefiltrada.MovimientosCtaCte;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        int saldo = 0;
        //aca programo mi grilla
        private void dgvMovimientosCliente_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            int debe = 0 ;
            int haber = 0;
            foreach (DataGridViewRow item in this.dgvMovimientosCliente.Rows)
            {
                Entidades.Sistema.MovimientoCtaCte ctacte = (Entidades.Sistema.MovimientoCtaCte)dgvMovimientosCliente.Rows[item.Index].DataBoundItem;
                item.Cells["Movimiento"].Value = ctacte.Concepto.Descripcion;
                if (ctacte.Concepto.TipoMovimiento == Entidades.Sistema.TipoMovimiento.Debe)
                {
                    item.Cells["Debe"].Value = ctacte.Monto;
                    debe = debe + ctacte.Monto;                        
                }
                else
                {
                    item.Cells["Haber"].Value = ctacte.Monto;
                    haber = haber + ctacte.Monto;
                }
            }
            saldo = debe - haber;
            label12.Text = saldo.ToString();
            
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            Microsoft.Reporting.WinForms.ReportParameter[] Parametros = new Microsoft.Reporting.WinForms.ReportParameter[2];  //instancio un arreglo de tamano 7
            Parametros[0] = new Microsoft.Reporting.WinForms.ReportParameter("Saldo",ctactefiltrada.Saldo.ToString());
            Parametros[1] = new Microsoft.Reporting.WinForms.ReportParameter("RazonSocial", ctactefiltrada.Cliente.RazonSocial);
          
            BindingSource bsLineas = new BindingSource();
            List<Entidades.Sistema.MovimientoCtaCte> ListaMovs = new List<Entidades.Sistema.MovimientoCtaCte>();
            ListaMovs = ctactefiltrada.MovimientosCtaCte;
            bsLineas.DataSource = ListaMovs;
            Reportes.FormReportes formReportes = new Reportes.FormReportes("ResumenCuenta", bsLineas, Parametros);
            formReportes.Show();
        }


    }
}
