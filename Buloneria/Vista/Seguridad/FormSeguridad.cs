﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace Vista.Seguridad
{
    public partial class FormSeguridad : Form
    {
        Entidades.Seguridad.Usuario oUsuarioLogueado;
        public FormSeguridad(Entidades.Seguridad.Usuario UsuarioLogueado)
        {
            InitializeComponent();
            oUsuarioLogueado = UsuarioLogueado;
        }

        private void btnAgregarPerfil_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Perfil oPerfil = new Entidades.Seguridad.Perfil();
            oPerfil.Grupo = (Entidades.Seguridad.Grupo)cBoxGrupos.SelectedItem;
            oPerfil.Formulario = (Entidades.Seguridad.Formulario)cBoxFormularios.SelectedItem;
            oPerfil.Permiso = (Entidades.Seguridad.Permiso)cBoxPermisos.SelectedItem;
            if (Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().AgregarPerfil(oPerfil))
            {
                this.ActualizarGrillaPerfiles();
            }
            else
            {
                MessageBox.Show("El perfil no se puede agregar. Verifique que no exista un perfil con los datos seleccionados.");
            }
        }
        BindingSource bsPerfiles, bsGrupos, bsUsuarios;

        private void FormSeguridad_Load(object sender, EventArgs e)
        {
            ActualizarGrillaUsuarios();
            //RecorrerBotones(tabControl1);
        }
     /*   private void CargarPermisos(ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles)
        {
            
            string tagBoton;
                foreach (TabPage oTabPage in tabControl1.TabPages)
                {
                    if(oTabPage.Tag=="Usuarios")
                    {
                       foreach (Entidades.Seguridad.Perfil oPerfil in Perfiles)
                       {
                           if (oPerfil.Formulario.Nombre == "Usuarios")
                           {
                               tagBoton = "boton";
                               tagBoton += oPerfil.Permiso.Tipo;
                               tagBoton += "Usuarios";
                               //falta q habilite el boton que correspondo al perfil encontrado
                           } 
                      }
                        
                 }
                 else if(oTabPage.Tag=="Grupos")
                    {
                            foreach (Entidades.Seguridad.Perfil oPerfil in Perfiles)
                            {
                                if (oPerfil.Formulario.Nombre == "Grupos")
                                {
                                    tagBoton = "boton";
                                    tagBoton += oPerfil.Permiso.Tipo;
                                    tagBoton += "Grupos";
                                    //falta q habilite el boton que correspondo al perfil encontrado
                                }
                            }

                        
                    }
                    else if (oTabPage.Tag == "Perfiles")
                    {
                        foreach (Entidades.Seguridad.Perfil oPerfil in Perfiles)
                        {
                            if (oPerfil.Formulario.Nombre == "Perfiles")
                            {
                                tagBoton = "boton";
                                tagBoton += oPerfil.Permiso.Tipo;
                                tagBoton += "Perfiles";
                                //falta q habilite el boton que correspondo al perfil encontrado
                            }
                        }
                    }
                }
        }
       */
        private void btnEliminarPerfil_Click(object sender, EventArgs e)
        {
            DialogResult Dr = MessageBox.Show("¿Desea eliminar el perfil seleccionado en la grilla?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Dr == System.Windows.Forms.DialogResult.Yes)
            {
                if (Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().EliminarPerfil((Entidades.Seguridad.Perfil)bsPerfiles.Current))
                {
                    this.ActualizarGrillaPerfiles();
                }
                else
                {
                    MessageBox.Show("No se ha podido eliminar el Perfil");
                }
            }
        }

        private void ActualizarGrillaPerfiles()
        {
            this.bsPerfiles.ResetBindings(false);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (this.tabControl1.SelectedTab.Text == "Perfiles")
            {
                this.cBoxGrupos.DataSource = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarGrupos();
                this.cBoxGrupos.DisplayMember = "Nombre";
                this.cBoxFormularios.DataSource = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarFormularios();
                this.cBoxFormularios.DisplayMember = "Nombre";
                this.cBoxPermisos.DataSource = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarPermisos();
                this.cBoxPermisos.DisplayMember = "Tipo";
                bsPerfiles = new BindingSource();
                dgvPerfiles.AutoGenerateColumns = false;
                this.bsPerfiles.DataSource = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarPerfiles();
                this.dgvPerfiles.DataSource = bsPerfiles;
            }
            else if (this.tabControl1.SelectedTab.Text == "Grupos") 
            {
                bsGrupos = new BindingSource();
                dgvGrupos.AutoGenerateColumns = false;
                this.bsGrupos.DataSource = Controladora.Seguridad.ControladoraGestionarGrupos.ObtenerInstancia().RecuperarGrupos();
                this.dgvGrupos.DataSource = bsGrupos;
            }
        }

        private void ActualizarGrillaUsuarios() 
        {
            bsUsuarios = new BindingSource();
            dgvUsuarios.AutoGenerateColumns = false;
            this.bsUsuarios.DataSource = Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().RecuperarUsuarios();
            this.dgvUsuarios.DataSource = bsUsuarios;

        }

        private void ActualizarGrillaGrupos() 
        {
            this.bsGrupos.ResetBindings(false);
        }

        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)bsUsuarios.Current;
            DialogResult Dr = MessageBox.Show("¿Desea eliminar el usuario seleccionado en la grilla?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Dr == System.Windows.Forms.DialogResult.Yes)
            {
                Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().EliminarUsuario(oUsuario);
                this.ActualizarGrillaUsuarios();
            }
        }

        private void btnHabilitarUsuario_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)bsUsuarios.Current;
            if (oUsuario.EstadoSituacion == false) 
            {
                DialogResult Dr = MessageBox.Show("El usuario se encuentra inhabilitado ¿Desea habilitarlo?","Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Dr == System.Windows.Forms.DialogResult.Yes)
                {
                    oUsuario.EstadoSituacion = true;
                    Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().HabilitarUsuario(oUsuario);
                    this.ActualizarGrillaUsuarios();
                }
            }
        }

        private void btnInhabilitarUsuario_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)bsUsuarios.Current;
            if (oUsuario.EstadoSituacion == true) 
            {
                DialogResult Dr = MessageBox.Show("El usuario se encuentra habilitado ¿Desea inhabilitarlo?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Dr == System.Windows.Forms.DialogResult.Yes)
                {
                    oUsuario.EstadoSituacion = false;
                    Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().InhabilitarUsuario(oUsuario);
                    this.ActualizarGrillaUsuarios();
                }
            }
        }

        private void dgvUsuarios_SelectionChanged(object sender, EventArgs e)
        {
            Entidades.Seguridad.Usuario oUsuario = (Entidades.Seguridad.Usuario)bsUsuarios.Current;
            if (oUsuario.EstadoSituacion == true) 
            {
                btnHabilitarUsuario.Enabled = false;
                btnInhabilitarUsuario.Enabled = true;
            }
            else if (oUsuario.EstadoSituacion == false) 
            {
                btnHabilitarUsuario.Enabled = true;
                btnInhabilitarUsuario.Enabled = false;
            }
        }

        private void txtFiltrarUsuarios_Leave(object sender, EventArgs e)
        {
            if (txtFiltrarUsuarios.Text == "") 
            {
                txtFiltrarUsuarios.Font = new Font(txtFiltrarUsuarios.Font, FontStyle.Italic);
                txtFiltrarUsuarios.ForeColor = System.Drawing.Color.DarkGray;
                txtFiltrarUsuarios.Text = "Ingrese aquí el Nombre y Apellido o el Nombre de Usuario...";
            }
        }

        private void txtFiltrarUsuarios_KeyUp(object sender, KeyEventArgs e)
        {
            Filtrar();
        }

        private void txtFiltrarUsuarios_Enter(object sender, EventArgs e)
        {
            if (txtFiltrarUsuarios.Text == "Ingrese aquí el Nombre y Apellido o el Nombre de Usuario...")
            {
                txtFiltrarUsuarios.Font = new Font(txtFiltrarUsuarios.Font, FontStyle.Regular);
                txtFiltrarUsuarios.ForeColor = System.Drawing.Color.Black;
                txtFiltrarUsuarios.Text = "";
            }
        }

        private void txtFiltrarUsuarios_Click(object sender, EventArgs e)
        {
            if (txtFiltrarUsuarios.Text == "Ingrese aquí el Nombre y Apellido o el Nombre de Usuario...")
            {
                txtFiltrarUsuarios.Text = "";
            }
            else if (txtFiltrarUsuarios.Text != "Ingrese aquí el Nombre y Apellido o el Nombre de Usuario...") { }
        }

        public void Filtrar() 
        {
            bsUsuarios.DataSource = Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().Filtrar(this.txtFiltrarUsuarios.Text);
            dgvUsuarios.DataSource = bsUsuarios;
        }

        private void btnAgregarUsuario_Click(object sender, EventArgs e)
        {
            FormUsuario oFormUsuario = new FormUsuario();
            DialogResult Dr = oFormUsuario.ShowDialog();
            if (Dr == System.Windows.Forms.DialogResult.OK) 
            {
                ActualizarGrillaUsuarios();
            }
        }

        private void btnAgregarGrupo_Click(object sender, EventArgs e)
        {
            FormGrupo oFormGrupo = new FormGrupo();
            DialogResult Dr = oFormGrupo.ShowDialog();
            if (Dr == System.Windows.Forms.DialogResult.OK)
            {
                ActualizarGrillaGrupos();
            }
        }

        private void btnModificarGrupo_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Grupo oGrupo = (Entidades.Seguridad.Grupo)bsGrupos.Current;
            FormGrupo oFormGrupo = new FormGrupo(oGrupo, "Modificacion");
            DialogResult Dr = oFormGrupo.ShowDialog();
            if (Dr == System.Windows.Forms.DialogResult.OK)
            {
                this.ActualizarGrillaGrupos();
            }
        }

        private void btnEliminarGrupo_Click(object sender, EventArgs e)
        {
            DialogResult Dr = MessageBox.Show("¿Desea eliminar el grupo seleccionado en la grilla?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Dr == System.Windows.Forms.DialogResult.Yes)
            {
                Controladora.Seguridad.ControladoraGestionarGrupos.ObtenerInstancia().EliminarGrupo((Entidades.Seguridad.Grupo)bsGrupos.Current);
                this.ActualizarGrillaGrupos();
            }
        }

        private void btnResetearClave_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.RowCount >= 1)
            {
                bool rta = Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().ResetearContraseña((Entidades.Seguridad.Usuario)this.bsUsuarios.Current);
                if (rta)MessageBox.Show("Contrasena reseteada correctamente");
                else MessageBox.Show("Error al resetear la contrasena.");
            }
        }

        private void btnModificarUsuario_Click(object sender, EventArgs e)
        {
            FormUsuario oUsuario = new FormUsuario((Entidades.Seguridad.Usuario)this.bsUsuarios.Current, "Modificacion");
            oUsuario.Show();
        }



    }
}
