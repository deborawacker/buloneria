﻿namespace Vista.Seguridad
{
    partial class FormSeguridad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageUsuarios = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnResetearClave = new System.Windows.Forms.Button();
            this.btnAgregarUsuario = new System.Windows.Forms.Button();
            this.btnInhabilitarUsuario = new System.Windows.Forms.Button();
            this.btnEliminarUsuario = new System.Windows.Forms.Button();
            this.btnHabilitarUsuario = new System.Windows.Forms.Button();
            this.btnModificarUsuario = new System.Windows.Forms.Button();
            this.txtFiltrarUsuarios = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.colNombreYApellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colContraseña = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNombreUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEstadoSituacion = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPageGrupos = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEliminarGrupo = new System.Windows.Forms.Button();
            this.btnAgregarGrupo = new System.Windows.Forms.Button();
            this.btnModificarGrupo = new System.Windows.Forms.Button();
            this.dgvGrupos = new System.Windows.Forms.DataGridView();
            this.colNombreGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPagePerfiles = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAgregarPerfil = new System.Windows.Forms.Button();
            this.btnEliminarPerfil = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cBoxPermisos = new System.Windows.Forms.ComboBox();
            this.cBoxFormularios = new System.Windows.Forms.ComboBox();
            this.cBoxGrupos = new System.Windows.Forms.ComboBox();
            this.dgvPerfiles = new System.Windows.Forms.DataGridView();
            this.colGrupo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFormulario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPermiso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPageUsuarios.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.tabPageGrupos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).BeginInit();
            this.tabPagePerfiles.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPerfiles)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageUsuarios);
            this.tabControl1.Controls.Add(this.tabPageGrupos);
            this.tabControl1.Controls.Add(this.tabPagePerfiles);
            this.tabControl1.Location = new System.Drawing.Point(12, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(727, 374);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPageUsuarios
            // 
            this.tabPageUsuarios.Controls.Add(this.groupBox2);
            this.tabPageUsuarios.Controls.Add(this.txtFiltrarUsuarios);
            this.tabPageUsuarios.Controls.Add(this.dgvUsuarios);
            this.tabPageUsuarios.Location = new System.Drawing.Point(4, 22);
            this.tabPageUsuarios.Name = "tabPageUsuarios";
            this.tabPageUsuarios.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUsuarios.Size = new System.Drawing.Size(719, 348);
            this.tabPageUsuarios.TabIndex = 0;
            this.tabPageUsuarios.Tag = "Usuarios";
            this.tabPageUsuarios.Text = "Usuarios";
            this.tabPageUsuarios.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnResetearClave);
            this.groupBox2.Controls.Add(this.btnAgregarUsuario);
            this.groupBox2.Controls.Add(this.btnInhabilitarUsuario);
            this.groupBox2.Controls.Add(this.btnEliminarUsuario);
            this.groupBox2.Controls.Add(this.btnHabilitarUsuario);
            this.groupBox2.Controls.Add(this.btnModificarUsuario);
            this.groupBox2.Location = new System.Drawing.Point(579, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(134, 345);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // btnResetearClave
            // 
            this.btnResetearClave.Location = new System.Drawing.Point(27, 297);
            this.btnResetearClave.Name = "btnResetearClave";
            this.btnResetearClave.Size = new System.Drawing.Size(81, 42);
            this.btnResetearClave.TabIndex = 7;
            this.btnResetearClave.Text = "Resetear Contraseña";
            this.btnResetearClave.UseVisualStyleBackColor = true;
            this.btnResetearClave.Click += new System.EventHandler(this.btnResetearClave_Click);
            // 
            // btnAgregarUsuario
            // 
            this.btnAgregarUsuario.Location = new System.Drawing.Point(27, 6);
            this.btnAgregarUsuario.Name = "btnAgregarUsuario";
            this.btnAgregarUsuario.Size = new System.Drawing.Size(81, 52);
            this.btnAgregarUsuario.TabIndex = 1;
            this.btnAgregarUsuario.Text = "Agregar";
            this.btnAgregarUsuario.UseVisualStyleBackColor = true;
            this.btnAgregarUsuario.Click += new System.EventHandler(this.btnAgregarUsuario_Click);
            // 
            // btnInhabilitarUsuario
            // 
            this.btnInhabilitarUsuario.Location = new System.Drawing.Point(27, 238);
            this.btnInhabilitarUsuario.Name = "btnInhabilitarUsuario";
            this.btnInhabilitarUsuario.Size = new System.Drawing.Size(81, 52);
            this.btnInhabilitarUsuario.TabIndex = 6;
            this.btnInhabilitarUsuario.Text = "Inhabilitar";
            this.btnInhabilitarUsuario.UseVisualStyleBackColor = true;
            this.btnInhabilitarUsuario.Click += new System.EventHandler(this.btnInhabilitarUsuario_Click);
            // 
            // btnEliminarUsuario
            // 
            this.btnEliminarUsuario.Location = new System.Drawing.Point(27, 122);
            this.btnEliminarUsuario.Name = "btnEliminarUsuario";
            this.btnEliminarUsuario.Size = new System.Drawing.Size(81, 52);
            this.btnEliminarUsuario.TabIndex = 3;
            this.btnEliminarUsuario.Text = "Eliminar";
            this.btnEliminarUsuario.UseVisualStyleBackColor = true;
            this.btnEliminarUsuario.Click += new System.EventHandler(this.btnEliminarUsuario_Click);
            // 
            // btnHabilitarUsuario
            // 
            this.btnHabilitarUsuario.Location = new System.Drawing.Point(27, 180);
            this.btnHabilitarUsuario.Name = "btnHabilitarUsuario";
            this.btnHabilitarUsuario.Size = new System.Drawing.Size(81, 52);
            this.btnHabilitarUsuario.TabIndex = 5;
            this.btnHabilitarUsuario.Text = "Habilitar";
            this.btnHabilitarUsuario.UseVisualStyleBackColor = true;
            this.btnHabilitarUsuario.Click += new System.EventHandler(this.btnHabilitarUsuario_Click);
            // 
            // btnModificarUsuario
            // 
            this.btnModificarUsuario.Location = new System.Drawing.Point(27, 64);
            this.btnModificarUsuario.Name = "btnModificarUsuario";
            this.btnModificarUsuario.Size = new System.Drawing.Size(81, 52);
            this.btnModificarUsuario.TabIndex = 2;
            this.btnModificarUsuario.Text = "Modificar";
            this.btnModificarUsuario.UseVisualStyleBackColor = true;
            this.btnModificarUsuario.Click += new System.EventHandler(this.btnModificarUsuario_Click);
            // 
            // txtFiltrarUsuarios
            // 
            this.txtFiltrarUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic);
            this.txtFiltrarUsuarios.ForeColor = System.Drawing.Color.DarkGray;
            this.txtFiltrarUsuarios.Location = new System.Drawing.Point(26, 23);
            this.txtFiltrarUsuarios.Name = "txtFiltrarUsuarios";
            this.txtFiltrarUsuarios.Size = new System.Drawing.Size(543, 20);
            this.txtFiltrarUsuarios.TabIndex = 4;
            this.txtFiltrarUsuarios.Text = "Ingrese aquí el Nombre y Apellido o el Nombre de Usuario...";
            this.txtFiltrarUsuarios.Click += new System.EventHandler(this.txtFiltrarUsuarios_Click);
            this.txtFiltrarUsuarios.Enter += new System.EventHandler(this.txtFiltrarUsuarios_Enter);
            this.txtFiltrarUsuarios.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFiltrarUsuarios_KeyUp);
            this.txtFiltrarUsuarios.Leave += new System.EventHandler(this.txtFiltrarUsuarios_Leave);
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.AllowUserToResizeColumns = false;
            this.dgvUsuarios.AllowUserToResizeRows = false;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNombreYApellido,
            this.colContraseña,
            this.colNombreUsuario,
            this.colEmail,
            this.colEstadoSituacion});
            this.dgvUsuarios.Location = new System.Drawing.Point(26, 54);
            this.dgvUsuarios.MultiSelect = false;
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.RowHeadersVisible = false;
            this.dgvUsuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsuarios.Size = new System.Drawing.Size(543, 284);
            this.dgvUsuarios.TabIndex = 0;
            this.dgvUsuarios.SelectionChanged += new System.EventHandler(this.dgvUsuarios_SelectionChanged);
            // 
            // colNombreYApellido
            // 
            this.colNombreYApellido.DataPropertyName = "NombreYApellido";
            this.colNombreYApellido.HeaderText = "Nombre y Apellido";
            this.colNombreYApellido.MaxInputLength = 50;
            this.colNombreYApellido.Name = "colNombreYApellido";
            this.colNombreYApellido.ReadOnly = true;
            this.colNombreYApellido.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colNombreYApellido.Width = 175;
            // 
            // colContraseña
            // 
            this.colContraseña.HeaderText = "Contraseña";
            this.colContraseña.Name = "colContraseña";
            this.colContraseña.ReadOnly = true;
            this.colContraseña.Visible = false;
            // 
            // colNombreUsuario
            // 
            this.colNombreUsuario.DataPropertyName = "NombreUsuario";
            this.colNombreUsuario.HeaderText = "Usuario";
            this.colNombreUsuario.MaxInputLength = 20;
            this.colNombreUsuario.Name = "colNombreUsuario";
            this.colNombreUsuario.ReadOnly = true;
            this.colNombreUsuario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // colEmail
            // 
            this.colEmail.DataPropertyName = "Email";
            this.colEmail.HeaderText = "Email";
            this.colEmail.MaxInputLength = 50;
            this.colEmail.Name = "colEmail";
            this.colEmail.ReadOnly = true;
            this.colEmail.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colEmail.Width = 200;
            // 
            // colEstadoSituacion
            // 
            this.colEstadoSituacion.DataPropertyName = "EstadoSituacion";
            this.colEstadoSituacion.HeaderText = "Habilitado";
            this.colEstadoSituacion.Name = "colEstadoSituacion";
            this.colEstadoSituacion.ReadOnly = true;
            this.colEstadoSituacion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colEstadoSituacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colEstadoSituacion.Width = 65;
            // 
            // tabPageGrupos
            // 
            this.tabPageGrupos.Controls.Add(this.groupBox1);
            this.tabPageGrupos.Controls.Add(this.dgvGrupos);
            this.tabPageGrupos.Location = new System.Drawing.Point(4, 22);
            this.tabPageGrupos.Name = "tabPageGrupos";
            this.tabPageGrupos.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGrupos.Size = new System.Drawing.Size(719, 348);
            this.tabPageGrupos.TabIndex = 1;
            this.tabPageGrupos.Tag = "Grupos";
            this.tabPageGrupos.Text = "Grupos";
            this.tabPageGrupos.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnEliminarGrupo);
            this.groupBox1.Controls.Add(this.btnAgregarGrupo);
            this.groupBox1.Controls.Add(this.btnModificarGrupo);
            this.groupBox1.Location = new System.Drawing.Point(562, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 309);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnEliminarGrupo
            // 
            this.btnEliminarGrupo.Location = new System.Drawing.Point(29, 241);
            this.btnEliminarGrupo.Name = "btnEliminarGrupo";
            this.btnEliminarGrupo.Size = new System.Drawing.Size(81, 52);
            this.btnEliminarGrupo.TabIndex = 7;
            this.btnEliminarGrupo.Text = "Eliminar";
            this.btnEliminarGrupo.UseVisualStyleBackColor = true;
            this.btnEliminarGrupo.Click += new System.EventHandler(this.btnEliminarGrupo_Click);
            // 
            // btnAgregarGrupo
            // 
            this.btnAgregarGrupo.Location = new System.Drawing.Point(29, 21);
            this.btnAgregarGrupo.Name = "btnAgregarGrupo";
            this.btnAgregarGrupo.Size = new System.Drawing.Size(81, 52);
            this.btnAgregarGrupo.TabIndex = 5;
            this.btnAgregarGrupo.Text = "Agregar";
            this.btnAgregarGrupo.UseVisualStyleBackColor = true;
            this.btnAgregarGrupo.Click += new System.EventHandler(this.btnAgregarGrupo_Click);
            // 
            // btnModificarGrupo
            // 
            this.btnModificarGrupo.Location = new System.Drawing.Point(29, 130);
            this.btnModificarGrupo.Name = "btnModificarGrupo";
            this.btnModificarGrupo.Size = new System.Drawing.Size(81, 52);
            this.btnModificarGrupo.TabIndex = 6;
            this.btnModificarGrupo.Text = "Modificar";
            this.btnModificarGrupo.UseVisualStyleBackColor = true;
            this.btnModificarGrupo.Click += new System.EventHandler(this.btnModificarGrupo_Click);
            // 
            // dgvGrupos
            // 
            this.dgvGrupos.AllowUserToAddRows = false;
            this.dgvGrupos.AllowUserToDeleteRows = false;
            this.dgvGrupos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrupos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNombreGrupo,
            this.colDescripcion});
            this.dgvGrupos.Location = new System.Drawing.Point(21, 27);
            this.dgvGrupos.Name = "dgvGrupos";
            this.dgvGrupos.ReadOnly = true;
            this.dgvGrupos.RowHeadersVisible = false;
            this.dgvGrupos.Size = new System.Drawing.Size(528, 303);
            this.dgvGrupos.TabIndex = 0;
            // 
            // colNombreGrupo
            // 
            this.colNombreGrupo.DataPropertyName = "Nombre";
            this.colNombreGrupo.HeaderText = "Nombre de Grupo";
            this.colNombreGrupo.Name = "colNombreGrupo";
            this.colNombreGrupo.ReadOnly = true;
            this.colNombreGrupo.Width = 115;
            // 
            // colDescripcion
            // 
            this.colDescripcion.DataPropertyName = "Descripcion";
            this.colDescripcion.HeaderText = "Descripción";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            this.colDescripcion.Width = 410;
            // 
            // tabPagePerfiles
            // 
            this.tabPagePerfiles.Controls.Add(this.groupBox3);
            this.tabPagePerfiles.Controls.Add(this.label3);
            this.tabPagePerfiles.Controls.Add(this.label2);
            this.tabPagePerfiles.Controls.Add(this.label1);
            this.tabPagePerfiles.Controls.Add(this.cBoxPermisos);
            this.tabPagePerfiles.Controls.Add(this.cBoxFormularios);
            this.tabPagePerfiles.Controls.Add(this.cBoxGrupos);
            this.tabPagePerfiles.Controls.Add(this.dgvPerfiles);
            this.tabPagePerfiles.Location = new System.Drawing.Point(4, 22);
            this.tabPagePerfiles.Name = "tabPagePerfiles";
            this.tabPagePerfiles.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePerfiles.Size = new System.Drawing.Size(719, 348);
            this.tabPagePerfiles.TabIndex = 2;
            this.tabPagePerfiles.Tag = "Perfiles";
            this.tabPagePerfiles.Text = "Perfiles";
            this.tabPagePerfiles.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnAgregarPerfil);
            this.groupBox3.Controls.Add(this.btnEliminarPerfil);
            this.groupBox3.Location = new System.Drawing.Point(519, 53);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(166, 275);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            // 
            // btnAgregarPerfil
            // 
            this.btnAgregarPerfil.Location = new System.Drawing.Point(43, 32);
            this.btnAgregarPerfil.Name = "btnAgregarPerfil";
            this.btnAgregarPerfil.Size = new System.Drawing.Size(81, 52);
            this.btnAgregarPerfil.TabIndex = 1;
            this.btnAgregarPerfil.Text = "Agregar";
            this.btnAgregarPerfil.UseVisualStyleBackColor = true;
            this.btnAgregarPerfil.Click += new System.EventHandler(this.btnAgregarPerfil_Click);
            // 
            // btnEliminarPerfil
            // 
            this.btnEliminarPerfil.Location = new System.Drawing.Point(43, 197);
            this.btnEliminarPerfil.Name = "btnEliminarPerfil";
            this.btnEliminarPerfil.Size = new System.Drawing.Size(81, 52);
            this.btnEliminarPerfil.TabIndex = 2;
            this.btnEliminarPerfil.Text = "Eliminar";
            this.btnEliminarPerfil.UseVisualStyleBackColor = true;
            this.btnEliminarPerfil.Click += new System.EventHandler(this.btnEliminarPerfil_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(494, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Permiso:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(266, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Formulario:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Grupo:";
            // 
            // cBoxPermisos
            // 
            this.cBoxPermisos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxPermisos.FormattingEnabled = true;
            this.cBoxPermisos.Location = new System.Drawing.Point(547, 16);
            this.cBoxPermisos.Name = "cBoxPermisos";
            this.cBoxPermisos.Size = new System.Drawing.Size(138, 21);
            this.cBoxPermisos.TabIndex = 5;
            // 
            // cBoxFormularios
            // 
            this.cBoxFormularios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxFormularios.FormattingEnabled = true;
            this.cBoxFormularios.Location = new System.Drawing.Point(330, 16);
            this.cBoxFormularios.Name = "cBoxFormularios";
            this.cBoxFormularios.Size = new System.Drawing.Size(138, 21);
            this.cBoxFormularios.TabIndex = 4;
            // 
            // cBoxGrupos
            // 
            this.cBoxGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxGrupos.FormattingEnabled = true;
            this.cBoxGrupos.Location = new System.Drawing.Point(98, 16);
            this.cBoxGrupos.Name = "cBoxGrupos";
            this.cBoxGrupos.Size = new System.Drawing.Size(138, 21);
            this.cBoxGrupos.TabIndex = 3;
            // 
            // dgvPerfiles
            // 
            this.dgvPerfiles.AllowUserToAddRows = false;
            this.dgvPerfiles.AllowUserToDeleteRows = false;
            this.dgvPerfiles.AllowUserToResizeColumns = false;
            this.dgvPerfiles.AllowUserToResizeRows = false;
            this.dgvPerfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPerfiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGrupo,
            this.colFormulario,
            this.colPermiso});
            this.dgvPerfiles.Location = new System.Drawing.Point(54, 53);
            this.dgvPerfiles.MultiSelect = false;
            this.dgvPerfiles.Name = "dgvPerfiles";
            this.dgvPerfiles.ReadOnly = true;
            this.dgvPerfiles.RowHeadersVisible = false;
            this.dgvPerfiles.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvPerfiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPerfiles.Size = new System.Drawing.Size(454, 275);
            this.dgvPerfiles.TabIndex = 0;
            // 
            // colGrupo
            // 
            this.colGrupo.DataPropertyName = "Grupo";
            this.colGrupo.HeaderText = "Grupo";
            this.colGrupo.MaxInputLength = 30;
            this.colGrupo.Name = "colGrupo";
            this.colGrupo.ReadOnly = true;
            this.colGrupo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colGrupo.Width = 150;
            // 
            // colFormulario
            // 
            this.colFormulario.DataPropertyName = "Formulario";
            this.colFormulario.HeaderText = "Formulario";
            this.colFormulario.MaxInputLength = 150;
            this.colFormulario.Name = "colFormulario";
            this.colFormulario.ReadOnly = true;
            this.colFormulario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colFormulario.Width = 150;
            // 
            // colPermiso
            // 
            this.colPermiso.DataPropertyName = "Permiso";
            this.colPermiso.HeaderText = "Permiso";
            this.colPermiso.MaxInputLength = 20;
            this.colPermiso.Name = "colPermiso";
            this.colPermiso.ReadOnly = true;
            this.colPermiso.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colPermiso.Width = 150;
            // 
            // FormSeguridad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 399);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormSeguridad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seguridad";
            this.Load += new System.EventHandler(this.FormSeguridad_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageUsuarios.ResumeLayout(false);
            this.tabPageUsuarios.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.tabPageGrupos.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupos)).EndInit();
            this.tabPagePerfiles.ResumeLayout(false);
            this.tabPagePerfiles.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPerfiles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageUsuarios;
        private System.Windows.Forms.TabPage tabPageGrupos;
        private System.Windows.Forms.TabPage tabPagePerfiles;
        private System.Windows.Forms.Button btnEliminarPerfil;
        private System.Windows.Forms.Button btnAgregarPerfil;
        private System.Windows.Forms.DataGridView dgvPerfiles;
        private System.Windows.Forms.ComboBox cBoxPermisos;
        private System.Windows.Forms.ComboBox cBoxFormularios;
        private System.Windows.Forms.ComboBox cBoxGrupos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvGrupos;
        private System.Windows.Forms.Button btnInhabilitarUsuario;
        private System.Windows.Forms.Button btnHabilitarUsuario;
        private System.Windows.Forms.Button btnAgregarUsuario;
        private System.Windows.Forms.Button btnModificarUsuario;
        private System.Windows.Forms.Button btnEliminarUsuario;
        private System.Windows.Forms.TextBox txtFiltrarUsuarios;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNombreYApellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn colContraseña;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNombreUsuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmail;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colEstadoSituacion;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnEliminarGrupo;
        private System.Windows.Forms.Button btnAgregarGrupo;
        private System.Windows.Forms.Button btnModificarGrupo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFormulario;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPermiso;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNombreGrupo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.Button btnResetearClave;
    }
}
