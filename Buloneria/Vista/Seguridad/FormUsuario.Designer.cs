﻿namespace Vista.Seguridad
{
    partial class FormUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gBoxUsuarios = new System.Windows.Forms.GroupBox();
            this.chkEstadoSituacion = new System.Windows.Forms.CheckBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtNombreUsuario = new System.Windows.Forms.TextBox();
            this.txtNombreyApellido = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gBoxGrupos = new System.Windows.Forms.GroupBox();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.btnAsignar = new System.Windows.Forms.Button();
            this.listBoxGrupos = new System.Windows.Forms.ListBox();
            this.cmbGrupos = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gBoxUsuarios.SuspendLayout();
            this.gBoxGrupos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre y Apellido";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre de Usuario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Email";
            // 
            // gBoxUsuarios
            // 
            this.gBoxUsuarios.Controls.Add(this.chkEstadoSituacion);
            this.gBoxUsuarios.Controls.Add(this.txtEmail);
            this.gBoxUsuarios.Controls.Add(this.txtNombreUsuario);
            this.gBoxUsuarios.Controls.Add(this.txtNombreyApellido);
            this.gBoxUsuarios.Controls.Add(this.label6);
            this.gBoxUsuarios.Controls.Add(this.label2);
            this.gBoxUsuarios.Controls.Add(this.label5);
            this.gBoxUsuarios.Controls.Add(this.label1);
            this.gBoxUsuarios.Location = new System.Drawing.Point(3, 3);
            this.gBoxUsuarios.Name = "gBoxUsuarios";
            this.gBoxUsuarios.Size = new System.Drawing.Size(306, 261);
            this.gBoxUsuarios.TabIndex = 5;
            this.gBoxUsuarios.TabStop = false;
            this.gBoxUsuarios.Text = "Datos Usuario";
            // 
            // chkEstadoSituacion
            // 
            this.chkEstadoSituacion.AutoSize = true;
            this.chkEstadoSituacion.Checked = true;
            this.chkEstadoSituacion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEstadoSituacion.Enabled = false;
            this.chkEstadoSituacion.Location = new System.Drawing.Point(133, 191);
            this.chkEstadoSituacion.Name = "chkEstadoSituacion";
            this.chkEstadoSituacion.Size = new System.Drawing.Size(15, 14);
            this.chkEstadoSituacion.TabIndex = 11;
            this.chkEstadoSituacion.UseVisualStyleBackColor = true;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(133, 145);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(152, 20);
            this.txtEmail.TabIndex = 10;
            // 
            // txtNombreUsuario
            // 
            this.txtNombreUsuario.Location = new System.Drawing.Point(133, 100);
            this.txtNombreUsuario.Name = "txtNombreUsuario";
            this.txtNombreUsuario.Size = new System.Drawing.Size(152, 20);
            this.txtNombreUsuario.TabIndex = 7;
            // 
            // txtNombreyApellido
            // 
            this.txtNombreyApellido.Location = new System.Drawing.Point(133, 56);
            this.txtNombreyApellido.Name = "txtNombreyApellido";
            this.txtNombreyApellido.Size = new System.Drawing.Size(152, 20);
            this.txtNombreyApellido.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Habilitado";
            // 
            // gBoxGrupos
            // 
            this.gBoxGrupos.Controls.Add(this.btnQuitar);
            this.gBoxGrupos.Controls.Add(this.btnAsignar);
            this.gBoxGrupos.Controls.Add(this.listBoxGrupos);
            this.gBoxGrupos.Controls.Add(this.cmbGrupos);
            this.gBoxGrupos.Controls.Add(this.label7);
            this.gBoxGrupos.Location = new System.Drawing.Point(315, 3);
            this.gBoxGrupos.Name = "gBoxGrupos";
            this.gBoxGrupos.Size = new System.Drawing.Size(305, 261);
            this.gBoxGrupos.TabIndex = 6;
            this.gBoxGrupos.TabStop = false;
            this.gBoxGrupos.Text = "Grupos";
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(215, 193);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(81, 57);
            this.btnQuitar.TabIndex = 4;
            this.btnQuitar.Text = "Quitar";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // btnAsignar
            // 
            this.btnAsignar.Location = new System.Drawing.Point(35, 193);
            this.btnAsignar.Name = "btnAsignar";
            this.btnAsignar.Size = new System.Drawing.Size(81, 57);
            this.btnAsignar.TabIndex = 3;
            this.btnAsignar.Text = "Asignar";
            this.btnAsignar.UseVisualStyleBackColor = true;
            this.btnAsignar.Click += new System.EventHandler(this.btnAsignar_Click);
            // 
            // listBoxGrupos
            // 
            this.listBoxGrupos.FormattingEnabled = true;
            this.listBoxGrupos.Location = new System.Drawing.Point(35, 53);
            this.listBoxGrupos.Name = "listBoxGrupos";
            this.listBoxGrupos.Size = new System.Drawing.Size(261, 134);
            this.listBoxGrupos.TabIndex = 2;
            // 
            // cmbGrupos
            // 
            this.cmbGrupos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrupos.FormattingEnabled = true;
            this.cmbGrupos.Location = new System.Drawing.Point(100, 26);
            this.cmbGrupos.Name = "cmbGrupos";
            this.cmbGrupos.Size = new System.Drawing.Size(196, 21);
            this.cmbGrupos.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Grupos";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(431, 11);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(81, 57);
            this.btnGuardar.TabIndex = 7;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(527, 11);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(81, 57);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Location = new System.Drawing.Point(5, 263);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(614, 74);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // FormUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 339);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gBoxGrupos);
            this.Controls.Add(this.gBoxUsuarios);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Usuarios";
            this.Load += new System.EventHandler(this.FormUsuario_Load);
            this.gBoxUsuarios.ResumeLayout(false);
            this.gBoxUsuarios.PerformLayout();
            this.gBoxGrupos.ResumeLayout(false);
            this.gBoxGrupos.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox gBoxUsuarios;
        private System.Windows.Forms.CheckBox chkEstadoSituacion;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtNombreUsuario;
        private System.Windows.Forms.TextBox txtNombreyApellido;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gBoxGrupos;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnAsignar;
        private System.Windows.Forms.ListBox listBoxGrupos;
        private System.Windows.Forms.ComboBox cmbGrupos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}