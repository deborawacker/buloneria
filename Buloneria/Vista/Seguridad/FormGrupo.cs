﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista.Seguridad
{
    public partial class FormGrupo : Form
    {
        public FormGrupo()
        {
            InitializeComponent();
        }

        string accion;
        Entidades.Seguridad.Grupo oGrupo;
        public FormGrupo(Entidades.Seguridad.Grupo ogrupo, string modo)
        {
            InitializeComponent();
            oGrupo = ogrupo;
            accion = modo;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (accion == "Modificacion") 
            {
                if (ValidarDatos())
                {
                    oGrupo = new Entidades.Seguridad.Grupo();
                    oGrupo.Nombre = txtNombreGrupo.Text;
                    oGrupo.Descripcion = richTxtDescripcionGrupo.Text;
                    if(Controladora.Seguridad.ControladoraGestionarGrupos.ObtenerInstancia().ModificarGrupo(oGrupo))
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                }
            }
            else if (accion != "Modificacion")
            {
                if (ValidarDatos())
                {
                    oGrupo = new Entidades.Seguridad.Grupo();
                    oGrupo.Nombre = txtNombreGrupo.Text;
                    oGrupo.Descripcion = richTxtDescripcionGrupo.Text;
                    if (Controladora.Seguridad.ControladoraGestionarGrupos.ObtenerInstancia().AgregarGrupo(oGrupo))
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                }
            }
        }
        private bool ValidarDatos()
        {
            if (txtNombreGrupo.Text == "" || richTxtDescripcionGrupo.Text == "")
            {
                return false;
            }
            return true;
        }

        private void FormGrupo_Load(object sender, EventArgs e)
        {
            if (accion == "Modificacion")
            {
                CargarCampos();
            }
        }

        public void CargarCampos() 
        {
            txtNombreGrupo.Text = oGrupo.Nombre;
            richTxtDescripcionGrupo.Text = oGrupo.Descripcion;
            txtNombreGrupo.Enabled = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    
    }
}
