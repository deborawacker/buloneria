﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista.Seguridad
{
    public partial class FormCambiarContraseña : Form
    {
        Entidades.Seguridad.Usuario oUsuarioLogueado;

        public FormCambiarContraseña(Entidades.Seguridad.Usuario oUsuario)
        {
            InitializeComponent();
            oUsuarioLogueado = oUsuario;
        }

        private void FormCambiarContraseña_Load(object sender, EventArgs e)
        {
            txtNombreUsuario.Text = oUsuarioLogueado.NombreUsuario;
            txtNombreUsuario.Enabled = false;
            txtContraseñaActual.Focus();
        }

        public bool ValidarDatos()
        {
            if (string.IsNullOrEmpty(txtContraseñaActual.Text))
            {
                MessageBox.Show("Complete los campos");
                return false;
            }
            if (string.IsNullOrEmpty(txtContraseñaNueva.Text))
            {
                MessageBox.Show("Complete los campos");
                return false;
            }
            if (string.IsNullOrEmpty(txtConfirmarContraseña.Text))
            {
                MessageBox.Show("Complete los campos");
                return false;
            }
            if (txtContraseñaNueva.Text != txtConfirmarContraseña.Text) 
            {
                MessageBox.Show("Las contraseñas no coinciden.");
                return false;
            }
            return true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ValidarDatos())
            {
                string PassNueva, PassVieja, Confirmacion;
                PassVieja = txtContraseñaActual.Text;
                PassNueva = txtContraseñaNueva.Text;
                if (Controladora.Seguridad.ControladoraCambiarContraseña.ObtenerInstancia().CambiarContraseña(oUsuarioLogueado, PassVieja, PassNueva))
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else MessageBox.Show("No se pudo cambiar la contraseña.","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult Dr = MessageBox.Show("¿Desea cancelar la operación? El cambio de contraseña no se realizará.", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (Dr == System.Windows.Forms.DialogResult.Yes) 
            {
                this.Close();
            }
        }
    }
}
