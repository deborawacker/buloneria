﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista.Seguridad
{
    public partial class FormUsuario : Form
    {
        public FormUsuario()
        {
            InitializeComponent();
            oUsuario = new Entidades.Seguridad.Usuario();
        }

        BindingSource bsGruposUsuario;
        string accion;
        Entidades.Seguridad.Usuario oUsuario;

        public FormUsuario(Entidades.Seguridad.Usuario oUsu, string modo)
        {
            InitializeComponent();
            oUsuario = oUsu;
            accion = modo;
        }

        private void FormUsuario_Load(object sender, EventArgs e)
        {
            CargarGrupos();
            if (accion == "Modificacion") { CargarDatos(); }
            bsGruposUsuario = new BindingSource();
            bsGruposUsuario.DataSource = oUsuario.Grupos;
            listBoxGrupos.DataSource = bsGruposUsuario;
        }

        public void CargarGrupos()
        {
            cmbGrupos.DataSource = Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().RecuperarGrupos();
            cmbGrupos.DisplayMember = "Nombre";
        }

        public bool ValidarDatos() 
        {
            if (string.IsNullOrEmpty(txtNombreyApellido.Text)) 
            {
                MessageBox.Show("Complete el campo Nombre y Apellido.");
                return false;
            }
            if (string.IsNullOrEmpty(txtNombreUsuario.Text))
            {
                MessageBox.Show("Complete el campo Nombre de Usuario.");
                return false;
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                MessageBox.Show("Complete el campo Email.");
                return false;
            }
            if(oUsuario.Grupos.Count()==0)
            {
                MessageBox.Show("Debe asignar al menos un grupo al Usuario");
                return false;
            }
            return true;
        }

        public void CargarDatos() 
        {
            txtNombreyApellido.Text = oUsuario.NombreYApellido.ToString();
            txtNombreUsuario.Text = oUsuario.NombreUsuario.ToString();
            txtNombreUsuario.Enabled = false;
            txtEmail.Text = oUsuario.Email.ToString();
        }

        private void btnAsignar_Click(object sender, EventArgs e)
        {
            if (!listBoxGrupos.Items.Contains(cmbGrupos.SelectedItem))
            {
                //cmbGrupos.selected
                oUsuario.AgregarGrupo((Entidades.Seguridad.Grupo)cmbGrupos.SelectedItem);
                listBoxGrupos.DataSource = null;
                
                listBoxGrupos.DataSource = oUsuario.Grupos;
            }
            else MessageBox.Show("El grupo que intenta asignar ya fue asignado anteriormente.","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            oUsuario.EliminarGrupo((Entidades.Seguridad.Grupo)listBoxGrupos.SelectedItem);
            listBoxGrupos.DataSource = null;
            listBoxGrupos.DataSource = oUsuario.Grupos;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult Dr = MessageBox.Show("¿Desea cancelar la operación? Los datos no guardados se perderán.","Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (Dr == System.Windows.Forms.DialogResult.Yes) 
            {
                this.Close();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (accion == "Modificacion")
            {
                if (ValidarDatos())
                {
                    oUsuario.NombreYApellido = txtNombreyApellido.Text;
                    oUsuario.NombreUsuario = txtNombreUsuario.Text;
                    oUsuario.Email = txtEmail.Text;
                    oUsuario.EstadoSituacion = true;
                    
                    if (Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().ModificarUsuario(oUsuario))
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    this.Close();
                }
            }
            else
            {
                if (ValidarDatos())
                {
                    oUsuario.NombreYApellido = txtNombreyApellido.Text;
                    oUsuario.NombreUsuario = txtNombreUsuario.Text;
                    oUsuario.Email = txtEmail.Text;
                    oUsuario.EstadoSituacion = true;
                    if (Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().AgregarUsuario(oUsuario))
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    this.Close();
                }
            }
        }
    }
}
