﻿using System;
using System.Windows.Forms;

namespace Vista.Seguridad
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            this.ActiveControl = this.txtUsuario;
        }

        /// <summary>
        /// Metodo que verifica si los textbox están vacios.
        /// </summary>
        /// <returns>True, si los textbox contienen strings, false si alguno de los textbox está vacio.</returns>
        private bool ValidarDatosLogin()
        {
            if (string.IsNullOrEmpty(this.txtUsuario.Text) || string.IsNullOrEmpty(this.txtClave.Text))
            {
                MessageBox.Show("No deje campos vacios", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtUsuario.Focus();
                return false;
            }
            return true;
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            this.txtUsuario.Text = this.txtUsuario.Text.Trim();
        }

        /// <summary>
        /// Cierra la aplicación y libera los recursos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalir_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        private Entidades.Seguridad.Usuario oUsuario;
        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            if (this.ValidarDatosLogin())
            {
                oUsuario = Controladora.Seguridad.ControladoraIniciarSesion.ObtenerInstancia().IniciarSesion(this.txtUsuario.Text, this.txtClave.Text);
                if (oUsuario != null) 
                {
                    formMenu formmenu = new formMenu(oUsuario);
                    this.Hide();
                    formmenu.Show();
                }
                else
                {
                    MessageBox.Show("Verifique haber ingresado correctamente el nombre de usuario y la contraseña", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.txtUsuario.Focus();
                    txtClave.Rese tText();
                    txtUsuario.ResetText();
                }
            }
            
        }
        public Entidades.Seguridad.Usuario ObtenerUsuario()
        {
            return this.oUsuario;
        }
        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }


    }
}
