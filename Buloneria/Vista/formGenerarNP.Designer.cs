﻿namespace Vista
{
    partial class formGenerarNP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCliente = new System.Windows.Forms.ComboBox();
            this.btnSeleccionarCliente = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbGrupoArticulos = new System.Windows.Forms.ComboBox();
            this.btnSeleccionarGrupo = new System.Windows.Forms.Button();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.dgvArticulosDisponibles = new System.Windows.Forms.DataGridView();
            this.colCod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAlic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFechaEmision = new System.Windows.Forms.DateTimePicker();
            this.txtNumeroNotaPedido = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnAgregarArticulo = new System.Windows.Forms.Button();
            this.btnQuitarArticulo = new System.Windows.Forms.Button();
            this.dgvArticulosSolicitados = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSubtotalFinal = new System.Windows.Forms.TextBox();
            this.txtMontoIvaFinal = new System.Windows.Forms.TextBox();
            this.txtTotalFinal = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.colCantPed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrecioVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSubtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosDisponibles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosSolicitados)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número Nota de Pedido:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha de Emisión:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Forma de Pago:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(109, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cliente:";
            // 
            // cmbCliente
            // 
            this.cmbCliente.FormattingEnabled = true;
            this.cmbCliente.Location = new System.Drawing.Point(157, 94);
            this.cmbCliente.Name = "cmbCliente";
            this.cmbCliente.Size = new System.Drawing.Size(155, 21);
            this.cmbCliente.TabIndex = 2;
            // 
            // btnSeleccionarCliente
            // 
            this.btnSeleccionarCliente.Location = new System.Drawing.Point(318, 94);
            this.btnSeleccionarCliente.Name = "btnSeleccionarCliente";
            this.btnSeleccionarCliente.Size = new System.Drawing.Size(128, 23);
            this.btnSeleccionarCliente.TabIndex = 3;
            this.btnSeleccionarCliente.Text = "Seleccionar Cliente";
            this.btnSeleccionarCliente.UseVisualStyleBackColor = true;
            this.btnSeleccionarCliente.Click += new System.EventHandler(this.btnSeleccionarCliente_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Grupo de Artículos:";
            // 
            // cmbGrupoArticulos
            // 
            this.cmbGrupoArticulos.FormattingEnabled = true;
            this.cmbGrupoArticulos.Location = new System.Drawing.Point(157, 121);
            this.cmbGrupoArticulos.Name = "cmbGrupoArticulos";
            this.cmbGrupoArticulos.Size = new System.Drawing.Size(155, 21);
            this.cmbGrupoArticulos.TabIndex = 4;
            // 
            // btnSeleccionarGrupo
            // 
            this.btnSeleccionarGrupo.Location = new System.Drawing.Point(318, 119);
            this.btnSeleccionarGrupo.Name = "btnSeleccionarGrupo";
            this.btnSeleccionarGrupo.Size = new System.Drawing.Size(128, 23);
            this.btnSeleccionarGrupo.TabIndex = 5;
            this.btnSeleccionarGrupo.Text = "Seleccionar Grupo";
            this.btnSeleccionarGrupo.UseVisualStyleBackColor = true;
            this.btnSeleccionarGrupo.Click += new System.EventHandler(this.btnSeleccionarGrupo_Click);
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(157, 67);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(155, 21);
            this.cmbFormaPago.TabIndex = 1;
            // 
            // dgvArticulosDisponibles
            // 
            this.dgvArticulosDisponibles.AllowUserToAddRows = false;
            this.dgvArticulosDisponibles.AllowUserToDeleteRows = false;
            this.dgvArticulosDisponibles.AllowUserToResizeColumns = false;
            this.dgvArticulosDisponibles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosDisponibles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCod,
            this.colDesc,
            this.colCant,
            this.colAlic});
            this.dgvArticulosDisponibles.Location = new System.Drawing.Point(26, 180);
            this.dgvArticulosDisponibles.MultiSelect = false;
            this.dgvArticulosDisponibles.Name = "dgvArticulosDisponibles";
            this.dgvArticulosDisponibles.ReadOnly = true;
            this.dgvArticulosDisponibles.RowHeadersVisible = false;
            this.dgvArticulosDisponibles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosDisponibles.ShowCellErrors = false;
            this.dgvArticulosDisponibles.ShowCellToolTips = false;
            this.dgvArticulosDisponibles.ShowEditingIcon = false;
            this.dgvArticulosDisponibles.ShowRowErrors = false;
            this.dgvArticulosDisponibles.Size = new System.Drawing.Size(642, 150);
            this.dgvArticulosDisponibles.TabIndex = 11;
            // 
            // colCod
            // 
            this.colCod.DataPropertyName = "CodArticulo";
            this.colCod.HeaderText = "Codigo Articulo";
            this.colCod.Name = "colCod";
            this.colCod.ReadOnly = true;
            // 
            // colDesc
            // 
            this.colDesc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDesc.DataPropertyName = "Descripcion";
            this.colDesc.HeaderText = "Descripcion";
            this.colDesc.Name = "colDesc";
            this.colDesc.ReadOnly = true;
            // 
            // colCant
            // 
            this.colCant.DataPropertyName = "CantActual";
            this.colCant.HeaderText = "Cantidad Actual";
            this.colCant.Name = "colCant";
            this.colCant.ReadOnly = true;
            // 
            // colAlic
            // 
            this.colAlic.DataPropertyName = "Alicuota";
            this.colAlic.HeaderText = "Alicuota";
            this.colAlic.Name = "colAlic";
            this.colAlic.ReadOnly = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(226, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Artículos Disponibles del Grupo Seleccionado:";
            // 
            // dtpFechaEmision
            // 
            this.dtpFechaEmision.Location = new System.Drawing.Point(157, 43);
            this.dtpFechaEmision.Name = "dtpFechaEmision";
            this.dtpFechaEmision.Size = new System.Drawing.Size(155, 20);
            this.dtpFechaEmision.TabIndex = 0;
            // 
            // txtNumeroNotaPedido
            // 
            this.txtNumeroNotaPedido.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtNumeroNotaPedido.Location = new System.Drawing.Point(157, 17);
            this.txtNumeroNotaPedido.Name = "txtNumeroNotaPedido";
            this.txtNumeroNotaPedido.Size = new System.Drawing.Size(155, 20);
            this.txtNumeroNotaPedido.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(456, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(175, 24);
            this.label8.TabIndex = 17;
            this.label8.Text = "Bulonería Wacker";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(388, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 16);
            this.label9.TabIndex = 18;
            this.label9.Text = "Bv. Seguí 2131 - 2000 Rosario - Santa Fe";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(457, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 16);
            this.label10.TabIndex = 19;
            this.label10.Text = "TeleFax: 0341-4620890";
            // 
            // btnAgregarArticulo
            // 
            this.btnAgregarArticulo.Location = new System.Drawing.Point(26, 338);
            this.btnAgregarArticulo.Name = "btnAgregarArticulo";
            this.btnAgregarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnAgregarArticulo.TabIndex = 6;
            this.btnAgregarArticulo.Text = "Agregar Articulo";
            this.btnAgregarArticulo.UseVisualStyleBackColor = true;
            this.btnAgregarArticulo.Click += new System.EventHandler(this.btnAgregarArticulo_Click_1);
            // 
            // btnQuitarArticulo
            // 
            this.btnQuitarArticulo.Location = new System.Drawing.Point(107, 338);
            this.btnQuitarArticulo.Name = "btnQuitarArticulo";
            this.btnQuitarArticulo.Size = new System.Drawing.Size(75, 48);
            this.btnQuitarArticulo.TabIndex = 7;
            this.btnQuitarArticulo.Text = "Quitar Articulo";
            this.btnQuitarArticulo.UseVisualStyleBackColor = true;
            this.btnQuitarArticulo.Click += new System.EventHandler(this.btnQuitarArticulo_Click_1);
            // 
            // dgvArticulosSolicitados
            // 
            this.dgvArticulosSolicitados.AllowUserToAddRows = false;
            this.dgvArticulosSolicitados.AllowUserToDeleteRows = false;
            this.dgvArticulosSolicitados.AllowUserToResizeColumns = false;
            this.dgvArticulosSolicitados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosSolicitados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCantPed,
            this.Articulo,
            this.colPrecioVenta,
            this.colSubtotal});
            this.dgvArticulosSolicitados.Location = new System.Drawing.Point(29, 430);
            this.dgvArticulosSolicitados.MultiSelect = false;
            this.dgvArticulosSolicitados.Name = "dgvArticulosSolicitados";
            this.dgvArticulosSolicitados.ReadOnly = true;
            this.dgvArticulosSolicitados.RowHeadersVisible = false;
            this.dgvArticulosSolicitados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosSolicitados.ShowCellErrors = false;
            this.dgvArticulosSolicitados.ShowCellToolTips = false;
            this.dgvArticulosSolicitados.ShowEditingIcon = false;
            this.dgvArticulosSolicitados.ShowRowErrors = false;
            this.dgvArticulosSolicitados.Size = new System.Drawing.Size(638, 150);
            this.dgvArticulosSolicitados.TabIndex = 22;
            this.dgvArticulosSolicitados.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvArticulosSolicitados_DataBindingComplete_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 414);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(201, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Articulos Agregados a la Nota de Pedido:";
            // 
            // txtSubtotalFinal
            // 
            this.txtSubtotalFinal.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtSubtotalFinal.Location = new System.Drawing.Point(568, 586);
            this.txtSubtotalFinal.Name = "txtSubtotalFinal";
            this.txtSubtotalFinal.Size = new System.Drawing.Size(100, 20);
            this.txtSubtotalFinal.TabIndex = 27;
            // 
            // txtMontoIvaFinal
            // 
            this.txtMontoIvaFinal.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtMontoIvaFinal.Location = new System.Drawing.Point(568, 612);
            this.txtMontoIvaFinal.Name = "txtMontoIvaFinal";
            this.txtMontoIvaFinal.Size = new System.Drawing.Size(100, 20);
            this.txtMontoIvaFinal.TabIndex = 28;
            // 
            // txtTotalFinal
            // 
            this.txtTotalFinal.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtTotalFinal.Location = new System.Drawing.Point(568, 638);
            this.txtTotalFinal.Name = "txtTotalFinal";
            this.txtTotalFinal.Size = new System.Drawing.Size(100, 20);
            this.txtTotalFinal.TabIndex = 29;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnAceptar.Location = new System.Drawing.Point(26, 619);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(98, 41);
            this.btnAceptar.TabIndex = 8;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click_1);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnCancelar.Location = new System.Drawing.Point(130, 619);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(98, 41);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(513, 593);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Subtotal:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(478, 619);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "Monto de I.V.A.:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(528, 645);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Total:";
            // 
            // colCantPed
            // 
            this.colCantPed.DataPropertyName = "Cantidad";
            this.colCantPed.HeaderText = "Cantidad Pedida";
            this.colCantPed.Name = "colCantPed";
            this.colCantPed.ReadOnly = true;
            // 
            // Articulo
            // 
            this.Articulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Articulo.HeaderText = "Articulo";
            this.Articulo.Name = "Articulo";
            this.Articulo.ReadOnly = true;
            // 
            // colPrecioVenta
            // 
            this.colPrecioVenta.HeaderText = "Precio Venta";
            this.colPrecioVenta.Name = "colPrecioVenta";
            this.colPrecioVenta.ReadOnly = true;
            // 
            // colSubtotal
            // 
            this.colSubtotal.HeaderText = "Subtotal";
            this.colSubtotal.Name = "colSubtotal";
            this.colSubtotal.ReadOnly = true;
            // 
            // formGenerarNP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 672);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.txtTotalFinal);
            this.Controls.Add(this.txtMontoIvaFinal);
            this.Controls.Add(this.txtSubtotalFinal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvArticulosSolicitados);
            this.Controls.Add(this.btnQuitarArticulo);
            this.Controls.Add(this.btnAgregarArticulo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtNumeroNotaPedido);
            this.Controls.Add(this.dtpFechaEmision);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvArticulosDisponibles);
            this.Controls.Add(this.cmbFormaPago);
            this.Controls.Add(this.btnSeleccionarGrupo);
            this.Controls.Add(this.cmbGrupoArticulos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSeleccionarCliente);
            this.Controls.Add(this.cmbCliente);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "formGenerarNP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "Ata";
            this.Text = "Generar Nota de Pedido";
            this.Load += new System.EventHandler(this.formGenerarNP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosDisponibles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosSolicitados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbCliente;
        private System.Windows.Forms.Button btnSeleccionarCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbGrupoArticulos;
        private System.Windows.Forms.Button btnSeleccionarGrupo;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.DataGridView dgvArticulosDisponibles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFechaEmision;
        private System.Windows.Forms.TextBox txtNumeroNotaPedido;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnAgregarArticulo;
        private System.Windows.Forms.Button btnQuitarArticulo;
        private System.Windows.Forms.DataGridView dgvArticulosSolicitados;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSubtotalFinal;
        private System.Windows.Forms.TextBox txtMontoIvaFinal;
        private System.Windows.Forms.TextBox txtTotalFinal;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCod;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCant;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAlic;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantPed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrecioVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSubtotal;
    }
}