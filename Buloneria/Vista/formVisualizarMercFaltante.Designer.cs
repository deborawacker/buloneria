﻿namespace Vista
{
    partial class formVisualizarMercFaltante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgvArticulosFaltantes = new System.Windows.Forms.DataGridView();
            this.cmbNroOrdenCompra = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCerrar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.colDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCantFalt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFaltantes)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nro. Orden de Compra:";
            // 
            // dgvArticulosFaltantes
            // 
            this.dgvArticulosFaltantes.AllowUserToAddRows = false;
            this.dgvArticulosFaltantes.AllowUserToDeleteRows = false;
            this.dgvArticulosFaltantes.AllowUserToResizeColumns = false;
            this.dgvArticulosFaltantes.AllowUserToResizeRows = false;
            this.dgvArticulosFaltantes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticulosFaltantes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDescripcion,
            this.colCantFalt});
            this.dgvArticulosFaltantes.Location = new System.Drawing.Point(15, 95);
            this.dgvArticulosFaltantes.MultiSelect = false;
            this.dgvArticulosFaltantes.Name = "dgvArticulosFaltantes";
            this.dgvArticulosFaltantes.ReadOnly = true;
            this.dgvArticulosFaltantes.RowHeadersVisible = false;
            this.dgvArticulosFaltantes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArticulosFaltantes.ShowCellErrors = false;
            this.dgvArticulosFaltantes.ShowCellToolTips = false;
            this.dgvArticulosFaltantes.ShowEditingIcon = false;
            this.dgvArticulosFaltantes.ShowRowErrors = false;
            this.dgvArticulosFaltantes.Size = new System.Drawing.Size(375, 169);
            this.dgvArticulosFaltantes.TabIndex = 2;
            // 
            // cmbNroOrdenCompra
            // 
            this.cmbNroOrdenCompra.FormattingEnabled = true;
            this.cmbNroOrdenCompra.Location = new System.Drawing.Point(134, 9);
            this.cmbNroOrdenCompra.Name = "cmbNroOrdenCompra";
            this.cmbNroOrdenCompra.Size = new System.Drawing.Size(155, 21);
            this.cmbNroOrdenCompra.TabIndex = 3;
            this.cmbNroOrdenCompra.SelectedIndexChanged += new System.EventHandler(this.cmbNroOrdenCompra_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Articulos Faltantes:";
            // 
            // txtCerrar
            // 
            this.txtCerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtCerrar.Location = new System.Drawing.Point(292, 270);
            this.txtCerrar.Name = "txtCerrar";
            this.txtCerrar.Size = new System.Drawing.Size(98, 41);
            this.txtCerrar.TabIndex = 5;
            this.txtCerrar.Text = "Cerrar";
            this.txtCerrar.UseVisualStyleBackColor = true;
            this.txtCerrar.Click += new System.EventHandler(this.txtCerrar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Estado:";
            // 
            // txtEstado
            // 
            this.txtEstado.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtEstado.Enabled = false;
            this.txtEstado.Location = new System.Drawing.Point(134, 37);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(155, 20);
            this.txtEstado.TabIndex = 7;
            // 
            // colDescripcion
            // 
            this.colDescripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colDescripcion.HeaderText = "Descripcion";
            this.colDescripcion.Name = "colDescripcion";
            this.colDescripcion.ReadOnly = true;
            // 
            // colCantFalt
            // 
            this.colCantFalt.HeaderText = "Cant Faltante";
            this.colCantFalt.Name = "colCantFalt";
            this.colCantFalt.ReadOnly = true;
            // 
            // formVisualizarMercFaltante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 334);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCerrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbNroOrdenCompra);
            this.Controls.Add(this.dgvArticulosFaltantes);
            this.Controls.Add(this.label1);
            this.Name = "formVisualizarMercFaltante";
            this.Text = "Mercaderia Faltante de Orden de Compra";
            //this.Load += new System.EventHandler(this.formVisualizarMercFaltante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticulosFaltantes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvArticulosFaltantes;
        private System.Windows.Forms.ComboBox cmbNroOrdenCompra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button txtCerrar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCantFalt;
    }
}