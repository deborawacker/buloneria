﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using Vista.BackupRestore;

namespace Vista
{
    public partial class formMenu : Form
    {
        Entidades.Seguridad.Usuario oUsuario;
        ReadOnlyCollection<Entidades.Seguridad.Perfil> Perfiles;
        public formMenu(Entidades.Seguridad.Usuario oUsu) //constructor
        {
            oUsuario = oUsu;
            Perfiles = Controladora.Seguridad.ControladoraIniciarSesion.ObtenerInstancia().RecuperarPerfilesXGrupos(oUsuario.Grupos);
            InitializeComponent();
            CargarPermisos();
        }


        private void CargarPermisos()
        {
                
                foreach (ToolStripItem oItem in menuStrip1.Items)//recorre cada uno de los elementos del menu principal
                {
                    //si el menu tiene submenu
                    if (((ToolStripMenuItem)oItem).DropDownItems.Count > 0)
                    {
                        //se revisan los elementos del submenu
                        foreach (ToolStripMenuItem oOpcion in ((ToolStripMenuItem)oItem).DropDownItems)
                        {
                            foreach (Entidades.Seguridad.Perfil oPerfil in Perfiles)
                            {
                                //Si se encuentra en la lista de perfiles un formulario con el nombre del elemento del menu lo habilita y continua con el siguiente elemento del menu
                                if (oPerfil.Formulario.Nombre == oOpcion.Text)
                                {
                                    if (oPerfil.Formulario.Nombre == "Seguridad" || oPerfil.Formulario.Nombre == "Auditoría")
                                    {
                                        if (oPerfil.Permiso.Tipo == "Acceso") oOpcion.Enabled = true;
                                    }
                                    else
                                    oOpcion.Enabled = true;
                                    break;
                                }
                                //Si no lo encuentra lo deshabilita
                                else
                                {
                                    oOpcion.Enabled = false;
                                }
                            }
                        }

                    }
                    else
                    {
                        foreach (Entidades.Seguridad.Perfil oPerfil in Perfiles)
                        {
                            //Si se encuentra en la lista de perfiles un formulario con el nombre del elemento del menu lo habilita y continua con el siguiente elemento del menu
                            if (oPerfil.Formulario.Nombre == oItem.Text)
                            {
                                oItem.Enabled = true;
                                break;
                            }
                            //Si no lo encuentra lo deshabilita
                            else
                            {
                                //oItem.Enabled = false;
                            }
                        }
                    }
                }
                
        }


        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGProveedores formgestionproveedores = new formGProveedores(Perfiles);
            formgestionproveedores.Show(); //aca muestra el form
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGClientes formgestionclientes = new formGClientes(Perfiles);
            formgestionclientes.Show();
        }

        private void mercaderíasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGGruposArticulos formgestionargruposarticulos = new formGGruposArticulos(Perfiles);
            formgestionargruposarticulos.Show();
        }

        private void artículosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGArticulos formgestionararticulos = new formGArticulos(Perfiles);
            formgestionararticulos.Show();
        }

        private void ordenesDeComprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGestionarOC formgestionarordenescompras = new formGestionarOC(Perfiles,this.oUsuario);
            formgestionarordenescompras.Show();
        }

        private void ordenDeCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGenerarOC formgenerarordencompra = new formGenerarOC(Perfiles,this.oUsuario);
            formgenerarordencompra.Show();
        }

        private void mercaderíaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formIMercaderia formingresarmercaderia = new formIMercaderia(Perfiles,oUsuario);
            formingresarmercaderia.Show();
        }

        private void seguridadToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Seguridad.FormSeguridad formseguridad = new Seguridad.FormSeguridad(oUsuario);
            formseguridad.Show();
            
        }

        private void cambiarEMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.FormConfigurarMail formconfigurarmail = new Seguridad.FormConfigurarMail();
            formconfigurarmail.Show();
        }

        private void grupoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.FormUsuario formusuario = new Seguridad.FormUsuario();
            formusuario.Show();
        }

        private void grupoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Seguridad.FormGrupo formgrupo = new Seguridad.FormGrupo();
            formgrupo.Show();
        }

        private void formMenu_Load(object sender, EventArgs e)
        {
            Perfiles = Controladora.Seguridad.ControladoraIniciarSesion.ObtenerInstancia().RecuperarPerfilesXGrupos(oUsuario.Grupos);
        }

        private void notaDePedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGenerarNP formgenerarnp = new formGenerarNP(Perfiles);
            formgenerarnp.Show();
        }

        private void notaDePedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            formGestionarNP formgestionarnp = new formGestionarNP(Perfiles);
            formgestionarnp.Show();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void formMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Controladora.Seguridad.ControladoraCerrarSesion.ObtenerInstancia().CerrarSesion(this.oUsuario);
        }

        private void auditoríaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Auditoria.FormAuditoriaLogs formAuditoria = new Auditoria.FormAuditoriaLogs();
            formAuditoria.Show();
        }

        private void cambiarClaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Seguridad.FormCambiarContraseña formcambiarcontras = new Seguridad.FormCambiarContraseña(oUsuario);
            formcambiarcontras.Show();
       
        }

        private void resetearContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void gestiónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void visualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formVCtaCteCliente formvctactecli = new formVCtaCteCliente();
            formvctactecli.Show();
        }

        private void registrarPagoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formRegistrarPagoCliente formregpagocli = new formRegistrarPagoCliente();
            formregpagocli.Show();
        }

        private void conceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formGConceptos formgconceptos = new formGConceptos();
            formgconceptos.Show();
        }

        private void mercaderíaFaltanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formVisualizarMercFaltante formVMFaltante = new formVisualizarMercFaltante();
            formVMFaltante.Show();
        }

        private void backupRestoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBackupRestore formBackupRestore = new FrmBackupRestore();
            formBackupRestore.Show();
        }

        private void informaciónGerencialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formInformes formInformes = new formInformes(this.Perfiles);
            formInformes.Show();            
            formInformes.Size = new System.Drawing.Size(678, 652); //le doy un tamano al formulario
        }
    }
}
