﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formConsultarOC : Form
    {
        private Entidades.Sistema.OrdenCompra _oc;
        
        public formConsultarOC(Entidades.Sistema.OrdenCompra ordencompra)
        {
            _oc = ordencompra;
            InitializeComponent();
            this.dgvArticulosAgregados.AutoGenerateColumns = false;
            this.dgvArticulosAgregados.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dgvArticulosAgregados_DataBindingComplete);
        }

        void dgvArticulosAgregados_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvArticulosAgregados.Rows)
            {
                item.Cells["colSubTotal"].Value = Convert.ToInt32(item.Cells["colCantidadPedir"].Value) * Convert.ToInt32(item.Cells["colPrecio"].Value);   
            }
        }

        private void formConsultarOC_Load(object sender, EventArgs e)
        {
            this.cmbEstado.DropDownStyle = ComboBoxStyle.Simple;
            this.cmbFormaPago.DropDownStyle = ComboBoxStyle.Simple;
            this.cmbProveedor.DropDownStyle = ComboBoxStyle.Simple;
            txtNumeroOrdenCompra.Text = _oc.NroOrdenCompra.ToString();
            dtpFechaEmision.Text = _oc.FechaEmision.ToString();
            dtpFechaRecepcion.Text = _oc.FechaPlazo.ToString();
            cmbEstado.Text = _oc.Estado.ToString();
            cmbFormaPago.Text = _oc.FormaPago.ToString();
            cmbProveedor.Text = _oc.Proveedor.ToString();
            this.cmbFormaPago.Enabled = false;
            this.cmbEstado.Enabled = false;
            dtpFechaEmision.Enabled = false;
            dtpFechaRecepcion.Enabled = false;
            this.dgvArticulosAgregados.DataSource = _oc.LineasOrdenCompra;
            decimal subtotaliva = 0; 
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaOrdenCompra linea in _oc.LineasOrdenCompra)
            {
                int subtotallinea = linea.PrecioUnitario * linea.CantPedir;
                subtotal += subtotallinea;
                decimal subtotallineaiva = (subtotallinea * linea.Articulo.Alicuota.Valor) / 100;
                subtotaliva += subtotallineaiva;
            }
            txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = subtotaliva.ToString();
            decimal total = subtotal + subtotaliva;
            txtTotal.Text = total.ToString();
        }
        
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNumeroOrdenCompra_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnVisualizarMercaderiaFaltante_Click(object sender, EventArgs e)
        {
            if(_oc.Estado == Entidades.Sistema.EstadoOrdenCompra.Pendiente || _oc.Estado == Entidades.Sistema.EstadoOrdenCompra.Anulado)
            {
                formVisualizarMercFaltante oform = new formVisualizarMercFaltante(_oc); 
                oform.Show();
            }
            else
            {
                MessageBox.Show("No se puede visualizar una Orden de Compra con estado EN CURSO o FINALIZADO", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }        
    }
}
