﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formECliente : Form
    {
        Entidades.Sistema.Cliente _cliente;
        public formECliente(Entidades.Sistema.Cliente cliente) //aca paso como parametro del form gestionar clientes
        {
            _cliente = cliente;
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formECliente_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _cliente.Celular;
            txtCodigo.Text = _cliente.CodCliente.ToString();
            txtCodigoPostal.Text = _cliente.CodPostal.ToString();            
            txtCuit.Text = _cliente.Cuit.ToString();
            txtDireccion.Text = _cliente.Direccion;
            txtEmail.Text = _cliente.Email;
            txtFax.Text = _cliente.Fax;
            txtNombreFantasia.Text = _cliente.NombreFantasia;            
            txtRazonSocial.Text = _cliente.RazonSocial;
            txtTelefono.Text = _cliente.Telefono;
            cmbCuidad.Text = _cliente.Ciudad.Nombre;            
            cmbProvincia.Text = _cliente.Ciudad.Provincia.Nombre;
            cmbTipoIva.Text = _cliente.TipoIva.Nombre;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DialogResult respuesta = MessageBox.Show("Desea eliminar el cliente?", "Eliminar", MessageBoxButtons.YesNo);
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                bool resultado = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().EliminarCliente(_cliente);
                if (resultado)
                {
                    MessageBox.Show("El cliente se elimino exitosamente.", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ha ocurrido un error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }


    }
}
