﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formReporteFaltanteStock : Form
    {
        public formReporteFaltanteStock(BindingSource bs)
        {
            List<FaltanteStock> falt = new List<FaltanteStock>();
            InitializeComponent();
            foreach (DataRow item in ((DataTable)bs.DataSource).Rows)
            {
                FaltanteStock faltstock = new FaltanteStock();
                faltstock.CodigoArticulo = item.ItemArray[0].ToString();
                faltstock.Desc = item.ItemArray[1].ToString();
                faltstock.Cantmin = Convert.ToInt32(item.ItemArray[2]);
                faltstock.Cantactual = Convert.ToInt32(item.ItemArray[3]);
                faltstock.Cantfalt = Convert.ToInt32(item.ItemArray[4]);
                falt.Add(faltstock);
            }
            this.FaltanteStockBindingSource.DataSource = falt;
        }

        private void formReporteFaltanteStock_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }

    public class FaltanteStock
    {
        private string _codigoArticulo;

        public string CodigoArticulo
        {
            get { return _codigoArticulo; }
            set { _codigoArticulo = value; }
        }
        private string _desc;

        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }
        private int _cantmin;

        public int Cantmin
        {
            get { return _cantmin; }
            set { _cantmin = value; }
        }
        private int _cantactual;

        public int Cantactual
        {
            get { return _cantactual; }
            set { _cantactual = value; }
        }
        private int _cantfalt;

        public int Cantfalt
        {
            get { return _cantfalt; }
            set { _cantfalt = value; }
        }
    }
}
