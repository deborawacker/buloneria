﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formCCliente : Form
    {
        private Entidades.Sistema.Cliente _cliente;
        public formCCliente(Entidades.Sistema.Cliente cliente)  //aca paso como parametro del form gestionar clientes
        {
            _cliente = cliente;
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formCCliente_Load(object sender, EventArgs e)
        {
            txtCelular.Text = _cliente.Celular;
            txtCodigo.Text = _cliente.CodCliente.ToString();
            txtCodPostal.Text = _cliente.CodPostal.ToString();
            txtCuit.Text = _cliente.Cuit.ToString();
            txtDireccion.Text = _cliente.Direccion;
            txtEmail.Text = _cliente.Email;
            txtFax.Text = _cliente.Fax;
            txtNomFantasia.Text = _cliente.NombreFantasia;
            txtRazonSocial.Text = _cliente.RazonSocial;
            txtTelefono.Text = _cliente.Telefono;
            cmbCiudad.Text = _cliente.Ciudad.Nombre;
            cmbProvincia.Text = _cliente.Ciudad.Provincia.Nombre;
            cmbTipoIva.Text = _cliente.TipoIva.Nombre;
        }
    }
}
