﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Vista
{
    public partial class formReporteMontoCompraCli : Form
    {
        public formReporteMontoCompraCli(BindingSource bs)
        {
            List<MontosComprasCli> monto = new List<MontosComprasCli>();
            InitializeComponent();
            foreach (DataRow item in ((DataTable)bs.DataSource).Rows)
            {
                MontosComprasCli montocomp = new MontosComprasCli();
                montocomp.Montomes = Convert.ToInt32(item.ItemArray[0]);
                montocomp.Mes = Convert.ToInt32(item.ItemArray[1].ToString());
                montocomp.Razonsocial = item.ItemArray[2].ToString();                
                monto.Add(montocomp);
            }
            this.MontosComprasCliBindingSource.DataSource = monto;

        }

        private void formReporteMontoCompraCli_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }

    public class MontosComprasCli
    {
        private int _montomes;

        public int Montomes
        {
            get { return _montomes; }
            set { _montomes = value; }
        }
        private int _mes;

        public int Mes
        {
            get { return _mes; }
            set { _mes = value; }
        }
        private string _razonsocial;

        public string Razonsocial
        {
            get { return _razonsocial; }
            set { _razonsocial = value; }
        }
    }
}
