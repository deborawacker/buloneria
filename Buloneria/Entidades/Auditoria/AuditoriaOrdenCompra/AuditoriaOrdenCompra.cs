﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.Sistema;

namespace Entidades.Auditoria
{
    public class AuditoriaOrdenCompra
    { 
        public string Usuario { get; set; }
         
        public DateTime FechaRegistro{ get; set; }
        
        public int NroOrdenCompra{ get; set; }
         
        public EstadoOrdenCompra Estado { get; set; }
           
    }
}
