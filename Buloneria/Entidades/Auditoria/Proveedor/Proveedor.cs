﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Auditoria
{
    public class Proveedor
    {
        #region Properties
        public string TipoIva { get; set; }

        public string Email { get; set; }

        public string Celular { get; set; }

        public string Fax { get; set; }

        public string Telefono { get; set; }

        public int CodPostal { get; set; }

        public int Ciudad { get; set; }

        public string Direccion { get; set; }

        public string NombreFantasia { get; set; }

        public string RazonSocial { get; set; }

        public Int64 Cuit { get; set; }

        public int CodProveedor { get; set; }

        public DateTime FechaHora { get; set; }

        public string Accion { get; set; }

        public string Usuario { get; set; }  
        #endregion

        #region Methods
        public override string ToString()
        {
            return RazonSocial;
        }
         
        public void SetearDatosProveedor(Sistema.Proveedor proveedor)
        {
            this.Celular = proveedor.Celular;
            this.Ciudad = proveedor.Ciudad.CodLocalidad;
            this.CodPostal = proveedor.CodPostal;
            this.CodProveedor = proveedor.CodProveedor;
            this.Cuit = proveedor.Cuit;
            this.Direccion = proveedor.Direccion;
            this.Email = proveedor.Email;
            this.Fax = proveedor.Fax;
            this.NombreFantasia = proveedor.NombreFantasia;
            this.RazonSocial = proveedor.RazonSocial;
            this.Telefono = proveedor.Telefono;
            this.TipoIva = proveedor.TipoIva.Nombre;
        }

        #endregion
    }
}