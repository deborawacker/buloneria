﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Auditoria
{
    public class ProveedorCriterio
    {
        public string RazonSocial { get; set; }

        public string Usuario { get; set; }
    }
}
