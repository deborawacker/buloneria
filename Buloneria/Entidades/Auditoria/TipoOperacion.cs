﻿
namespace Entidades.Auditoria
{
    public enum TipoOperacion
    {
        Agregar,
        Modificar,
        Eliminar,
    }
}
