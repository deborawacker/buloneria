﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Auditoria
{
    public class LoginOutCriterio
    {
        public string Usuario { get; set; }

        public bool TraerIngresos { get; set; }

        public bool TraerEgresos { get; set; }
    }
}
