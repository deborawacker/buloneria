﻿using System;

namespace Entidades.Auditoria
{
    public class LogInOut
    {

        private TipoInicioSesion _Operacion;

        public TipoInicioSesion Operacion
        {
            get { return _Operacion; }
            set { _Operacion = value; }
        }

        private string _Usuario;

        public string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        
        private string _NombreEquipo = Environment.MachineName;

        public string NombreEquipo
        {
            get { return _NombreEquipo; }
            set { _NombreEquipo = value; }
        }

        private DateTime _FechaHora;

        public DateTime FechaHora
        {
            get { return _FechaHora; }
            set { _FechaHora = value; }
        }
    }
}
