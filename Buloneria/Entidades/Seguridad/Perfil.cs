﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Seguridad
{
    public class Perfil
    {
        #region Constructor
        public Perfil()
        {
            ColAcciones = new List<Seguridad.Accion>();
        }

        #endregion

        #region Properties

        public int IdPerfil{ get; set; }
         
        public string NombrePerfil{ get; set; }
         
        public string DescripcionPerfil { get; set; }

        public List<Accion> ColAcciones { get; set; }

        #endregion
    }
}
