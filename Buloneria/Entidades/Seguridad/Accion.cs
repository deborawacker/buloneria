﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Seguridad
{
    public class Accion
    {
        #region Properties
        public int IdAccion { get; set; }
          
        public string NombreAccion { get; set; }

        public string DescripcionAccion { get; set; }

        #endregion
    }
}
