﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Entidades.Seguridad
{
    public class Usuario
    {

        #region Constructor

        public Usuario()
        {
            ColPerfiles = new List<Seguridad.Perfil>();
        }

        public Usuario(string nombreUsuario, string nombreApellido, string email)
        {
            this.NombreUsuario = nombreUsuario; 
            this.NombreYApellido = nombreApellido;
            this.Email = email;
            ColPerfiles = new List<Seguridad.Perfil>();
        }

        #endregion

        #region Properties

        public int IdUsuario { get; set; }

        public string NombreUsuario { get; set; }

        public string NombreYApellido { get; set; }

        public string Email { get; set; }

        public string Clave { get; set; }

        public List<Perfil> ColPerfiles { get; set; }

        public string ClaveNoEncriptada { get; set; }

        #endregion

        #region Methods

        public bool CambiarContraseña(string PassAntigua, string NuevaPass)
        {
            if (PassAntigua == Clave)
            {
                this.Clave = NuevaPass;
                return true;
            }
            else return false;
        }

        #endregion

    }
}
