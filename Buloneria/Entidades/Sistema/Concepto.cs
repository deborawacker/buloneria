﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema

{
    [Serializable]
    public class Concepto
    {
        private string _descripcion;

        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        private TipoMovimiento _TipoMovimiento;

        public TipoMovimiento TipoMovimiento
        {
            get { return _TipoMovimiento; }
            set { _TipoMovimiento = value; }
        }

        private string _concepto;

        public string Concepto1
        {
            get { return _concepto; }
            set { _concepto = value; }
        }
    }
}
