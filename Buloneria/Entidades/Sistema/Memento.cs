﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Entidades.Sistema
{
    [Serializable]
    public class Memento
    {
        public List<LineaNotaPedido> ColLineasNotaPedido { get; set; }

        #region Constructor

        public Memento()
        {

        }

        public Memento(List<LineaNotaPedido> colLineasNP)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, colLineasNP);
                ms.Position = 0;
                var colLineas = (List<LineaNotaPedido>)formatter.Deserialize(ms);
                this.ColLineasNotaPedido = colLineasNP;
            }
        }

        #endregion
    }
}
