﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class TipoIva
    {

        private string _Nombre; //declaro atributo como privado y su _TipoMovimiento string

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Descripcion;

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public override string ToString() //le digo que sobreescriba y salga el nombre en la grilla
        {
            return _Nombre;
        }
    }
}
