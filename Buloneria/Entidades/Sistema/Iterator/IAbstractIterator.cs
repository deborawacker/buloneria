﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public interface IAbstractIterator
    {
        object First();
        object Next();
        bool HasNext { get; }
        object ObjActual { get; }
    }
}
