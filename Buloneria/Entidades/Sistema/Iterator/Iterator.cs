﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class Iterator : IAbstractIterator
    {
        private readonly Collection _collection;
        private int _current = 0;
        private int _step = 1;

        // Constructor
        public Iterator(Collection collection)
        {
            this._collection = collection;
        }

        // Gets first item
        public object First()
        {
            _current = 0;
            return _collection[_current] as object;
        }

        // Gets next item
        public object Next()
        {
            _current += _step;
            if (!HasNext)
                return _collection[_current] as object;
            else
                return null;
        }

        // Gets or sets stepsize
        public int Step
        {
            get { return _step; }
            set { _step = value; }
        }

        // Gets current iterator item
        public object ObjActual
        {
            get { return _collection[_current] as object; }
        }

        // Gets whether iteration is complete
        public bool HasNext
        {
            get { return _current >= _collection.Count; }
        }
    }
}
