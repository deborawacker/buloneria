﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public interface IAbstractCollection
    {
        Iterator CreateIterator();
    }
}
