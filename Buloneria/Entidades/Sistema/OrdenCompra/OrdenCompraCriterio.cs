﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class OrdenCompraCriterio 
    {
        #region Properties
        public int CodProveedor { get; set; }

        public int NroOrdenCompra { get; set; }

        public String RazonSocial { get; set; }
        #endregion

        #region Methods
        public bool EstaVacio()
        {
            return (CodProveedor == 0 && NroOrdenCompra == 0 && RazonSocial.Equals(String.Empty)) ? true : false;
        }
        #endregion

    }
}
