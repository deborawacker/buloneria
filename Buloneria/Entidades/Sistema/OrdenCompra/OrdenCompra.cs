﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class OrdenCompra
    {
        public OrdenCompra()
        {
            this.LineasOrdenCompra = new List<LineaOrdenCompra>();

            this.Estado = EstadoOrdenCompra.EnCurso;
        }

        #region Properties
        public int NroOrdenCompra { get; set; }
        public DateTime FechaEmision { get; set; }

        public DateTime FechaPlazo { get; set; }

        public FormaPago FormaPago { get; set; }
        public Proveedor Proveedor { get; set; }

        public EstadoOrdenCompra Estado { get; set; }

        public List<LineaOrdenCompra> LineasOrdenCompra { get; set; }

        public DateTime FechaHora { get; set; }

        public string Usuario { get; set; }

        #endregion

        #region Custom Properties

        public string FechaEmisionCorta
        {
            get
            {
                return this.FechaEmision.ToShortDateString();
            }
        }

        public string FechaPlazoCorta
        {
            get
            {
                return this.FechaPlazo.ToShortDateString();
            }
        }

        
        public bool TieneLineasConCantidadFaltante
        {
            get
            {
                return this.LineasOrdenCompra.Any(linea => linea.CantFaltante > 0);
            }
        }

        public bool TieneFaltante
        {
            get
            {
                return this.LineasOrdenCompra.Any(l => l.CantFaltante > 0);
            }
        }

        public decimal Subtotal
        {
            get
            {
                return LineasOrdenCompra.Sum(linea => linea.PrecioUnitario * linea.CantPedir);
            }
        }

        public decimal MontoIva
        {
            get
            {
                return this.LineasOrdenCompra.Sum(linea => linea.SubtotalIva);
            }
        }

        public decimal Total
        {
            get { return Subtotal + MontoIva; }
        }

        #endregion


        #region Methods

        public void AgregarLineaOrdenCompra(LineaOrdenCompra oLineaOrdenCompra)
        {
            this.LineasOrdenCompra.Add(oLineaOrdenCompra);
        }

        #endregion
    }
}
