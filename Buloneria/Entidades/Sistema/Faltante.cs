﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema

{
    public class Faltante
    {
        private int _cantFaltante;

        public int CantFaltante
        {
            get { return _cantFaltante; }
            set { _cantFaltante = value; }
        }
        private Entidades.Sistema.LineaOrdenCompra _LineaOrdenCompra;

        public Entidades.Sistema.LineaOrdenCompra LineaOrdenCompra
        {
            get { return _LineaOrdenCompra; }
            set { _LineaOrdenCompra = value; }
        }
        public override string ToString()
        {
            return _LineaOrdenCompra.Articulo.Descripcion;
        }
    }
}
