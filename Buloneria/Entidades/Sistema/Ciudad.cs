﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    //TODO: cambiar properties
    [Serializable]
    public class Ciudad
    {
        #region Properties
        public string Nombre { get; set; }

        public int CodLocalidad { get; set; }

        public Provincia Provincia { get; set; } 
        #endregion
    }
}
