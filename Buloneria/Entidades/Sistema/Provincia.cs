﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Provincia
    {
        public Provincia()
        {
            this.Ciudades = new List<Ciudad>();
        }
        
        public string Nombre { get; set; }

        public List<Ciudad> Ciudades { get; set; }

        public void AgregarCiudad(Ciudad ciudad)
        {
            ciudad.Provincia = this; 

            Ciudades.Add(ciudad); //le agrega la ciudad
        }

        
    }
}
