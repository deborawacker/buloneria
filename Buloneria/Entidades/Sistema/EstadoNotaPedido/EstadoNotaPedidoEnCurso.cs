﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class EstadoNotaPedidoEnCurso : EstadoNotaPedido
    {

        #region Constructor

        public EstadoNotaPedidoEnCurso()
        {
        }

        #endregion

        public override void PasarAPendienteEntrega(NotaPedido notaPedido)
        {
            notaPedido.EstadoNotaPedido = EstadoNotaPedidoFactory.Crear(EstadoNotaPedidoParameters.EstadoNotaPedidoPendienteEntrega) ;

            //TODO: aplicar descuento
            //notaPedido.AgregarDescuento();

        }
        public override void Finalizar(NotaPedido notaPedido)
        {
            notaPedido.EstadoNotaPedido = EstadoNotaPedidoFactory.Crear(EstadoNotaPedidoParameters.EstadoNotaPedidoFinalizado);
        }

        public override void FinalizarConFaltante(NotaPedido notaPedido)
        {
            notaPedido.EstadoNotaPedido = EstadoNotaPedidoFactory.Crear(EstadoNotaPedidoParameters.EstadoNotaPedidoFinConFalt);
        }
    }
}
