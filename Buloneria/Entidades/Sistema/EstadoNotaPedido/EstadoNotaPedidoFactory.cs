﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Entidades.Sistema
{
    public static class EstadoNotaPedidoFactory
    {
        public static EstadoNotaPedido Crear(Int64 id)
        {
            switch (id)
            {
                case 1:
                    return new EstadoNotaPedidoEnCurso() { Id = 1, Descripcion = "En curso" };
                case 2:
                    return new EstadoNotaPedidoPendienteEntrega() { Id = 2, Descripcion = "Pendiente de entrega" };
                case 3:
                    return new EstadoNotaPedidoFinalizadaConFaltanteEntrega() { Id = 3, Descripcion = "FinConFalt" };
                case 4:
                    return new EstadoNotaPedidoFinalizada() { Id = 4, Descripcion = "Finalizada" };
                default:
                    throw new System.ArgumentException("No se pudo resolver la instancia", "original");
            }
        }
    }
}
