﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class EstadoNotaPedidoPendienteEntrega : EstadoNotaPedido
    {


        public override void Finalizar(NotaPedido notaPedido)
        {
            notaPedido.EstadoNotaPedido = EstadoNotaPedidoFactory.Crear(EstadoNotaPedidoParameters.EstadoNotaPedidoFinalizado);
        }

        public override void FinalizarConFaltante(NotaPedido notaPedido)
        {
            notaPedido.EstadoNotaPedido =
                EstadoNotaPedidoFactory.Crear(EstadoNotaPedidoParameters.EstadoNotaPedidoFinConFalt);
        }

        public override void PasarAPendienteEntrega(NotaPedido notaPedido)
        {
            //Sigue en el mismo estado
        }
    }
}
