﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class EstadoNotaPedidoParameters
    {
        public static int EstadoNotaPedidoEnCurso
        { 
            get
            {
                return 1;
            }
        }
        public static int EstadoNotaPedidoPendienteEntrega
        {
            get 
            {
                return 2;
            }
        }
        public static int EstadoNotaPedidoFinConFalt 
        {
            get
            {
                return 3;
            }
        }
        public static int EstadoNotaPedidoFinalizado 
        {
            get 
            {
                return 4;
            }
        }
    }
}
