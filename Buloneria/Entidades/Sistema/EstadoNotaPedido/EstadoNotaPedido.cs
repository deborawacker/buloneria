﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public abstract class EstadoNotaPedido
    {
        public Int64 Id;

        public String Descripcion { get; set; }

        public abstract void Finalizar(NotaPedido notaPedido);

        public abstract void FinalizarConFaltante(NotaPedido notaPedido);

        public abstract void PasarAPendienteEntrega(NotaPedido notaPedido);
    }
}
