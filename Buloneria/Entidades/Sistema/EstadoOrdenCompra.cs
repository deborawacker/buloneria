﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public enum EstadoOrdenCompra
    {
        //Anulado, Finalizado Con Faltante
        FinConFalt,
        EnCurso,
        Finalizado,
        Pendiente
    }
}
