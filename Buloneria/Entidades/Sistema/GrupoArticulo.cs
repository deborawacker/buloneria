﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class GrupoArticulo
    {
        #region Properties 
        public int CodGrupoArticulo { get; set; }


        public string Descripcion { get; set; }

        public string NombreGrupo { get; set; }

        #endregion

        #region Methods
        public override string ToString() //le digo que sobreescriba y salga el codigo en la grilla
        {
            return Descripcion;
        }
        #endregion
    }
}
