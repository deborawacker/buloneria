﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Alicuota
    {
        #region Constructor
        public Alicuota()
        {
                
        }
        public Alicuota(Decimal valorAlicuota)
        {
            this.Valor = valorAlicuota;
        }

        #endregion

        #region Properties
        public decimal Valor { get; set; }
         
        #endregion

        #region Methods
        public override string ToString()
        {
            return Valor.ToString();
        }

        #endregion
    }
}
