﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class LineaOrdenCompra
    {
        #region Properties
        public int NroLineaOrdenCompra { get; set; }

        public Articulo Articulo { get; set; }

        public int CantPedir { get; set; }

        public int PrecioUnitario { get; set; }

        //Ingreso de Mercaderia 
        public int CantRecibida { get; set; }

        #endregion

        #region Custom Properties

        public decimal SubtotalIva
        {
            get
            {
                return CantPedir * (Articulo.PrecioVenta * Articulo.Alicuota.Valor) / 100;
            }

        }
        public decimal Subtotal //en la linea de la orden de compra calculo el subtotal de la linea
        {
            get
            {
                return CantPedir * PrecioUnitario; ;
            }

        }

        public int CantFaltante
        {
            get { return CantPedir - CantRecibida; }
        }

        //Esta propiedad solo se usa para hacer funcionar el ingreso de Mercaderia es la que se ingresa en el textbox
        public int CantIngresada { get; set; }

        #endregion
    }
}
