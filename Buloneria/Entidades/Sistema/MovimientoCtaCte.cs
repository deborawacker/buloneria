﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class MovimientoCtaCte
    {
        private Concepto _Concepto;

        public Concepto Concepto
        {
            get { return _Concepto; }
            set { _Concepto = value; }
        }
        private DateTime _fecha;

        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
        private int _monto;

        public int Monto
        {
            get { return _monto; }
            set { _monto = value; }
        }

        private int _nroMovimiento;

        public int NroMovimiento
        {
            get { return _nroMovimiento; }
            set { _nroMovimiento = value; }
        }
        public override string ToString()
        {
            return _Concepto.Descripcion;
        }
    }
}
