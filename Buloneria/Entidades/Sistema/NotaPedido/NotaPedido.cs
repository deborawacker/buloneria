﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class NotaPedido
    {
        #region Constructor
        public NotaPedido()
        {
            this.LineasNotaPedido = new List<LineaNotaPedido>();

            this.EstadoNotaPedido = new EstadoNotaPedidoEnCurso();
        }
        #endregion

        #region Properties

        public int NroNotaPedido { get; set; }

        public DateTime FechaEmision { get; set; }

        public FormaPago FormaPago { get; set; }

        public Cliente Cliente { get; set; }

        public Entidades.Seguridad.Usuario Usuario { get; set; }

        public EstadoNotaPedido EstadoNotaPedido { get; set; }

        public List<LineaNotaPedido> LineasNotaPedido { get; set; }

        #endregion

        #region Custom Properties
        public String FechaEmisionCorta
        {
            get
            {
                return this.FechaEmision.ToShortDateString();
            }
        }

        public Decimal Subtotal
        {
            get
            {
                return this.LineasNotaPedido.Sum(l => l.Subtotal);
            }
        }

        public Decimal SubtotalIVA
        {
            get { return this.LineasNotaPedido.Sum(l => l.SubtotalIva); }

        }

        public Decimal Total
        {
            get
            {
                Decimal subtotal = 0m;
                Decimal montoIVA = 0m;

                foreach (LineaNotaPedido linea in this.LineasNotaPedido)
                {
                    int subtotalLinea = linea.PrecioUnitario * linea.CantPedida;
                    subtotal += subtotalLinea;

                    //Calcular IVA
                    decimal subtotalLineaIva = (subtotalLinea * linea.Articulo.Alicuota.Valor) / 100;
                    montoIVA += subtotalLineaIva;
                }

                Decimal total = subtotal + montoIVA;

                return total;

            }
        }

        public bool TieneFaltanteEntrega
        {
            get
            {
                return this.LineasNotaPedido.Any(linea => linea.CantFaltanteEntrega > 0);
            }
        }

        #endregion

        #region Methods

        public void AgregarLineaNotaPedido(LineaNotaPedido oLineaNotaPedido)
        {
            LineasNotaPedido.Add(oLineaNotaPedido);
        }

        public void QuitarLineaNotaPedido(LineaNotaPedido oLineaNotaPedido)
        {
            LineasNotaPedido.Remove(oLineaNotaPedido);
        }

        public void FinalizarConFaltante()
        {
            this.EstadoNotaPedido.FinalizarConFaltante(this);
        }

        public void PasarAPendienteEntrega()
        {
            this.EstadoNotaPedido.PasarAPendienteEntrega(this);
        }

        public void Finalizar()
        {
            this.EstadoNotaPedido.Finalizar(this);
        }

        public void PasarASiguienteEstado()
        {
            if (this.TieneFaltanteEntrega)
            {
                this.PasarAPendienteEntrega();
            }
            else
            {
                this.Finalizar();
            }
        }

        #endregion

        #region Memento
        public Memento CrearMemento()
        {
            return new Memento(new List<LineaNotaPedido>(this.LineasNotaPedido));
        }

        public bool RestablecerMemento(Memento memento)
        {
            if (memento != null)
            {
                //TODO: Esta linea resolvió el problema del Memento, al hacer el new se asegura que la coleccion del memento y la de 
                //la nota de pedido sean diferentes, de manera que al agregar una linea a la coleccion
                //la agrega a la que corresponda y no a ambas (cuando apuntan a la misma direccion de memoria)
                this.LineasNotaPedido = new List<LineaNotaPedido>(memento.ColLineasNotaPedido);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

       
    }
}
