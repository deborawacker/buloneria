﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    /// <summary>
    /// Se utiliza para utilizar el metodo de busqueda por criterio
    /// </summary>
    public class NotaPedidoCriterio
    {
        #region Properties
        public int CodCliente { get; set; }

        public int NroNotaPedido { get; set; }

        public String RazonSocial { get; set; }
        #endregion

        #region Methods
        public bool EstaVacio()
        {
            return (CodCliente == 0 && NroNotaPedido == 0 && RazonSocial.Equals(String.Empty)) ? true : false;
        }
        #endregion

    }
}
