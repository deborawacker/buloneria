﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class FormaPago
    {
        private string _tipoFormaPago;

        public string TipoFormaPago
        {
            get { return _tipoFormaPago; }
            set { _tipoFormaPago = value; }
        }
        public override string ToString()
        {
            return _tipoFormaPago;
        }
    }
}
