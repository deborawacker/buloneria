﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Proveedor
    {
        #region Properties

        private string CodProveedorPropertyLabel = " Cod. Proveedor";
        public int CodProveedor { get; set; }

        private String RazonSocialPropertyLabel = " Razon Social";
        public String RazonSocial { get; set; }

        private String NombreFantasiaPropertyLabel = " Nombre Fantasia";
        public String NombreFantasia { get; set; }

        private String CuitPropertyLabel = " Cuit";
        public Int64 Cuit { get; set; }

        private String CodPostalPropertyLabel = " CodPostal";
        public int CodPostal { get; set; }

        private String DireccionPropertyLabel = " Direccion";
        public String Direccion { get; set; }

        private String CiudadPropertyLabel = " Ciudad";
        public Ciudad Ciudad { get; set; }

        private String TelefonoPropertyLabel = " Telefono";
        public String Telefono { get; set; }

        private String CelularPropertyLabel = " Celular";
        public String Celular { get; set; }

        private String EmailPropertyLabel = " Email";
        public String Email { get; set; }

        private String FaxPropertyLabel = " Fax";
        public String Fax { get; set; }

        private String TipoIvaPropertyLabel = "Tipo Iva";
        public TipoIva TipoIva { get; set; }



        #endregion

        #region CustomProperties

        public string NombreUsuario { get; set; }

        public String ModoAuditoria { get; set; }

        #endregion

        public override string ToString()
        {
            return RazonSocial;
        }

        public void Validate()
        {

            if (string.IsNullOrEmpty(this.RazonSocial))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, RazonSocialPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.NombreFantasia) )
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, NombreFantasiaPropertyLabel));
            }

            if (this.Cuit <= 0)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CuitPropertyLabel));
            }

            if (this.CodPostal <= 0)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CodPostalPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.Direccion) )
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, DireccionPropertyLabel));
            }

            if (this.Ciudad == null)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CiudadPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.Telefono))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, TelefonoPropertyLabel));
            }

            if (this.TipoIva == null)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, TipoIvaPropertyLabel));
            } 

        }
    }
}
