﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema.Reportes
{
     [Serializable]
    public class ArticulosPedidosXClientes
    {
        private string _razonSocial;

        public string RazonSocial
        {
            get { return _razonSocial; }
            set { _razonSocial = value; }
        }
        private int _codCli;

        public int CodCli
        {
            get { return _codCli; }
            set { _codCli = value; }
        }
        private string _articuloDesc;

        public string ArticuloDesc
        {
            get { return _articuloDesc; }
            set { _articuloDesc = value; }
        }
        private int _codArticulo;

        public int CodArticulo
        {
            get { return _codArticulo; }
            set { _codArticulo = value; }
        }
        private int _cantidad;

        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }
        private int _nroNotaPedido;

        public int NroNotaPedido
        {
            get { return _nroNotaPedido; }
            set { _nroNotaPedido = value; }
        }
        private DateTime _fechaEmision;

        public DateTime FechaEmision
        {
            get { return _fechaEmision; }
            set { _fechaEmision = value; }
        }
        private string _nombreApellido;

        public string NombreApellido
        {
            get { return _nombreApellido; }
            set { _nombreApellido = value; }
        }
        
    }
}
