﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Cuidador
    {
        #region Properties

        private readonly Stack<Memento> coleccionMemento;

        #endregion

        #region Constructor
        public Cuidador()
        {
            coleccionMemento = new Stack<Memento>();
        }

        #endregion

        public void AgregarMemento(Memento memento)
        {
            coleccionMemento.Push(memento);
        }

        public Memento RecuperarEstado()
        {
            if (coleccionMemento.Count != 0)
            {
                coleccionMemento.Pop(); 

                return coleccionMemento.Peek();
            }
            else
                return null;
        }

        /// <summary>
        /// Para habilitar/deshabilitar el Deshacer
        /// </summary>
        public bool TieneMasDeUnMemento
        {
            get { return this.coleccionMemento.Count > 1; }
        }
    }
}
