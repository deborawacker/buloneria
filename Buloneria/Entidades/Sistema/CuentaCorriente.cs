﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    public class CuentaCorriente
    {
        public CuentaCorriente()
        {
            _MovimientosCtaCte = new List<MovimientoCtaCte>();
        }

        private Cliente _Cliente;

        public Cliente Cliente
        {
            get { return _Cliente; }
            set { _Cliente = value; }
        }
        private List<MovimientoCtaCte> _MovimientosCtaCte;

        public List<MovimientoCtaCte> MovimientosCtaCte
        {
            get { return _MovimientosCtaCte; }
            set { _MovimientosCtaCte = value; }
        }
        private int _saldo;

        public int Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }

        public void AgregarMovimiento(MovimientoCtaCte oMovimiento)
        {            
            _MovimientosCtaCte.Add(oMovimiento);
        }

        private int _nroCtaCte;

        public int NroCtaCte
        {
            get { return _nroCtaCte; }
            set { _nroCtaCte = value; }
        }
    }
}
