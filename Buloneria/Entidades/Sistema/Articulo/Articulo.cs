﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Articulo
    {
        #region Constructor
        public Articulo()
        {
            Proveedores = new List<Proveedor>();
        }
        #endregion

        #region Properties

        //Este es el Id Autinumerico de la BD
        public int IdArticulo { get; set; }

        //Este codigo articulo lo ingresa el usuario. Es un string
        public string CodArticulo { get; set; }

        public string Descripcion { get; set; }

        public int PrecioVenta { get; set; }

        public List<Proveedor> Proveedores { get; private set; }

        public GrupoArticulo Grupo { get; set; }

        public int CantMinima { get; set; }

        public int CantActual { get; set; }

        public Alicuota Alicuota { get; set; }

        #endregion

        #region Methods

        public void AgregarProveedor(Proveedor oProveedor)
        {
            Proveedores.Add(oProveedor);
        }

        public void EliminarProveedor(Proveedor oProveedor)
        {
            Proveedores.Remove(oProveedor);
        }

        public void AgregarProveedores(List<Proveedor> listaProveedores)
        {
            this.Proveedores = listaProveedores;
        }

        public override string ToString()
        {
            return Descripcion;
        }

        #endregion

        public void Validate()
        {
            if (this.Proveedores.Count == 0)
                throw new Exception("Debe ingresar al menos un proveedor");
        }
    }
}
