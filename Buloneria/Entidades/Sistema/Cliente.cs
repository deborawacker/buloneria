﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class Cliente
    {
        #region Properties

        public int CodCliente { get; set; }

        private string CuitPropertyLabel = "Cuit";
        public long Cuit { get; set; }

        private string RazonSocialPropertyLabel = "Razon Social";
        public string RazonSocial { get; set; }

        private string NombreFantasiaPropertyLabel = "Nombre Fantasia";
        public string NombreFantasia { get; set; }

        private string DireccionPropertyLabel = "Direccion";
        public string Direccion { get; set; }

        private string CodPostalPropertyLabel = "Codigo postal";
        public int CodPostal { get; set; }

        private string TelefonoPropertyLabel = "Telefono";
        public string Telefono { get; set; }

        public string Fax { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }

        private string TipoIvaPropertyLabel = "Tipo IVA";
        public TipoIva TipoIva { get; set; }

        private string CiudadPropertyLabel = "Ciudad";
        public Ciudad Ciudad { get; set; }

        #endregion

        #region Methods

        public void Validate()
        {
            if (string.IsNullOrEmpty(this.RazonSocial))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, RazonSocialPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.NombreFantasia))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, NombreFantasiaPropertyLabel));
            }

            if (this.Cuit <= 0)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CuitPropertyLabel));
            }

            if (this.CodPostal <= 0)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CodPostalPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.Direccion))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, DireccionPropertyLabel));
            }

            if (this.Ciudad == null)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, CiudadPropertyLabel));
            }

            if (string.IsNullOrEmpty(this.Telefono))
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, TelefonoPropertyLabel));
            }

            if (this.TipoIva == null)
            {
                throw new Exception(String.Format(Messages.ElCampoXEsDeIngresoObligatorio, TipoIvaPropertyLabel));
            } 
        }
        #endregion
    }
}
