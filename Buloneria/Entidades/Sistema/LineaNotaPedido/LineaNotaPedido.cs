﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.Sistema
{
    [Serializable]
    public class LineaNotaPedido
    {
        #region Properties

        public int NroLineaNotaPedido { get; set; } 
        
        public Articulo Articulo { get; set; } 
        
        public int CantPedida { get; set; }
        
        public int PrecioUnitario { get; set; } 
        
        public int CantEntregada { get; set; } 

        #endregion

        #region Custom Properties
         
        public int CantFaltanteEntrega
        {
            get
            {
                return CantPedida - CantEntregada;
            }
        }

        private decimal _subtotalIva;
        public decimal SubtotalIva
        {
            get { return _subtotalIva; }

        }
        private decimal _Subtotal;

        public decimal Subtotal //en la linea de la orden de compra calculo el subtotal de la linea
        {
            get
            {
                calcularSubtotal();
                return _Subtotal;
            }

        }

        public Boolean TieneFaltante 
        { 
            get
            {
                return (CantFaltanteEntrega > 0);

            }
        }
        #endregion

        #region Methods

        public void calcularSubtotal()
        {
            _Subtotal = CantPedida * PrecioUnitario;
            //TODO: ver Articulo.Precio-venta viene 0
            _subtotalIva = CantPedida * (Articulo.PrecioVenta * Articulo.Alicuota.Valor) / 100;
        } 

        #endregion 
    }
}
