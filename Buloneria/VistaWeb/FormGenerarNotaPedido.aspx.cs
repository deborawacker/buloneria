﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades.Seguridad;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormGenerarNotaPedido : System.Web.UI.Page
    {
        #region SessionVariables

        private const String SessionVariableName = "SesionGenerarNotaPedido";
        private SesionGenerarNotaPedido _SessionVariable;
        public SesionGenerarNotaPedido SessionVariable
        {
            get
            {
                _SessionVariable = (SesionGenerarNotaPedido)Session[SessionVariableName];
                return _SessionVariable;
            }
            set
            {
                _SessionVariable = value;
                Session[SessionVariableName] = _SessionVariable;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();


            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            Usuario usuario = (Usuario)Session["user"];

            if (usuario == null)
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";

                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }
        }

        private void InicializarPaneles()
        {
            this.PanelSeleccionCliente.Visible = true;
            this.PanelIngresoNotaPedido.Visible = false;
        }

        private void Inicializar()
        {
            this.InicializarFechaEmision();

            this.InicializarPaneles();

            this.InicializarValidaciones();

            this.InicializarGrillaClientes();

            this.InicializarComboFormaPago();

            this.InicializarSesion();

            Memento memento = this.SessionVariable.NotaPedido.CrearMemento();

            this.SessionVariable.Cuidador.AgregarMemento(memento);
        }

        private void InicializarValidaciones()
        {
            this.txtCantidadArticulo.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
        }

        private void InicializarSesion()
        {
            this.SessionVariable = new SesionGenerarNotaPedido();
        }

        private void InicializarComboFormaPago()
        {
            this.ddlFormaPago.DataSource = Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().RecuperarFormasPago();
            ddlFormaPago.DataTextField = "TipoFormaPago";
            ddlFormaPago.DataBind();
        }

        private void InicializarGrillaClientes()
        {
            this.GrillaClientes.DataSource = Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().RecuperarClientes();
            this.GrillaClientes.DataBind();
        }

        private void InicializarFechaEmision()
        {
            this.lblFechaEmision.Text = DateTime.Now.Date.ToShortDateString();
        }

        protected void GrillaClientes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SeleccionarCliente")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaClientes.Rows[index];
                TableCell tbcCodCliente = selectedRow.Cells[1];
                string strCodCliente = tbcCodCliente.Text;
                int codProveedorBuscado = Convert.ToInt32(strCodCliente);

                Cliente clienteSeleccionado = this.BuscarCliente(codProveedorBuscado);

                this.SessionVariable.NotaPedido.Cliente = clienteSeleccionado;

                this.HabilitarIngresoNotaPedido(clienteSeleccionado);
            }
        }

        private void HabilitarIngresoNotaPedido(Cliente clienteSeleccionado)
        {
            this.PanelSeleccionCliente.Visible = false;
            this.PanelIngresoNotaPedido.Visible = true;

            this.MostrarDatosCliente(clienteSeleccionado);

            this.InicializarGrillaArticulosDisponibles();
        }

        private void InicializarGrillaArticulosDisponibles()
        {
            this.GrillaArticulosDisponibles.DataSource = Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().RecuperarArticulos();
            this.GrillaArticulosDisponibles.DataBind();
        }

        private Cliente BuscarCliente(int codClienteBuscado)
        {
            return Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().BuscarCliente(codClienteBuscado);
        }

        private void MostrarDatosCliente(Cliente clienteSeleccionado)
        {
            this.lblNombreCliente.Text = clienteSeleccionado.NombreFantasia;
            this.lblCUITCliente.Text = clienteSeleccionado.Cuit.ToString();
            this.lblDireccion.Text = clienteSeleccionado.Direccion;
            this.lblTelefono.Text = clienteSeleccionado.Telefono;
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        private Articulo BuscarArticulo(int idArticuloBuscado)
        {
            return Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().BuscarArticulo(idArticuloBuscado);
        }

        private void HabilitarIngresoCantidadArticulo(Articulo articuloSeleccionado)
        {
            this.PanelIngresoCantidadArticulo.Visible = true;

            this.MostrarDatosArticulo(articuloSeleccionado);
        }

        private void MostrarDatosArticulo(Articulo articuloSeleccionado)
        {
            this.lblDescripcionArticulo.Text = articuloSeleccionado.Descripcion;
        }

        private bool ValidarIngresoCantidad()
        {
            string mjeError = String.Empty;
            string strIngCant = txtCantidadArticulo.Text;
            int cantIngresada = 0;
            bool valido = int.TryParse(strIngCant, out cantIngresada);

            if (!(cantIngresada > 0))
                valido = false;

            if (!valido)
                mjeError += Textos.DebeIngresarUnValorMayorACero;

            this.lblErrorIngresoCantidadArticulo.Text = mjeError;
            return valido;
        }

        private bool ArticuloYaFueSeleccionado(Articulo articulo)
        {
            return this.SessionVariable.NotaPedido.LineasNotaPedido.Any(loc => loc.Articulo.IdArticulo == articulo.IdArticulo);
        }

        private void OcultarIngresoCantidad()
        {
            this.PanelIngresoCantidadArticulo.Visible = false;
        }

        private void AgregarCantidadArticuloSeleccionado(Articulo articulo, int cantidadIngresada)
        {
            if (!this.ArticuloYaFueSeleccionado(articulo))
            {
                LineaNotaPedido lineaNotaPedido = new LineaNotaPedido
                {
                    Articulo = articulo,
                    CantPedida = cantidadIngresada,
                    PrecioUnitario = articulo.PrecioVenta
                };

                this.SessionVariable.NotaPedido.AgregarLineaNotaPedido(lineaNotaPedido);
            }
            else
            {
                //Si el articulo ya fue agregado en una linea de nota de pedido, no agrega otra linea a la lista sino que agrega mas cantidad
                LineaNotaPedido lineaNotaPedido = this.SessionVariable.NotaPedido.LineasNotaPedido.Where(l => l.Articulo.IdArticulo == articulo.IdArticulo).ToList().FirstOrDefault();

                lineaNotaPedido.CantPedida += cantidadIngresada;
            }

            this.RefrescarGrillaArticulosAgregadosNotaPedido();
        }

        private void RefrescarGrillaArticulosAgregadosNotaPedido()
        {
            this.GrillaArticulosAgregadosNotaPedido.DataSource = this.SessionVariable.NotaPedido.LineasNotaPedido;
            this.GrillaArticulosAgregadosNotaPedido.DataBind();

            this.VerificarHabilitacionBotonAceptarCancelar();
        }

        private void VerificarHabilitacionBotonAceptarCancelar()
        {
            this.PanelAceptarCancelar.Visible = false;
            if (this.SessionVariable.NotaPedido.LineasNotaPedido.Count > 0)
            {
                this.PanelAceptarCancelar.Visible = true;
            }
        }

        private void GenerarNuevaNotaPedido()
        {

            this.SessionVariable.NotaPedido.Usuario = (Usuario)Session["user"]; 

            FormaPago formaPagoOC = new FormaPago();
            formaPagoOC.TipoFormaPago = this.ddlFormaPago.SelectedValue;
            this.SessionVariable.NotaPedido.FormaPago = formaPagoOC;

            this.SessionVariable.NotaPedido.FechaEmision = DateTime.Now;

            if (Controladora.Sistema.ControladoraCUGenerarNotaPedido.ObtenerInstancia().AgregarNotaPedido(this.SessionVariable.NotaPedido))
            {
                //Si esta toodo bien redirecciona y muestra mensaje OK
                String mje = "La Nota de Pedido nro " + this.SessionVariable.NotaPedido.NroNotaPedido + " se generó exitosamente";

                //Borra los datos de la OC realizada para no superponer si se desea hacer otra OC.
                //Session["proveedor"] = null;

                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);


            }
        }

        #region Events

        protected void btnVolverAMenu_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void GrillaArticulosDisponibles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == Textos.SeleccionarCommand)
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaArticulosDisponibles.Rows[index];
                TableCell tbcIdArticulo = selectedRow.Cells[1];
                string strIdArticulo = tbcIdArticulo.Text;
                int idArticuloBuscado = Convert.ToInt32(strIdArticulo);

                Articulo articuloSeleccionado = this.BuscarArticulo(idArticuloBuscado);

                this.SessionVariable.ArticuloSeleccionado = articuloSeleccionado;

                this.HabilitarIngresoCantidadArticulo(articuloSeleccionado);
            }
        }

        protected void btnIngresarCantidad_Click(object sender, EventArgs e)
        {
            this.IngresarCantidad();

        }

        private void IngresarCantidad()
        {
            if (this.ValidarIngresoCantidad())
            {
                this.OcultarIngresoCantidad();

                int cantidadIngresada = int.Parse(txtCantidadArticulo.Text);

                this.VerificarHabilitacionBotonDeshacer();

                this.AgregarCantidadArticuloSeleccionado(this.SessionVariable.ArticuloSeleccionado, cantidadIngresada);

                this.ActualizarTotales();

                Memento memento = this.SessionVariable.NotaPedido.CrearMemento();

                this.SessionVariable.Cuidador.AgregarMemento(memento);

            }
        }

       
       

        private void ActualizarTotales()
        {
            this.lblSubtotal.Text = this.SessionVariable.NotaPedido.Subtotal.ToString();
            this.lblIVA.Text = this.SessionVariable.NotaPedido.SubtotalIVA.ToString();
            this.lblTotal.Text = this.SessionVariable.NotaPedido.Total.ToString();
        } 

        protected void GrillaArticulosAgregadosNotaPedido_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == Textos.EliminarCommand)
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaArticulosAgregadosNotaPedido.Rows[index];
                TableCell tbcCodArticulo = selectedRow.Cells[1];
                string strCodArticulo = tbcCodArticulo.Text;

                this.EliminarLineaNotaPedidoSeleccionada(strCodArticulo);

            }
        }

        private void EliminarLineaNotaPedidoSeleccionada(string strCodArticulo)
        {
             
            this.SessionVariable.Cuidador.AgregarMemento(this.SessionVariable.NotaPedido.CrearMemento());

            LineaNotaPedido lnpAEliminar = this.SessionVariable.NotaPedido.LineasNotaPedido.Where(loc => loc.Articulo.CodArticulo == strCodArticulo).ToList().FirstOrDefault();

            this.SessionVariable.NotaPedido.LineasNotaPedido.Remove(lnpAEliminar);

            this.RefrescarGrillaArticulosAgregadosNotaPedido();
            
            this.VerificarHabilitacionBotonDeshacer();

            this.ActualizarTotales();
           
        }


        private void Deshacer()
        {

             
            if( this.SessionVariable.NotaPedido.RestablecerMemento(this.SessionVariable.Cuidador.RecuperarEstado()))
            {
                this.RefrescarGrillaArticulosAgregadosNotaPedido();

                this.VerificarHabilitacionBotonDeshacer();

                this.ActualizarTotales();
            }
        }
       
        private void VerificarHabilitacionBotonDeshacer()
        {
             
            this.btnDeshacer.Visible = this.SessionVariable.Cuidador.TieneMasDeUnMemento;
        }

        #region Events
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            this.GenerarNuevaNotaPedido();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnDeshacer_Click(object sender, EventArgs e)
        {
            this.Deshacer();
        }
        #endregion
         
        #endregion
    }
}