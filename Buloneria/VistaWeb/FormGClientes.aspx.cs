﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGClientes : System.Web.UI.Page
    {
        List<Entidades.Sistema.Cliente> _listaClientes;

        protected void Page_Init(object sender, EventArgs e)
        {
            _listaClientes = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarClientes();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dgvClientes.DataSource = _listaClientes;
                dgvClientes.DataBind();
            }
        }

        protected void btnSeleccionarCliente_Click(object sender, EventArgs e)
        {
            int fila = dgvClientes.SelectedIndex;
            //Si no se selecciono un cliente
            if (fila < 0)
            {
                string mje = "Debe seleccionar un cliente.";
                lblClienteNoSel.Text = mje;
            }
            else
            {
                Entidades.Sistema.Cliente cliente = _listaClientes.ElementAt(fila);
                //Guarda el objeto en la sesion
                Session.Add("cliente", cliente);
                //Vuelve
                Page.Response.Redirect("FormGenerarNP.aspx");
            }
        }
    }
}