﻿<%@ Page Title="Ingresar Mercaderia" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormIMercaderia.aspx.cs" Inherits="VistaWeb.FormIMercaderia" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
     <script>
        function soloNumeros(e) {
            var key = window.Event ? e.which : e.keyCode
            return (key >= 48 && key <= 57)
        }
    </script>
    <asp:Label ID="lblMensajeInicio" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    <asp:Panel ID="pnlSeleccionarNroOC" runat="server">
        
&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Fecha de Recepción:"></asp:Label>
    <asp:TextBox ID="txtFechaRecepcion" runat="server" Enabled="False"></asp:TextBox><br /><br />
    <asp:Label ID="Label11" runat="server" Text="Seleccione Nro de Orden Compra: "></asp:Label>
    <asp:DropDownList ID="dplNrosOC" runat="server">
    </asp:DropDownList>
        &nbsp;&nbsp;<asp:Button ID="btnSeleccionarNroOC" runat="server" Text="Seleccionar" OnClick="btnSeleccionarNroOC_Click" />
    <br />
    </asp:Panel>
    

    <asp:Panel ID="pnlDatosOC" runat="server">
        <asp:Label ID="Label2" runat="server" Text="DATOS DE LA ORDEN DE COMPRA" ForeColor="#FF6600"></asp:Label><br />
        <asp:Label ID="Label4" runat="server" Text="Número Orden Compra:"></asp:Label>
        &nbsp;<asp:Label ID="lblNroOrdenCompra" runat="server" Text="Nrooc"></asp:Label>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Proveedor:"></asp:Label>
        &nbsp;<asp:Label ID="lblProveedor" runat="server" Text="proveedor"></asp:Label>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Fecha Orden Compra: "></asp:Label>
        <asp:Label ID="lblFechaOrdenCompra" runat="server" Text="fechaOrdenCompra"></asp:Label>
        <br />
        <asp:Label ID="Label7" runat="server" Text="Estado:"></asp:Label>
        &nbsp;<asp:Label ID="lblEstadoOC" runat="server" Text="estadoOC"></asp:Label>
        <br />
    <asp:Label ID="Label8" runat="server" Text="Artículos de la Orden de Compra Seleccionada:"></asp:Label>
    <br />
    <asp:GridView ID="dgvArticulosOC" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="735px" AutoGenerateColumns="False" OnRowCommand="dgvArticulosOC_RowCommand">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Seleccionar" Text="Seleccionar" />
            <asp:BoundField DataField="NroLineaOC" HeaderText="NroLineaOC" ReadOnly="True" />
            <asp:BoundField DataField="CantPedir" HeaderText="Cantidad" />
            <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
            <asp:BoundField DataField="CantFaltante" HeaderText="Faltante" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    </asp:Panel>
     
    
    <br />
    <asp:Panel ID="pnlAgregarArticuloRecibido" runat="server">
        <asp:Label ID="Label13" runat="server" Text="Nombre de Articulo seleccionado: "></asp:Label> 
        <asp:Label ID="lblNombreArticulo" runat="server" Text=""></asp:Label>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Cantidad pedida: "></asp:Label> 
        <asp:Label ID="lblCantPedida" runat="server" Text=""></asp:Label><br />
        <asp:Label ID="Label10" runat="server" Text="Faltante al dia de la fecha: "></asp:Label> 
        <asp:Label ID="lblCantFaltanteActual" runat="server"></asp:Label><br />
        <asp:Label ID="Label12" runat="server" Text="Ingrese Cantidad Recibida: "></asp:Label>
        <asp:TextBox ID="txtCantidadRecibida" runat="server" Width="69px" onKeyPress="return soloNumeros(event)"></asp:TextBox><br />
        <asp:Button ID="btnIngresarCantidadRecibida" runat="server" Text="Ingresar" OnClick="btnIngresarCantidadRecibida_Click" />
        <asp:Button ID="btnCancelarIngreso" runat="server" Text="Cancelar" OnClick="btnCancelarIngreso_Click" />
        <br />
        <asp:Label ID="lblErrorCantRecibida" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
    </asp:Panel>
    <br />
    <br />
    <br />
     
    
    <br />
    <asp:Label ID="Label9" runat="server" Text="Artículos Agregados al Ingreso de Mercadería:"></asp:Label>
    <br />
    <asp:GridView ID="dgvArticulosRecibidos" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="735px" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField HeaderText="Cod Articulo" DataField="Articulo.CodArticulo" />
            <asp:BoundField HeaderText="Descripcion" DataField="Articulo.Descripcion" />
            <asp:BoundField HeaderText="Cant Recibida" DataField="CantIngresada" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <br />
    <br />
    <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" OnClick="btnAceptar_Click" style="height: 26px" Enabled="False" />
    <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" OnClick="btnCancelar_Click" />
</asp:Content>
