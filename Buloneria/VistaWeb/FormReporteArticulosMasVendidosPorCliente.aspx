﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormReporteArticulosMasVendidosPorCliente.aspx.cs" Inherits="VistaWeb.FormReporteArticulosMasVendidosPorCliente" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte Articulos mas Vendidos por Cliente</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label><br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="861px" Height="602px">
            <LocalReport ReportPath="Reportes\ReporteArticulosMasVendidosPorCliente.rdlc"> 
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dsArticulosMasVendidosPorCliente" />
                </DataSources>
            </LocalReport>

        </rsweb:ReportViewer> 
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Sistema %>" SelectCommand="sp_RepArticulosMasVendidosPorCliente" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="vCuitCliente" SessionField="cuitCliente" Type="Int64" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
