﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormBackUpRestore.aspx.cs" Inherits="VistaWeb.FormBackUpRestore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br/>
        <br/>
        <asp:Panel ID="Panel1" runat="server">
            <asp:Button ID="lnkBackupAuditoria" runat="server" Text="Crear BackUp Auditoria" CssClass="btn-warning" OnClick="lnkBackupAuditoria_Click"  Width="20%"/><br /><br />
            <asp:Button ID="lnkBackupBuloneria" runat="server" Text="Crear BackUp Buloneria" CssClass="btn-warning" OnClick="lnkBackupBuloneria_Click"  Width="20%"/><br /><br />
            <asp:Button ID="lnkBackupSeguridad" runat="server" Text="Crear BackUp Seguridad" CssClass="btn-warning" OnClick="lnkBackupSeguridad_Click" Width="20%"/><br /><br />
            <br /><br /><br />
            <asp:Button ID="lnkRestoreAuditoria" runat="server" Text="Restaurar Auditoria" CssClass="btn-warning" OnClick="lnkRestoreAuditoria_Click"  Width="20%"/><br /><br />
            <asp:Button ID="lnkRestoreBuloneria" runat="server" Text="Restaurar Buloneria" CssClass="btn-warning" OnClick="lnkRestoreBuloneria_Click"  Width="20%"/><br /><br />
            <asp:Button ID="lnkRestoreSeguridad" runat="server" Text="Restaurar Seguridad" CssClass="btn-warning" OnClick="lnkRestoreSeguridad_Click"  Width="20%"/><br /><br />

            <br />
            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
            <br />
            <asp:Button ID="btnVolverAtras" CssClass="btn-primary" runat="server" Text="Volver atras" OnClick="btnVolverAtras_Click" />
        </asp:Panel>
        <br /><br />
    </div>
</asp:Content>
