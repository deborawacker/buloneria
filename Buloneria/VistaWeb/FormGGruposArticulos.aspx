﻿<%@ Page Title="Gestionar Grupos Articulos" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGGruposArticulos.aspx.cs" Inherits="VistaWeb.FormWeb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        <asp:Button ID="btnAgregar" runat="server" Text="AGREGAR" OnClick="btnAgregar_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="MODIFICAR" OnClick="btnModificar_Click" />
        <asp:Button ID="btnConsultar" runat="server" Text="CONSULTAR" OnClick="btnConsultar_Click" />
        <asp:Button ID="btnEliminar" runat="server" Text="ELIMINAR" OnClick="btnEliminar_Click" />
        <asp:Label ID="Label1" runat="server" style="position: absolute; z-index: 1; left: 436px; top: 24px; height: 19px" Text="Buscar:"></asp:Label>
    </p>
    <br />
    <asp:Button ID="btnSeleccionarGrupoArticulo" runat="server" Text="Seleccionar Grupo" OnClick="btnSeleccionarGrupoArticulo_Click" />
    <br />
     <asp:Label ID="lblGrupoArticuloNoSeleccionado" runat="server" Font-Size="Medium" ForeColor="#CC0000"></asp:Label>
     <br />
    <asp:GridView ID="dgvGruposArticulos" style="position:relative; margin:auto" Width ="500px" runat="server" CellPadding="4" ShowHeaderWhenEmpty="True" ForeColor="#333333" GridLines="None" AutoGenerateSelectButton="True" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="CodGrupoArticulo" HeaderText="Codigo" ReadOnly="True" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ReadOnly="True" >
            </asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:TextBox ID="TextBox1" runat="server" style="position: relative; top: -339px; left: 483px; width: 492px"></asp:TextBox>
</asp:Content>
