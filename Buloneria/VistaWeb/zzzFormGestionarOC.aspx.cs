﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGestionarOC : System.Web.UI.Page
    {
        //private List<Entidades.Sistema.OrdenCompra> _ordenesCompra; 
        private Entidades.Seguridad.Usuario _usuario;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dgvOrdenesCompra.DataSource = "";
                dgvOrdenesCompra.DataBind();

                CargarGrillaOrdenesCompra(); 
            }
            else
            {
                lblMensajeError.Text = String.Empty;
                lblMensajeOK.Text = String.Empty; 
            }
        }

        private void CargarGrillaOrdenesCompra()
        {
            dgvOrdenesCompra.DataSource = null;
            //en la grilla voy a cargar los articulos
            dgvOrdenesCompra.DataSource = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().RecuperarOrdenesCompras(); 
            //le doy la orden
            dgvOrdenesCompra.DataBind(); 
        }
       
         
        protected void btnSalir_Click(object sender, EventArgs e)
        {
            //Vuelve
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void dgvOrdenesCompra_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            Entidades.Sistema.OrdenCompra ordenCompraSeleccionada = buscarOrdenCompraSeleccionada(e);
             
            switch (e.CommandName)
            {
                case "Consultar":
                    pnlGrillaOrdenesCompra.Visible = false;
                    pnlConsultar.Visible = true;
                     
                    cargarGrillaLineasOCConsultada(ordenCompraSeleccionada);
                    lblDatosOCConsultada.Text = string.Format("Nro de Orden: {0} <br/> Proveedor: {1} <br/>  Fecha de emision: {2} <br/> Fecha de plazo: {3} <br/> Forma de pago utilizada: {4} <br/> Estado: {5} <br/>  SubTotal: {6} <br/> Monto IVA: {7} <br/> Total: {8} <br/>",ordenCompraSeleccionada.NroOrdenCompra,ordenCompraSeleccionada.Proveedor.RazonSocial,ordenCompraSeleccionada.FechaEmision,ordenCompraSeleccionada.FechaPlazo,ordenCompraSeleccionada.FormaPago,ordenCompraSeleccionada.Estado,ordenCompraSeleccionada.Subtotal,ordenCompraSeleccionada.MontoIva,ordenCompraSeleccionada.Total);
                    break; 
                case "Anular":
                    //if (ordenCompraSeleccionada.Estado == Entidades.Sistema.EstadoOrdenCompra.FinConFalt || ordenCompraSeleccionada.Estado == Entidades.Sistema.EstadoOrdenCompra.Finalizado)
                    //{
                    //    string mje = "No se pueden anular Ordenes de Compra finalizadas con faltante ni finalizadas.";
                    //    lblMensajeError.Text = mje;
                    //}
                    //else
                    //{
                    //    if (Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().FinalizarConFaltante(ordenCompraSeleccionada, _usuario))
                    //    {
                    //        string mje = "La orden de compra nro: " + ordenCompraSeleccionada.NroOrdenCompra + " ha pasado a estado Finalizado con faltante";
                    //        lblMensajeError.Text = "";
                    //        lblMensajeOK.Text = mje;
                    //        CargarGrillaOrdenesCompra();
                    //    }

                    //}
                    break;
                default:
                    break;
            }


        }

       

        private void cargarGrillaLineasOCConsultada(Entidades.Sistema.OrdenCompra ordenCompraSeleccionada)
        {
            dgvArticulosOCConsultada.DataSource = ordenCompraSeleccionada.LineasOrdenCompra;
            dgvArticulosOCConsultada.DataBind();
        }

        protected void btnAceptarConsulta_Click(object sender, EventArgs e)
        {
            pnlConsultar.Visible = false;
            pnlGrillaOrdenesCompra.Visible = true;
        }
        private Entidades.Sistema.OrdenCompra buscarOrdenCompraSeleccionada(GridViewCommandEventArgs e)
        {
            Entidades.Sistema.OrdenCompra ordenCompraSeleccionada = null;
            int index;
            GridViewRow selectedRow;
            TableCell tbcCodOC;
            string strCodOC;
            int nroCodOCSeleccionado;
            index = Convert.ToInt32(e.CommandArgument);
            selectedRow = dgvOrdenesCompra.Rows[index];
            tbcCodOC = selectedRow.Cells[2];
            strCodOC = tbcCodOC.Text;
            nroCodOCSeleccionado = Convert.ToInt32(strCodOC);
            ordenCompraSeleccionada = Controladora.Sistema.ControladoraCUGestionarOC.ObtenerInstancia().BuscarOrdenCompra(nroCodOCSeleccionado);
            return ordenCompraSeleccionada;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario == null) 
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }
        } 
    }
}