﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormGestionarProveedores : System.Web.UI.Page
    {
        #region SessionVariable

        private const String SessionVariableName = "SesionGestionarProveedores";
        private SesionGestionarProveedores _SessionVariable;
        public SesionGestionarProveedores SessionVariable
        {
            get
            {
                _SessionVariable = (SesionGestionarProveedores)Session[SessionVariableName];
                return _SessionVariable;
            }
            set
            {
                _SessionVariable = value;
                Session[SessionVariableName] = _SessionVariable;
            }
        }

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.UserSessionVariable == null)
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + Textos.DebeLoguearseEnElSistema);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();
            }
        }

        private void Inicializar()
        {
            this.RellenarGrilla();

            this.InicializarCombos();

            this.InicializarSesion();

            this.VerificarPermisos();

            this.InicializarValidacionesTextBoxs();
        }

        private void VerificarPermisos()
        {
            this.btnNuevo.Enabled = this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionAgregarProveedor));
        }

        private void InicializarValidacionesTextBoxs()
        {
            this.txtCodPostal.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtCuit.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtTelefono.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtFax.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtCelular.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
        }

        private void InicializarSesion()
        {
            this.SessionVariable = new SesionGestionarProveedores();
        }

        private void InicializarCombos()
        {
            var provincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();

            this.CargarControl(this.ddlProvincia, provincias, "Nombre");

            string nombreProvinciaSeleccionada = this.ddlProvincia.SelectedValue.ToString();

            Provincia provinciaSelected = provincias.Find(p => p.Nombre == nombreProvinciaSeleccionada);

            this.CargarControl(this.ddlCiudad, provinciaSelected.Ciudades, "Nombre");

            this.CargarControl(this.ddlTipoIva, Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva());
        }

        private void CargarControl(ListControl control, IEnumerable<Object> list)
        {
            control.DataSource = list;

            control.DataBind();
        }

        private void CargarControl(ListControl control, IEnumerable<Object> list, string displayMember)
        {
            control.DataSource = list;

            control.DataTextField = displayMember;

            control.DataBind();
        }

        private void RellenarGrilla()
        {
            this.GrillaProveedores.DataSource =
                Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();
            this.GrillaProveedores.DataBind();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        private void Guardar()
        {
            try
            {
                this.GuardarProveedor(this.SessionVariable.Proveedor);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleProveedor.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, e.Message, TypeNotif.Error);
            }
        }

        private void GuardarProveedor(Proveedor proveedor)
        {
            ValidarCamposObligatorios();

            if (SessionVariable.Modo == Modo.Alta)
                proveedor = new Proveedor();

            #region DatosPara Auditoria

            Entidades.Seguridad.Usuario user = (Entidades.Seguridad.Usuario)Session["user"];

            proveedor.NombreUsuario = user.NombreUsuario;

            proveedor.ModoAuditoria = SessionVariable.Modo.ToString();
            #endregion

            proveedor.RazonSocial = this.txtRazonSocial.Text;

            proveedor.NombreFantasia = this.txtNombreFantasia.Text;

            proveedor.Cuit = Convert.ToInt64(this.txtCuit.Text);

            proveedor.CodPostal = Convert.ToInt32(this.txtCodPostal.Text);

            proveedor.Direccion = this.txtDireccion.Text;

            String ciudadDescripcion = this.ddlCiudad.SelectedValue;

            proveedor.Ciudad =
                Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarCiudadPorDescripcion(ciudadDescripcion);

            proveedor.Telefono = this.txtTelefono.Text;

            proveedor.Celular = this.txtCelular.Text;

            proveedor.Email = this.txtEmail.Text;

            proveedor.Fax = this.txtFax.Text;

            string tipoIvaDescripcion = this.ddlTipoIva.SelectedValue;

            proveedor.TipoIva =
                Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarTipoIvaPorDescripcion(tipoIvaDescripcion);

            Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().GuardarProveedor(proveedor);

        }

        private void ValidarCamposObligatorios()
        {
            if (string.IsNullOrEmpty((this.txtCuit.Text.Trim())))
                throw new Exception(String.Format(Textos.ElCampoXEsDeIngresoObligatorio, "Cuit"));

            if (string.IsNullOrEmpty((this.txtCodPostal.Text.Trim())))
                throw new Exception(String.Format(Textos.ElCampoXEsDeIngresoObligatorio, "Codigo Postal"));
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Guardar();
        }

        private void MostrarPanelDetalle(Proveedor proveedor, Modo modo)
        {
            this.SessionVariable.Modo = modo;

            proveedor.ModoAuditoria = SessionVariable.Modo.ToString();

            Entidades.Seguridad.Usuario user = (Entidades.Seguridad.Usuario)Session["user"];

            proveedor.NombreUsuario = user.NombreUsuario;

            this.SessionVariable.Proveedor = proveedor;

            this.PanelDetalleProveedor.Enabled = (modo != Modo.Consulta);

            if (modo == Modo.Consulta || modo == Modo.Modificacion)
            {
                this.SetearControles(proveedor);
            }

            this.PanelDetalleProveedor.Visible = true;
        }

        private void SetearControles(Proveedor proveedor)
        {
            this.txtCodigo.Text = proveedor.CodProveedor.ToString();
            this.txtRazonSocial.Text = proveedor.RazonSocial;
            this.txtNombreFantasia.Text = proveedor.NombreFantasia;
            this.txtCuit.Text = proveedor.Cuit.ToString();
            this.txtCodPostal.Text = proveedor.CodPostal.ToString();
            this.txtDireccion.Text = proveedor.Direccion;
            this.ddlProvincia.ClearSelection();
            this.ddlProvincia.Items.FindByValue(proveedor.Ciudad.Provincia.Nombre).Selected = true;

            var provincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();

            string nombreProvinciaSeleccionada = this.ddlProvincia.SelectedValue.ToString();

            Provincia provinciaSelected = provincias.Find(p => p.Nombre == nombreProvinciaSeleccionada);


            this.ddlCiudad.DataSource = provinciaSelected.Ciudades;
            this.ddlCiudad.DataBind();

            this.ddlCiudad.ClearSelection();

            string sel = this.ddlCiudad.SelectedItem.Text;
            this.ddlCiudad.Items.FindByValue(sel).Selected = false;
            this.ddlCiudad.Items.FindByValue(proveedor.Ciudad.Nombre).Selected = true;
            this.txtTelefono.Text = proveedor.Telefono;
            this.txtCelular.Text = proveedor.Celular;
            this.txtEmail.Text = proveedor.Email;
            this.txtFax.Text = proveedor.Fax;
            this.ddlTipoIva.ClearSelection();
            this.ddlTipoIva.Items.FindByValue(proveedor.TipoIva.Nombre).Selected = true;
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }


        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();

            this.PanelDetalleProveedor.Visible = false;
        }

        protected void GrillaProveedores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.PanelDetalleProveedor.Visible = false;

            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = GrillaProveedores.Rows[index];
            TableCell tbcCodProveedor = selectedRow.Cells[3];
            string strCodProveedor = tbcCodProveedor.Text;
            int idProveedor = int.Parse(strCodProveedor);

            Proveedor proveedor =
                    Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarProveedor(idProveedor);


            if (e.CommandName == Textos.ConsultarCommand)
            {
                this.MostrarPanelDetalle(proveedor, Modo.Consulta);
            }

            if (e.CommandName == Textos.ModificarCommand)
            {

                this.MostrarPanelDetalle(proveedor, Modo.Modificacion);
            }

            if (e.CommandName == Textos.EliminarCommand)
            {
                this.Eliminar(proveedor);
            }
        }

        private void Eliminar(Proveedor proveedor)
        {
            try
            {
                this.SessionVariable.Modo = Modo.Eliminacion;

                #region DatosPara Auditoria
                Entidades.Seguridad.Usuario user = (Entidades.Seguridad.Usuario)Session["user"];

                proveedor.NombreUsuario = user.NombreUsuario;

                proveedor.ModoAuditoria = SessionVariable.Modo.ToString();
                #endregion

                Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().EliminarProveedor(proveedor);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleProveedor.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, String.Format(Textos.NoSePuedeEliminarXVerifiqueLasReferenciasDeOtrasTablas, proveedor.GetType().Name), TypeNotif.Error);
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();

            this.Inicializar();

            this.AgregarNuevoProveedor();
        }

        private void AgregarNuevoProveedor()
        {
            this.MostrarPanelDetalle(new Proveedor(), Modo.Alta);
        }


        private void LimpiarControles()
        {
            this.txtCodigo.Text = string.Empty;

            this.txtRazonSocial.Text = string.Empty;
            this.txtNombreFantasia.Text = string.Empty;
            this.txtCuit.Text = string.Empty;
            this.txtCodPostal.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtTelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtFax.Text = string.Empty;

        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Provincia> _listaProvincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();

            //Cargo ciudades en base a la provincia seleccionada
            DropDownList lista = (DropDownList)sender;
            int indice = lista.SelectedIndex;
            ddlCiudad.DataSource = _listaProvincias[indice].Ciudades;
            ddlCiudad.DataTextField = "Nombre";
            ddlCiudad.DataBind();
        }

        protected void GrillaProveedores_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var btnModificarProveedor = e.Row.Cells[1];

            Boolean puedeModificarProveedor =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionModificarProveedor));

            btnModificarProveedor.Enabled = puedeModificarProveedor;

            var btnEliminarProveedor = e.Row.Cells[2];

            Boolean puedeEliminarProveedor =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionEliminarProveedor));

            btnEliminarProveedor.Enabled = puedeEliminarProveedor; 
        }


    }
}