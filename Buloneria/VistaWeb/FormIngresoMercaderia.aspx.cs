﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades.Seguridad;
using Entidades.Sistema;
using VistaWeb.Sesion;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormIngresoMercaderia : System.Web.UI.Page
    {

        #region SessionVariable
        private const String SessionVariableName = "SesionIngresoMercaderia";
        private SesionIngresoMercaderia _SessionVariable;
        public SesionIngresoMercaderia SessionVariable
        {
            get
            {
                _SessionVariable = (SesionIngresoMercaderia)Session[SessionVariableName];
                return _SessionVariable;
            }
            set
            {
                _SessionVariable = value;
                Session[SessionVariableName] = _SessionVariable;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();
            }
        }

        private void Inicializar()
        {
            this.lblFechaRecepcion.Text = DateTime.Now.ToShortDateString();

            this.InicializarValidaciones();

            this.InicializarSesion();

        }

        private void InicializarSesion()
        {
            this.SessionVariable = new SesionIngresoMercaderia();
        }

        private void LimpiarControles()
        {
            this.txtNroOrdenCompra.Text = String.Empty;
            this.lblMensajeFiltro.Text = String.Empty;
            this.lblMensajeAceptar.Text = String.Empty;
        }

        private void InicializarValidaciones()
        {
            this.txtNroOrdenCompra.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtCantidadRecibida.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
        }

        private void BuscarOrdenCompra()
        {
            

            String strNroOrdenCompra = this.txtNroOrdenCompra.Text;
            int nroOrdenCompra = 0;

            if (strNroOrdenCompra.Equals(String.Empty))
            {
                this.Notify(lblMensajeFiltro, Textos.DebeIngresarAlMenosUnCriterio, TypeNotif.Error);
                return;
            }

            nroOrdenCompra = int.Parse(strNroOrdenCompra);

            OrdenCompra ordenCompra =
                Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia()
                    .BuscarOrdenCompraPorNumero(nroOrdenCompra);

            this.LimpiarControles();

            if (ordenCompra != null)
            {
                this.SessionVariable.OrdenCompra = ordenCompra;

                this.MostrarDetalleOrdenCompra(this.SessionVariable.OrdenCompra);
            }
            else
            {
                this.Notify(lblMensajeFiltro, Textos.LaOrdenIngresadaNoExisteOSeEncuentraFinalizada, TypeNotif.Error);
                this.PanelDetalleOrdenCompra.Visible = false;

            }
        }

        private void MostrarDetalleOrdenCompra(OrdenCompra ordenCompra)
        {
            this.lblNroOrdenCompra.Text = ordenCompra.NroOrdenCompra.ToString();
            this.lblProveedor.Text = ordenCompra.Proveedor.RazonSocial;
            this.lblFechaOrdenCompra.Text = ordenCompra.FechaEmision.ToShortDateString();
            this.lblEstadoOrdenCompra.Text = ordenCompra.Estado.ToString();

            this.CargarGrilla(GrillaArticulosOrdenCompra, ordenCompra.LineasOrdenCompra);

            this.PanelDetalleOrdenCompra.Visible = true;
        }

        private void CargarGrilla(GridView control, IEnumerable<Object> list)
        {
            control.DataSource = list;
            control.DataBind();
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }

        private void SeleccionarLineaOrdenCompra(int nroLineaOCBuscada)
        {
            LineaOrdenCompra lineaOrdenCompra =
                this.SessionVariable.OrdenCompra.LineasOrdenCompra.First(loc => loc.NroLineaOrdenCompra == nroLineaOCBuscada);

            SessionVariable.LineaOrdenCompraElegida = lineaOrdenCompra;

            MostrarDetalleLineaOrdenCompra(lineaOrdenCompra);
        }

        private void MostrarDetalleLineaOrdenCompra(LineaOrdenCompra lineaOrdenCompra)
        {
            this.lblNombreArticulo.Text = lineaOrdenCompra.Articulo.Descripcion;
            this.lblCantidadPedida.Text = lineaOrdenCompra.CantPedir.ToString();
            this.lblCantidadFaltante.Text = lineaOrdenCompra.CantFaltante.ToString();
            this.PanelDetalleArticuloSeleccionado.Visible = true;
            this.PanelIngresoCantidad.Visible = true;
        }

        private int ConvertirACantidadEnteraMayorACero(string cantidadStr)
        {
            int cantidad = int.Parse(cantidadStr);

            if (cantidad <= 0)
            {
                throw new Exception(Textos.DebeIngresarUnValorMayorACero);
            }

            return cantidad;

        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        #region Events

        protected void btnBuscarOrdenCompra_Click(object sender, EventArgs e)
        {
            this.BuscarOrdenCompra();


        }
        protected void btnLimpiarCampos_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();
        }

        protected void GrillaArticulosOrdenCompra_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Seleccionar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaArticulosOrdenCompra.Rows[index];
                TableCell tbcNroLinea = selectedRow.Cells[1];
                string strNroLinea = tbcNroLinea.Text;
                int nroLineaOCBuscada = Convert.ToInt32(strNroLinea);

                SessionVariable.IndexFila = index;

                PanelFiltro.Visible = false;

                this.SeleccionarLineaOrdenCompra(nroLineaOCBuscada);
            }
        }

        protected void btnIngresarCantidad_Click(object sender, EventArgs e)
        {
            try
            {
                String cantidadStr = this.txtCantidadRecibida.Text;

                int cantidadRecibida = ConvertirACantidadEnteraMayorACero(cantidadStr);

                ValidarCantidadRecibidaMenorOIgualACantidadFaltante(cantidadRecibida, SessionVariable.LineaOrdenCompraElegida);

                //Actualiza en la linea
                SessionVariable.LineaOrdenCompraElegida.CantIngresada = cantidadRecibida;

                SessionVariable.LineaOrdenCompraElegida.CantRecibida += cantidadRecibida;

                SessionVariable.LineasArticulosRecibidos.Add(SessionVariable.LineaOrdenCompraElegida);

                this.CargarGrilla(GrillaArticulosRecibidos, SessionVariable.LineasArticulosRecibidos);

                PanelIngresoCantidad.Visible = false;

                DeshabilitarSeleccionItemGrillaArticulosOrdenCompra();

                PanelArticulosRecibidos.Visible = true;

                PanelAceptarCancelar.Visible = true;

            }
            catch (Exception ex)
            {
                this.lblMensajeIngresoCantidad.Text = ex.Message;
            }
        }

        private void DeshabilitarSeleccionItemGrillaArticulosOrdenCompra()
        {
            GrillaArticulosOrdenCompra.Rows[SessionVariable.IndexFila].Enabled = false;
        }

        private void ValidarCantidadRecibidaMenorOIgualACantidadFaltante(int cantidadRecibida, LineaOrdenCompra lineaOrdenCompra)
        {
            if (cantidadRecibida > lineaOrdenCompra.CantFaltante)
            {
                throw new Exception(Textos.NoPuedeIngresarUnaCantidadMayorALaFaltante);
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = (Usuario)Session["user"];

                if (SessionVariable.LineasArticulosRecibidos.Count == 0)
                    throw new Exception(Textos.DebeIngresarAlMenosUnaLinea);

                Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().IngresarMercaderia(SessionVariable.OrdenCompra, usuario);

                string message = String.Format(Textos.OrdenCompraXPasoAEstadoY, SessionVariable.OrdenCompra.NroOrdenCompra, SessionVariable.OrdenCompra.Estado);

                Page.Response.Redirect("FormMensajes.aspx?mensaje=" + message);
            }
            catch (Exception ex)
            {
                this.Notify(lblMensajeAceptar, ex.Message, TypeNotif.Error);
            }

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnVolverAFiltro_Click(object sender, EventArgs e)
        {
            this.PanelFiltro.Visible = true;
            this.PanelDetalleOrdenCompra.Visible = false;
            this.PanelDetalleArticuloSeleccionado.Visible = false;
            this.PanelArticulosRecibidos.Visible = false;

        }

        #endregion
    }
}