﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormProveedor : System.Web.UI.Page
    {
        List<Entidades.Sistema.TipoIva> tipoiva;
        List<Entidades.Sistema.Provincia> provincia;
        protected void Page_Init(object sender, EventArgs e)
        {
            tipoiva = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva();  //aca llamo a la capa controladora, y le pido que ejecute el metodo recuperar _TipoMovimiento iva de la controladoraCUGProveedores
            provincia = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
        
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //CARGO TIPO IVA
                this.dplTipoIva.DataSource = tipoiva;
                this.dplTipoIva.DataTextField = "Nombre";
                this.dplTipoIva.DataBind();
                //CARGO PROVINCIA
                this.dplProvincia.DataSource = provincia;
                this.dplProvincia.DataTextField = "Nombre";                               
                this.dplProvincia.DataBind();

            }

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
                       
            Entidades.Sistema.Proveedor proveedor = new Entidades.Sistema.Proveedor();
            proveedor.CodProveedor = Convert.ToInt32(this.txtCodigo.Text);
            proveedor.Cuit = Convert.ToInt64(this.txtCuit.Text);
            //FALTA DROP DOWN LIST TIPO IVA... COMBO?
            proveedor.RazonSocial = this.txtRazonSocial.Text;
            proveedor.NombreFantasia = this.txtNombreFantasia.Text;
            proveedor.Direccion = this.txtDireccion.Text;
            //FALTA DROP DOWN LIST PROVINCIA Y CIUDAD
            proveedor.CodPostal = Convert.ToInt32(this.txtCodigoPostal.Text);
            proveedor.Telefono = this.txtTelefono.Text;
            proveedor.Fax = this.txtFax.Text;
            proveedor.Celular = this.txtCelular.Text;
            proveedor.Email = this.txtEmail.Text;

           // Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().AgregarProveedor(proveedor);
            Response.Redirect("FormGProveedores");

        }

        
        protected void dplProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
           //CARGO CIUDADES EN BASE A LA PROVINCIA SELECCIONADA DEL DPLPROVINCIA
           DropDownList lista  =(DropDownList)sender;
           int indice = lista.SelectedIndex;
           dplCiudad.DataSource = provincia[indice].Ciudades;
           dplCiudad.DataTextField = "Nombre";
           dplCiudad.DataBind();
        }
    }
}