﻿<%@ Page Title="Generar Orden Compra" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="zzzFormGenerarOC.aspx.cs" Inherits="VistaWeb.FormGenerarOC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="js/datepicker.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script>
        function soloNumeros(e) {
            var key = window.Event ? e.which : e.keyCode
            return (key >= 48 && key <= 57)
        }
    </script>
    <div class="content" id="div">
        <asp:Label ID="lblFechaEmision" runat="server" Text="Fecha de Emisión: "></asp:Label>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Proveedor:"></asp:Label>
        <asp:TextBox ID="txtProveedor" runat="server" Width="158px" ReadOnly="True"></asp:TextBox>
        <asp:ImageButton ID="btnGestionarProveedores" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/lupa-icono-16x16.png" OnClick="btnGestionarProveedores_Click" Width="22px" />
        <br />
        <!--BEGIN Selector Fecha Plazo -->
        <asp:Label ID="Label3" runat="server" Text="Fecha de Plazo:"></asp:Label>
        <asp:TextBox ID="txtFechaPlazo" runat="server" ReadOnly="True" Width="117px"></asp:TextBox>
        <asp:ImageButton ID="btnCalendarFechaPlazo" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/date_add.png" OnClick="btnCalendarFechaPlazo_Click" Width="22px" />
        <asp:Calendar ID="calendarFechaPlazo" runat="server" OnSelectionChanged="CalendarFechaPlazo_SelectionChanged" ForeColor="#003399" Font-Size="10pt"
            Font-Names="Verdana" BorderColor="#3366CC" DayNameFormat="Shortest" CellPadding="1" Visible="false">
            <TodayDayStyle ForeColor="White" BackColor="#99CCCC"></TodayDayStyle>
            <SelectorStyle BackColor="#99CCCC" ForeColor="#336666"></SelectorStyle>
            <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF"></NextPrevStyle>
            <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px"></DayHeaderStyle>
            <SelectedDayStyle Font-Bold="True" ForeColor="#CCFF99" BackColor="#009999"></SelectedDayStyle>
            <TitleStyle Font-Bold="True" BorderColor="#3366CC" BackColor="#003399" BorderWidth="1px" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px"></TitleStyle>
            <WeekendDayStyle BackColor="#CCCCFF"></WeekendDayStyle>
            <OtherMonthDayStyle ForeColor="#999999"></OtherMonthDayStyle>
        </asp:Calendar>
        <asp:Label ID="lblErrorFecha" runat="server" Font-Size="Small" ForeColor="#CC0000" Text=""></asp:Label>
        <!--END Selector Fecha Plazo -->

        <asp:Label ID="lblErrorProvNoSeleccionado" runat="server" Font-Size="Medium" ForeColor="#CC0000"></asp:Label>
        <br />

        <asp:Panel ID="pnlGenerarOC" runat="server" Visible="true">
            <!--BEGIN Forma de pago -->
            <asp:Label ID="Label4" runat="server" Text="Forma de Pago:"></asp:Label>
            <asp:DropDownList ID="dplFormaPago" runat="server" Width="157px" />
            <!--END Forma de pago -->
            <br /><br />
            <asp:Label ID="Label6" runat="server" Text="Artículos Disponibles del Proveedor Seleccionado:"></asp:Label>
            <br />
            <asp:GridView ID="dgvArticulosDisponiblesProv" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="4" ForeColor="#333333" GridLines="None" ShowHeaderWhenEmpty="True" Width="733px">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField HeaderText="Cod Art" DataField="CodArticulo" />
                    <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Grupo Articulo" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Cant Minima" DataField="CantMinima" />
                    <asp:BoundField HeaderText="Cant Actual" DataField="CantActual" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
            <br />

            <asp:Label ID="Label11" runat="server" Text="Cantidad a pedir:"></asp:Label>
            <asp:TextBox ID="txtCantAPedir" runat="server" onKeyPress="return soloNumeros(event)" Width="81px"></asp:TextBox>
            <asp:Label ID="lblErrorAgregarArticulo" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
            <br />
            <asp:Button ID="btnAgregarArticulo" runat="server" Text="Agregar Artículo" OnClick="btnAgregarArticulo_Click" />
            <br />
            <br />
            <asp:Button ID="btnQuitarArticulo" runat="server" Text="Quitar Artículo" OnClick="btnQuitarArticulo_Click" />
            <asp:Label ID="lblErrorQuitarArticulo" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
            <br />

            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text="Artículos Agregados a la Orden de Compra:"></asp:Label>
            <br />
            <asp:GridView ID="dgvArticulosAgregadosOC" runat="server" CellPadding="4" AutoGenerateSelectButton="True" ForeColor="#333333" GridLines="None" Width="738px" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="Articulo.codArticulo" HeaderText="CodArticulo" />
                    <asp:BoundField DataField="CantPedir" HeaderText="Cant a pedir" />
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Descripcion" />
                    <asp:BoundField DataField="Articulo.PrecioVenta" HeaderText="Precio unitario" />
                    <asp:BoundField HeaderText="Subtotal" DataField="Subtotal" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label8" runat="server" Text="Subtotal:"></asp:Label>
            <asp:TextBox ID="txtSubtotal" runat="server" ReadOnly="True"></asp:TextBox>
            <br />
            <br />
            &nbsp;
         <asp:Label ID="Label9" runat="server" Text="I.V.A.:"></asp:Label>
            <asp:TextBox ID="txtMontoIva" runat="server" ReadOnly="True"></asp:TextBox>
            <br />
            <br />
            &nbsp;
        <asp:Label ID="Label10" runat="server" Text="Total:"></asp:Label>
            <asp:TextBox ID="txtTotal" runat="server" ReadOnly="True"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="lblErrorCompraSinArticulos" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
            <br />
            <br />
            <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" OnClick="btnAceptar_Click" />
            <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" OnClick="btnCancelar_Click" />
            <br />

        </asp:Panel>

    </div>
</asp:Content>
