﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zzzFormABMProveedores.aspx.cs" Inherits="VistaWeb.FormABMProveedores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>ABM Proveedores</h2>
            <asp:Label ID="lblMensaje" runat="server" Font-Bold="True" ForeColor="Red" Visible="false"></asp:Label>
            <asp:Panel ID="pnlGrilla" runat="server">
                
                <asp:GridView ID="grdProveedores" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" OnRowCommand="grdProveedores_RowCommand" AutoGenerateColumns="False">
                    <Columns>
                        <asp:ButtonField CommandName="Consultar" Text="Consultar" ButtonType="Button" />
                        <asp:ButtonField CommandName="Modificar" Text="Modificar" ButtonType="Button" />
                        <asp:ButtonField CommandName="Eliminar" Text="Eliminar" ButtonType="Button" />
                        <asp:BoundField DataField="CodProveedor" HeaderText="Codigo" />
                        <asp:BoundField DataField="RazonSocial" HeaderText="RazonSocial" />
                        <asp:BoundField DataField="Ciudad" HeaderText="Ciudad" />
                        <asp:BoundField DataField="TipoIVA" HeaderText="TipoIVA" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
                <br />
                <asp:Button ID="btnAgregarNuevo" runat="server" Text="Agregar Nuevo" OnClick="btnAgregarNuevo_Click" />
                &nbsp;&nbsp;&nbsp;<asp:Button ID="btnSalir" runat="server" Text="     Salir     " OnClick="btnSalir_Click" />
            </asp:Panel>
             
            <asp:Panel ID="pnlAltaProveedores" runat="server" Visible="False"> 
                <p>
                    <asp:Label ID="Label3" runat="server" Text="Cuit:"></asp:Label>
                    &nbsp;<asp:TextBox ID="txtCuit" runat="server" Width="278px"></asp:TextBox>
                </p>
                <asp:Label ID="Label4" runat="server" Text="Tipo IVA:"></asp:Label>
                &nbsp;<asp:DropDownList ID="dplTipoIva" runat="server" Height="30px" Width="253px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="Label5" runat="server" Text="Razón Social:"></asp:Label>
                &nbsp;<asp:TextBox ID="txtRazonSocial" runat="server" Width="223px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="Nombre Fantasía:"></asp:Label>
                &nbsp;<asp:TextBox ID="txtNombreFantasia" runat="server" Width="195px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label7" runat="server" Text="Dirección:"></asp:Label>
                &nbsp;<asp:TextBox ID="txtDireccion" runat="server" Width="240px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label8" runat="server" Text="Provincia:"></asp:Label>
                &nbsp;<asp:DropDownList ID="dplProvincia" AutoPostBack="true" OnSelectedIndexChanged="dplProvincia_SelectedIndexChanged" runat="server" Height="36px" Width="251px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="Label9" runat="server" Text="Ciudad:"></asp:Label>
                &nbsp;<asp:DropDownList ID="dplCiudad" runat="server" Height="40px" Width="262px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="Código Postal:"></asp:Label>
                &nbsp;<asp:TextBox ID="txtCodigoPostal" runat="server" Width="211px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label11" runat="server" Text="Teléfono:"></asp:Label>
                &nbsp;<asp:TextBox ID="txtTelefono" runat="server" Style="margin-bottom: 0px" Width="242px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label12" runat="server" Text="Fax (*):"></asp:Label>
                &nbsp;<asp:TextBox ID="txtFax" runat="server" Width="250px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label13" runat="server" Text="Celular (*):"></asp:Label>
                &nbsp;<asp:TextBox ID="txtCelular" runat="server" Width="229px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label14" runat="server" Text="E-Mail (*):"></asp:Label>
                &nbsp;<asp:TextBox ID="txtEmail" runat="server" Width="229px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label15" runat="server" Text="(*) Campos Opcionales"></asp:Label>
                <br />
                <br />
                <asp:Button ID="btnAgregar" runat="server" Text="Aceptar" OnClick="btnAgregar_Click" />
                <asp:Button ID="btnSalirAlta" runat="server" Text="Cancelar" OnClick="btnSalirAlta_Click" />
            </asp:Panel>
            <asp:Panel ID="pnlConsultar" runat="server" Visible="false">
                <asp:Label ID="lblDatosProveedorConsultado" runat="server" Text="Datos prov"></asp:Label><br />
                <asp:Button ID="btnAceptarDeConsulta" runat="server" Text="Aceptar" OnClick="btnAceptarDeConsulta_Click" /> 
            </asp:Panel>
        </div>
    </form>
</body>
</html>
