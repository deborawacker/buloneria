﻿using Controladora.Sistema;
using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormGestionarNotaPedido : System.Web.UI.Page
    {
        #region SessionVariables

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PanelDetalleNotaPedido.Visible = false;

            if (!Page.IsPostBack)
            {
                this.RellenarGrilla();
            }
        }

        
        private void RellenarGrilla()
        {
            List<NotaPedido> listaNotaPedido = Controladora.Sistema.ControladoraCUGestionarNP.ObtenerInstancia().RecuperarNotasPedidos();

            this.GrillaNotasPedidos.DataSource = listaNotaPedido;
            this.GrillaNotasPedidos.DataBind();
        }

        protected void GrillaNotasPedidos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.Resetear();

            #region Obtener Nota Pedido Buscada

            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = GrillaNotasPedidos.Rows[index];
            TableCell tbcNroNotaPedido = selectedRow.Cells[2];
            string strNroNotaPedido = tbcNroNotaPedido.Text;

            #endregion

            int nroNotaPedidoBuscada = Convert.ToInt32(strNroNotaPedido);

            if (e.CommandName == Textos.ConsultarCommand)
            {
                this.MostrarDetalle(this.BuscarNotaPedido(nroNotaPedidoBuscada));
            }

            if (e.CommandName == Textos.FinalizarConFaltanteCommand)
            {
                NotaPedido notaPedidoSeleccionada = this.BuscarNotaPedido(nroNotaPedidoBuscada);

                try
                {
                    notaPedidoSeleccionada.FinalizarConFaltante();

                    ControladoraCUGestionarNP.ObtenerInstancia().ActualizarEstadoNotaPedido(notaPedidoSeleccionada);

                    this.RellenarGrilla();

                    //TODO: revisar null estado
                    this.Notify(lblMensaje, String.Format(Textos.SeHanGuardadoLosCambiosDetalleX, String.Format(Textos.NotaPedidoXPasoAEstadoY, notaPedidoSeleccionada.NroNotaPedido, notaPedidoSeleccionada.EstadoNotaPedido.Descripcion)), TypeNotif.Success);

                }
                catch (Exception)
                {
                    this.Notify(lblMensaje, String.Format(Textos.NoSePuedeFinalizarConFaltanteUnaNotaEnEstadoX, notaPedidoSeleccionada.EstadoNotaPedido.Descripcion), TypeNotif.Error);
                }

            }
        }

        private void Resetear()
        {
            this.lblMensaje.Text = String.Empty;
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }

        private NotaPedido BuscarNotaPedido(int nroNotaPedidoBuscada)
        {
            return ControladoraCUGestionarNP.ObtenerInstancia().BuscarNotasDePedidoPorNro(nroNotaPedidoBuscada);
        }

        private void MostrarDetalle(NotaPedido notaPedidoSeleccionada)
        {
            this.PanelDetalleNotaPedido.Visible = true;
            this.lblCliente.Text = notaPedidoSeleccionada.Cliente.RazonSocial;
            this.lblNotaPedido.Text = notaPedidoSeleccionada.NroNotaPedido.ToString();
            this.lblFechaEmision.Text = notaPedidoSeleccionada.FechaEmisionCorta;
            this.lblEstado.Text = notaPedidoSeleccionada.EstadoNotaPedido.Descripcion;
            this.lblSubtotal.Text = notaPedidoSeleccionada.Subtotal.ToString();
            this.lblSubtotalIVA.Text = notaPedidoSeleccionada.SubtotalIVA.ToString();
            this.lblTotal.Text = notaPedidoSeleccionada.Total.ToString();

            this.RellenarGrillaDetalleLineasNotaPedido(notaPedidoSeleccionada);
        }

        private void RellenarGrillaDetalleLineasNotaPedido(NotaPedido notaPedidoSeleccionada)
        {
            this.GrillaLineasNotaPedido.DataSource = notaPedidoSeleccionada.LineasNotaPedido;
            this.GrillaLineasNotaPedido.DataBind();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void GrillaNotasPedidos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var btnFinalizarConFaltanteNotaPedido = e.Row.Cells[1];

            Boolean puedeFinalizarConFaltanteNotaPedido =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == "FinalizarConFaltanteNotaPedido"));

            btnFinalizarConFaltanteNotaPedido.Enabled = puedeFinalizarConFaltanteNotaPedido;
        }

    }
}