﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGConceptos : System.Web.UI.Page
    {
        List<Entidades.Sistema.Concepto> _listaConceptos;

        protected void Page_Init(object sender, EventArgs e)
        {
            _listaConceptos = Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().RecuperarConceptos();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                dgvConceptos.DataSource = _listaConceptos;
                dgvConceptos.DataBind();
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormConcepto.aspx");
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int indice = dgvConceptos.SelectedIndex;
            Entidades.Sistema.Concepto concepto = _listaConceptos[indice];
            Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().EliminarConcepto(concepto);
            dgvConceptos.DataSource = _listaConceptos;
            dgvConceptos.DataBind();
        }
    }
}