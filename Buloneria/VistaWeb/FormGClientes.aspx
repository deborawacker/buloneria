﻿<%@ Page Title="Gestionar Clientes" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGClientes.aspx.cs" Inherits="VistaWeb.FormGClientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="btnAgregar" runat="server" Text="AGREGAR" />
    <asp:Button ID="btnModificar" runat="server" Text="MODIFICAR" />
    <asp:Button ID="btnConsultar" runat="server" Text="CONSULTAR" />
    <asp:Button ID="btnEliminar" runat="server" Text="ELIMINAR" />
    <br />
&nbsp;<asp:Label ID="Label1" runat="server" Text="Buscar:"></asp:Label>
&nbsp;<asp:TextBox ID="txtBuscar" runat="server" Width="343px"></asp:TextBox>
    <br />
    <asp:Button ID="btnSeleccionarCliente" runat="server" Text="Seleccionar Cliente" OnClick="btnSeleccionarCliente_Click" />
    <br />
    <asp:Label ID="lblClienteNoSel" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    <br />
    <asp:GridView ID="dgvClientes" runat="server" AutoGenerateSelectButton="True" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <br />
</asp:Content>