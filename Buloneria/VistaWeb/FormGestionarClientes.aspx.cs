﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormGestionarClientes : System.Web.UI.Page
    {
        #region SessionVariable

        private const String SessionVariableName = "SesionGestionarClientes";
        private SesionGestionarClientes _SessionVariable;
        public SesionGestionarClientes SessionVariable
        {
            get
            {
                _SessionVariable = (SesionGestionarClientes)Session[SessionVariableName];
                return _SessionVariable;
            }
            set
            {
                _SessionVariable = value;
                Session[SessionVariableName] = _SessionVariable;
            }
        }

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }
         
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();

            } 
        }

        private void Inicializar()
        {
            this.RellenarGrilla();

            this.InicializarCombos();

            this.InicializarSesion();

            this.VerificarPermisos();
        }

        private void VerificarPermisos()
        {
            this.btnNuevo.Enabled = this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionAgregarCliente));
        }

        private void InicializarCombos()
        {
            var provincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();

            this.CargarControl(this.ddlProvincia, provincias, "Nombre");

            string nombreProvinciaSeleccionada = this.ddlProvincia.SelectedValue.ToString();

            Provincia provinciaSelected = provincias.Find(p => p.Nombre == nombreProvinciaSeleccionada);

            this.CargarControl(this.ddlCiudad, provinciaSelected.Ciudades, "Nombre");

            this.CargarControl(this.ddlTipoIva, Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva());
        }

        private void CargarControl(ListControl control, IEnumerable<Object> list)
        {
            control.DataSource = list;

            control.DataBind();
        }

        private void CargarControl(ListControl control, IEnumerable<Object> list, string displayMember)
        {
            control.DataSource = list;

            control.DataTextField = displayMember;

            control.DataBind();
        }

        private void InicializarSesion()
        {
            this.SessionVariable = new SesionGestionarClientes();
        }


        private void RellenarGrilla()
        {
            this.GrillaClientes.DataSource =
                Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarClientes();
            this.GrillaClientes.DataBind();
        }

        private void LimpiarControles()
        {
            this.txtCodigo.Text = string.Empty;

            this.txtRazonSocial.Text = string.Empty;
            this.txtNombreFantasia.Text = string.Empty;
            this.txtCuit.Text = string.Empty;
            this.txtCodPostal.Text = string.Empty;
            this.txtDireccion.Text = string.Empty;
            this.txtTelefono.Text = string.Empty;
            this.txtCelular.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtFax.Text = string.Empty;
        }

        private void Guardar()
        {
            try
            {
                this.GuardarCliente(this.SessionVariable.Cliente);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleCliente.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, e.Message, TypeNotif.Error);
            }
        }

        private void GuardarCliente(Cliente cliente)
        {
            ValidarCamposObligatorios();

            if (SessionVariable.Modo == Modo.Alta)
                cliente = new Cliente(); 

            cliente.RazonSocial = this.txtRazonSocial.Text;

            cliente.NombreFantasia = this.txtNombreFantasia.Text;

            cliente.Cuit = Convert.ToInt64(this.txtCuit.Text);

            cliente.CodPostal = Convert.ToInt32(this.txtCodPostal.Text);

            cliente.Direccion = this.txtDireccion.Text;

            String ciudadDescripcion = this.ddlCiudad.SelectedValue;

            cliente.Ciudad =
                Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().BuscarCiudadPorDescripcion(ciudadDescripcion);

            cliente.Telefono = this.txtTelefono.Text;

            cliente.Celular = this.txtCelular.Text;

            cliente.Email = this.txtEmail.Text;

            cliente.Fax = this.txtFax.Text;

            string tipoIvaDescripcion = this.ddlTipoIva.SelectedValue;

            cliente.TipoIva =
                Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().BuscarTipoIvaPorDescripcion(tipoIvaDescripcion);

            Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().Guardar(cliente);
        }

        private void ValidarCamposObligatorios()
        {
            if (string.IsNullOrEmpty((this.txtCuit.Text.Trim())))
                throw new Exception(String.Format(Textos.ElCampoXEsDeIngresoObligatorio, "Cuit"));

            if (string.IsNullOrEmpty((this.txtCodPostal.Text.Trim())))
                throw new Exception(String.Format(Textos.ElCampoXEsDeIngresoObligatorio, "Codigo Postal"));
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();

            this.Inicializar();

            this.AgregarNuevoCliente();
        }

        private void AgregarNuevoCliente()
        {
            this.MostrarPanelDetalle(new Cliente(), Modo.Alta);
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void GrillaClientes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.PanelDetalleCliente.Visible = false;

            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = GrillaClientes.Rows[index];
            TableCell tbcCodCliente = selectedRow.Cells[3];
            string strCodCliente = tbcCodCliente.Text;
            int idCliente = int.Parse(strCodCliente);

            Cliente cliente =
                    Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().BuscarCliente(idCliente);


            if (e.CommandName == Textos.ConsultarCommand)
            {
                this.MostrarPanelDetalle(cliente, Modo.Consulta);
            }

            if (e.CommandName == Textos.ModificarCommand)
            {
                this.MostrarPanelDetalle(cliente, Modo.Modificacion);
            }

            if (e.CommandName == Textos.EliminarCommand)
            {
                this.Eliminar(cliente);
            }

        }


        private void Eliminar(Cliente cliente)
        {
            try
            {
                this.SessionVariable.Modo = Modo.Eliminacion;

                Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().EliminarCliente(cliente);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleCliente.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, String.Format(Textos.NoSePuedeEliminarXVerifiqueLasReferenciasDeOtrasTablas, cliente.GetType().Name), TypeNotif.Error);
            }
        }

        private void MostrarPanelDetalle(Cliente cliente, Modo modo)
        {
            this.SessionVariable.Modo = modo; 

            this.SessionVariable.Cliente = cliente;

            this.PanelDetalleCliente.Enabled = (modo != Modo.Consulta);

            if (modo == Modo.Consulta || modo == Modo.Modificacion)
            {
                this.SetearControles(cliente);
            }

            this.PanelDetalleCliente.Visible = true;
        }

        private void SetearControles(Cliente cliente)
        {
            this.txtCodigo.Text = cliente.CodCliente.ToString();
            this.txtRazonSocial.Text = cliente.RazonSocial;
            this.txtNombreFantasia.Text = cliente.NombreFantasia;
            this.txtCuit.Text = cliente.Cuit.ToString();
            this.txtCodPostal.Text = cliente.CodPostal.ToString();
            this.txtDireccion.Text = cliente.Direccion;
            this.ddlProvincia.ClearSelection();
            this.ddlProvincia.Items.FindByValue(cliente.Ciudad.Provincia.Nombre).Selected = true;

            var provincias = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarProvincias();

            string nombreProvinciaSeleccionada = this.ddlProvincia.SelectedValue.ToString();

            Provincia provinciaSelected = provincias.Find(p => p.Nombre == nombreProvinciaSeleccionada);


            this.ddlCiudad.DataSource = provinciaSelected.Ciudades;
            this.ddlCiudad.DataBind();

            this.ddlCiudad.ClearSelection();

            string sel = this.ddlCiudad.SelectedItem.Text;
            this.ddlCiudad.Items.FindByValue(sel).Selected = false;
            this.ddlCiudad.Items.FindByValue(cliente.Ciudad.Nombre).Selected = true;
            this.txtTelefono.Text = cliente.Telefono;
            this.txtCelular.Text = cliente.Celular;
            this.txtEmail.Text = cliente.Email;
            this.txtFax.Text = cliente.Fax;
            this.ddlTipoIva.ClearSelection();
            this.ddlTipoIva.Items.FindByValue(cliente.TipoIva.Nombre).Selected = true;
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Provincia> _listaProvincias = Controladora.Sistema.ControladoraCUGestionarClientes.ObtenerInstancia().RecuperarProvincias();

            //Cargo ciudades en base a la provincia seleccionada
            DropDownList lista = (DropDownList)sender;
            int indice = lista.SelectedIndex;
            ddlCiudad.DataSource = _listaProvincias[indice].Ciudades;
            ddlCiudad.DataTextField = "Nombre";
            ddlCiudad.DataBind();
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Guardar();
        } 

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();

            this.PanelDetalleCliente.Visible = false;
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }

        protected void GrillaClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var btnModificarCliente = e.Row.Cells[1];

            Boolean puedeModificarCliente =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionModificarCliente));

            btnModificarCliente.Enabled = puedeModificarCliente;

            var btnEliminarCliente = e.Row.Cells[2];

            Boolean puedeEliminarCliente =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == Textos.AccionEliminarCliente));

            btnEliminarCliente.Enabled = puedeEliminarCliente; 
        }
    }
}