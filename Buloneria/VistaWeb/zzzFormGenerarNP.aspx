﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Principal.Master" CodeBehind="zzzFormGenerarNP.aspx.cs" Inherits="VistaWeb.FormGenerarNP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function soloNumeros(e) {
            var key = window.Event ? e.which : e.keyCode
            return (key >= 48 && key <= 57)
        }


    </script>
    <asp:Label ID="Label3" runat="server" Text="Generar Nota de Pedido"></asp:Label><br />

    <asp:Label ID="Label2" runat="server" Text="Fecha de Emisión:"></asp:Label>
    &nbsp;<asp:Label ID="lblFechaEmision" runat="server" Text="Fecha de Emisión:"></asp:Label>

    <br />
    <br />
    <asp:Label ID="lblMensajeAccion" runat="server" Font-Size="Medium" ForeColor="#003399"></asp:Label>
    <br />
    <br />
    <asp:Label ID="Label5" runat="server" Text="Cliente:"></asp:Label>
    <asp:TextBox ID="txtCliente" runat="server" Width="158px" ReadOnly="True"></asp:TextBox>
    <asp:ImageButton ID="btnGestionarClientes" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/lupa-icono-16x16.png" OnClick="btnGestionarClientes_Click" Width="22px" />
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Grupo Articulo:"></asp:Label>
    <asp:TextBox ID="txtGrupoArticulo" runat="server" Width="158px" ReadOnly="True"></asp:TextBox>
    <asp:ImageButton ID="btnGestionarGruposArticulos" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/lupa-icono-16x16.png" OnClick="btnGestionarGruposArticulos_Click" Width="22px" />
    <br />
    <br />
    <asp:Panel ID="pnlGenerarNP" runat="server">

         <br />
         <asp:Label ID="Label4" runat="server" Text="Forma de Pago:"></asp:Label>
         <asp:DropDownList ID="dplFormaPago" runat="server" Height="16px" Width="157px">
         </asp:DropDownList>
         <br />

         <br />
    <asp:Label ID="lblGrillaArticulosSeleccionados" runat="server" Text="Artículos del Grupo Seleccionado:"></asp:Label>

    <asp:GridView ID="dgvArticulosGrupo" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="4" ForeColor="#333333" GridLines="None" ShowHeaderWhenEmpty="True" Width="733px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="CodArticulo" HeaderText="Codigo" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
            <asp:BoundField DataField="PrecioVenta" HeaderText="Precio" />
            <asp:BoundField DataField="CantMinima" HeaderText="Cant. Minima" />
            <asp:BoundField DataField="CantActual" HeaderText="Cant. Actual" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <br />
    <asp:Label ID="Label11" runat="server" Text="Cantidad pedida:"></asp:Label>
    <asp:TextBox ID="txtCantPedida" runat="server" onKeyPress="return soloNumeros(event)" Width="81px"></asp:TextBox>
    <asp:Label ID="lblErrorAgregarArticulo" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
    <br />
    <asp:Button ID="btnAgregarArticulo" runat="server" Text="Agregar Artículo" OnClick="btnAgregarArticulo_Click" />

    <br />
    <br />
    <asp:Button ID="btnQuitarArticulo" runat="server" Text="Quitar Artículo" OnClick="btnQuitarArticulo_Click" />
    <asp:Label ID="lblErrorQuitarArticulo" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
    <br />

    <br />
    <asp:Label ID="lblGrillaArticulosAgregados" runat="server" Text="Artículos Agregados a la Nota de pedido:"></asp:Label>
         <br />
         <asp:Label ID="lblErrorGrillaVacia" runat="server" Font-Size="Small" ForeColor="#003399"></asp:Label>
    <br />
    <asp:GridView ID="dgvArticulosAgregadosNP" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="4" ForeColor="#333333" GridLines="None" ShowHeaderWhenEmpty="True" Width="733px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Articulo.CodArticulo" HeaderText="Codigo Articulo" />
            <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Descripcion" />
            <asp:BoundField DataField="Articulo.PrecioVenta" HeaderText="Precio" />
            <asp:BoundField DataField="CantPedida" HeaderText="CantPedida" />
            <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnDeshacer" runat="server" OnClick="btnDeshacer_Click" Height="48px" ImageUrl="~/img/UndoButton.png" Width="74px" />
    <br />
    <hr />
    <asp:Label ID="Label8" runat="server" Text="Subtotal:"></asp:Label>
    <asp:TextBox ID="txtSubtotal" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
    <br />
    &nbsp;
         <asp:Label ID="Label9" runat="server" Text="I.V.A.:"></asp:Label>
    <asp:TextBox ID="txtMontoIva" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
    <br />
    &nbsp;
        <asp:Label ID="Label10" runat="server" Text="Total:"></asp:Label>
    <asp:TextBox ID="txtTotal" runat="server" ReadOnly="True"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblMensajesError" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" OnClick="btnAceptar_Click" />
    <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" OnClick="btnCancelar_Click" />
    <br />
    </asp:Panel>
    <br />
   
    <br />
    <asp:Button ID="btnVolver" runat="server" Text="Volver" OnClick="btnVolver_Click"/>
    <br />

</asp:Content>
