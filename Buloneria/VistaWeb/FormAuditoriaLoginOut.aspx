﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Principal.Master" CodeBehind="FormAuditoriaLoginOut.aspx.cs" Inherits="VistaWeb.FormAuditoriaLoginOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
         <br/>
        <asp:Button ID="btnVolver" runat="server" Text="Volver a menu" OnClick="btnVolver_Click" CssClass="btn-primary" />
        <br/><br/>
        <div class="form-group">
            <label for="txtNombreUsuario">Usuario:</label>
            <asp:TextBox ID="txtNombreUsuario" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="form-group">
            <asp:CheckBox ID="chbIngresos" runat="server" Text="Ingresos" Checked="True" />
            <asp:CheckBox ID="chbEgresos" runat="server" Text="Egresos" Checked="True" />
        </div> 
        
        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" CssClass="btn-primary" OnClick="btnFiltrar_Click" />
        <br/>
        <br/>

        <a href="#abajo">Ir a la parte de abajo</a>
        <asp:GridView ID="GrillaLogInOut" runat="server" AutoGenerateColumns="False" CssClass="table-condensed">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Operacion" HeaderText="Operacion" />
                <asp:BoundField DataField="Usuario" HeaderText="Usuario" />
                <asp:BoundField DataField="NombreEquipo" HeaderText="NombreEquipo" />
                <asp:BoundField DataField="FechaHora" HeaderText="FechaHora" />
            </Columns>
            <RowStyle BackColor="#EFF3FB" />
        </asp:GridView>
        <br />
        <a name="abajo"></a>
        
        <br />
        <br />
    </div>
</asp:Content>
