﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormUsuario.aspx.cs" Inherits="VistaWeb.Seguridad.FormUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <div class="form-group">
            <label for="txtNombreUsuario">Nombre de Usuario:</label>
            <asp:TextBox ID="txtNombreUsuario" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtNombreApellido">Nombre y apellido:</label>
            <asp:TextBox ID="txtNombreApellido" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtEmail">Email:</label>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <asp:DropDownList ID="dplPerfiles" runat="server"></asp:DropDownList>
        &nbsp;&nbsp;
   
        <asp:Button ID="btnAsignarPerfil" runat="server" Text="Asignar Perfil" OnClick="btnAsignarPerfil_Click" CssClass="btn btn-primary" />
        <br />
        <asp:ListBox ID="lsbPerfiles" runat="server" Width="190px"></asp:ListBox>

        <asp:Button ID="btnQuitarPerfil" runat="server" Text="Quitar Perfil" OnClick="btnQuitarPerfil_Click" CssClass="btn btn-primary"/>
        <br />
        <br />
        <asp:Label ID="lblError" runat="server" Font-Size="Small" ForeColor="#CC0000" CssClass="text-danger"></asp:Label>
        <br />
        <br /> 
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" CssClass="btn btn-default"/>
         
        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CssClass="btn btn-default"/>
    </div>

</asp:Content>
