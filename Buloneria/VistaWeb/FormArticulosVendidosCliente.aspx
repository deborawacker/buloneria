﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormArticulosVendidosCliente.aspx.cs" Inherits="VistaWeb.FormArticulosVendidosCliente" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <asp:Button ID="btnVolverAMenu" runat="server" Text="Volver a menu" CssClass="btn-primary" OnClick="btnVolverAMenu_Click" />
        <br />
        <br />
        <asp:Panel ID="pnlFiltro" runat="server">
            <label for="txtRazonSocial">Razon Social:</label>
            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control"></asp:TextBox>
            <br />
            <label for="txtArticulo">Articulo:</label>
            <asp:TextBox ID="txtArticulo" runat="server" CssClass="form-control"></asp:TextBox>
            <br />

            <asp:Label ID="txtDesde" runat="server" Text="Desde:"></asp:Label>
            <asp:Label ID="txtFechaDesde" runat="server" Text=""></asp:Label>
            <asp:ImageButton ID="btnCalendarFechaDesde" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/date_add.png" OnClick="btnCalendarFechaDesde_Click" Width="22px" />
            <asp:Calendar ID="calendarFechaDesde" runat="server" OnSelectionChanged="CalendarFechaDesde_SelectionChanged"
                ForeColor="#003399" Height="200px" Font-Size="8pt"
                Font-Names="Verdana" BorderColor="#3366CC" DayNameFormat="Shortest" CellPadding="1">
                <TodayDayStyle ForeColor="White" BackColor="#99CCCC"></TodayDayStyle>
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666"></SelectorStyle>
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF"></NextPrevStyle>
                <DayHeaderStyle
                    BackColor="#99CCCC" ForeColor="#336666" Height="1px"></DayHeaderStyle>
                <SelectedDayStyle Font-Bold="True" ForeColor="#CCFF99"
                    BackColor="#009999"></SelectedDayStyle>
                <TitleStyle Font-Bold="True" BorderColor="#3366CC"
                    BackColor="#003399" BorderWidth="1px" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px"></TitleStyle>
                <WeekendDayStyle BackColor="#CCCCFF"></WeekendDayStyle>
                <OtherMonthDayStyle ForeColor="#999999"></OtherMonthDayStyle>
            </asp:Calendar>
          
            <asp:Label ID="lblErrorDesde" runat="server" Font-Size="Small" ForeColor="#CC0000" Text=""></asp:Label>
              <br/>
            <asp:Label ID="txtHasta" runat="server" Text="Hasta:"></asp:Label>
            <asp:Label ID="txtFechaHasta" runat="server" Text=""></asp:Label>
            <asp:ImageButton ID="btnCalendarFechaHasta" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/date_add.png" OnClick="btnCalendarFechaHasta_Click" Width="22px" />
            <asp:Calendar ID="calendarFechaHasta" runat="server" OnSelectionChanged="CalendarFechaHasta_SelectionChanged"
                ForeColor="#003399" Height="200px" Font-Size="8pt"
                Font-Names="Verdana" BorderColor="#3366CC" DayNameFormat="Shortest" CellPadding="1">
                <TodayDayStyle ForeColor="White" BackColor="#99CCCC"></TodayDayStyle>
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666"></SelectorStyle>
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF"></NextPrevStyle>
                <DayHeaderStyle
                    BackColor="#99CCCC" ForeColor="#336666" Height="1px"></DayHeaderStyle>
                <SelectedDayStyle Font-Bold="True" ForeColor="#CCFF99"
                    BackColor="#009999"></SelectedDayStyle>
                <TitleStyle Font-Bold="True" BorderColor="#3366CC"
                    BackColor="#003399" BorderWidth="1px" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px"></TitleStyle>
                <WeekendDayStyle BackColor="#CCCCFF"></WeekendDayStyle>
                <OtherMonthDayStyle ForeColor="#999999"></OtherMonthDayStyle>
            </asp:Calendar>
            <asp:Label ID="lblErrorFechaHasta" runat="server" Font-Size="Small" ForeColor="#CC0000" Text=""></asp:Label>
            <br /><br />
            <asp:Button ID="btnEmitirReporte" runat="server" Text="Emitir Reporte" OnClick="btnEmitirReporte_Click" CssClass="btn-success" />
            <br />
            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
            <br /><br />
        </asp:Panel>

    </div>


</asp:Content>
