﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGestionarNotaPedido.aspx.cs" Inherits="VistaWeb.FormGestionarNotaPedido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <h4>Gestión de Notas de Pedido</h4>
        <br />
        <asp:Button ID="btnVolver" runat="server" OnClick="btnVolver_Click" Text="Volver a Menu" CssClass="btn-primary" />
        <br />
        <br />
        <hr/>

        <table border="0" style="width: 750px">
            <tbody>
                <tr>
                    <th style="width: 270px; padding-top: 10px"></th>
                    <th style="width: 150px">Nro.</th>
                    <th style="width: 150px">Fecha Emisión</th>
                    <th style="width: 150px">Estado</th>
                </tr>
            </tbody>
        </table>
        <div style="height: 300px; width: 750px; overflow-y: scroll; overflow-x: hidden">



            <asp:GridView ID="GrillaNotasPedidos" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaNotasPedidos_RowCommand" ShowHeader="false" OnRowDataBound="GrillaNotasPedidos_RowDataBound">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Consultar" Text="Consultar" ControlStyle-CssClass="btn-info center-block" ControlStyle-Width="100" />
                    <asp:ButtonField ButtonType="Button" CommandName="FinalizarConFaltante" Text="Finalizar con Faltante" ControlStyle-CssClass="btn-danger" ControlStyle-Width="150" />
                    <asp:BoundField DataField="NroNotaPedido" HeaderText="Nro NotaPedido" ItemStyle-Width="150" />
                    <asp:BoundField DataField="FechaEmisionCorta" HeaderText="Fecha Emisión" ItemStyle-Width="150" />
                    <asp:BoundField DataField="EstadoNotaPedido.Descripcion" HeaderText="Estado" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>
        </div>

        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        <br />

        <asp:Panel ID="PanelDetalleNotaPedido" runat="server" CssClass="container">
            <h4>Detalle Nota Pedido</h4>
            <label for="lblCliente">Cliente:</label>
            <asp:Label ID="lblCliente" runat="server"></asp:Label>
            <br />
            <label for="lblNotaPedido">Numero Nota Pedido:</label>
            <asp:Label ID="lblNotaPedido" runat="server"></asp:Label>
            <br />
            <label for="lblFechaEmision">Fecha Emisión:</label>
            <asp:Label ID="lblFechaEmision" runat="server"></asp:Label>
            <br />
            <label for="lblEstado">Estado:</label>
            <asp:Label ID="lblEstado" runat="server"></asp:Label>
            <br />

            <asp:GridView ID="GrillaLineasNotaPedido" runat="server" AutoGenerateColumns="False" CssClass="table-condensed">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                    <asp:BoundField DataField="CantPedida" HeaderText="Pedido" />
                    <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio Unitario" />
                    <asp:BoundField DataField="CantEntregada" HeaderText="Entregado" />
                    <asp:BoundField DataField="CantFaltanteEntrega" HeaderText="Falta Entregar" />
                    <asp:BoundField DataField="SubtotalIva" HeaderText="Subtotal IVA" />
                    <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
                </Columns>
            </asp:GridView>
            <br />

            <label for="lblSubtotal">Subtotal:</label>
            <asp:Label ID="lblSubtotal" runat="server"></asp:Label>
            <br />
            <label for="lblSubtotalIVA">Monto IVA:</label>
            <asp:Label ID="lblSubtotalIVA" runat="server"></asp:Label>
            <br />
            <label for="lblTotal">Total:</label>
            <asp:Label ID="lblTotal" runat="server"></asp:Label>

        </asp:Panel>

        <br />
        <br />
        <br />
    </div>
</asp:Content>
