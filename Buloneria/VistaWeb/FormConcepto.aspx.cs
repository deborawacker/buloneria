﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormConcepto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            
            Entidades.Sistema.Concepto concepto = new Entidades.Sistema.Concepto();
            concepto.Concepto1 = this.txtConcepto.Text;
            concepto.Descripcion = this.txtDescripcion.Text;
            //FALTA DROP DOWN LIST TIPO MOVIMIENTO... COMBO?

            Controladora.Sistema.ControladoraCUGConceptos.ObtenerInstancia().AgregarConcepto(concepto);
            Response.Redirect("FormGConceptos.aspx");
        }
    }
}