﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGenerarOrdenCompra : System.Web.UI.Page
    {
        #region SessionVariables
        private const String ArticuloSeleccionadoSessionVariableName = "ArticuloSeleccionado";
        private Articulo _ArticuloSeleccionadoSessionVariable;
        public Articulo ArticuloSeleccionadoSessionVariable
        {
            get
            {
                _ArticuloSeleccionadoSessionVariable = (Articulo)Session[ArticuloSeleccionadoSessionVariableName];
                return _ArticuloSeleccionadoSessionVariable;
            }
            set
            {
                _ArticuloSeleccionadoSessionVariable = value;
                Session[ArticuloSeleccionadoSessionVariableName] = _ArticuloSeleccionadoSessionVariable;
            }
        }

        private const String ProveedorSeleccionadoSessionVariableName = "ProveedorSeleccionado";
        private Proveedor _ProveedorSeleccionadoSessionVariable;
        public Proveedor ProveedorSeleccionadoSessionVariable
        {
            get
            {
                _ProveedorSeleccionadoSessionVariable = (Proveedor)Session[ProveedorSeleccionadoSessionVariableName];
                return _ProveedorSeleccionadoSessionVariable;
            }
            set
            {
                _ProveedorSeleccionadoSessionVariable = value;
                Session[ProveedorSeleccionadoSessionVariableName] = _ProveedorSeleccionadoSessionVariable;
            }
        }


        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[ArticuloSeleccionadoSessionVariableName] = _UserSessionVariable;
            }
        }

        private const String LineasOrdenCompraIngresadasSessionVariableName = "LineasOrdenCompraIngresadas";
        private List<LineaOrdenCompra> _LineasOrdenCompraIngresadasSessionVariable;

        public List<LineaOrdenCompra> LineasOrdenCompraIngresadasSessionVariable
        {
            get
            {
                _LineasOrdenCompraIngresadasSessionVariable = (List<LineaOrdenCompra>)Session[LineasOrdenCompraIngresadasSessionVariableName];

                return _LineasOrdenCompraIngresadasSessionVariable;
            }
            set
            {
                _LineasOrdenCompraIngresadasSessionVariable = value;

                Session[LineasOrdenCompraIngresadasSessionVariableName] = _LineasOrdenCompraIngresadasSessionVariable;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();

                this.HabilitarSeleccionProveedor();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.UserSessionVariable == null)
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

        }

        private void Inicializar()
        {
            this.InicializarFechaEmision();
            this.InicializarComboFormaPago();
            this.InicializarFechaPlazoPorDefault();
            this.InicializarValidacionesTextBoxs();
            this.InicializarPaneles();
            this.InicializarListaSession();
        }

        private void InicializarValidacionesTextBoxs()
        {
            this.txtCantidadArticulo.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
        }

        private void InicializarListaSession()
        {
            this.LineasOrdenCompraIngresadasSessionVariable = new List<LineaOrdenCompra>();
        }

        private void InicializarFechaPlazoPorDefault()
        {
            //Por defecto: sumo 15 dias a la fecha de emision (hoy)
            DateTime fechaPlazo = DateTime.Now.AddDays(15);
            txtFechaPlazo.Text = fechaPlazo.ToShortDateString();
        }

        private void InicializarComboFormaPago()
        {
            this.ddlFormaPago.DataSource = Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().RecuperarFormasPago();
            ddlFormaPago.DataTextField = "TipoFormaPago";
            ddlFormaPago.DataBind();
        }

        private void InicializarPaneles()
        {
            this.PanelSeleccionProveedor.Visible = true;
            this.PanelIngresoOrdenCompra.Visible = false;
        }

        private void InicializarFechaEmision()
        {
            this.lblFechaEmision.Text = DateTime.Now.Date.ToShortDateString();
        }

        private void HabilitarSeleccionProveedor()
        {
            this.PanelSeleccionProveedor.Visible = true;

            this.InicializarGrillaProveedores(); 
        }

        private void InicializarGrillaProveedores()
        {
            this.GrillaProveedores.DataSource = Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().RecuperarProveedores();
            this.GrillaProveedores.DataBind();
        }

        private void HabilitarIngresoOrdenCompra(Proveedor proveedorSeleccionado)
        {
            this.PanelSeleccionProveedor.Visible = false;
            this.PanelIngresoOrdenCompra.Visible = true;

            this.MostrarDatosProveedor(proveedorSeleccionado);

            this.InicializarGrillaArticulosDisponiblesProveedorSeleccionado(proveedorSeleccionado);
        }

        private void InicializarGrillaArticulosDisponiblesProveedorSeleccionado(Proveedor proveedorSeleccionado)
        {
            this.GrillaArticulosDisponiblesProveedorSeleccionado.DataSource = Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().RecuperarArticulosProveedor(proveedorSeleccionado);
            this.GrillaArticulosDisponiblesProveedorSeleccionado.DataBind();
        }

        private void MostrarDatosProveedor(Proveedor proveedorSeleccionado)
        {
            this.lblNombreProveedor.Text = proveedorSeleccionado.NombreFantasia;
            this.lblCUITProveedor.Text = proveedorSeleccionado.Cuit.ToString();
            this.lblDireccion.Text = proveedorSeleccionado.Direccion;
            this.lblTelefono.Text = proveedorSeleccionado.Telefono;
        }

        private Proveedor BuscarProveedor(int codProveedorBuscado)
        {
            return Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().BuscarProveedor(codProveedorBuscado);
        }

        private Articulo BuscarArticulo(int idArticuloBuscado)
        {
            return Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().BuscarArticulo(idArticuloBuscado);
        }

        private void ValidarFechaPlazo()
        {
            String mje;
            DateTime fechaEmision = DateTime.Now;
            DateTime fechaPlazo = DateTime.Parse(calendarFechaPlazo.SelectedDate.ToString());

            //Si fechaPlazo es menor que fechaEmision
            if (fechaPlazo.Date.CompareTo(fechaEmision.Date) < 0)
            {
                mje = Textos.LaFechaIngresadaDebeSerMayorOIgualALaActual;
            }
            else
            {
                txtFechaPlazo.Text = calendarFechaPlazo.SelectedDate.ToShortDateString();
                mje = "";
            }

            lblErrorFecha.Text = mje;

            if (calendarFechaPlazo.Visible)
            {
                calendarFechaPlazo.Visible = false;
            }
        }

        private void HabilitarIngresoCantidadArticulo(Articulo articuloSeleccionado)
        {
            this.PanelIngresoCantidadArticulo.Visible = true;

            this.MostrarDatosArticulo(articuloSeleccionado);
        }

        private void MostrarDatosArticulo(Articulo articuloSeleccionado)
        {
            this.lblDescripcionArticulo.Text = articuloSeleccionado.Descripcion;
        }

        private void AgregarCantidadArticuloSeleccionado(Articulo articulo, int cantidadIngresada)
        {
            if (!this.ArticuloYaFueSeleccionado(articulo))
            {
                LineaOrdenCompra lineaOrdenCompra = new LineaOrdenCompra();

                lineaOrdenCompra.Articulo = articulo;
                lineaOrdenCompra.CantPedir = cantidadIngresada;
                lineaOrdenCompra.PrecioUnitario = articulo.PrecioVenta;

                this.LineasOrdenCompraIngresadasSessionVariable.Add(lineaOrdenCompra);
            }
            else
            {
                //Si el articulo ya fue agregado en una linea de orden de compra, no agrega otra linea a la lista sino que agrega mas cantidad
                LineaOrdenCompra lineaOrdenCompra = this.LineasOrdenCompraIngresadasSessionVariable.Where(l => l.Articulo.IdArticulo == articulo.IdArticulo).ToList().FirstOrDefault();

                lineaOrdenCompra.CantPedir += cantidadIngresada;
            }

            this.RefrescarGrillaArticulosAgregadosOrdenCompra();
        }

        private void RefrescarGrillaArticulosAgregadosOrdenCompra()
        {
            this.GrillaArticulosAgregadosOrdenCompra.DataSource = this.LineasOrdenCompraIngresadasSessionVariable;
            this.GrillaArticulosAgregadosOrdenCompra.DataBind();

            this.VerificarHabilitacionBotonAceptarCancelar();
        }

        private void VerificarHabilitacionBotonAceptarCancelar()
        {
            this.PanelAceptarCancelar.Visible = false;
            if (this.LineasOrdenCompraIngresadasSessionVariable.Count > 0)
            {
                this.PanelAceptarCancelar.Visible = true;
            }
        }

        private bool ArticuloYaFueSeleccionado(Articulo articulo)
        {
            return this.LineasOrdenCompraIngresadasSessionVariable.Any(loc => loc.Articulo.IdArticulo == articulo.IdArticulo);
        }

        private void OcultarIngresoCantidad()
        {
            this.PanelIngresoCantidadArticulo.Visible = false;
        }

        private bool ValidarIngresoCantidad()
        {
            string mjeError = String.Empty;
            string strIngCant = txtCantidadArticulo.Text;
            int cantIngresada = 0;
            Boolean valido = true;

            if (!int.TryParse(strIngCant, out cantIngresada))
                valido = false;

            if (!(cantIngresada > 0))
                valido = false;

            if (!valido)
                mjeError += Textos.DebeIngresarUnValorMayorACero;

            this.lblErrorIngresoCantidadArticulo.Text = mjeError;
            return valido;
        }

        private void EliminarLineaOrdenCompraSeleccionada(string strCodArticulo)
        {
            LineaOrdenCompra locAEliminar = this.LineasOrdenCompraIngresadasSessionVariable.Where(loc => loc.Articulo.CodArticulo == strCodArticulo).ToList().FirstOrDefault();

            this.LineasOrdenCompraIngresadasSessionVariable.Remove(locAEliminar);

            this.RefrescarGrillaArticulosAgregadosOrdenCompra();

            this.ActualizarTotales();
        }

        private void GenerarNuevaOrdenCompra()
        {
            OrdenCompra ordenCompra = new OrdenCompra();

            ordenCompra.Usuario = this.UserSessionVariable.NombreUsuario;

            ordenCompra.Proveedor = this.ProveedorSeleccionadoSessionVariable;

            ordenCompra.LineasOrdenCompra.AddRange(this.LineasOrdenCompraIngresadasSessionVariable);

            Entidades.Sistema.FormaPago formaPagoOC = new Entidades.Sistema.FormaPago();
            formaPagoOC.TipoFormaPago = this.ddlFormaPago.SelectedValue;
            ordenCompra.FormaPago = formaPagoOC;

            ordenCompra.FechaEmision = DateTime.Now;

            ordenCompra.FechaPlazo = DateTime.Parse(this.txtFechaPlazo.Text);

            if (Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().AgregarOrdenCompra(ordenCompra, this.UserSessionVariable))
            {
                //Si esta toodo bien redirecciona y muestra mensaje OK
                String mje = "La orden de compra nro " + ordenCompra.NroOrdenCompra + " se generó exitosamente";

                //Borra los datos de la OC realizada para no superponer si se desea hacer otra OC.
                //Session["proveedor"] = null;

                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);


            }
        }

        private void ActualizarTotales()
        {
            this.lblSubtotal.Text = this.CalcularSubTotal();
            this.lblIVA.Text = this.CalcularMontoIva();
            this.lblTotal.Text = this.Calculartotal();
        }

        private string CalcularMontoIva()
        {
            return this.LineasOrdenCompraIngresadasSessionVariable.Sum(l => l.SubtotalIva).ToString();
        }

        private string Calculartotal()
        {
            return this.LineasOrdenCompraIngresadasSessionVariable.Sum(l => l.Subtotal + l.SubtotalIva).ToString();
        }

        private string CalcularSubTotal()
        {
            return this.LineasOrdenCompraIngresadasSessionVariable.Sum(l => l.Subtotal).ToString();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        #region Events

        protected void btnCalendarFechaPlazo_Click(object sender, EventArgs e)
        {
            calendarFechaPlazo.Visible = !calendarFechaPlazo.Visible;
        }

        protected void GrillaProveedores_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "SeleccionarProveedor")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaProveedores.Rows[index];
                TableCell tbcCodProveedor = selectedRow.Cells[1];
                string strCodProveedor = tbcCodProveedor.Text;
                int codProveedorBuscado = Convert.ToInt32(strCodProveedor);

                Proveedor proveedorSeleccionado = this.BuscarProveedor(codProveedorBuscado);

                this.ProveedorSeleccionadoSessionVariable = proveedorSeleccionado;

                this.HabilitarIngresoOrdenCompra(proveedorSeleccionado);
            }
        }

        protected void CalendarFechaPlazo_SelectionChanged(object sender, EventArgs e)
        {
            this.ValidarFechaPlazo();
        }

        protected void GrillaArticulosDisponiblesProveedorSeleccionado_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == Textos.SeleccionarCommand)
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaArticulosDisponiblesProveedorSeleccionado.Rows[index];
                TableCell tbcIdArticulo = selectedRow.Cells[1];
                string strIdArticulo = tbcIdArticulo.Text;
                int idArticuloBuscado = Convert.ToInt32(strIdArticulo);

                Articulo articuloSeleccionado = this.BuscarArticulo(idArticuloBuscado);

                this.ArticuloSeleccionadoSessionVariable = articuloSeleccionado;

                this.HabilitarIngresoCantidadArticulo(articuloSeleccionado);
            }
        }

        protected void btnIngresarCantidad_Click(object sender, EventArgs e)
        {
            if (this.ValidarIngresoCantidad())
            {
                this.OcultarIngresoCantidad();

                int cantidadIngresada = int.Parse(txtCantidadArticulo.Text);

                this.AgregarCantidadArticuloSeleccionado(this.ArticuloSeleccionadoSessionVariable, cantidadIngresada);

                this.ActualizarTotales();

            }
        }

        protected void GrillaArticulosAgregadosOrdenCompra_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == Textos.EliminarCommand)
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaArticulosAgregadosOrdenCompra.Rows[index];
                TableCell tbcCodArticulo = selectedRow.Cells[1];
                string strCodArticulo = tbcCodArticulo.Text;

                this.EliminarLineaOrdenCompraSeleccionada(strCodArticulo);

            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            this.GenerarNuevaOrdenCompra();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnVolverAMenu_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }
        #endregion 
    }
}