﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades.Sistema;
using System.Drawing;
using System.Net.Mime;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormEgresoMercaderia : System.Web.UI.Page
    {
        #region Properties

        private const String IndexFilaPropertyName = "IndexFila";
        public Int32 IndexFila { get; set; }


        private List<NotaPedido> ListaNotasPedido { get; set; }

        private List<LineaNotaPedido> ListaLineasNotasPedido { get; set; }

        private const String ListaLineasNotasPedidoSeleccionadasPropertyName = "ListaLineasNotasPedidoSeleccionadas";
        public List<LineaNotaPedido> ListaLineasNotasPedidoSeleccionadas { get; set; }

        private const String NotaPedidoSeleccionadaPropertyName = "NotaPedidoSeleccionada";
        private NotaPedido NotaPedidoSeleccionada { get; set; }

        public NotaPedidoCriterio NotaPedidoCriterio { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                this.Inicializar();
            }
            else
            {
                this.ResetFormulario();
            }
        }

        private void Inicializar()
        {
            this.lblFechaEgreso.Text = DateTime.Now.ToShortDateString();

            IsVisibleFiltroListaDetalle(true, false, false);

            PanelIngresoCantidad.Visible = false;

            this.InicializarValidaciones();

            this.InicializarListasSession();

        }

        private void InicializarValidaciones()
        {
            this.txtCodigoCliente.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtNroNotaPedido.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
            this.txtCantidadEntregada.Attributes.Add("OnKeyPress", "return AcceptNum(event)");
        }

        private void InicializarListasSession()
        {
            this.ListaLineasNotasPedidoSeleccionadas = new List<LineaNotaPedido>();
            Session[ListaLineasNotasPedidoSeleccionadasPropertyName] = this.ListaLineasNotasPedidoSeleccionadas;
        }

        #region Filtro

        protected void btnBuscarNotaPedido_Click(object sender, EventArgs e)
        {
            try
            {
                #region ObtenerDatosIngresados
                int nroNotaPedido = 0;
                int codCliente = 0;

                String nroNotaPedidoStr = this.txtNroNotaPedido.Text.Trim();
                if (!nroNotaPedidoStr.Equals(String.Empty))
                {
                    nroNotaPedido = int.Parse(nroNotaPedidoStr);
                }

                String codClienteStr = this.txtCodigoCliente.Text.Trim();
                if (!codClienteStr.Equals(String.Empty))
                {
                    codCliente = int.Parse(codClienteStr);
                }


                String razonSocial = this.txtRazonSocial.Text;

                NotaPedidoCriterio npCriterio = new NotaPedidoCriterio();

                npCriterio.NroNotaPedido = nroNotaPedido;
                npCriterio.CodCliente = codCliente;
                npCriterio.RazonSocial = razonSocial;

                this.NotaPedidoCriterio = npCriterio;

                #endregion

                if (this.NotaPedidoCriterio.EstaVacio())
                    throw new Exception(Textos.DebeIngresarAlMenosUnCriterio);

                ListaNotasPedido = Controladora.Sistema.ControladoraCUEgresoMercaderia.ObtenerInstancia().BuscarNotasDePedidoPorCriterio(npCriterio);

                this.GrillaNotasPedido.DataSource = ListaNotasPedido;
                this.GrillaNotasPedido.DataBind();

                if (ListaNotasPedido.Any())
                    IsVisibleFiltroListaDetalle(true, true, false);
                else
                    throw new Exception(Textos.NoSeEncontraronResultados);

            }
            catch (Exception ex)
            {
                Notify(lblMensajeFiltro, ex.Message, TypeNotif.Error);
            }


        }

        protected void btnLimpiarCampos_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();
        }

        private void LimpiarControles()
        {
            Session["notaPedidoSeleccionada"] = null;
            Session[ListaLineasNotasPedidoSeleccionadasPropertyName] = new List<LineaNotaPedido>();
            this.ListaNotasPedido = null;
            this.ListaLineasNotasPedido = null;

            this.GrillaArticulosAEntregar.DataBind();

            this.txtCodigoCliente.Text = String.Empty;
            this.txtNroNotaPedido.Text = String.Empty;
            this.txtRazonSocial.Text = String.Empty;

            this.lblMensajeFiltro.Text = String.Empty;
            this.lblMensajeLista.Text = String.Empty;
            this.lblMensajeDetalle.Text = String.Empty;
        }
        #endregion

        #region Lista
        protected void GrillaNotasPedido_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Seleccionar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow selectedRow = GrillaNotasPedido.Rows[index];
                TableCell tbcNroNotaPedido = selectedRow.Cells[1];
                string strNroNotaPedido = tbcNroNotaPedido.Text;
                int nroNotaPedidoBuscada = Convert.ToInt32(strNroNotaPedido);

                this.SeleccionarNotaPedido(nroNotaPedidoBuscada);
            }
        }

        private void SeleccionarNotaPedido(int nroNotaPedidoBuscada)
        {
            NotaPedido notaPedidoSeleccionada = Controladora.Sistema.ControladoraCUEgresoMercaderia.ObtenerInstancia().BuscarNotasDePedidoPorNro(nroNotaPedidoBuscada);

            Session["notaPedidoSeleccionada"] = notaPedidoSeleccionada;

            IsVisibleFiltroListaDetalle(false, false, true);

            MostrarNotaPedido(notaPedidoSeleccionada);
        }

        protected void btnVolverAFiltro_Click(object sender, EventArgs e)
        {
            this.IsVisibleFiltroListaDetalle(true, false, false);

            this.LimpiarControles();
        }

        private void MostrarNotaPedido(NotaPedido notaPedidoSeleccionada)
        {
            this.lblNroNotaPedido.Text = notaPedidoSeleccionada.NroNotaPedido.ToString();
            this.lblRazonSocialCliente.Text = notaPedidoSeleccionada.Cliente.RazonSocial;

            GrillaDetalleArticulosNotaPedido.DataSource = null;
            GrillaDetalleArticulosNotaPedido.DataSource = notaPedidoSeleccionada.LineasNotaPedido;
            GrillaDetalleArticulosNotaPedido.DataBind();

        }
        #endregion

        #region Detalle
        protected void GrillaDetalleArticulosNotaPedido_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Seleccionar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                Session[IndexFilaPropertyName] = index;
                GridViewRow selectedRow = GrillaDetalleArticulosNotaPedido.Rows[index];
                TableCell tbcCodigoArticulo = selectedRow.Cells[2];
                string CodigoArticuloBuscado = tbcCodigoArticulo.Text;

                TableCell tbcNroLineaNotaPedido = selectedRow.Cells[1];
                string NroLineaNotaPedidoBuscadaString = tbcNroLineaNotaPedido.Text;
                int NroLineaNotaPedidoBuscada = int.Parse(NroLineaNotaPedidoBuscadaString);

                this.SeleccionarArticulo(CodigoArticuloBuscado);
                this.SeleccionarLineaNotaPedido(NroLineaNotaPedidoBuscada);
            }
        }
        private void SeleccionarArticulo(string CodigoArticuloBuscado)
        {
            ArticuloCriterio criterio = new ArticuloCriterio();
            criterio.CodigoArticulo = CodigoArticuloBuscado;

            Articulo articuloSeleccionado = Controladora.Sistema.ControladoraCUEgresoMercaderia.ObtenerInstancia().BuscarArticuloPorCriterio(criterio);

            PanelIngresoCantidad.Visible = true;

            Session["articuloSeleccionado"] = articuloSeleccionado;

            MostrarArticuloSeleccionado(articuloSeleccionado);
        }

        private void SeleccionarLineaNotaPedido(int NroLineaNotaPedidoBuscada)
        {
            this.NotaPedidoSeleccionada = (NotaPedido)Session["notaPedidoSeleccionada"];

            LineaNotaPedido lineaNotaPedidoSeleccionada = Controladora.Sistema.ControladoraCUEgresoMercaderia.ObtenerInstancia().BuscarLineaNotaPedidoEnNotaPedido(NroLineaNotaPedidoBuscada, this.NotaPedidoSeleccionada);

            Session["lineaNotaPedidoSeleccionada"] = lineaNotaPedidoSeleccionada;

        }

        private void MostrarArticuloSeleccionado(Articulo articuloSeleccionado)
        {
            this.lblNombreArticulo.Text = articuloSeleccionado.Descripcion;
        }

        protected void btnIngresarCantidad_Click(object sender, EventArgs e)
        {

            String cantidadStr = this.txtCantidadEntregada.Text;

            int cantidad = 0;

            if (ConvertirACantidadEntera(cantidadStr, out cantidad))
            {
                Articulo articuloSeleccionado = (Articulo)Session["articuloSeleccionado"];

                LineaNotaPedido lineaNotaPedidoSeleccionada = (LineaNotaPedido)Session["lineaNotaPedidoSeleccionada"];

                if (cantidad > lineaNotaPedidoSeleccionada.CantFaltanteEntrega)
                {
                    lblMensajeDetalle.Text = String.Format(Textos.NoPuedeIngresarUnaCantidadMayorAX, "la linea");
                    return;
                }

                lineaNotaPedidoSeleccionada.CantEntregada += cantidad;

                lineaNotaPedidoSeleccionada.Articulo.CantActual -= cantidad;

                AgregarArticuloAEntregar(lineaNotaPedidoSeleccionada);

                PanelIngresoCantidad.Visible = false;

                this.PanelGrillaArticulosAEntregar.Visible = true;

            }

        }

        private void AgregarArticuloAEntregar(LineaNotaPedido lineaNotaPedidoSeleccionada)
        {
            this.ListaLineasNotasPedidoSeleccionadas = (List<LineaNotaPedido>)Session[ListaLineasNotasPedidoSeleccionadasPropertyName];

            this.ListaLineasNotasPedidoSeleccionadas.Add(lineaNotaPedidoSeleccionada);

            this.btnGuardar.Visible = true;

            ActualizarGrillaArticulosEntregados();
        }

        private void ActualizarGrillaArticulosEntregados()
        {
            this.GrillaArticulosAEntregar.DataSource = this.ListaLineasNotasPedidoSeleccionadas;
            this.GrillaArticulosAEntregar.DataBind();

            this.IndexFila = (Int32)Session[IndexFilaPropertyName];

            GridViewRow selectedRow = GrillaDetalleArticulosNotaPedido.Rows[this.IndexFila];
            selectedRow.Enabled = false;

            this.PanelGrillaArticulosAEntregar.Visible = true;
        }

        private bool ConvertirACantidadEntera(string cantidadStr, out int cantidad)
        {
            cantidad = 0;

            try
            {
                this.lblMensajeDetalle.Text = "";
                cantidad = int.Parse(cantidadStr);
                if (cantidad <= 0)
                {
                    this.lblMensajeDetalle.Text = Textos.DebeIngresarUnValorMayorACero;
                    return false;
                }
                return cantidad > 0;
            }
            catch (Exception)
            {
                this.lblMensajeDetalle.Text = Textos.DebeIngresarUnValorMayorACero;
                return false;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            this.EgresarMercaderia();

            PanelGrillaArticulosAEntregar.Visible = false;
        }

        private void ResetFormulario()
        {
            lblMensajeFiltro.Text = string.Empty;

            lblMensajeLista.Text = string.Empty;

            lblMensajeDetalle.Text = string.Empty;

        }

        private void EgresarMercaderia()
        {
            try
            {
                this.ListaLineasNotasPedidoSeleccionadas = (List<LineaNotaPedido>)Session[ListaLineasNotasPedidoSeleccionadasPropertyName];

                this.NotaPedidoSeleccionada = (NotaPedido)Session[NotaPedidoSeleccionadaPropertyName];

                Controladora.Sistema.ControladoraCUEgresoMercaderia.ObtenerInstancia().ActualizarEgresoMercaderia(this.NotaPedidoSeleccionada);

                IsVisibleFiltroListaDetalle(true, false, false);

                String detail = String.Format(Textos.NotaPedidoXPasoAEstadoY, this.NotaPedidoSeleccionada.NroNotaPedido, this.NotaPedidoSeleccionada.EstadoNotaPedido.Descripcion);

                this.LimpiarControles();

                this.Notify(lblMensajeFiltro, String.Format(Textos.SeHanGuardadoLosCambiosDetalleX, detail), TypeNotif.Success);
            }
            catch (Exception ex)
            {
                this.Notify(lblMensajeFiltro, ex.Message, TypeNotif.Error);
            }

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.IsVisibleFiltroListaDetalle(false, true, false);
        }

        private void IsVisibleFiltroListaDetalle(bool isVisibleFiltro, bool isVisibleLista, bool isVisibleDetalle)
        {
            this.PanelFiltro.Visible = isVisibleFiltro;
            this.PanelLista.Visible = isVisibleLista;
            this.PanelDetalle.Visible = isVisibleDetalle;
        }

        protected void GrillaArticulosAEntregar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }


        #endregion


        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }
    }
}