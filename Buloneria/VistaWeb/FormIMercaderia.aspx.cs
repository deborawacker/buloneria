﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormIMercaderia : System.Web.UI.Page
    {
        //private List<Entidades.Sistema.OrdenCompra> _ordenesCompra;
        private Entidades.Sistema.OrdenCompra _ordenCompra;
        private Sesion.SesionIngresoMercaderia _sesionIM;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _sesionIM = new Sesion.SesionIngresoMercaderia();

                //Guarda el objeto sesion con todos los datos necesarios en la sesion para que no se pierdan los datos
                Session.Add("sesionIM", _sesionIM);

                List<Entidades.Sistema.OrdenCompra> ocPendYEC = null; //Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().RecuperarOrdenesComprasEnCursoYPendientes();

                if (ocPendYEC.Count == 0)
                {
                    pnlSeleccionarNroOC.Visible = false;
                    lblMensajeInicio.Text = "No hay Ordenes de compra en curso y/o pendientes";
                }
                else
                {
                    //Combo
                    dplNrosOC.DataSource = ocPendYEC;
                    dplNrosOC.DataTextField = "NroOrdenCompra";
                    dplNrosOC.DataBind();
                }
                

            }
            else
            {

                lblErrorCantRecibida.Text = " "; 
            }

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario != null)
            {

                this.txtFechaRecepcion.Text = DateTime.Now.Date.ToString(); 

                pnlSeleccionarNroOC.Visible = true;
                pnlDatosOC.Visible = false;
                pnlAgregarArticuloRecibido.Visible = false;


            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }
        }

        protected void btnSeleccionarNroOC_Click(object sender, EventArgs e)
        {
            //Recupero datos sesion IM de la sesion
            _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"];

            string strNroOCSeleccionado = this.dplNrosOC.SelectedValue;
            int nroOCSeleccionado = int.Parse(strNroOCSeleccionado);
            // BUSCA POR ORDEN COMPRA
            //_ordenCompra = (Entidades.Sistema.OrdenCompra)buscarOrdenCompraPorNro(nroOCSeleccionado);
            List<Entidades.Sistema.OrdenCompra> ocPendYEC = null;// Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().RecuperarOrdenesComprasEnCursoYPendientes();
            _ordenCompra = null;//buscarOrdenCompraPorNroEn(nroOCSeleccionado, ocPendYEC);
            
            _sesionIM.OrdenCompra = _ordenCompra;
            lblNroOrdenCompra.Text = _ordenCompra.NroOrdenCompra.ToString();
            lblProveedor.Text = _ordenCompra.Proveedor.RazonSocial;
            lblFechaOrdenCompra.Text = _ordenCompra.FechaEmision.ToShortDateString();
            lblEstadoOC.Text = _ordenCompra.Estado.ToString();
             

            dgvArticulosOC.DataSource = _ordenCompra.LineasOrdenCompra;
            dgvArticulosOC.DataBind(); 

            pnlSeleccionarNroOC.Visible = false;
            pnlDatosOC.Visible = true;
        }

        protected void dgvArticulosOC_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            if (e.CommandName == "Seleccionar")
            {
                //Recupero datos sesion IM de la sesion
                _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"];

                int index = Convert.ToInt32(e.CommandArgument);
                _sesionIM.IndexFila = index;

                // Get the last name of the selected author from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = dgvArticulosOC.Rows[index];
                TableCell tbcIdLOC = selectedRow.Cells[1];
                string strIdLOC = tbcIdLOC.Text;
                int nroLOCBuscada = Convert.ToInt32(strIdLOC);

                // BUSCA POR LINEA DE ORDEN COMPRA buscarOrdenCompraPorNroEn
                //Entidades.Sistema.LineaOrdenCompra locElegida = (Entidades.Sistema.LineaOrdenCompra)buscarLineaOrdenCompraPorNro(nroLOCBuscada);
                Entidades.Sistema.LineaOrdenCompra locElegida = null;// buscarLineaOrdenCompraPorNroEn(nroLOCBuscada, _sesionIM.OrdenCompra.LineasOrdenCompra);


                lblCantPedida.Text = locElegida.CantPedir.ToString();
                lblNombreArticulo.Text = locElegida.Articulo.Descripcion; 
                lblCantFaltanteActual.Text = locElegida.CantFaltante.ToString();

                _sesionIM.LineaOrdenCompraElegida = locElegida;

                pnlAgregarArticuloRecibido.Visible = true;
                 
            }
        }

      
        protected void btnIngresarCantidadRecibida_Click(object sender, EventArgs e)
        {
            bool valido = true;
            string mjeError = string.Empty;

             //Valida que el campo no este vacio
            if (txtCantidadRecibida.Text.Equals(String.Empty))
            {
                mjeError += "Debe ingresar una cantidad <br/>";
                valido = false;
            }

            //Valida que la cantRecibida no sea menor o igual a cero
            if (!txtCantidadRecibida.Text.Equals(String.Empty))
            {
                int cantRecibida = int.Parse(txtCantidadRecibida.Text.ToString());
                if (cantRecibida <= 0)
                {
                    mjeError += "Debe ingresar una cantidad mayor que 0 <br/>";
                    valido = false;
                }
            }

            //Valida que no se ingrese una cantidad mayor a la faltante
            if (valido)
            {
                int cantRecibida = int.Parse(txtCantidadRecibida.Text.ToString());
                //Recupero datos sesion IM de la sesion
                _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"]; 

                Entidades.Sistema.LineaOrdenCompra linea = _sesionIM.LineaOrdenCompraElegida;

                if (cantRecibida > linea.CantFaltante)
                {
                    mjeError += "No puede ingresar una cantidad mayor a la faltante <br/>";
                    valido = false;
                } 
            }


            if (valido)
            {
                //Recupera la cantidad recibida ingresada
                int cantRecibida = int.Parse(txtCantidadRecibida.Text.ToString()); 

                btnAceptar.Enabled = true;

                //Recupero datos sesion IM de la sesion
                _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"];
                 
                Entidades.Sistema.LineaOrdenCompra linea = _sesionIM.LineaOrdenCompraElegida;
                 
                //Actualiza en la linea
                linea.CantRecibida += cantRecibida;
                linea.CantIngresada = cantRecibida;

                _sesionIM.LineasArticulosRecibidos.Add(linea);

                dgvArticulosRecibidos.DataSource = _sesionIM.LineasArticulosRecibidos;
                dgvArticulosRecibidos.DataBind();

                pnlAgregarArticuloRecibido.Visible = false;

                GridViewRow selectedRow = dgvArticulosOC.Rows[_sesionIM.IndexFila];
                selectedRow.Enabled = false;
            }
            else
            {
                lblErrorCantRecibida.Text = mjeError;
            }

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            //Recupero datos sesion IM de la sesion
            _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"];
            //Si hubo ingreso de  mercaderia
            if (_sesionIM.LineasArticulosRecibidos.Count > 0)
            {
                Controladora.Sistema.ControladoraCUIngresoMercaderia.ObtenerInstancia().IngresarMercaderia(_sesionIM.OrdenCompra, _usuario);
                string mje = "La orden de compra " + _sesionIM.OrdenCompra.NroOrdenCompra + " paso a estado: " + _sesionIM.OrdenCompra.Estado;


                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

       
        /*private Entidades.Sistema.LineaOrdenCompra buscarLineaOrdenCompraPorNro(int nroLOCBuscado)
        {
            _sesionIM = (Sesion.SesionIngresoMercaderia)Session["sesionIM"];
            List<Entidades.Sistema.LineaOrdenCompra> _listaLOC = _sesionIM.OrdenCompra.LineasOrdenCompra;
            Entidades.Sistema.LineaOrdenCompra locBuscada = _listaLOC.Find(delegate(Entidades.Sistema.LineaOrdenCompra loc) { return loc.NroLineaOC == nroLOCBuscado; });
            return locBuscada;
        }

        private Entidades.Sistema.OrdenCompra buscarOrdenCompraPorNro(int nroOCBuscado)
        { 
            List<Entidades.Sistema.OrdenCompra> _listaOC=Controladora.Sistema.ControladoraCUIngresarMercaderia.ObtenerInstancia().RecuperarOrdenesComprasEnCursoYPendientes();
            Entidades.Sistema.OrdenCompra ocBuscada = _listaOC.Find(delegate(Entidades.Sistema.OrdenCompra oc) { return oc.NroOrdenCompra == nroOCBuscado; });
            return ocBuscada;
        }*/

        protected void btnCancelarIngreso_Click(object sender, EventArgs e)
        {
            pnlAgregarArticuloRecibido.Visible = false;
        }

        

    }
}