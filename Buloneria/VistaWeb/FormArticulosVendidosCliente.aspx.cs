﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormArticulosVendidosCliente : System.Web.UI.Page
    {
        public DateTime hasta;
        public DateTime desde;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEmitirReporte_Click(object sender, EventArgs e)
        {
            String Articulo = this.txtArticulo.Text.Trim();
            String razonSocial = txtRazonSocial.Text.Trim(); 


            //cambiar a la lista que corresponda para el reporte
            List<Entidades.Sistema.Reportes.ArticulosPedidosXClientes> listaClientes = Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarClientePorArticulosVendidos(Articulo, razonSocial, calendarFechaDesde.SelectedDate, calendarFechaHasta.SelectedDate);


            

            if (listaClientes != null && listaClientes.Count > 0)
            {
                lblMensaje.Text = "";

                generarReporte();
                
            }
            else
            {
                lblMensaje.Text = Textos.NoSeEncontroNingunResultadoConLosCriteriosDeBusquedaIngresados;
            }

        }
        protected void CalendarFechaDesde_SelectionChanged(object sender, EventArgs e)
        {

            desde = DateTime.Parse(calendarFechaDesde.SelectedDate.ToString());
            string mje;
            lblErrorDesde.Text = "";

            ////Si fechaPlazo es menor que fechaEmision
            if (desde.Date.CompareTo(DateTime.Now) > 0)
            {
                txtFechaDesde.Text = "";
                mje = "Debe ingresar una fecha de plazo valida";
                lblErrorDesde.Text = mje;
            }
            else
            {
                txtFechaDesde.Text = calendarFechaDesde.SelectedDate.ToShortDateString();
                mje = "";
            }

            if (calendarFechaDesde.Visible)
            {
                calendarFechaDesde.Visible = false;
            }


        }
        protected void CalendarFechaHasta_SelectionChanged(object sender, EventArgs e)
        {
            hasta = DateTime.Parse(calendarFechaHasta.SelectedDate.ToString());
            string mje;
            lblErrorFechaHasta.Text = "";
            
            //Si fechaPlazo es menor que fechaEmision
            if (hasta.Date.CompareTo(calendarFechaDesde.SelectedDate) < 0)
            {
                txtFechaHasta.Text = "";
                mje = "Debe ingresar una fecha de plazo valida";
                lblErrorFechaHasta.Text = mje;
            }
            else
            {
                txtFechaHasta.Text = calendarFechaHasta.SelectedDate.ToShortDateString();
                mje = "";
            }

            if (calendarFechaHasta.Visible)
            {
                calendarFechaHasta.Visible = false;
            }


        }

        protected void btnCalendarFechaDesde_Click(object sender, EventArgs e)
        {
            if (calendarFechaDesde.Visible)
            {
                calendarFechaDesde.Visible = false;
            }
            else
            {
                calendarFechaDesde.Visible = true;
            }

        }
        protected void btnCalendarFechaHasta_Click(object sender, EventArgs e)
        {
            if (calendarFechaHasta.Visible)
            {
                calendarFechaHasta.Visible = false;
            }
            else
            {
                calendarFechaHasta.Visible = true;
            }

        } 

        protected void grdClientes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int indexGrilla = Convert.ToInt32(e.CommandArgument);
            switch (e.CommandName)
            {
                case "VerReporte":
                    generarReporte();
                    break;
                default:
                    break;
            }
        }

        private void generarReporte()
        {


            Session["DateTimeInicio"] = txtFechaDesde.Text;
            Session["DateTimeFin"] = txtFechaHasta.Text;
            Session["RazonSocial"] = txtRazonSocial.Text;
            Session["ArticuloDesc"] = txtArticulo.Text;
            //Session["codCli"] = "4";//txtArticulo.Text;
            //Page.Response.Redirect("FormArtXCliente.aspx");
            Response.Redirect("FormArtXCliente.aspx");
        }


        private Entidades.Sistema.Cliente seleccionarCliente(int indexGrilla)
        {
            #region BusquedaIndiceGrilla
            GridViewRow selectedRow=null;
            TableCell tbcCuitCliente;
            string strCuitCliente;
            Int64 nroCuitClienteSeleccionado;
            Entidades.Sistema.Cliente clienteSeleccionado;

            tbcCuitCliente = selectedRow.Cells[1];
            strCuitCliente = tbcCuitCliente.Text;
            nroCuitClienteSeleccionado = Convert.ToInt64(strCuitCliente);
            #endregion

            clienteSeleccionado = Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarClientePorCuit(nroCuitClienteSeleccionado);
            return clienteSeleccionado;
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnVolverAMenu_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }
    }
}