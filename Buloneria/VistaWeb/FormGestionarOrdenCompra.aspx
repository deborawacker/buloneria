﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGestionarOrdenCompra.aspx.cs" Inherits="VistaWeb.FormGestionarOrdenCompra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h4>Gestión de Ordenes de Compra</h4>
        <br />
        <asp:Button ID="btnVolver" runat="server" Text="Volver a Menu" CssClass="btn-primary" OnClick="btnVolver_Click" />
        <br />
        <hr />

        <table border="0" style="width: 1000px">
            <tbody>
                <tr>
                    <th style="width: 300px; padding-top: 4px"></th>
                    <th style="width: 150px">Nro.</th>
                    <th style="width: 150px">Proveedor</th>
                    <th style="width: 150px">Fecha Emisión</th>
                    <th style="width: 150px">Fecha Plazo</th>
                    <th style="width: 150px">Estado</th>
                </tr>
            </tbody>
        </table>

        <div style="height: 300px; width: 1000px; overflow-y: scroll; overflow-x: hidden">


            <asp:GridView ID="GrillaOrdenesCompras" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaOrdenesCompras_RowCommand" ShowHeader="false" OnRowDataBound="GrillaOrdenesCompras_RowDataBound">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Consultar" Text="Consultar" ControlStyle-CssClass="btn-info" ControlStyle-Width="100" />
                    <asp:ButtonField ButtonType="Button" CommandName="FinalizarConFaltante" Text="Finalizar con Faltante" ControlStyle-CssClass="btn-danger" ControlStyle-Width="150" />
                    <asp:BoundField DataField="NroOrdenCompra" HeaderText="Nro OC" ItemStyle-Width="150" />
                    <asp:BoundField DataField="Proveedor.RazonSocial" HeaderText="Proveedor" ItemStyle-Width="150" />
                    <asp:BoundField DataField="FechaEmisionCorta" HeaderText="Fecha Emision" ItemStyle-Width="150" />
                    <asp:BoundField DataField="FechaPlazoCorta" HeaderText="Fecha Plazo" ItemStyle-Width="150" />
                    <asp:BoundField DataField="Estado" HeaderText="Estado" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>
        </div>
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        <br />
        <asp:Panel ID="PanelDetalleOrdenCompra" runat="server" CssClass="container">
            <h4>Detalle Orden Compra</h4>
            <label for="lblProveedor">Proveedor:</label>
            <asp:Label ID="lblProveedor" runat="server"></asp:Label>
            <br />
            <label for="lblOrdenCompra">Numero Orden Compra:</label>
            <asp:Label ID="lblOrdenCompra" runat="server"></asp:Label>
            <br />
            <label for="lblFechaEmision">Fecha Emisión:</label>
            <asp:Label ID="lblFechaEmision" runat="server"></asp:Label>
            <br />
            <label for="lblEstado">Estado:</label>
            <asp:Label ID="lblEstado" runat="server"></asp:Label>

            <asp:GridView ID="GrillaLineasOrdenCompra" runat="server" AutoGenerateColumns="False" CssClass="table-condensed">
                <AlternatingRowStyle BackColor="#CCCCCC" />
                <Columns>
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                    <asp:BoundField DataField="CantPedir" HeaderText="Cantidad Pedida" />
                    <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio" />
                    <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
                    <asp:BoundField DataField="SubtotalIva" HeaderText="Subtotal Iva" />
                    <asp:BoundField DataField="CantRecibida" HeaderText="Recibido" />
                    <asp:BoundField DataField="CantFaltante" HeaderText="Falta recibir" />
                </Columns>
            </asp:GridView>
            <br />
            <br />
            <label for="lblSubtotal">Subtotal:</label>
            <asp:Label ID="lblSubtotal" runat="server"></asp:Label>
            <br />
            <label for="lblSubtotalIVA">Monto IVA:</label>
            <asp:Label ID="lblSubtotalIVA" runat="server"></asp:Label>
            <br />
            <label for="lblTotal">Total:</label>
            <asp:Label ID="lblTotal" runat="server"></asp:Label>
        </asp:Panel>
        <br />
        <br />
        <br />
    </div>
</asp:Content>
