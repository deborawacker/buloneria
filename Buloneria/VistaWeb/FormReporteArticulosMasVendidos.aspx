﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormReporteArticulosMasVendidos.aspx.cs" Inherits="VistaWeb.FormReporteArticulosMasVendidos" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="774px">
            <LocalReport ReportPath="Reportes\ReporteArticulosMasVendidos.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="odsArticulosMasVendidos" Name="dataSetArticulosMasVendidos" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="odsArticulosMasVendidos" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="dataSetArticulosMasVendidosTableAdapters.sp_RepArticulosMasVendidosTableAdapter"></asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
