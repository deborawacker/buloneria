﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormAuditoriaOC.aspx.cs" Inherits="VistaWeb.Auditoria.FormAuditoriaOC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <br />
        &nbsp; 
         <asp:Button ID="btnVolver" runat="server" Text="Volver a menu" OnClick="btnVolver_Click" CssClass="btn-primary" />
        <br />
        <hr />
        <div class="form-group">
            <label for="txtNombreUsuario">Usuario:</label>
            <asp:TextBox ID="txtNombreUsuario" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtNroOrdenCompra">Nro Orden Compra:</label>
            <asp:TextBox ID="txtNroOrdenCompra" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" CssClass="btn-primary" OnClick="btnFiltrar_OnClick" />
        <br />
        <br />

        <hr />

        <table border="1" style="width: 600px">
            <tbody>
                <tr>
                    <th style="width: 150px">Usuario</th>
                    <th style="width: 150px">Estado</th>
                    <th style="width: 150px">FechaRegistro</th>
                    <th style="width: 150px">NroOrdenCompra</th>
                </tr>
            </tbody>
        </table>

        <div style="height: 500px; width: 620px; overflow: auto;">
            <asp:GridView ID="GrillaAuditoriaOrdenCompra" runat="server" CssClass="table-condensed" AutoGenerateColumns="false" ShowHeader="false">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Usuario" HeaderText="Usuario" ItemStyle-Width="150px" />
                    <asp:BoundField DataField="Estado" HeaderText="Estado" ItemStyle-Width="150px" />
                    <asp:BoundField DataField="FechaRegistro" HeaderText="FechaRegistro" ItemStyle-Width="150px" />
                    <asp:BoundField DataField="NroOrdenCompra" HeaderText="Nro Orden Compra" ItemStyle-Width="150px" />
                </Columns>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#b2ffff" />
            </asp:GridView>
        </div>
        <br />


    </div>
</asp:Content>

