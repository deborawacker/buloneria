﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormWeb : System.Web.UI.Page
    {
        List<Entidades.Sistema.GrupoArticulo> _listaGruposArticulos;
        protected void Page_Init(object sender, EventArgs e)
        {
            _listaGruposArticulos = Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dgvGruposArticulos.DataSource = _listaGruposArticulos;
                dgvGruposArticulos.DataBind();
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Session.Add("operacion", "Agregar");
            Response.Redirect("FormGrupoArticulo.aspx");
            
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvGruposArticulos.SelectedRow != null)
            {
                int indice = dgvGruposArticulos.SelectedIndex;
                Entidades.Sistema.GrupoArticulo grupo = _listaGruposArticulos[indice];
                Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().EliminarGrupoArticulo(grupo);
                dgvGruposArticulos.DataSource = _listaGruposArticulos;
                dgvGruposArticulos.DataBind();
            }
            
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            int indice = dgvGruposArticulos.SelectedIndex;
            Entidades.Sistema.GrupoArticulo grupo = _listaGruposArticulos[indice];
            Session.Add("operacion", "Modificar");
            Session.Add("grupoArticulo", grupo);
            Response.Redirect("FormGrupoArticulo.aspx");
            
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (dgvGruposArticulos.SelectedIndex >= 0)
            {
                int indice = dgvGruposArticulos.SelectedIndex;
                Entidades.Sistema.GrupoArticulo grupo = _listaGruposArticulos[indice];
                Session.Add("operacion", "Consultar");
                Session.Add("grupoArticulo", grupo);
                Response.Redirect("FormGrupoArticulo.aspx");
            }
        }

        protected void btnSeleccionarGrupoArticulo_Click(object sender, EventArgs e)
        {
            int fila = dgvGruposArticulos.SelectedIndex;
            //Si no se selecciono un grupo
            if (fila < 0)
            {
                string mje = "Debe seleccionar un grupo de articulo.";
                lblGrupoArticuloNoSeleccionado.Text = mje;
            }
            else
            {
                Entidades.Sistema.GrupoArticulo grupoArticulo = _listaGruposArticulos.ElementAt(fila);
                //Guarda el objeto en la sesion
                Session.Add("grupo", grupoArticulo);
                //Vuelve
                Page.Response.Redirect("FormGenerarNP.aspx");
            }
        }
    }
}