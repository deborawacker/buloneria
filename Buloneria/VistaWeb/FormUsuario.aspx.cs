﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb.Seguridad
{
    public partial class FormUsuario : System.Web.UI.Page
    {
        private List<Entidades.Seguridad.Perfil> _perfiles;
        private List<Entidades.Seguridad.Perfil> _listaPerfilesSeleccionados;
        private Entidades.Seguridad.Usuario _usuario;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!Page.IsPostBack)
            {
                _listaPerfilesSeleccionados = new List<Entidades.Seguridad.Perfil>();
                Session.Add("listaPerfilesSeleccionados", _listaPerfilesSeleccionados);

            }
            else
            {

            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario != null)
            {
                _perfiles = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarPerfiles();


                dplPerfiles.DataSource = _perfiles;
                dplPerfiles.DataTextField = "NombrePerfil";
                dplPerfiles.DataBind();


            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

        }

        protected void btnAsignarPerfil_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Perfil perfilSeleccionado = new Entidades.Seguridad.Perfil();
            //Recupero el indice seleccionado del combo
            int index = dplPerfiles.SelectedIndex;
            //Recupero el objeto perfil que esta en la coleccion en el orden index
            perfilSeleccionado = _perfiles.ElementAt(dplPerfiles.SelectedIndex);

            _listaPerfilesSeleccionados = (List<Entidades.Seguridad.Perfil>)Session["listaPerfilesSeleccionados"];
            //Si la lista perfiles seleccionados no contiene el perfil seleccionado lo agrego
            if (!_listaPerfilesSeleccionados.Contains(perfilSeleccionado))
            {
                _listaPerfilesSeleccionados.Add(perfilSeleccionado);
                Session["listaPerfilesSeleccionados"] = _listaPerfilesSeleccionados;
            }

            lsbPerfiles.DataTextField = "NombrePerfil";
            lsbPerfiles.DataSource = _listaPerfilesSeleccionados;
            lsbPerfiles.DataBind();

        }

        protected void btnQuitarPerfil_Click(object sender, EventArgs e)
        {
            Entidades.Seguridad.Perfil perfilSeleccionado = new Entidades.Seguridad.Perfil();
            //Recupero el indice seleccionado del combo
            int index = lsbPerfiles.SelectedIndex;
            //El usuario tiene que seleccionar un elemento del listbox
            if (index > -1)
            {
                //Recupero el objeto perfil que esta en la coleccion en el orden index
                string strPerfSelected = lsbPerfiles.SelectedItem.ToString();

                _listaPerfilesSeleccionados = (List<Entidades.Seguridad.Perfil>)Session["listaPerfilesSeleccionados"];

                perfilSeleccionado = buscarPerfilPorNombreEn(strPerfSelected, _listaPerfilesSeleccionados);

                //Si la lista perfiles seleccionados contiene el perfil seleccionado lo quito
                if (_listaPerfilesSeleccionados.Contains(perfilSeleccionado))
                {
                    _listaPerfilesSeleccionados.Remove(perfilSeleccionado);
                    Session["listaPerfilesSeleccionados"] = _listaPerfilesSeleccionados;
                }

                lsbPerfiles.DataTextField = "NombrePerfil";
                lsbPerfiles.DataSource = _listaPerfilesSeleccionados;
                lsbPerfiles.DataBind();
            }
        }

        private Entidades.Seguridad.Perfil buscarPerfilPorNombreEn(string nombrePerfilBuscado, List<Entidades.Seguridad.Perfil> _listaPerfilesSeleccionados)
        {
            Entidades.Seguridad.Perfil perfilSeleccionado = _listaPerfilesSeleccionados.Find(delegate(Entidades.Seguridad.Perfil p) { return p.NombrePerfil == nombrePerfilBuscado; });
            return perfilSeleccionado;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            string nombreUsuario = txtNombreUsuario.Text;
            string nombreApellido = txtNombreApellido.Text;
            string email = txtEmail.Text;
            _listaPerfilesSeleccionados = (List<Entidades.Seguridad.Perfil>)Session["listaPerfilesSeleccionados"];

            string mjeError = validarDatos(nombreUsuario, nombreApellido, email, _listaPerfilesSeleccionados);
            lblError.Text = mjeError;
            string mensaje = "";
            if (mjeError.Equals(String.Empty))
            {

                Entidades.Seguridad.Usuario oUsuario = new Entidades.Seguridad.Usuario(nombreUsuario, nombreApellido, email);
                oUsuario.ColPerfiles = _listaPerfilesSeleccionados;

                if (Controladora.Seguridad.ControladoraGestionarUsuarios.ObtenerInstancia().GuardarUsuario(oUsuario))
                {
                    mensaje = string.Format("Sus datos para ingresar al sistema son: Nombre de Usuario: {0} Contraseña: {1} encriptada:{2}", oUsuario.NombreUsuario, oUsuario.ClaveNoEncriptada,oUsuario.Clave);

                    string asunto = "Su clave de uso del sistema";
                    Servicios.MailSender.EnviarMail(oUsuario.Email, mensaje, asunto);
                }



                //TODO: TESTEAR SI PASA POR ACA Envio el mensaje como parametro GET
                Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mensaje);

            }

        }

        private string validarDatos(string nombreUsuario, string nombreApellido, string email, List<Entidades.Seguridad.Perfil> _listaPerfilesSeleccionados)
        {
            string mje = String.Empty;
            if (nombreUsuario.Equals(String.Empty))
            {
                mje += " Nombre de usuario vacio \n ";
            }
            if (nombreApellido.Equals(String.Empty))
            {
                mje += " Nombre y Apellido vacio \n";
            }
            if (email.Equals(String.Empty))
            {
                mje += " Email vacio \n";
            }
            if (_listaPerfilesSeleccionados.Count == 0)
            {
                mje += " Debe asignar al menos un Perfil al usuario \n";
            }


            return mje;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

    }
}