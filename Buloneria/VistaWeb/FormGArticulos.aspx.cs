﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class formGArticulos : System.Web.UI.Page
    {
        List<Entidades.Sistema.Articulo> _listaArticulos;

        protected void Page_Init(object sender, EventArgs e) 
        {
            _listaArticulos = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarArticulos();
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                dgvArticulos.DataSource = _listaArticulos;
                dgvArticulos.DataBind();
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Session.Add("operacion","Agregar");
            Response.Redirect("FormArticulo.aspx");
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            int indice = dgvArticulos.SelectedIndex;
            Entidades.Sistema.Articulo articulo = _listaArticulos[indice];
            Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().EliminarArticulo(articulo);
            dgvArticulos.DataSource = _listaArticulos;
            dgvArticulos.DataBind();
            
        }


    }
}