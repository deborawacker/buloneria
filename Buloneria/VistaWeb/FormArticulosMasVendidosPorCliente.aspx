﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormArticulosMasVendidosPorCliente.aspx.cs" Inherits="VistaWeb.FormArticulosMasVendidosPorCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlFiltro" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Codigo Cliente:"></asp:Label>
        <asp:TextBox ID="txtCodCliente" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Nombre Cliente:"></asp:Label>
        <asp:TextBox ID="txtRazonSocial" runat="server" Width="197px"></asp:TextBox>
        <br />
        <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar" OnClick="btnBuscarCliente_Click" />
        <br />
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
    </asp:Panel>

    <asp:Panel ID="pnlLista" runat="server">
        <asp:GridView ID="grdClientes" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="grdClientes_RowCommand">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:ButtonField ButtonType="Button" Text="VerReporte" CommandName="VerReporte" />
                <asp:BoundField DataField="Cuit" HeaderText="CUIT" />
                <asp:BoundField DataField="RazonSocial" HeaderText="RazonSocial" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </asp:Panel>

</asp:Content>
