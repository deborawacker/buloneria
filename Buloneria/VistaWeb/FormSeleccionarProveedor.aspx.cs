﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormSeleccionarProveedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<Entidades.Sistema.Proveedor> _listaProveedores = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();
                grdProveedores.DataSource = _listaProveedores;
                grdProveedores.DataBind();
            }
        }

        protected void grdProveedores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index;
            GridViewRow selectedRow;
            TableCell tbcCodProv;
            string strCodProv;
            int nroCodProvSeleccionado;
            Entidades.Sistema.Proveedor proveedorSeleccionado;

            if (e.CommandName.Equals("Seleccionar"))
            {

                index = Convert.ToInt32(e.CommandArgument);
                selectedRow = grdProveedores.Rows[index];
                tbcCodProv = selectedRow.Cells[1];
                strCodProv = tbcCodProv.Text;
                nroCodProvSeleccionado = Convert.ToInt32(strCodProv);
                proveedorSeleccionado = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarProveedor(nroCodProvSeleccionado);
                //Guarda el objeto en la sesion
                Session.Add("proveedor", proveedorSeleccionado);
                //Vuelve
                Page.Response.Redirect("FormGenerarOC.aspx");
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormMenu.aspx");
           
        }


    }
}