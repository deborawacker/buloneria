﻿<%@ Page Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormMenu.aspx.cs" Inherits="VistaWeb.FormMenu" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        html {
            background: url(img/FondoBuloneria.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Buloneria</a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Generar<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li runat="server" id="generarOC" visible="false"><a href="../FormGenerarOrdenCompra.aspx">Orden de Compra</a></li>
                            <li runat="server" id="generarNP" visible="false"><a href="../FormGenerarNotaPedido.aspx">Nota de Pedido</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mercaderia<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li runat="server" id="ingresoMercaderia" visible="false"><a href="../FormIngresoMercaderia.aspx">Ingreso</a></li>
                            <li runat="server" id="egresoMercaderia" visible="false"><a href="../FormEgresoMercaderia.aspx">Egreso</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../FormGestionarProveedores.aspx">Proveedores</a></li>
                            <li><a href="../FormGestionarClientes.aspx">Clientes</a></li> 
                            <li><a href="../FormGestionarArticulos.aspx">Articulos</a></li>
                            <li><a href="../FormGestionarOrdenCompra.aspx">Ordenes Compra</a></li>
                            <li><a href="../FormGestionarNotaPedido.aspx">Notas de Pedidos</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li runat="server" id="reportes" visible="false"><a href="../FormArticulosVendidosCliente.aspx">Reportes</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Auditoria<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li runat="server" id="auditoriaLoginOut" visible="false"><a href="../FormAuditoriaLoginOut.aspx">LogInOut</a></li>
                            <li runat="server" id="auditoriaOC" visible="false"><a href="../FormAuditoriaOC.aspx">Estados OC</a></li>
                            <li runat="server" id="auditoriaProveedores" visible="false"><a href="../FormAuditoriaProveedores.aspx">Proveedores</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Seguridad<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li runat="server" id="altaUsuarios" visible="false"><a href="../FormUsuario.aspx">Alta Usuarios</a></li>
                            <li runat="server" id="altaPerfiles" visible="false"><a href="../FormPerfilesAcciones.aspx">Alta Perfiles</a></li>
                            <li runat="server" id="cambiarClave" visible="false"><a href="../FormCambiarClave.aspx">Cambiar Clave</a></li>
                            <li runat="server" id="backupRestore" visible="false"><a href="../FormBackUpRestore.aspx">BackUpRestore</a></li>
                            <li><a href="../RetirarSesion.aspx">Cerrar Sesion</a></li> 
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav> 
</asp:Content>
