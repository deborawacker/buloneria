﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades.Auditoria;

namespace VistaWeb.Auditoria
{
    public partial class FormAuditoriaProveedores : System.Web.UI.Page
    { 
        #region SessionVariables

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.UserSessionVariable == null)
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + Textos.DebeLoguearseEnElSistema);

        }

        private void CargarGrilla(GridView control, IEnumerable<Object> list)
        {
            control.DataSource = list;
            control.DataBind();
        }
         

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnFiltrar_OnClick(object sender, EventArgs e)
        {
            this.Filtrar();
        }

        private void Filtrar()
        {
            var usuario = this.txtNombreUsuario.Text.Trim();

            var razonSocial = this.txtRazonSocial.Text.Trim();

            ProveedorCriterio criterio = new ProveedorCriterio()
            {
                Usuario = usuario,
                RazonSocial = razonSocial,
            };

            var listaAuditProv =
                Controladora.Auditoria.ControladoraAuditoriaProveedor.ObtenerInstancia()
                    .RecuperarListadoPorCriterio(criterio);

            this.CargarGrilla(GrillaAuditoriaProveedores, listaAuditProv);
        }
    }
}