﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Principal.Master" CodeBehind="FormAuditoriaProveedores.aspx.cs" Inherits="VistaWeb.Auditoria.FormAuditoriaProveedores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <asp:Button ID="btnVolver" runat="server" Text="Volver a menú" CssClass="btn-primary" OnClick="btnVolver_Click" />
        <br />
        <br />
        <div class="form-group">
            <label for="txtNombreUsuario">Usuario:</label>
            <asp:TextBox ID="txtNombreUsuario" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtRazonSocial">Razon Social:</label>
            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" CssClass="btn-primary" OnClick="btnFiltrar_OnClick" />
        <br />
        <a href="#abajo">Ir a la parte de abajo</a>
        <br />
        <asp:GridView ID="GrillaAuditoriaProveedores" runat="server" CssClass="table-condensed" AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="White" />
            <RowStyle BackColor="#b2ffff" />
            <Columns>
                <asp:BoundField DataField="Usuario" HeaderText="Usuario" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Accion" HeaderText="Accion" ItemStyle-Width="150px" />
                <asp:BoundField DataField="FechaHora" HeaderText="FechaHora" ItemStyle-Width="150px" />
                <asp:BoundField DataField="RazonSocial" HeaderText="RazonSocial" ItemStyle-Width="150px" />
                <asp:BoundField DataField="NombreFantasia" HeaderText="NombreFantasia" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Cuit" HeaderText="Cuit" ItemStyle-Width="150px" />
                <asp:BoundField DataField="CodProveedor" HeaderText="CodProveedor" ItemStyle-Width="150px" />
                <asp:BoundField DataField="TipoIva" HeaderText="TipoIva" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Celular" HeaderText="Celular" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Fax" HeaderText="Fax" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Telefono" HeaderText="Telefono" ItemStyle-Width="150px" />
                <asp:BoundField DataField="CodPostal" HeaderText="CodPostal" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Ciudad" HeaderText="Ciudad" ItemStyle-Width="150px" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" ItemStyle-Width="150px" />
            </Columns>
        </asp:GridView>
        <br />
        <a name="abajo"></a>

        <br />
        <br />
    </div>
</asp:Content>

