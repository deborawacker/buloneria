﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGestionarArticulos.aspx.cs" Inherits="VistaWeb.FormGestionarArticulos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
         <h4>Gestión de Articulos</h4>
            <br />
        <asp:Panel ID="PanelBotones" runat="server">
           
            <asp:Button ID="btnNuevo" runat="server" Text="Agregar" CssClass="btn-primary" OnClick="btnNuevo_Click" />
            &nbsp;
             <asp:Button ID="btnVolverAMenu" runat="server" Text="Volver a menu" CssClass="btn-primary" OnClick="btnVolverAMenu_Click" />
            <br />
        </asp:Panel>
        <hr />
        <asp:GridView ID="GrillaArticulos" runat="server" CssClass="table-condensed" AutoGenerateColumns="False" OnRowCommand="GrillaArticulos_RowCommand" OnRowDataBound="GrillaArticulos_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Consultar" Text="Consultar" ControlStyle-CssClass="btn-info" />
                <asp:ButtonField ButtonType="Button" CommandName="Modificar" Text="Modificar" ControlStyle-CssClass="btn-warning" />
                <asp:ButtonField ButtonType="Button" CommandName="Eliminar" Text="Eliminar" ControlStyle-CssClass="btn-danger" />
                <asp:BoundField DataField="CodArticulo" HeaderText="Codigo Articulo" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
                <asp:BoundField DataField="PrecioVenta" HeaderText="Precio" />
            </Columns>
        </asp:GridView>
        <br />
        <asp:Label ID="lblMensajes" runat="server" Text="" ForeColor="red"></asp:Label>
        <asp:Panel ID="PanelDetalleArticulo" runat="server" Style="margin-top: 0px" Visible="False">
            <label for="txtCodigo">Codigo:</label>
            <asp:TextBox ID="txtCodigo" runat="server" CssClass="form-control"></asp:TextBox>
            <label for="txtDescripcion">Descripcion:</label>
            <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control"></asp:TextBox>
            <label for="ddlAlicuota" runat="server">Alicuota:</label>
            <asp:DropDownList ID="ddlAlicuota" runat="server" CssClass="form-control" />
            <label for="ddlGruposArticulos" runat="server">Grupo Articulo:</label>
            <asp:DropDownList ID="ddlGruposArticulos" runat="server" CssClass="form-control" />
            <label for="txtCantMinima">Cantidad Minima:</label>
            <asp:TextBox ID="txtCantMinima" runat="server" CssClass="form-control"></asp:TextBox>
            <label for="txtCantActual">Cantidad Actual:</label>
            <asp:TextBox ID="txtCantActual" runat="server" CssClass="form-control"></asp:TextBox>
            <label for="txtPrecioVenta">Precio Venta:</label>
            <asp:TextBox ID="txtPrecioVenta" runat="server" CssClass="form-control"></asp:TextBox>
            <label for="ddlProveedores">Proveedor:</label>
            <asp:DropDownList ID="ddlProveedores" runat="server" CssClass="form-control" />
            <asp:Button ID="btnAgregarProveedor" runat="server" Text="Agregar" CssClass="btn-primary" OnClick="btnAgregarProveedor_Click" />
            <br />
            <br />
            <label for="lsbProvSeleccionados">Proveedores seleccionados:</label>
            <asp:ListBox ID="lsbProvSeleccionados" runat="server" CssClass="form-control" />
            <asp:Button ID="btnQuitarProveedor" runat="server" Text="Quitar" CssClass="btn-danger" OnClick="btnQuitarProveedor_Click" />
            <br />
            <br />
            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="btn-success" OnClick="btnAceptar_Click" />&nbsp;
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn-danger" OnClick="btnCancelar_Click" />




        </asp:Panel>


    </div>
</asp:Content>
