﻿<%@ Page Title="Gestionar Orden Compra" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="zzzFormGestionarOC.aspx.cs" Inherits="VistaWeb.FormGestionarOC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="btnImprimir" runat="server" Text="IMPRIMIR" />
    <asp:Button ID="btnSalir" runat="server" Text="SALIR" OnClick="btnSalir_Click" />
    &nbsp;<br />
    &nbsp;<br />
    &nbsp; 
    <asp:Label ID="lblMensajeError" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    &nbsp; 
    <br />
    <br />
    &nbsp; 
    <asp:Label ID="lblMensajeOK" runat="server" Font-Size="Medium" ForeColor="Lime"></asp:Label>
    &nbsp; 
    <br />
    <asp:Panel ID="pnlGrillaOrdenesCompra" runat="server">
        <asp:GridView ID="dgvOrdenesCompra" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnRowCommand="dgvOrdenesCompra_RowCommand">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Consultar" Text="Consultar" />
                <asp:ButtonField ButtonType="Button" CommandName="Anular" Text="Anular" />
                <asp:BoundField DataField="NroOrdenCompra" HeaderText="Nro OC" />
                <asp:BoundField DataField="Proveedor.RazonSocial" HeaderText="Proveedor" />
                <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emision" />
                <asp:BoundField DataField="FechaPlazo" HeaderText="Fecha Plazo" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlConsultar" runat="server" Visible="false">
        <asp:Label ID="lblDatosOCConsultada" runat="server" Text="DatosOC"></asp:Label>
        <asp:GridView ID="dgvArticulosOCConsultada" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                <asp:BoundField DataField="CantPedir" HeaderText="Cantidad Pedida" />
                <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio" />
                <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:Button ID="btnAceptarConsulta" runat="server" Text="Aceptar" OnClick="btnAceptarConsulta_Click" />
    </asp:Panel>

    <br />
    <br />
    <br />
</asp:Content>
