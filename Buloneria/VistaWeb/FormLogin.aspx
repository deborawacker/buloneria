﻿<%@ Page Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormLogin.aspx.cs" Inherits="VistaWeb.FormLogin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="div-signin">
            <h2 class="form-signin-heading">Bienvenido</h2>
            <p>
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </p>
            <p>
                <asp:TextBox ID="txtUsuarioLogin" runat="server" OnTextChanged="txtUsuario_TextChanged" CssClass="form-control"></asp:TextBox>
            </p>
            <p>
                <asp:TextBox ID="txtClaveLogin" runat="server" OnTextChanged="txtContrasena_TextChanged" CssClass="form-control" TextMode="Password"></asp:TextBox>
            </p>
            <p>
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </p>
            <p>
                <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Ingresar" CssClass="btn btn-lg btn-primary btn-block" />
            </p>
        </div>
        <div align="center">
            <video width="600" height="400" controls>
                <source src="files/video.webm" type='video/webm; codecs="vp8, vorbis"' />
            </video>
        </div>
    </div>

</asp:Content>


