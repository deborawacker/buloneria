﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormArticulo : System.Web.UI.Page
    {
        List<Entidades.Sistema.Alicuota> _listaAlicuotas;
        List<Entidades.Sistema.GrupoArticulo> _listaGruposArticulos;
        List<Entidades.Sistema.Proveedor> _listProveedores;

        private Entidades.Sistema.Articulo _articulo;
        string operacion;
        protected void Page_Init(object sender, EventArgs e)
        {
            _listaAlicuotas = Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarAlicuotas();
            _listaGruposArticulos = Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().RecuperarGruposArticulos();
            _listProveedores = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();

            operacion = Session["operacion"].ToString();
            if (operacion == "Agregar")
            { 
                
            }
            if (operacion == "Modificar")
            {
                _articulo = (Entidades.Sistema.Articulo)Session["Articulo"];
                this.txtCodigo.Text = _articulo.CodArticulo;
                this.txtDescripcion.Text = _articulo.Descripcion;

            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlAlicuota.DataSource = _listaAlicuotas;
                ddlGrupoArticulos.DataSource = _listaGruposArticulos;
                ddlProveedor.DataSource = _listProveedores;
                ddlAlicuota.DataBind();
                ddlGrupoArticulos.DataBind();
                ddlProveedor.DataBind();
            }
        }

      
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            Entidades.Sistema.Articulo articulo = new Entidades.Sistema.Articulo();
            articulo.CodArticulo = this.txtCodigo.Text;
            articulo.Descripcion = this.txtDescripcion.Text;
            
            //ALICUOTA Y GRUPO ARTICULOS
            int indice = ddlAlicuota.SelectedIndex;  //tengo que seleccionar el indice del drop down list
            articulo.Alicuota = _listaAlicuotas[indice]; //aca guardo en la alicuota, la que se encuentra en ese indice de la lista

            int indiceGrupo = ddlGrupoArticulos.SelectedIndex; //tengo que seleccionar el indice del drop down list
            articulo.Grupo = _listaGruposArticulos[indiceGrupo]; //aca guardo en el grupo, el grupo que se encuentra en ese indice de la lista
            
            articulo.CantMinima = Convert.ToInt32(this.txtCantMinima.Text);
            articulo.CantActual = Convert.ToInt32(this.txtCantActual.Text);
            articulo.PrecioVenta = Convert.ToInt32(this.txtPrecioVenta.Text);
            
            //PROVEEDORES SELECCIONADOS LIST BOX
            foreach (object item in lstProvSeleccionados.Items)  //aca le digo que para cada item del listbox...
            {
                articulo.AgregarProveedor((Entidades.Sistema.Proveedor)item);  //...le agregue el proveedor al articulo, que esta guardado en item, y como es de _TipoMovimiento object lo tengo que castear: "entidades.sistema.proveedor", le digo que sea de _TipoMovimiento proveedor
            }

            Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().AgregarArticulo(articulo);
            Response.Redirect("FormGArticulos");



           
            
            
            
            

        }

        protected void btnAgregarProveedor_Click(object sender, EventArgs e)
        {

        }
    }
}