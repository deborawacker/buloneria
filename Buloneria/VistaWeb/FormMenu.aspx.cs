﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           GestionarSeguridadMenu(); 
        }
         
        protected void Page_Init(object sender, EventArgs e) //con el init inicializo una sola vez
        {
            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario == null)
            { 
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + Textos.IngreseSusCredenciales);
            }
        }

        public void salirSesion_Click(object sender, EventArgs e)
        {
            Response.Redirect("../FormGenerarOC.aspx");
        }

        private void GestionarSeguridadMenu()
        {
            List<Control> controlesLi = new List<Control>();

            //Los li son las opciones del menu
            //para que sean visibles de este lado deben tener un id y un runat="server"
            controlesLi.Add(altaUsuarios);
            controlesLi.Add(altaPerfiles);
            controlesLi.Add(cambiarClave);
            controlesLi.Add(generarOC);
            controlesLi.Add(generarNP);
            controlesLi.Add(ingresoMercaderia);
            controlesLi.Add(egresoMercaderia);
            controlesLi.Add(reportes);
            controlesLi.Add(auditoriaLoginOut);
            controlesLi.Add(auditoriaOC);
            controlesLi.Add(auditoriaProveedores);
            controlesLi.Add(backupRestore);

            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            //Los li tienen un ID que si coincide con el nombre de la accion del usuario se habilita
            List<Entidades.Seguridad.Perfil> perfiles = _usuario.ColPerfiles;
            foreach (Entidades.Seguridad.Perfil perfil in perfiles)
            {
                List<Entidades.Seguridad.Accion> acciones = perfil.ColAcciones;
                foreach (Entidades.Seguridad.Accion accion in acciones)
                {
                    foreach (Control ctrlLi in controlesLi)
                    {
                        if (ctrlLi.ID.Equals(accion.NombreAccion))
                        {
                            ctrlLi.Visible = true;
                        }
                    }
                }
            }
        }
    }
}