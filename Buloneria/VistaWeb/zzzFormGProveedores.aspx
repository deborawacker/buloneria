﻿<%@ Page Title="Gestionar Proveedores" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="zzzFormGProveedores.aspx.cs" Inherits="VistaWeb.FormGProveedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        function OnClose() {
            if (window.opener != null && !window.opener.closed) {
               // window.opener.HideModalDiv();
            }
        }
        window.onunload = OnClose;


</script>
    <asp:Button ID="btnAgregar" runat="server" OnClick="btnAgregar_Click" Text="AGREGAR" />
    <asp:Button ID="btnModificar" runat="server" Text="MODIFICAR" />
    <asp:Button ID="btnConsultar" runat="server" Text="CONSULTAR" />
    <asp:Button ID="btnEliminar" runat="server" OnClick="btnEliminar_Click" Text="ELIMINAR" />
&nbsp;<br />
    <asp:Label ID="Label1" runat="server" Text="Buscar:"></asp:Label>
&nbsp;<asp:TextBox ID="txtBuscar" runat="server" Width="343px"></asp:TextBox>
    <br />
    <br />
    
    <asp:Button ID="btnSeleccionarProveedor" runat="server" Text="Seleccionar Proveedor" OnClick="btnSeleccionarProveedor_Click" />
   
    &nbsp; 
    <asp:Label ID="lblProveedorNoSel" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    &nbsp; 
   <br />
    <asp:GridView ID="dgvProveedores" runat="server" AutoGenerateSelectButton="True" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <br />
    <br />
    <br />
</asp:Content>
