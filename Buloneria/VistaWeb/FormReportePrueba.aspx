﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormReportePrueba.aspx.cs" Inherits="VistaWeb.FormReportePrueba" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" style="margin-right: 286px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
            <LocalReport ReportPath="Reportes\ReporteArticulosMasVendidosPorCliente.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dsArticulosMasVendidosPorCliente" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Sistema %>" SelectCommand="sp_RepArticulosMasVendidosPorCliente" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="vCuitCliente" SessionField="cuit" Type="Int64" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        
    </div>
    </form>
</body>
</html>
