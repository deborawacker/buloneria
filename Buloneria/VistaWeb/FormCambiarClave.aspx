﻿<%@ Page Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormCambiarClave.aspx.cs" Inherits="VistaWeb.Seguridad.FormCambiarClave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
         <br />
        <div class="form-group">
            <label for="txtNombreUsuario">Nombre de Usuario:</label>
            <asp:TextBox ID="txtNombreUsuario" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtClaveActual">Clave actual:</label>
            <asp:TextBox ID="txtClaveActual" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtClaveActual">Nueva clave:</label>
            <asp:TextBox ID="txtNuevaClave" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="txtConfirmarClave">Confirmar Nueva Clave:</label>
            <asp:TextBox ID="txtConfirmarClave" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        </div>
        <br />
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" CssClass="btn btn-default"/>
        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"  CssClass="btn btn-default"/>
    </div>
</asp:Content>
