﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb.Seguridad
{
    public partial class FormCambiarClave : System.Web.UI.Page
    {
        Entidades.Seguridad.Usuario _user;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            _user = (Entidades.Seguridad.Usuario)Session["user"];
            this.txtNombreUsuario.Text = _user.NombreUsuario;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            _user = (Entidades.Seguridad.Usuario)Session["user"];
            string strClaveActual, strNuevaClave, strConfirmarClave, claveActualEncriptada;
            strClaveActual = this.txtClaveActual.Text.Trim();
            strNuevaClave = this.txtNuevaClave.Text.Trim();
            strConfirmarClave = this.txtConfirmarClave.Text.Trim();
            claveActualEncriptada = Servicios.Cryptos.Encriptar(strClaveActual);

            string mjeError = String.Empty;
            if (claveActualEncriptada != _user.Clave)
            {
                mjeError += " Clave actual incorrecta \n";
            }
            if (strClaveActual.Trim().Equals(String.Empty) || strNuevaClave.Trim().Equals(String.Empty) || strConfirmarClave.Trim().Equals(String.Empty))
            {
                mjeError += " Campos Vacios \n";
            }
            if (strNuevaClave != strConfirmarClave)
            {
                mjeError += " Claves no coinciden \n";
            }
            if (mjeError.Equals(String.Empty))
            {
                if (Controladora.Seguridad.ControladoraCambiarContraseña.ObtenerInstancia().CambiarContraseña(_user, strClaveActual, strNuevaClave))
                {
                    //Si esta todo bien redirecciona y muestra mensaje OK
                    string mje = "Se ha cambiado la clave exitosamente"; 
                    //Envio el mensaje como parametro GET
                    Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);
                }
            }
            else
            {
                lblError.Text = mjeError;
            } 
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }
    }
}