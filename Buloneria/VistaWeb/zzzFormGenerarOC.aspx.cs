﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;

namespace VistaWeb
{
    public partial class FormGenerarOC : System.Web.UI.Page
    {
        
        private List<Entidades.Sistema.FormaPago> _formaspago;
        private List<Entidades.Sistema.Articulo> _listaArticulosProveedor;
        private Entidades.Sistema.OrdenCompra _ordenCompra;
        private Entidades.Seguridad.Usuario _usuario;
        private Entidades.Sistema.Proveedor _proveedor;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                CargarCombos();

                _proveedor = (Entidades.Sistema.Proveedor)Session["proveedor"];

                calendarFechaPlazo.Visible = false;

                dgvArticulosDisponiblesProv.DataSource = "";
                dgvArticulosDisponiblesProv.DataBind();

                dgvArticulosAgregadosOC.DataSource = "";
                dgvArticulosAgregadosOC.DataBind();

                if (_proveedor != null)
                {
                    pnlGenerarOC.Visible = true;

                    DateTime fechaHoy = DateTime.Now.Date;
                    DateTime fechaPlazo = Convert.ToDateTime(fechaHoy, new CultureInfo("es-AR"));
                    if (txtFechaPlazo.Text.Equals(String.Empty))
                    {
                        //Por defecto: sumo 15 dias a la fecha de emision
                        fechaPlazo = fechaPlazo.AddDays(15);
                        txtFechaPlazo.Text = fechaPlazo.ToShortDateString();
                    }


                    txtProveedor.Text = _proveedor.RazonSocial;
                                    
                    
                    //_listaArticulosProveedor = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().RecuperarArticulosProveedor(_proveedor);

                    CargarGrillaArticulosDisponibles();

                    //Se instancia la orden de compra
                    _ordenCompra = new Entidades.Sistema.OrdenCompra();

                    //Guarda la OC y la lista art prov en la sesion para que no se pierdan los datos
                    Session.Add("oc", _ordenCompra);
                    Session.Add("lap", _listaArticulosProveedor);

                    //Evita que se cambie el proveedor
                    txtProveedor.Enabled = false;
                    btnGestionarProveedores.Enabled = false;
                    

                    lblErrorProvNoSeleccionado.Text = "";
                }

            }
            else
            {

                lblErrorAgregarArticulo.Text = "";
                lblErrorCompraSinArticulos.Text = "";
                lblErrorQuitarArticulo.Text = "";
                lblErrorProvNoSeleccionado.Text = "";
                lblErrorFecha.Text = "";
            }
        }

        private void CargarCombos()
        {
            //cargo drop down list formas pago
            dplFormaPago.DataSource = _formaspago;
            dplFormaPago.DataTextField = "TipoFormaPago";
            dplFormaPago.DataBind();
        }

        protected void CalendarFechaPlazo_SelectionChanged(object sender, EventArgs e)
        {
            string mje;

            DateTime fechaEmision;
            DateTime fechaPlazo;
            fechaEmision = DateTime.Now;
            fechaPlazo = DateTime.Parse(calendarFechaPlazo.SelectedDate.ToString());


            //Si fechaPlazo es menor que fechaEmision
            if (fechaPlazo.Date.CompareTo(fechaEmision.Date) < 0)
            {
                txtFechaPlazo.Text = "";
                mje = "Debe ingresar una fecha de plazo valida";
                lblErrorFecha.Text = mje;
            }
            else
            {
                txtFechaPlazo.Text = calendarFechaPlazo.SelectedDate.ToShortDateString();
                mje = "";
            }

            if (calendarFechaPlazo.Visible)
            {
                calendarFechaPlazo.Visible = false;
            }


        }

        private bool validarFecha(string fecStr)
        {
            bool rdo = false;
            string fechaConFormato = string.Empty;
            return rdo;
            
        }
        protected void Page_Init(object sender, EventArgs e) 
        {
           
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario != null)
            { 
                //_formaspago = Controladora.Sistema.ControladoraCUGenerarOC.ObtenerInstancia().RecuperarFormasPago();
                  
                //Fecha de emision
                string fechaEm = DateTime.Now.ToShortDateString();

                lblFechaEmision.Text += fechaEm;               

            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }
            
        }

        private void CargarGrillaArticulosDisponibles()
        {
            dgvArticulosDisponiblesProv.DataSource = null;
            //en la grilla voy a cargar los articulos
            dgvArticulosDisponiblesProv.DataSource = _listaArticulosProveedor;
            //le doy la orden
            dgvArticulosDisponiblesProv.DataBind(); 
        }

        protected void btnAgregarArticulo_Click(object sender, EventArgs e)
        {
            string mje = String.Empty;

            //Si no se ingreso una cantidad a pedir
            if (txtCantAPedir.Text.Equals(""))
            {
                mje += "Debe ingresar una cantidad a pedir. ";
                lblErrorAgregarArticulo.Text = mje;
            }
            //Si no se selecciono un articulo
            if (dgvArticulosDisponiblesProv.SelectedIndex < 0)
            {
                mje += "Debe seleccionar un articulo.";
                lblErrorAgregarArticulo.Text = mje;
            }

            if (mje.Equals(String.Empty))
            {
                lblErrorAgregarArticulo.Text = "";

                //Recupera el objeto orden de compra de la sesion. En _orden de compra, guardo el objeto "oc" casteado como orden de compra
                _ordenCompra = (Entidades.Sistema.OrdenCompra)Session["oc"]; 
                              
                int fila = dgvArticulosDisponiblesProv.SelectedIndex;

                //Se recupera el articulo seleccionado de la grilla articulos disponibles
                _listaArticulosProveedor = (List<Entidades.Sistema.Articulo>)Session["lap"];
                Entidades.Sistema.Articulo articulo = _listaArticulosProveedor.ElementAt(fila);
                
                //creo una linea de orden de compra
                Entidades.Sistema.LineaOrdenCompra linea = new Entidades.Sistema.LineaOrdenCompra();
                //guardo el articulo en la linea
                linea.Articulo = articulo; 
                
                //guardo la cantpedir en la linea, convertido lo del textbox a int32
                String strCantAPedir = this.txtCantAPedir.Text;
                linea.CantPedir = Convert.ToInt32(strCantAPedir);   
      
                //Agrego la linea a la orden de compra
                _ordenCompra.AgregarLineaOrdenCompra(linea);

                CalcularTotales();
                CargarGrillaArtAgregadosOC();

                
            } 

             
        }

        private void CargarGrillaArtAgregadosOC()
        {
            //agrego el articulo a la grilla articulos agregados a la orden de compra
            dgvArticulosAgregadosOC.DataSource = _ordenCompra.LineasOrdenCompra;
            dgvArticulosAgregadosOC.DataBind();

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {

            Entidades.Sistema.OrdenCompra ordenDeCompraFinal = (Entidades.Sistema.OrdenCompra)Session["oc"];
            _proveedor = (Entidades.Sistema.Proveedor)Session["proveedor"];

            string mje;
            bool valido = true;
            //Solo para inicializar
            DateTime fechaEmision = DateTime.Now;
            DateTime fechaPlazo = DateTime.Now;

            
            //Valida que el usuario este logueado
            if (_usuario == null)
            {
                valido = false;
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

            //Valida que el usuario no presione aceptar sin hacer nada
            if (ordenDeCompraFinal == null)
            {
                valido = false;
                mje = "Debe seleccionar un proveedor";
                lblErrorProvNoSeleccionado.Text = mje;
            }

            //Valida que el usuario no presione aceptar sin comprar ningun producto
            if (ordenDeCompraFinal != null && ordenDeCompraFinal.Total == 0)
            {
                valido = false;
                mje = "Debe agregar al menos un articulo a la orden de compra";
                lblErrorCompraSinArticulos.Text = mje;
            }

            //Si no existe fecha Plazo
            if (txtFechaPlazo.Text.Trim() == "")
            {
                valido = false;
                mje = "Debe seleccionar una fecha de plazo";
                lblErrorFecha.Text = mje;
            }
            else
            {

                fechaEmision = DateTime.Now;
                fechaPlazo = DateTime.Parse(txtFechaPlazo.Text);


                //Si fechaPlazo es menor que fechaEmision
                if (fechaPlazo.Date.CompareTo(fechaEmision.Date) < 0)
                {
                    valido = false;
                    mje = "Debe ingresar una fecha de plazo valida";
                    lblErrorFecha.Text = mje;
                }
                else
                {

                }

            }


            if (valido)
            {
                //Cargo las fechas a la ordenDeCompraFinal
                ordenDeCompraFinal.FechaEmision = fechaEmision;
                ordenDeCompraFinal.FechaPlazo = fechaPlazo;

                //Agrego el proveedor
                ordenDeCompraFinal.Proveedor = _proveedor;
                 

                //Cargo la forma de pago a la ordenDeCompraFinal
                Entidades.Sistema.FormaPago formaPagoOC = new Entidades.Sistema.FormaPago();
                formaPagoOC.TipoFormaPago = this.dplFormaPago.SelectedValue;
                ordenDeCompraFinal.FormaPago = formaPagoOC;

                if (Controladora.Sistema.ControladoraCUGenerarOrdenCompra.ObtenerInstancia().AgregarOrdenCompra(ordenDeCompraFinal, _usuario))
                {
                    //Si esta todo bien redirecciona y muestra mensaje OK
                    mje = "La orden de compra nro " + ordenDeCompraFinal.NroOrdenCompra + " se generó exitosamente"; 

                    //Borra los datos de la OC realizada para no superponer si se desea hacer otra OC.
                    Session["proveedor"] = null;

                    //Envio el mensaje como parametro GET
                    Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);


                }
            }
                        
        }

        protected void btnQuitarArticulo_Click(object sender, EventArgs e)
        {
            String mje = String.Empty;
            //Si no selecciono un articulo para quitar
            if (dgvArticulosAgregadosOC.SelectedIndex < 0)
            {
                mje += "Debe seleccionar un articulo para quitar";
                lblErrorQuitarArticulo.Text = mje;
            }

            //Si no hay elementos en la grilla de articulos agregados
            if (_ordenCompra != null && _ordenCompra.LineasOrdenCompra.Count == 0)
            {
                mje += "No hay articulos agregados";
                lblErrorQuitarArticulo.Text = mje;
            }

            if (mje.Equals(String.Empty))
            {
                lblErrorQuitarArticulo.Text = "";
                //Recupera el objeto orden de compra de la sesion
                _ordenCompra = (Entidades.Sistema.OrdenCompra)Session["oc"];

                //Guardo el nro de fila seleccionada
                int nroFila = dgvArticulosAgregadosOC.SelectedIndex;
                if (_ordenCompra.LineasOrdenCompra.Count > 0)
                {
                    //Busco el objeto indicado en la coleccion de lineas de OC de la OC
                    Entidades.Sistema.LineaOrdenCompra linea = _ordenCompra.LineasOrdenCompra.ElementAt(nroFila);
                    //Remuevo esa linea de OC
                    //_ordenCompra.QuitarLineaOrdenCompra(linea);
                    //Actualizo la fuente de la grilla
                    dgvArticulosAgregadosOC.DataSource = null;
                    dgvArticulosAgregadosOC.DataSource = _ordenCompra.LineasOrdenCompra;
                    dgvArticulosAgregadosOC.DataBind();


                    //Recalcula los totales
                    CalcularTotales();
                }
            }
        }

        private void CalcularTotales()
        {
            decimal montoIVA = 0;
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaOrdenCompra linea in _ordenCompra.LineasOrdenCompra)
            {
                int subtotalLinea = linea.PrecioUnitario * linea.CantPedir;
                subtotal += subtotalLinea;
                decimal subtotalLineaIva = (subtotalLinea * linea.Articulo.Alicuota.Valor) / 100;
                montoIVA += subtotalLineaIva;
                
            }
            txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = montoIVA.ToString();
            decimal total = subtotal + montoIVA;

            txtTotal.Text = total.ToString();

            //Actualiza los montos de la orden de compra
            //_ordenCompra.Subtotal = subtotal;
            //_ordenCompra.MontoIva = montoIVA;
            //_ordenCompra.Total = total;

        } 

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["proveedor"] = null;
            Page.Response.Redirect("FormMenu.aspx");
        } 
         
        private void buscarProveedor()
        {
            Page.Response.Redirect("FormSeleccionarProveedor.aspx");
        }

        protected void btnGestionarProveedores_Click(object sender, ImageClickEventArgs e)
        {
            buscarProveedor();
        }

        protected void btnCalendarFechaPlazo_Click(object sender, EventArgs e)
        {
            if (calendarFechaPlazo.Visible)
            {
                calendarFechaPlazo.Visible = false;
            }
            else
            {
                calendarFechaPlazo.Visible = true;
            }
             
        } 

    }

}