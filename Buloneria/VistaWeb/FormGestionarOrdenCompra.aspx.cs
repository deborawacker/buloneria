﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades.Sistema;
using Controladora.Sistema;
using Entidades.Seguridad;
using VistaWeb.Adicionales;
using System.Drawing;

namespace VistaWeb
{
    public partial class FormGestionarOrdenCompra : System.Web.UI.Page
    {
        #region SessionVariables

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PanelDetalleOrdenCompra.Visible = false;

            if (!Page.IsPostBack)
            {
                this.RellenarGrilla();
            }
        }

        private void RellenarGrilla()
        {
            List<OrdenCompra> listaOrdenCompra = ControladoraCUGestionarOC.ObtenerInstancia().RecuperarOrdenesCompras();

            this.GrillaOrdenesCompras.DataSource = listaOrdenCompra;
            this.GrillaOrdenesCompras.DataBind();
        }

        protected void GrillaOrdenesCompras_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.ResetearControl(this.lblMensaje);

            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = GrillaOrdenesCompras.Rows[index];
            TableCell tbcNroOrdenCompra = selectedRow.Cells[2];
            string strNroOrdenCompra = tbcNroOrdenCompra.Text;
            int nroOrdenCompraBuscada = Convert.ToInt32(strNroOrdenCompra);

            if (e.CommandName == Textos.ConsultarCommand)
            {
                this.MostrarDetalle(this.BuscarOrdenCompra(nroOrdenCompraBuscada));
            }

            if (e.CommandName == Textos.FinalizarConFaltanteCommand)
            {
                OrdenCompra ordenCompraSeleccionada = this.BuscarOrdenCompra(nroOrdenCompraBuscada);

                try
                {
                    ControladoraCUGestionarOC.ObtenerInstancia().FinalizarConFaltante(ordenCompraSeleccionada, null);

                    this.RellenarGrilla();

                    Notify(lblMensaje,String.Format(Textos.SeHanGuardadoLosCambiosDetalleX, String.Format(Textos.OrdenCompraXPasoAEstadoY, ordenCompraSeleccionada.NroOrdenCompra, ordenCompraSeleccionada.Estado.ToString())), TypeNotif.Success);
                }
                catch (Exception ex)
                {
                    this.Notify(lblMensaje, ex.Message, TypeNotif.Error);
                }

            }
        }

        private void MostrarDetalle(OrdenCompra ordenCompraSeleccionada)
        {
            this.PanelDetalleOrdenCompra.Visible = true;
            this.lblProveedor.Text = ordenCompraSeleccionada.Proveedor.RazonSocial;
            this.lblOrdenCompra.Text = ordenCompraSeleccionada.NroOrdenCompra.ToString();
            this.lblFechaEmision.Text = ordenCompraSeleccionada.FechaEmision.ToShortDateString();
            this.lblEstado.Text = ordenCompraSeleccionada.Estado.ToString();
            this.lblSubtotal.Text = ordenCompraSeleccionada.Subtotal.ToString();
            this.lblSubtotalIVA.Text = ordenCompraSeleccionada.MontoIva.ToString();
            this.lblTotal.Text = ordenCompraSeleccionada.Total.ToString();

            this.RellenarGrillaDetalleLineasOrdenCompra(ordenCompraSeleccionada);
        }

        private void RellenarGrillaDetalleLineasOrdenCompra(OrdenCompra OrdenCompraSeleccionada)
        {
            this.GrillaLineasOrdenCompra.DataSource = OrdenCompraSeleccionada.LineasOrdenCompra;
            this.GrillaLineasOrdenCompra.DataBind();
        }

        private OrdenCompra BuscarOrdenCompra(int nroOrdenCompraBuscada)
        {
            return ControladoraCUGestionarOC.ObtenerInstancia().BuscarOrdenCompra(nroOrdenCompraBuscada);
        }

        private void ResetearControl(ITextControl label)
        {
            label.Text = string.Empty;
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void GrillaOrdenesCompras_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var btnFinalizarConFaltanteOrdenCompra = e.Row.Cells[1];

            Boolean puedeFinalizarConFaltanteOrdenCompra =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == "FinalizarConFaltanteOrdenCompra"));

            btnFinalizarConFaltanteOrdenCompra.Enabled = puedeFinalizarConFaltanteOrdenCompra;
        }
    }
}