﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb.Seguridad
{
    public partial class FormPerfilesAcciones : System.Web.UI.Page
    {
        private List<Entidades.Seguridad.Perfil> _perfiles;
        private List<Entidades.Seguridad.Accion> _acciones;
        private List<Entidades.Seguridad.Accion> _accionesSeleccionadas;
        private Entidades.Seguridad.Usuario _usuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session.Add("acciones", null);
                Session.Add("accionesSeleccionadas", null);
                pnlSeleccionAcciones.Visible = false;
                pnlMensaje.Visible = false;
                btnGuardar.Enabled = false;




            }
            else
            {

            }

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];


            if (_usuario != null)
            {
                //Recupera perfiles para el combo
                _perfiles = Controladora.Seguridad.ControladoraGestionarPerfiles.ObtenerInstancia().RecuperarPerfiles();

                dplPerfiles.DataSource = _perfiles;
                dplPerfiles.DataTextField = "NombrePerfil";
                dplPerfiles.DataBind();

                pnlMensaje.Visible = false;





            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

        }

        protected void btnSeleccionar_Click(object sender, EventArgs e)
        {
            string strPerfilSeleccionado = this.dplPerfiles.SelectedValue;
            dplPerfiles.Enabled = false;
            Entidades.Seguridad.Perfil perfilSeleccionado = buscarPerfilPorNombreEn(strPerfilSeleccionado, _perfiles);

            //Primero recupero todas las acciones y luego elimino de ella las que ya pertenecen al perfil
            //NOTA:En la sigLinea copio la lista recuperada, no uso la referencia ya que no puedo borrar de ahi
            List<Entidades.Seguridad.Accion> todasLasAcciones = new List<Entidades.Seguridad.Accion>(Controladora.Seguridad.ControladoraGestionarAcciones.ObtenerInstancia().RecuperarAcciones()); ;
            List<Entidades.Seguridad.Accion> accionesYaSeleccionadas = perfilSeleccionado.ColAcciones;
            //Remueve las acciones ya seleccionadas
            List<Entidades.Seguridad.Accion> accionesDelPerfil = new List<Entidades.Seguridad.Accion>();
            for (int i = 0; i < accionesYaSeleccionadas.Count; i++)
            {
                foreach (Entidades.Seguridad.Accion accion in todasLasAcciones)
                {
                    if (accionesYaSeleccionadas.ElementAt(i).NombreAccion.Equals(accion.NombreAccion))
                    {
                        accionesDelPerfil.Add(accion);
                    }
                }
            }
            foreach (Entidades.Seguridad.Accion accBorrar in accionesDelPerfil)
            {
                //Quedaran solo acciones que no fueron escogidas para el perfil
                todasLasAcciones.Remove(accBorrar);
            }
            _acciones = todasLasAcciones;
            _accionesSeleccionadas = accionesDelPerfil;
            actualizarListboxs();

            Session["acciones"] = _acciones;
            Session["accionesSeleccionadas"] = _accionesSeleccionadas;

            lblPerfilSeleccionado.Text = perfilSeleccionado.NombrePerfil;
            pnlSeleccionPerfil.Visible = false;
            pnlMensaje.Visible = false;
            pnlSeleccionAcciones.Visible = true;
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {

            _acciones = (List<Entidades.Seguridad.Accion>)Session["acciones"];
            _accionesSeleccionadas = (List<Entidades.Seguridad.Accion>)Session["accionesSeleccionadas"];

            int indAccionSeleccionada = this.lsbAcciones.SelectedIndex;
            if (indAccionSeleccionada > -1)
            {
                Entidades.Seguridad.Accion accionSeleccionada = _acciones.ElementAt(indAccionSeleccionada);
                if (!_accionesSeleccionadas.Contains(accionSeleccionada))
                {
                    Entidades.Seguridad.Accion accionAIntercambiar = _acciones.ElementAt(indAccionSeleccionada);
                    _accionesSeleccionadas.Add(accionAIntercambiar);
                    _acciones.Remove(accionAIntercambiar);

                    actualizarListboxs();

                    Session["acciones"] = _acciones;
                    Session["accionesSeleccionadas"] = _accionesSeleccionadas;
                }
                btnGuardar.Enabled = true;
            }
        }

        protected void btnQuitar_Click(object sender, EventArgs e)
        {
            _acciones = (List<Entidades.Seguridad.Accion>)Session["acciones"];
            _accionesSeleccionadas = (List<Entidades.Seguridad.Accion>)Session["accionesSeleccionadas"];

            int indAccionSeleccionada = this.lsbAccionesSeleccionadas.SelectedIndex;
            if (indAccionSeleccionada > -1)
            {
                Entidades.Seguridad.Accion accionAIntercambiar = _accionesSeleccionadas.ElementAt(indAccionSeleccionada);
                _acciones.Add(accionAIntercambiar);
                _accionesSeleccionadas.Remove(accionAIntercambiar);

                actualizarListboxs();


                Session["acciones"] = _acciones;
                Session["accionesSeleccionadas"] = _accionesSeleccionadas;

                if (_accionesSeleccionadas.Count > 0)
                {
                    btnGuardar.Enabled = true;
                }
                else
                {
                    btnGuardar.Enabled = false;
                }
            }
        }

        private void actualizarListboxs()
        {
            lsbAcciones.DataSource = null;
            lsbAcciones.DataSource = _acciones;
            lsbAcciones.DataTextField = "NombreAccion";
            lsbAcciones.DataBind();
            lsbAccionesSeleccionadas.DataSource = null;
            lsbAccionesSeleccionadas.DataSource = _accionesSeleccionadas;
            lsbAccionesSeleccionadas.DataTextField = "NombreAccion";
            lsbAccionesSeleccionadas.DataBind();

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Session["accionesSeleccionadas"] = null;
            _acciones = null;
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            _accionesSeleccionadas = (List<Entidades.Seguridad.Accion>)Session["accionesSeleccionadas"];

            string nomPerfil = this.dplPerfiles.SelectedValue;
            Entidades.Seguridad.Perfil perfilBuscado = buscarPerfilPorNombreEn(nomPerfil, _perfiles);
            Controladora.Seguridad.ControladoraGestionarAcciones.ObtenerInstancia().AsignarAccionesAPerfil(perfilBuscado, _accionesSeleccionadas);
            pnlSeleccionAcciones.Visible = false;
            pnlMensaje.Visible = true;
        }

        private Entidades.Seguridad.Perfil buscarPerfilPorNombreEn(string nombrePerfilBuscado, List<Entidades.Seguridad.Perfil> _listaPerfiles)
        {
            Entidades.Seguridad.Perfil perfilSeleccionado = _listaPerfiles.Find(delegate(Entidades.Seguridad.Perfil p) { return p.NombrePerfil == nombrePerfilBuscado; });
            return perfilSeleccionado;
        }
    }
}