﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGenerarNP : System.Web.UI.Page
    {
        Sesion.SesionNotaPedido _sesionNP;


        private Entidades.Seguridad.Usuario _usuario;
        private List<Entidades.Sistema.FormaPago> _formaspago;
        private Entidades.Sistema.Cliente _cliente;
        private Entidades.Sistema.GrupoArticulo _grupoArticulo;
        private bool _HabilitarBotones;
        public bool HabilitarBotones
        {
            get { return HabilitarBotones; }
            set
            {
                _HabilitarBotones = value;
                if (!_HabilitarBotones)
                { 
                    _cliente = (Entidades.Sistema.Cliente)Session["cliente"];
                    _grupoArticulo = (Entidades.Sistema.GrupoArticulo)Session["grupo"];
                    string mje="";
                    if(_cliente==null)
                    {
                        mje+="Debe seleccionar un cliente. ";
                    }
                    if(_grupoArticulo==null)
                    {
                        mje+="Debe seleccionar un grupoArticulo. ";
                    }
                    lblMensajeAccion.Text = mje;
                }
                else 
                {
                    lblMensajeAccion.Text = "";
                }
                //Deshabilito o habilito los botones segun se cumplan las precondiciones
                pnlGenerarNP.Visible = _HabilitarBotones;

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                _cliente = (Entidades.Sistema.Cliente)Session["cliente"];
                _grupoArticulo = (Entidades.Sistema.GrupoArticulo)Session["grupo"];



                if (_cliente != null)
                {
                    txtCliente.Text = _cliente.RazonSocial;

                }
                if (_grupoArticulo != null)
                {
                    txtGrupoArticulo.Text = _grupoArticulo.Descripcion;
                }

                if (_cliente != null && _grupoArticulo != null)
                {
                    _sesionNP = new Sesion.SesionNotaPedido();

                    List<Entidades.Sistema.Articulo> listaArticulosGrupo = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarArticulosGrupo(_grupoArticulo);
                    _sesionNP.GuardarListaArticulosGrupo(listaArticulosGrupo);
                    dgvArticulosGrupo.DataSource = _sesionNP.ListaArticulosGrupo;
                    dgvArticulosGrupo.DataBind();

                    lblErrorGrillaVacia.Text = "No hay articulos agregados";
                   

                    //Guarda el objeto sesion con todos los datos necesarios en la sesion para que no se pierdan los datos
                    Session.Add("sesionNP", _sesionNP);

                    //Habilita botones
                    this.HabilitarBotones = true;
                }
            }
            else
            {
                //VER MENSAJE GRILLA VACIA
                //Recupero datos sesion NP de la sesion
                /*_sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];
                if (_sesionNP==null)
                {
                    lblErrorGrillaVacia.Text = "No hay articulos agregados";
                }
                else
                {
                    if (_sesionNP.NotaPedido.LineasNotaPedido.Count==0)
                    {
                        lblErrorGrillaVacia.Text = "No hay articulos agregados";
                    }
                    else
                    {
                        lblErrorGrillaVacia.Text = "";
                    }
                }*/
            }

        }



        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario != null)
            {
                _formaspago = Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().RecuperarFormasPago();

                dplFormaPago.DataSource = _formaspago;
                dplFormaPago.DataBind();

                //Fecha de emision
                string fechaEm = DateTime.Now.ToShortDateString();
                lblFechaEmision.Text = fechaEm;

                this.HabilitarBotones = false;

            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

        }
         
        protected void btnAgregarArticulo_Click(object sender, EventArgs e)
        {

            string mje = String.Empty;

            //Si no se ingreso una cantidad pedida
            if (txtCantPedida.Text.Equals(""))
            {
                mje += "Debe ingresar una cantidad pedida. ";
                lblErrorAgregarArticulo.Text = mje;
            }
            //Si no se selecciono un articulo
            if (dgvArticulosGrupo.SelectedIndex < 0)
            {
                mje += "Debe seleccionar un articulo.";
                lblErrorAgregarArticulo.Text = mje;
            }

            if (mje.Equals(String.Empty))
            {
                lblErrorAgregarArticulo.Text = "";

                //Recupero datos sesion NP de la sesion
                _sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];

                _sesionNP.Cuidador.AgregarMemento(_sesionNP.NotaPedido.CrearMemento());

                int fila = dgvArticulosGrupo.SelectedIndex;

                Entidades.Sistema.Articulo articulo = _sesionNP.ListaArticulosGrupo.ElementAt(fila);

                //creo una linea de nota de pedido
                Entidades.Sistema.LineaNotaPedido linea = new Entidades.Sistema.LineaNotaPedido();
                //guardo el articulo en la linea
                linea.Articulo = articulo;

                //guardo la cantpedir en la linea, convertido lo del textbox a int32
                String strCantPedida = this.txtCantPedida.Text;
                linea.CantPedida = Convert.ToInt32(strCantPedida);

                //Agrego la linea a la nota de pedido
                _sesionNP.NotaPedido.AgregarLineaNotaPedido(linea);

                //VOLVER A GUARDAR SESIONNP EN LA SESION?????

                CargarGrillaArtAgregadosNP();

                CalcularTotales();
            }
        }

        private void CalcularTotales()
        {
            decimal montoIVA = 0;
            int subtotal = 0;
            foreach (Entidades.Sistema.LineaNotaPedido linea in _sesionNP.NotaPedido.LineasNotaPedido)
            {
                int subtotalLinea = linea.PrecioUnitario * linea.CantPedida;
                subtotal += subtotalLinea;
                decimal subtotalLineaIva = (subtotalLinea * linea.Articulo.Alicuota.Valor) / 100;
                montoIVA += subtotalLineaIva;

            }
            txtSubtotal.Text = subtotal.ToString();
            txtMontoIva.Text = montoIVA.ToString();
            decimal total = subtotal + montoIVA;

            txtTotal.Text = total.ToString();

        }

        private void CargarGrillaArtAgregadosNP()
        { 
            //agrego el articulo a la grilla articulos agregados a la nota de pedido
            dgvArticulosAgregadosNP.DataSource = _sesionNP.NotaPedido.LineasNotaPedido;
            dgvArticulosAgregadosNP.DataBind();
        }

        protected void btnQuitarArticulo_Click(object sender, EventArgs e)
        {

            String mje = String.Empty;
            //Si no selecciono un articulo para quitar
            if (dgvArticulosAgregadosNP.SelectedIndex < 0)
            {
                mje += "Debe seleccionar un articulo para quitar";
                lblErrorQuitarArticulo.Text = mje;
            }

            //Recupero datos sesion NP de la sesion
            _sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];

            //Si no hay elementos en la grilla de articulos agregados
            if (_sesionNP.NotaPedido != null && _sesionNP.NotaPedido.LineasNotaPedido.Count == 0)
            {
                mje += "No hay articulos agregados";
                lblErrorQuitarArticulo.Text = mje;
            }

            if (mje.Equals(String.Empty))
            {
                lblErrorQuitarArticulo.Text = "";

                //Recupero datos sesion NP de la sesion
                _sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];

                _sesionNP.Cuidador.AgregarMemento(_sesionNP.NotaPedido.CrearMemento());

                //Guardo el nro de fila seleccionada
                int nroFila = dgvArticulosAgregadosNP.SelectedIndex;
                if (_sesionNP.NotaPedido.LineasNotaPedido.Count > 0)
                {
                    //Busco el objeto indicado en la coleccion de lineas de NP de la NP
                    Entidades.Sistema.LineaNotaPedido linea = _sesionNP.NotaPedido.LineasNotaPedido.ElementAt(nroFila);
                    //Remuevo esa linea de NP
                    _sesionNP.NotaPedido.QuitarLineaNotaPedido(linea);
                    //Actualizo la fuente de la grilla
                    dgvArticulosAgregadosNP.DataSource = null;
                    dgvArticulosAgregadosNP.DataSource = _sesionNP.NotaPedido.LineasNotaPedido;
                    dgvArticulosAgregadosNP.DataBind();


                    //Recalcula los totales
                    CalcularTotales();
                }
            }
        }



        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            //Recupero datos sesion NP de la sesion
            _sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];

            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            string mje;
            bool valido = true;

            //Valida que el usuario este logueado
            if (_usuario == null)
            {
                valido = false;
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

            //Valida que el usuario no presione aceptar sin hacer nada
            if (_sesionNP.NotaPedido == null)
            {
                valido = false;
                mje = " Debe seleccionar un cliente. \n";
                lblMensajesError.Text = mje;
            }

            //Valida que el usuario no presione aceptar sin comprar ningun producto
            if (_sesionNP.NotaPedido != null && _sesionNP.NotaPedido.Total == 0)
            {
                valido = false;
                mje = " Debe agregar al menos un articulo a la nota de pedido. \n";
                lblMensajesError.Text = mje;
            }

            if (valido)
            {
                //Recupero cliente y grupoArticulo
                _cliente = (Entidades.Sistema.Cliente)Session["cliente"];
                //_grupoArticulo = (Entidades.Sistema.GrupoArticulo)Session["grupo"];

                //Agrega el cliente a la nota de pedido
                _sesionNP.NotaPedido.Cliente = _cliente;

                //Cargo la forma de pago a la ordenDeCompraFinal
                Entidades.Sistema.FormaPago formaPagoNP = new Entidades.Sistema.FormaPago();
                formaPagoNP.TipoFormaPago = this.dplFormaPago.SelectedValue;
                _sesionNP.NotaPedido.FormaPago = formaPagoNP;

                _sesionNP.NotaPedido.Usuario = _usuario;

                _sesionNP.NotaPedido.FechaEmision = DateTime.Now;

                if (Controladora.Sistema.ControladoraCUGenerarNP.ObtenerInstancia().AgregarNotaPedido(_sesionNP.NotaPedido))
                {
                    //Si esta todo bien redirecciona y muestra mensaje OK
                    mje = "La nota de pedido nro " + _sesionNP.NotaPedido.NroNotaPedido + " se generó exitosamente";
                    //Borra los datos de la NP realizada para no superponer si se desea hacer otra NP.
                    Session["cliente"] = null;
                    Session["grupo"] = null;

                    //Envio el mensaje como parametro GET
                    Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);


                }
                else
                {
                    //Si no esta todo bien  
                    mje = "No se guardaron los datos de la Nota de Pedido";

                    //Envio el mensaje como parametro GET
                    Page.Response.Redirect("FormMensajes.aspx?mensaje=" + mje);
                }



            }


        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            //Borra los datos de la NP realizada para no superponer si se desea hacer otra NP.
            Session["cliente"] = null;
            Session["grupo"] = null;
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnDeshacer_Click(object sender, ImageClickEventArgs e)
        {
            //Recupero datos sesion NP de la sesion
            _sesionNP = (Sesion.SesionNotaPedido)Session["sesionNP"];
            if (hayArticulosAgregadosANP()==true)
            { 
                //Si habia seleccionado el ultimo articulo en la grilla y con el deshacer lo borro daba error al buscarlo luego
                if(dgvArticulosAgregadosNP.SelectedIndex>0)
                {
                    dgvArticulosAgregadosNP.SelectedIndex = -1;

                }
                
                if (_sesionNP.NotaPedido.RestablecerMemento(_sesionNP.Cuidador.RecuperarEstado()))
                {
                    dgvArticulosAgregadosNP.DataSource = null;
                    dgvArticulosAgregadosNP.DataSource = _sesionNP.NotaPedido.LineasNotaPedido;
                    dgvArticulosAgregadosNP.DataBind();
                    CalcularTotales();
                }
            }
        }

        private bool hayArticulosAgregadosANP()
        {
            bool rdo;
            if (_sesionNP.NotaPedido.LineasNotaPedido==null)
            {
                rdo = false;
            }
            else
            {
                /*if (_sesionNP.NotaPedido.LineasNotaPedido.Count>0)
                {*/
                    rdo = true;
               /* }
                else
                {
                    rdo = false;
                }*/
            } 
            return rdo;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            //Borra los datos de la NP realizada para no superponer si se desea hacer otra NP.
            Session["cliente"] = null;
            Session["grupo"] = null;
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnGestionarGruposArticulos_Click(object sender, ImageClickEventArgs e)
        {
            Page.Response.Redirect("FormGGruposArticulos.aspx");
        }

        protected void btnGestionarClientes_Click(object sender, ImageClickEventArgs e)
        {
            Page.Response.Redirect("FormGClientes.aspx");
        }
    }
}