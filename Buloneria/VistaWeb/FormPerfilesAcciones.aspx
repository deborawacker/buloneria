﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormPerfilesAcciones.aspx.cs" Inherits="VistaWeb.Seguridad.FormPerfilesAcciones" %>

<%@ Import Namespace="System.Collections.Generic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <asp:Panel ID="pnlSeleccionPerfil" runat="server">
            <label>Seleccione un perfil:</label> <br />
            <asp:DropDownList ID="dplPerfiles" runat="server" CssClass="form-control"></asp:DropDownList>

            &nbsp;<asp:Button ID="btnSeleccionar" runat="server" Text="Seleccionar" OnClick="btnSeleccionar_Click" CssClass="btn btn-default" />
            &nbsp;
    &nbsp;&nbsp;
   
            <br />
        </asp:Panel>

        <asp:Panel ID="pnlSeleccionAcciones" runat="server">
            <table>
                <tr>
                    <td class="auto-style1" colspan="2">
                        <asp:Label ID="lblPerfilSeleccionado" runat="server" Text="" CssClass="label label-default"></asp:Label><br />
                        <span class="label label-default">Seleccione las acciones permitidas para el perfil:</span>
                        <br />

                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Font-Size="Small" Text="Acciones seleccionadas para el perfil:" CssClass="label label-default"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:ListBox ID="lsbAcciones" runat="server" Width="204px" Height="148px"></asp:ListBox></td>
                    <td>
                        <asp:Button ID="btnQuitar" runat="server" Text="<" Width="34px" Height="40px" OnClick="btnQuitar_Click" CssClass="btn-primary" />
                        <asp:Button ID="btnAgregar" runat="server" Text=">" Width="34px" Height="40px" OnClick="btnAgregar_Click" CssClass="btn-primary" /></td>
                    <td>
                        <asp:ListBox ID="lsbAccionesSeleccionadas" runat="server" Width="204px" Height="148px"></asp:ListBox></td>
                </tr>
            </table>
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" CssClass="btn btn-default"  />
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CssClass="btn btn-default" />

        </asp:Panel>
        <asp:Panel ID="pnlMensaje" runat="server">
            <% 
                List<Entidades.Seguridad.Accion> _listaAccionesSeleccionadas = (List<Entidades.Seguridad.Accion>)Session["accionesSeleccionadas"];
                if (_listaAccionesSeleccionadas != null)
                {
                %>
            <span style="font-size: small; font-weight: bold">Funciones permitidas para: <%= dplPerfiles.SelectedValue %></span><br />
            <%
                foreach (Entidades.Seguridad.Accion accion in _listaAccionesSeleccionadas)
                {
        %>
            <span style="font-size: small;"><%= accion.NombreAccion %></span><br />
            <%
                }

            } 
        %>
        </asp:Panel>

        <br />
        <asp:Button ID="btnVolver" runat="server" Text="Volver a Menu" OnClick="btnVolver_Click" CssClass="btn btn-default" />
    </div>
</asp:Content>
