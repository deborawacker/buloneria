﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormMensajes.aspx.cs" Inherits="VistaWeb.FormMensajes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <asp:Label ID="lblMensaje" runat="server" CssClass="text-success"></asp:Label> 
        <p>
            <asp:Button ID="btnVolver" runat="server" OnClick="btnVolver_Click" Text="Volver a Menu" CssClass="btn-success"/>
        </p>
    </div>
</asp:Content>
