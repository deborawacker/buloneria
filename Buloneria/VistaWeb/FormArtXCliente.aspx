﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormArtXCliente.aspx.cs" Inherits="VistaWeb.FormArtXCliente" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container">
        <asp:Button ID="btnVolverAtras" runat="server" Text="Volver atras" OnClick="btnVolverAtras_Click" />

        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="558px" Width="886px">
            <LocalReport ReportPath="Reportes\ReporteArtXCliente.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSourceArtXClientes" Name="DataSetArtXCliente" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSourceArtXClientes" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="VistaWeb.App_Code.DatosArtXClientesTableAdapters.sp_ReporteArticulosPedidosXClienteTableAdapter">
            <SelectParameters>
                <asp:SessionParameter Name="DateTimeInicio" SessionField="DateTimeInicio" Type="String" />
                <asp:SessionParameter Name="DateTimeFin" SessionField="DateTimeFin" Type="String" />
                <asp:SessionParameter Name="RazonSocial" SessionField="RazonSocial" Type="String" />
                <asp:SessionParameter Name="ArticuloDesc" SessionField="ArticuloDesc" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    
    </div>
    </form>
</body>
</html>
