﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VistaWeb.Adicionales;

namespace VistaWeb
{
    public partial class FormBackUpRestore : System.Web.UI.Page
    {
        private string Ruta = @"C:/Backup";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkBackupAuditoria_Click(object sender, EventArgs e)
        {
            CrearBackupDe("Auditoria");
        }

        protected void lnkBackupBuloneria_Click(object sender, EventArgs e)
        {
            CrearBackupDe("Sistema");
        }

        protected void lnkBackupSeguridad_Click(object sender, EventArgs e)
        {
            CrearBackupDe("Seguridad");
        }

        private void CrearBackupDe(string bd)
        {
            Controladora.BackupRestore.CtrlBackupRestore.ObtenerInstancia().GuardarBackup(bd);

            Notify(lblMensaje, string.Format(Textos.SeHaGeneradoElBackUpDeLaBaseXEnLaRutaY, bd, Ruta), TypeNotif.Success); 
        }

        protected void lnkRestoreAuditoria_Click(object sender, EventArgs e)
        {
            RestaurarBD("Auditoria");
        }

        protected void lnkRestoreBuloneria_Click(object sender, EventArgs e)
        {
            RestaurarBD("Sistema");
        }

        protected void lnkRestoreSeguridad_Click(object sender, EventArgs e)
        {
            RestaurarBD("Seguridad");
        }

        private void RestaurarBD(string bd)
        {
            try
            {
                Controladora.BackupRestore.CtrlBackupRestore.ObtenerInstancia().Restore(bd);

                Notify(lblMensaje, string.Format(Textos.SeRestauroLaBaseDeDatosX, bd), TypeNotif.Success);
            }
            catch (Exception ex)
            {
                Notify(lblMensaje, string.Format(Textos.NoSePudoRestaurarLaBaseX, bd), TypeNotif.Error);
            }

        }

        protected void btnVolverAtras_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }
    }
}