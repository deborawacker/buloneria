﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormProveedoresArticulo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBuscarArticulos_Click(object sender, EventArgs e)
        {
            String codigo = this.txtCodArticulo.Text;
            String descripcion = this.txtDescripcionArticulo.Text;
            List<Entidades.Sistema.Articulo> listaArticulos =
                Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarArticulosPorCodigoDescripcion(codigo, descripcion);

            if (listaArticulos != null && listaArticulos.Any())
            {
                LlenarGrilla(listaArticulos);
            }
            else
            {
                lblMensaje.Text = "No se encontro ningun articulo";
            }
        }

        private void LlenarGrilla(List<Entidades.Sistema.Articulo> listaArticulos)
        {
            this.grdArticulos.DataSource = listaArticulos;
            this.grdArticulos.DataBind();
        }

        protected void grdArticulos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int indexGrilla = Convert.ToInt32(e.CommandArgument);
            switch (e.CommandName)
            {
                case "VerReporte":
                    generarReporte(seleccionarArticulo(indexGrilla));
                    break;
                default:
                    break;
            }
        }

        private Entidades.Sistema.Articulo seleccionarArticulo(int indexGrilla)
        {
            #region BusquedaIndiceGrilla
            GridViewRow selectedRow;
            TableCell tbcCodArticulo;
            string codArticulo;
            Entidades.Sistema.Articulo articuloSeleccionado;

            selectedRow = this.grdArticulos.Rows[indexGrilla];
            tbcCodArticulo = selectedRow.Cells[1];
            codArticulo = tbcCodArticulo.Text;
            #endregion

            articuloSeleccionado = Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarArticuloPorCodigo(codArticulo);

            return articuloSeleccionado;

        }

        private void generarReporte(Entidades.Sistema.Articulo articulo)
        {
            throw new NotImplementedException();
        }

    }
}