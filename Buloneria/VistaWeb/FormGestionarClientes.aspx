﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGestionarClientes.aspx.cs" Inherits="VistaWeb.FormGestionarClientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h4>Gestión de Clientes</h4>
        <br />
        <br />
        <asp:Panel ID="PanelBotones" runat="server">
            <asp:Button ID="btnNuevo" runat="server" Text="Agregar" CssClass="btn-primary" OnClick="btnNuevo_Click" />
            &nbsp;
              <asp:Button ID="btnVolver" runat="server" Text="Volver a Menu" CssClass="btn-primary" OnClick="btnVolver_Click" />
            <br />
        </asp:Panel>
        <hr />

        <asp:GridView ID="GrillaClientes" runat="server" CssClass="table-condensed" AutoGenerateColumns="False" OnRowCommand="GrillaClientes_RowCommand" OnRowDataBound="GrillaClientes_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Consultar" Text="Consultar" ControlStyle-CssClass="btn-info" />
                <asp:ButtonField ButtonType="Button" CommandName="Modificar" Text="Modificar" ControlStyle-CssClass="btn-warning" />
                <asp:ButtonField ButtonType="Button" CommandName="Eliminar" Text="Eliminar" ControlStyle-CssClass="btn-danger" />
                <asp:BoundField DataField="CodCliente" HeaderText="Codigo Cliente" />
                <asp:BoundField DataField="RazonSocial" HeaderText="Razón Social" />
                <asp:BoundField DataField="NombreFantasia" HeaderText="Nombre Fantasia" />
                <asp:BoundField DataField="Cuit" HeaderText="Cuit" />
            </Columns>
        </asp:GridView>
        <br />
        <asp:Label ID="lblMensajes" runat="server" Text=""></asp:Label>

        <asp:Panel ID="PanelDetalleCliente" runat="server" Style="margin-top: 0px" Visible="False">
            <label for="txtCodigo">Codigo:</label>
            <asp:TextBox ID="txtCodigo" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
            <label for="txtRazonSocial">RazonSocial:</label>
            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
            <label for="txtNombreFantasia">NombreFantasia:</label>
            <asp:TextBox ID="txtNombreFantasia" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
            <label for="txtCuit">Cuit:</label>
            <asp:TextBox ID="txtCuit" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
            <label for="txtCodPostal">CodPostal:</label>
            <asp:TextBox ID="txtCodPostal" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
            <label for="txtDireccion">Direccion:</label>
            <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control"  MaxLength="90"></asp:TextBox>
            <label for="ddlProvincia">Provincia:</label>
            <asp:DropDownList ID="ddlProvincia" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlProvincia_SelectedIndexChanged" />
            <label for="ddlCiudad">Ciudad:</label>
            <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="form-control" />
            <label for="txtTelefono">Telefono:</label>
            <asp:TextBox ID="txtTelefono" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <label for="txtCelular">Celular:</label>
            <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control"  MaxLength="20"></asp:TextBox>
            <label for="txtEmail">Email:</label>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="90"></asp:TextBox>
            <label for="txtFax">Fax:</label>
            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <label for="ddlTipoIva">TipoIva:</label> 
            <asp:DropDownList ID="ddlTipoIva" runat="server" CssClass="form-control" />
            <br />
            <br />
            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="btn-success" OnClick="btnAceptar_Click" />&nbsp;
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn-danger" OnClick="btnCancelar_Click" />

        </asp:Panel>


    </div>
</asp:Content>
