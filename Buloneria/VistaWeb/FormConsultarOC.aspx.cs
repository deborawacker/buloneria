﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormConsultarOC : System.Web.UI.Page
    {
        private Entidades.Sistema.OrdenCompra _ordenCompra;
        private Entidades.Seguridad.Usuario _usuario;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario != null)
            {
                _ordenCompra = (Entidades.Sistema.OrdenCompra)Session["ordenCompra"];

                lblNroOC.Text = _ordenCompra.NroOrdenCompra.ToString();
                lblProveedor.Text = _ordenCompra.Proveedor.RazonSocial.ToString();
                lblFechaEmision.Text = _ordenCompra.FechaEmision.Date.ToShortDateString();
                lblFechaPlazo.Text = _ordenCompra.FechaPlazo.Date.ToShortDateString();
                lblFormaPago.Text = _ordenCompra.FormaPago.ToString();
                lblEstado.Text = _ordenCompra.Estado.ToString();

                dgvArticulosOC.DataSource = _ordenCompra.LineasOrdenCompra;
                dgvArticulosOC.DataBind();
            }
            else
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("FormLogin.aspx?mensaje=" + mje);
            }

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormGestionarOC.aspx");
        }
    }
}