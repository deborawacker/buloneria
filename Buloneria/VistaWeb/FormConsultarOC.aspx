﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormConsultarOC.aspx.cs" MasterPageFile="~/Principal.Master" Inherits="VistaWeb.FormConsultarOC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Label ID="Label1" runat="server" Text="Consultar OC"></asp:Label>
    
    <br />
    <asp:Label ID="Label2" runat="server" Text="Nro Orden de Compra: "></asp:Label>
    <asp:Label ID="lblNroOC" runat="server" Text=""></asp:Label>
     <br />
    <asp:Label ID="Label3" runat="server" Text="Proveedor: "></asp:Label>
    <asp:Label ID="lblProveedor" runat="server" Text=""></asp:Label>
     <br />
    <asp:Label ID="Label4" runat="server" Text="Fecha de Emisión: "></asp:Label>
    <asp:Label ID="lblFechaEmision" runat="server" Text=""></asp:Label>
     <br />
    <asp:Label ID="Label5" runat="server" Text="Fecha de Plazo: "></asp:Label>
    <asp:Label ID="lblFechaPlazo" runat="server" Text=""></asp:Label>
     <br />
    <asp:Label ID="Label6" runat="server" Text="Forma de Pago: "></asp:Label>
    <asp:Label ID="lblFormaPago" runat="server" Text=""></asp:Label>
     <br />
    <asp:Label ID="Label7" runat="server" Text="Estado: "></asp:Label>
    <asp:Label ID="lblEstado" runat="server" Text=""></asp:Label>
       <br />
    <asp:GridView ID="dgvArticulosOC" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
            <asp:BoundField DataField="CantPedir" HeaderText="Cantidad Pedida" />
            <asp:BoundField DataField="PrecioUnitario" HeaderText="Precio" />
            <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
      <br />
    <asp:Button ID="btnVolver" runat="server" Text="Volver" OnClick="btnVolver_Click"   />


    
</asp:Content>

