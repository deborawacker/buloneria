﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb.Seguridad
{
    public partial class RetirarSesion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            Entidades.Seguridad.Usuario user = (Entidades.Seguridad.Usuario)Session["user"];
            Controladora.Seguridad.ControladoraIniciarSesion.ObtenerInstancia().CerrarSesion(user);
            //Session["user"] = null;
            Session.Clear();
            Page.Response.Redirect("FormLogin.aspx");

        }
    }
}