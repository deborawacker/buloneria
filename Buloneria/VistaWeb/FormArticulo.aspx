﻿<%@ Page Title="Articulo" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormArticulo.aspx.cs" Inherits="VistaWeb.FormArticulo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Código:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCodigo" runat="server" Width="237px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label2" runat="server" Text="Descripción:"></asp:Label>
&nbsp;<asp:TextBox ID="txtDescripcion" runat="server" Width="210px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label3" runat="server" Text="Alícuota:"></asp:Label>
&nbsp;<asp:DropDownList ID="ddlAlicuota" runat="server" Height="16px" Width="238px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label4" runat="server" Text="Grupo de Artículos:"></asp:Label>
&nbsp;<asp:DropDownList ID="ddlGrupoArticulos" runat="server" Height="20px" Width="173px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label5" runat="server" Text="Cantidad Mínima:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCantMinima" runat="server" Width="175px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label6" runat="server" Text="Cantidad Actual:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCantActual" runat="server" Width="181px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label7" runat="server" Text="Precio Venta:"></asp:Label>
&nbsp;<asp:TextBox ID="txtPrecioVenta" runat="server" Width="199px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label8" runat="server" Text="Proveedor:"></asp:Label>
&nbsp;<asp:DropDownList ID="ddlProveedor" runat="server" Height="27px" Width="222px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label9" runat="server" Text="Proveedores Seleccionados:"></asp:Label>
    <br />
&nbsp;<asp:ListBox ID="lstProvSeleccionados" runat="server" Height="134px" Width="288px"></asp:ListBox>
    <br />
&nbsp;<asp:Button ID="btnAgregarProveedor" runat="server" OnClick="btnAgregarProveedor_Click" Text="Agregar Proveedor" Width="135px" />
&nbsp;
    <asp:Button ID="btnQuitarProveedor" runat="server" Text="Quitar Proveedor" />
    <br />
    <br />
    <br />
    <asp:Button ID="btnAceptar" runat="server" OnClick="btnAceptar_Click" Text="ACEPTAR" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" />
</asp:Content>
