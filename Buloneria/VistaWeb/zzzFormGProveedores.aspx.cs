﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGProveedores : System.Web.UI.Page
    {
             
        List<Entidades.Sistema.Proveedor> _listaProveedores;

        protected void Page_Init(object sender, EventArgs e) 
        {
            _listaProveedores = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                dgvProveedores.DataSource = _listaProveedores;
                dgvProveedores.DataBind();
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormProveedor");
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {            
            int indice = dgvProveedores.SelectedIndex;
            Entidades.Sistema.Proveedor proveedor = _listaProveedores[indice];
            Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().EliminarProveedor(proveedor);
            dgvProveedores.DataSource = _listaProveedores;
            dgvProveedores.DataBind();
        }

        protected void btnSeleccionarProveedor_Click(object sender, EventArgs e)
        {
            int fila = dgvProveedores.SelectedIndex;
            //Si no se selecciono un proveedor
            if (fila < 0)
            {
                string mje = "Debe seleccionar un proveedor.";
                lblProveedorNoSel.Text = mje;
            }
            else
            {
                Entidades.Sistema.Proveedor proveedor = _listaProveedores.ElementAt(fila);
                //Guarda el objeto en la sesion
                Session.Add("proveedor", proveedor);
                //Vuelve
                Page.Response.Redirect("FormGenerarOC.aspx");
            }
            

        }
    }
}