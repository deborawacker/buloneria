﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormABMProveedores : System.Web.UI.Page
    {
        private enum Modo
        {
            Alta,
            Modificacion,
            Eliminacion
        };
        private Modo modo;
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<Entidades.Sistema.TipoIva> _listaTiposIva = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva(); 
                List<Entidades.Sistema.Provincia> _listaProvincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();

                Session["modo"] = modo;

                //Cargo el tipo iva
                this.dplTipoIva.DataSource = _listaTiposIva;
                this.dplTipoIva.DataTextField = "Nombre";
                this.dplTipoIva.DataBind();
                //Cargo provincia
                this.dplProvincia.DataSource = _listaProvincias;
                this.dplProvincia.DataTextField = "Nombre";
                this.dplProvincia.DataBind();
                //Cargo ciudad
                dplCiudad.DataSource = _listaProvincias[0].Ciudades;
                dplCiudad.DataTextField = "Nombre";
                dplCiudad.DataBind();

            }

            LoadGrillaProveedores(); 
        }

        private void LoadGrillaProveedores()
        {
            List<Entidades.Sistema.Proveedor> listaProveedores = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();
            grdProveedores.DataSource = listaProveedores;
            grdProveedores.DataBind();
        }

        protected void grdProveedores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index;
            GridViewRow selectedRow;
            TableCell tbcCodProv;
            string strCodProv;
            int nroCodProvSeleccionado;
            Entidades.Sistema.Proveedor proveedorSeleccionado;

            switch (e.CommandName)
            {
                //TODO: hacer el formulario de la auditoria de proveedor
                case "Consultar":
                    pnlAltaProveedores.Visible = false;
                    pnlGrilla.Visible = false;
                    pnlConsultar.Visible = true;

                    index = Convert.ToInt32(e.CommandArgument);  
                    selectedRow = grdProveedores.Rows[index];
                    tbcCodProv = selectedRow.Cells[3];
                    strCodProv = tbcCodProv.Text;
                    nroCodProvSeleccionado = Convert.ToInt32(strCodProv);
                    proveedorSeleccionado = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarProveedor(nroCodProvSeleccionado);
                    lblDatosProveedorConsultado.Text = string.Format("CUIT: {0} <br/> Razon Social: {1} <br/> Nombre de Fantasia: {2} <br/> Tipo IVA: {3} <br/> Direccion: {4} <br/> Cod. Postal: {5} <br/>  Ciudad: {6} <br/> Provincia: {7} <br/> Telefono: {8} <br/> Celular: {9} FAX: {10} <br/>Email: {11} <br/>",proveedorSeleccionado.Cuit,proveedorSeleccionado.RazonSocial,proveedorSeleccionado.NombreFantasia,proveedorSeleccionado.TipoIva,proveedorSeleccionado.Direccion,proveedorSeleccionado.CodPostal,proveedorSeleccionado.Ciudad.Nombre,proveedorSeleccionado.Ciudad.Provincia.Nombre,proveedorSeleccionado.Telefono,proveedorSeleccionado.Celular,proveedorSeleccionado.Fax,proveedorSeleccionado.Email);
                    break;
                case "Modificar":
                    pnlGrilla.Visible = false;
                    cambiarAModoModificacion(); 
                    pnlAltaProveedores.Visible = true;
                    index = Convert.ToInt32(e.CommandArgument);  
                    selectedRow = grdProveedores.Rows[index];
                    tbcCodProv = selectedRow.Cells[3];
                    strCodProv = tbcCodProv.Text;
                    nroCodProvSeleccionado = Convert.ToInt32(strCodProv);
                    proveedorSeleccionado = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarProveedor(nroCodProvSeleccionado);
                    //Guardo en sesion
                    Session["proveedorABM"] = proveedorSeleccionado;
                    mostrarDatosAModificar(proveedorSeleccionado); 
                    break;
                case "Eliminar":
                    index = Convert.ToInt32(e.CommandArgument);
                    cambiarAModoEliminacion();
                    selectedRow = grdProveedores.Rows[index];
                    tbcCodProv = selectedRow.Cells[3];
                    strCodProv = tbcCodProv.Text;
                    nroCodProvSeleccionado = Convert.ToInt32(strCodProv);
                    proveedorSeleccionado = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().BuscarProveedor(nroCodProvSeleccionado);

                    Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().EliminarProveedor(proveedorSeleccionado);
                    registrarAuditoria(proveedorSeleccionado);
                    LoadGrillaProveedores();
                    break;
                default:
                    break;
            }
        }

        
         
        private void mostrarDatosAModificar(Entidades.Sistema.Proveedor proveedorSeleccionado)
        {
            txtCuit.Text = proveedorSeleccionado.Cuit.ToString();
            dplTipoIva.SelectedValue = proveedorSeleccionado.TipoIva.ToString();
            txtRazonSocial.Text = proveedorSeleccionado.RazonSocial;
            txtNombreFantasia.Text = proveedorSeleccionado.NombreFantasia;
            txtDireccion.Text = proveedorSeleccionado.Direccion;
            dplProvincia.SelectedValue = proveedorSeleccionado.Ciudad.Provincia.Nombre;
            dplCiudad.SelectedValue = proveedorSeleccionado.Ciudad.Nombre;
            txtCodigoPostal.Text = proveedorSeleccionado.CodPostal.ToString();
            txtTelefono.Text = proveedorSeleccionado.Telefono;
            txtFax.Text = proveedorSeleccionado.Fax;
            txtCelular.Text = proveedorSeleccionado.Celular;
            txtEmail.Text = proveedorSeleccionado.Email;
        }

        protected void btnAgregarNuevo_Click(object sender, EventArgs e)
        {
            pnlGrilla.Visible = false;
            cambiarAModoAlta();
            
            //TEST
            lblMensaje.Text = modo.ToString();
            limpiarCamposFormulario();
            pnlAltaProveedores.Visible = true;
             
        }
         
        protected void dplProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Entidades.Sistema.Provincia> _listaProvincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
            //Cargo ciudades en base a la provincia seleccionada
            DropDownList lista = (DropDownList)sender;
            int indice = lista.SelectedIndex;
            dplCiudad.DataSource = _listaProvincias[indice].Ciudades;
            dplCiudad.DataTextField = "Nombre";
            dplCiudad.DataBind();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            List<Entidades.Sistema.TipoIva> _listaTiposIva = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarTiposIva();
            List<Entidades.Sistema.Provincia> _listaProvincias = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProvincias();
            modo = (Modo)Session["modo"];

            Entidades.Sistema.Proveedor proveedor=null;

            if (modo == Modo.Alta) 
            {
                
                proveedor = new Entidades.Sistema.Proveedor();
                //proveedor.CodProveedor = Convert.ToInt32(this.txtCodigo.Text);
                proveedor.Cuit = Convert.ToInt64(this.txtCuit.Text);

                int indexTipoIva = this.dplTipoIva.SelectedIndex;
                Entidades.Sistema.TipoIva tipoIva = _listaTiposIva.ElementAt(indexTipoIva);

                proveedor.TipoIva = tipoIva;

                proveedor.RazonSocial = this.txtRazonSocial.Text;
                proveedor.NombreFantasia = this.txtNombreFantasia.Text;
                proveedor.Direccion = this.txtDireccion.Text;

                int indexProvincia = this.dplProvincia.SelectedIndex;
                Entidades.Sistema.Provincia provincia = _listaProvincias.ElementAt(indexProvincia);
                List<Entidades.Sistema.Ciudad> _listaCiudadesProvincia = provincia.Ciudades;
                int indexCiudad = this.dplCiudad.SelectedIndex;
                Entidades.Sistema.Ciudad ciudad = _listaCiudadesProvincia.ElementAt(indexCiudad);

                proveedor.Ciudad = ciudad;

                proveedor.CodPostal = Convert.ToInt32(this.txtCodigoPostal.Text);
                proveedor.Telefono = this.txtTelefono.Text;
                proveedor.Fax = this.txtFax.Text;
                proveedor.Celular = this.txtCelular.Text;
                proveedor.Email = this.txtEmail.Text;

                //Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().AgregarProveedor(proveedor);
                //lblMensaje.Text = "Se ha agregado proveedor";
            }

            if (modo == Modo.Modificacion)
            {
                proveedor = (Entidades.Sistema.Proveedor)Session["proveedorABM"];

                proveedor.Cuit = Convert.ToInt64(this.txtCuit.Text);

                int indexTipoIva = this.dplTipoIva.SelectedIndex;
                Entidades.Sistema.TipoIva tipoIva = _listaTiposIva.ElementAt(indexTipoIva);

                proveedor.TipoIva = tipoIva;

                proveedor.RazonSocial = this.txtRazonSocial.Text;
                proveedor.NombreFantasia = this.txtNombreFantasia.Text;
                proveedor.Direccion = this.txtDireccion.Text;

                int indexProvincia = this.dplProvincia.SelectedIndex;
                Entidades.Sistema.Provincia provincia = _listaProvincias.ElementAt(indexProvincia);
                List<Entidades.Sistema.Ciudad> _listaCiudadesProvincia = provincia.Ciudades;
                int indexCiudad = this.dplCiudad.SelectedIndex;
                Entidades.Sistema.Ciudad ciudad = _listaCiudadesProvincia.ElementAt(indexCiudad);

                proveedor.Ciudad = ciudad;

                proveedor.CodPostal = Convert.ToInt32(this.txtCodigoPostal.Text);
                proveedor.Telefono = this.txtTelefono.Text;
                proveedor.Fax = this.txtFax.Text;
                proveedor.Celular = this.txtCelular.Text;
                proveedor.Email = this.txtEmail.Text;

                //Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().ModificarProveedor(proveedor);
                //TODO: test
                lblMensaje.Text = "Se ha modificado proveedor";

            }
            registrarAuditoria(proveedor);
            pnlAltaProveedores.Visible = false;
            LoadGrillaProveedores();
            pnlGrilla.Visible = true;


        }

        private void registrarAuditoria(Entidades.Sistema.Proveedor proveedor)
        {
            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            Entidades.Auditoria.Proveedor oAuditoriaProveedor = new Entidades.Auditoria.Proveedor();
            oAuditoriaProveedor.FechaHora = System.DateTime.Now;
            oAuditoriaProveedor.SetearDatosProveedor(proveedor);
            oAuditoriaProveedor.FechaHora = DateTime.Now;
            oAuditoriaProveedor.Usuario = _usuario.NombreUsuario;
            Modo modo = (Modo)Session["modo"];
            oAuditoriaProveedor.Accion = modo.ToString();

            Controladora.Auditoria.ControladoraAuditoriaProveedor.ObtenerInstancia().RegistrarAuditoriaProveedor(oAuditoriaProveedor);
            
           
        }

        

        protected void btnSalirAlta_Click(object sender, EventArgs e)
        {
            pnlAltaProveedores.Visible = false;
            pnlGrilla.Visible = true;
        }

        private void cambiarAModoModificacion()
        {
            modo = Modo.Modificacion;
            Session["modo"] = modo;
            lblMensaje.Text = modo.ToString();
            txtCuit.Enabled = false;
            txtRazonSocial.Enabled = false;
        }

        private void cambiarAModoAlta()
        {
            modo = Modo.Alta;
            Session["modo"] = modo;
            txtCuit.Enabled = true;
            txtRazonSocial.Enabled = true;
        }

        private void cambiarAModoEliminacion()
        {
            modo = Modo.Eliminacion;
            Session["modo"] = modo;
        }

        private void limpiarCamposFormulario()
        {
            txtCuit.Text = string.Empty;
            dplTipoIva.SelectedIndex = 0;
            txtRazonSocial.Text = string.Empty;
            txtNombreFantasia.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            dplProvincia.SelectedIndex = 0;
            dplCiudad.SelectedIndex = 0;
            txtCodigoPostal.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtCelular.Text = string.Empty;
            txtEmail.Text = string.Empty; 
        }

        protected void btnAceptarDeConsulta_Click(object sender, EventArgs e)
        {
            pnlConsultar.Visible = false;
            pnlGrilla.Visible = true;
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            volverAMenu();
        }

        private void volverAMenu()
        {
            Page.Response.Redirect("~/FormMenu.aspx");
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //Recupero usuario de la sesion
            Entidades.Seguridad.Usuario _usuario = (Entidades.Seguridad.Usuario)Session["user"];

            if (_usuario == null)
            {
                //Si pasa por aca es porque no esta logueado, obliga a loguearse
                string mje = "Usted debe loguearse";
                //Envio el mensaje como parametro GET
                Page.Response.Redirect("~/FormLogin.aspx?mensaje=" + mje);
            }

        }

    }
}