﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormLogin : System.Web.UI.Page
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Inicializar();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            this.Loguear();
        }

        protected void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            this.txtUsuarioLogin.Text = this.txtUsuarioLogin.Text.Trim();
        }

        protected void txtContrasena_TextChanged(object sender, EventArgs e)
        {
            this.txtClaveLogin.Text = this.txtClaveLogin.Text.Trim();
        }

        #endregion

        #region Methods

        private void Inicializar()
        {
            this.lblMensaje.Text = Request.QueryString["mensaje"];
            this.txtUsuarioLogin.Attributes.Add("placeholder", "Ingrese Usuario");
            this.txtClaveLogin.Attributes.Add("placeholder", "Ingrese Password");
            this.Form.DefaultButton = this.btnLogin.UniqueID;
        }
         
        private void Loguear()
        {
            if (this.ValidarDatosLogin())
            {
                Entidades.Seguridad.Usuario oUsuario = Controladora.Seguridad.ControladoraIniciarSesion.ObtenerInstancia().IniciarSesion(this.txtUsuarioLogin.Text, this.txtClaveLogin.Text);
                if (oUsuario != null)
                {
                    Session.Add("user", oUsuario);

                    Page.Response.Redirect("FormMenu.aspx");
                }
                else
                {
                    this.lblError.Text = Textos.VerificarIngresoCorrectoUsuarioContrasena;
                    this.txtUsuarioLogin.Focus();

                    this.ResetearCampos();
                }
            }
        }

        private bool ValidarDatosLogin()
        {
            if (string.IsNullOrEmpty(this.txtUsuarioLogin.Text) || string.IsNullOrEmpty(this.txtClaveLogin.Text))
            {
                this.lblError.Text = Textos.NoDejeCamposVacios;
                this.txtUsuarioLogin.Focus();
                return false;
            }

            return true;
        } 

        private void ResetearCampos()
        {
            this.txtUsuarioLogin.Text = string.Empty;
            this.txtClaveLogin.Text = string.Empty;
        }

        #endregion
    }
}