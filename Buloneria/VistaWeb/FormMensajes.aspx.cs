﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormMensajes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //recupero el parametro enviado via GET
            this.lblMensaje.Text = Request.QueryString["mensaje"];
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect("FormMenu.aspx");
        }
    }
}