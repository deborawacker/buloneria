﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormGrupoArticulo : System.Web.UI.Page
    {
        private Entidades.Sistema.GrupoArticulo _grupo;
        string operacion;
        protected void Page_Init(object sender, EventArgs e) 
        {
           operacion = Session["operacion"].ToString();
           if (operacion == "Modificar")
           {
               _grupo = (Entidades.Sistema.GrupoArticulo)Session["grupoArticulo"];
               this.txtCodigo.Text = _grupo.CodGrupoArticulo.ToString();
               this.txtDescripcion.Text = _grupo.Descripcion;
               this.txtCodigo.Enabled = false;
           }
           else if (operacion == "Consultar")
           {
               _grupo = (Entidades.Sistema.GrupoArticulo)Session["grupoArticulo"];
               this.txtCodigo.Text = _grupo.CodGrupoArticulo.ToString();
               this.txtDescripcion.Text = _grupo.Descripcion;
               this.txtCodigo.Enabled = false;
               this.txtDescripcion.Enabled = false;
           
           }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            if (operacion == "Agregar")
            {
                Entidades.Sistema.GrupoArticulo grupoarticulo = new Entidades.Sistema.GrupoArticulo();
                grupoarticulo.CodGrupoArticulo =Convert.ToInt32( this.txtCodigo.Text);
                grupoarticulo.Descripcion = this.txtDescripcion.Text;

                Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().AgregarGrupoArticulo(grupoarticulo);
                Response.Redirect("FormGGruposArticulos.aspx");
            }
            else if(operacion == "Modificar")
            {
                _grupo.Descripcion = this.txtDescripcion.Text;
                Controladora.Sistema.ControladoraCUGGruposArticulos.ObtenerInstancia().ModificarGrupoArticulo(_grupo);
                Response.Redirect("FormGGruposArticulos.aspx");
            }
            else if (operacion == "Consultar")
            {
                Response.Redirect("FormGGruposArticulos.aspx");
            }
        }
    }
}