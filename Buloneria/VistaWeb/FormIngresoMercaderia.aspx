﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormIngresoMercaderia.aspx.cs" Inherits="VistaWeb.FormIngresoMercaderia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <br />
        <asp:Button ID="btnVolver" runat="server" Text="Volver a Menu" CssClass="btn-success" OnClick="btnVolver_Click" />
        <br />
        <label for="lblFechaRecepcion">Fecha de Recepción:</label>
        <asp:Label ID="lblFechaRecepcion" runat="server" />
        <br />

        <asp:Label ID="lblDebug" runat="server" ForeColor="Red"></asp:Label><br />
        <asp:Panel ID="PanelFiltro" runat="server">
            <span>FILTRO</span>
            <br />
            <span>Numero de Orden de Compra:</span>
            <asp:TextBox ID="txtNroOrdenCompra" runat="server"></asp:TextBox>&nbsp;&nbsp; 
            <asp:Button ID="btnBuscarOrdenCompra" runat="server" Text="Buscar" Height="26px" CssClass="btn-primary" OnClick="btnBuscarOrdenCompra_Click" />
            &nbsp;&nbsp;
            <asp:Button ID="btnLimpiarCampos" runat="server" Text="Limpiar" CssClass="btn-info" OnClick="btnLimpiarCampos_Click" />
            <br />
            <asp:Label ID="lblMensajeFiltro" runat="server" ForeColor="Blue"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="PanelDetalleOrdenCompra" runat="server" Visible="False">
            <label for="lblNroOrdenCompra">Nro Orden Compra:</label>
            <asp:Label ID="lblNroOrdenCompra" runat="server"></asp:Label><br />
            <label for="lblProveedor">Proveedor:</label>
            <asp:Label ID="lblProveedor" runat="server"></asp:Label><br />
            <label for="lblFechaOrdenCompra">Fecha:</label>
            <asp:Label ID="lblFechaOrdenCompra" runat="server"></asp:Label><br />
            <label for="lblEstadoOrdenCompra">Estado:</label>
            <asp:Label ID="lblEstadoOrdenCompra" runat="server"></asp:Label><br />

            <h4>Articulos Ordenes Compra</h4>

            <asp:GridView ID="GrillaArticulosOrdenCompra" runat="server" AutoGenerateColumns="False" CssClass="table-condensed" OnRowCommand="GrillaArticulosOrdenCompra_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" Text="Seleccionar" CommandName="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField DataField="NroLineaOrdenCompra" HeaderText="Linea Orden Compra" />
                    <asp:BoundField DataField="CantPedir" HeaderText="Cantidad" />
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                    <asp:BoundField DataField="CantFaltante" HeaderText="Faltante" />
                </Columns>
            </asp:GridView>
            <br />

            <%--<asp:Button ID="btnVolverAFiltro" runat="server" Text="Volver a Filtro" OnClick="btnVolverAFiltro_Click" CssClass="btn-primary" />--%>
        </asp:Panel>

        <br />

        <asp:Panel ID="PanelDetalleArticuloSeleccionado" runat="server" Visible="False">
            <label for="lblNombreArticulo">Nombre Articulo:</label>
            <asp:Label ID="lblNombreArticulo" runat="server"></asp:Label><br />
            <label for="lblCantidadPedida">Cantidad Pedida:</label>
            <asp:Label ID="lblCantidadPedida" runat="server"></asp:Label><br />
            <label for="lblCantidadFaltante">Faltante al dia de la fecha:</label>
            <asp:Label ID="lblCantidadFaltante" runat="server"></asp:Label><br />

            <asp:Panel ID="PanelIngresoCantidad" runat="server">
                <span>Cantidad:</span>&nbsp;
                    <asp:TextBox ID="txtCantidadRecibida" runat="server" Width="69px"></asp:TextBox>&nbsp;
                    <asp:Label ID="lblMensajeIngresoCantidad" runat="server" ForeColor="Red"></asp:Label><br />
                <asp:Button ID="btnIngresarCantidad" runat="server" Text="Ingresar" CssClass="btn-primary" OnClick="btnIngresarCantidad_Click" />
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="PanelArticulosRecibidos" runat="server">
            <h4>Articulos Recibidos</h4>
            <asp:GridView ID="GrillaArticulosRecibidos" runat="server" AutoGenerateColumns="False" CssClass="table-condensed">
                <Columns>
                    <asp:BoundField DataField="Articulo.CodArticulo" HeaderText="Codigo Articulo" />
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                    <asp:BoundField DataField="CantRecibida" HeaderText="Cantidad Recibida" />
                    <asp:BoundField DataField="CantFaltante" HeaderText="Faltante" />
                </Columns>
            </asp:GridView>
        </asp:Panel>

        <br />
        <br />

        <asp:Panel ID="PanelAceptarCancelar" runat="server" CssClass="container" Visible="false">
            <asp:Label ID="lblMensajeAceptar" runat="server" Text=""></asp:Label>
            <br />
            <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" CssClass="btn-success" OnClick="btnAceptar_Click" />
            <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" CssClass="btn-primary" OnClick="btnCancelar_Click" />
        </asp:Panel>

    </div>
</asp:Content>
