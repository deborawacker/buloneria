﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entidades.Sistema;

namespace VistaWeb.Sesion
{
    public class SesionIngresoMercaderia
    { 
        public SesionIngresoMercaderia()
        {
            LineasArticulosRecibidos = new List<LineaOrdenCompra>();
        }
         
        public int IndexFila { get; set; } 

        public OrdenCompra OrdenCompra { get; set; } 
         
        public LineaOrdenCompra LineaOrdenCompraElegida{ get; set; }  

        public List<LineaOrdenCompra> LineasArticulosRecibidos{ get; set; } 
    }
}