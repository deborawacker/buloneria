﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VistaWeb.Adicionales
{
    public enum TypeNotif
    {
        Success,
        Error,
    };

    public enum Modo
    {
        Alta,
        Modificacion,
        Consulta,
        Eliminacion
    }; 
}