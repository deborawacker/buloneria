﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VistaWeb.Adicionales
{
    public class SesionGestionarProveedores
    {
        public Proveedor Proveedor { get; set; }

        public Modo Modo { get; set; }
    }
}