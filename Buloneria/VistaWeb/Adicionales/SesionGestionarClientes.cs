﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VistaWeb.Adicionales
{
    public class SesionGestionarClientes
    {
        public Modo Modo { get; set; }

        public Cliente Cliente { get; set; }
    }
}