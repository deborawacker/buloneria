﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entidades.Sistema;

namespace VistaWeb.Adicionales
{
    [Serializable]
    public class SesionGenerarNotaPedido
    {
        #region Constructor

        
        public SesionGenerarNotaPedido()
        {
            NotaPedido = new NotaPedido();
            
            Cuidador = new Cuidador();
        }

        #endregion

        #region Properties

        public NotaPedido NotaPedido { get; set; }

        public Articulo ArticuloSeleccionado { get; set; }

        public Cuidador Cuidador { get; set; }

        #endregion


        
    }
}