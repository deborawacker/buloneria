﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VistaWeb.Sesion
{
    public class SesionNotaPedido
    {
        
        private List<Entidades.Sistema.Articulo> _listaArticulosGrupo;
        public List<Entidades.Sistema.Articulo> ListaArticulosGrupo
        {
            get { return _listaArticulosGrupo; }
            set { _listaArticulosGrupo = value; }
        }

        private Entidades.Sistema.Cuidador _cuidador;
        public Entidades.Sistema.Cuidador Cuidador
        {
            get { return _cuidador; }
            set { _cuidador = value; }
        }
        private Entidades.Sistema.NotaPedido _notaPedido;
        public Entidades.Sistema.NotaPedido NotaPedido
        {
            get { return _notaPedido; }
            set { _notaPedido = value; }
        }

        public SesionNotaPedido()
        {
            _notaPedido = new Entidades.Sistema.NotaPedido();
            _listaArticulosGrupo = new List<Entidades.Sistema.Articulo>();
            _cuidador = new Entidades.Sistema.Cuidador();
        
        }


        internal void GuardarListaArticulosGrupo(List<Entidades.Sistema.Articulo> list)
        {
            _listaArticulosGrupo = list;
        }
 
    }
}