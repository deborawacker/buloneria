﻿using Entidades.Sistema;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VistaWeb.Adicionales;

namespace VistaWeb
{


    public partial class FormGestionarArticulos : System.Web.UI.Page
    {
        #region SessionVariables
        private const String ModoSessionVariableName = "Modo";
        private Modo _ModoSessionVariable;

        public Modo ModoSessionVariable
        {
            get
            {
                _ModoSessionVariable = (Modo)Session[ModoSessionVariableName];
                return _ModoSessionVariable;
            }
            set
            {
                _ModoSessionVariable = value;
                Session[ModoSessionVariableName] = _ModoSessionVariable;
            }
        }


        private const String ProveedoresArticuloSessionVariableName = "ProveedoresArticulo";
        private List<Proveedor> _ProveedoresArticuloSessionVariable;

        public List<Proveedor> ProveedoresArticuloSessionVariable
        {
            get
            {
                _ProveedoresArticuloSessionVariable = (List<Proveedor>)Session[ProveedoresArticuloSessionVariableName];
                return _ProveedoresArticuloSessionVariable;
            }
            set
            {
                _ProveedoresArticuloSessionVariable = value;
                Session[ProveedoresArticuloSessionVariableName] = _ProveedoresArticuloSessionVariable;
            }
        }

        private const String ProveedoresSessionVariableName = "Proveedores";
        private List<Proveedor> _ProveedoresSessionVariable;

        public List<Proveedor> ProveedoresSessionVariable
        {
            get
            {
                _ProveedoresSessionVariable = (List<Proveedor>)Session[ProveedoresSessionVariableName];
                return _ProveedoresSessionVariable;
            }
            set
            {
                _ProveedoresSessionVariable = value;
                Session[ProveedoresSessionVariableName] = _ProveedoresSessionVariable;
            }
        }


        private const String ArticuloSessionVariableName = "Articulo";
        private Articulo _ArticuloSessionVariable;

        public Articulo ArticuloSessionVariable
        {
            get
            {
                _ArticuloSessionVariable = (Articulo)Session[ArticuloSessionVariableName];
                return _ArticuloSessionVariable;
            }
            set
            {
                _ArticuloSessionVariable = value;
                Session[ArticuloSessionVariableName] = _ArticuloSessionVariable;
            }
        }

        private const String UserSessionVariableName = "user";
        private Entidades.Seguridad.Usuario _UserSessionVariable;
        public Entidades.Seguridad.Usuario UserSessionVariable
        {
            get
            {
                _UserSessionVariable = (Entidades.Seguridad.Usuario)Session[UserSessionVariableName];
                return _UserSessionVariable;
            }
            set
            {
                _UserSessionVariable = value;
                Session[UserSessionVariableName] = _UserSessionVariable;
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Inicializar();

            }
        }

        private void Inicializar()
        {
            this.RellenarGrillaArticulos();

            this.InicializarListasSession();

            this.VerificarPermisos();

            this.InicializarCombos();

        }

        private void VerificarPermisos()
        {
            this.btnNuevo.Enabled = this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == "AgregarProveedor"));

        }


        private void InicializarListasSession()
        {
            this.ProveedoresArticuloSessionVariable = new List<Proveedor>();
        }

        private void InicializarCombos()
        {
            this.InicializarComboAlicuotas();

            this.InicializarComboGruposArticulo();

            this.InicializarComboProveedores();
        }

        private void InicializarComboProveedores()
        {
            this.ProveedoresSessionVariable = Controladora.Sistema.ControladoraCUGestionarProveedores.ObtenerInstancia().RecuperarProveedores();

            this.CargarControl(this.ddlProveedores, this.ProveedoresSessionVariable);

        }

        private void CargarControl(ListControl control, IEnumerable<Object> list)
        {
            control.DataSource = list;
            control.DataBind();
        }

        private void InicializarComboGruposArticulo()
        {
            this.CargarControl(this.ddlGruposArticulos, Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarGruposArticulos());
        }

        private void InicializarComboAlicuotas()
        {
            this.CargarControl(this.ddlAlicuota, Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarAlicuotas());
        }

        private void RellenarGrillaArticulos()
        {
            this.GrillaArticulos.DataSource =
                Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().RecuperarArticulos();
            this.GrillaArticulos.DataBind();
        }

        protected void GrillaArticulos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.PanelDetalleArticulo.Visible = false;

            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow selectedRow = GrillaArticulos.Rows[index];
            TableCell tbcCodArticulo = selectedRow.Cells[3];
            string strCodArticulo = tbcCodArticulo.Text;

            if (e.CommandName == Textos.ConsultarCommand)
            {

                Articulo articulo =
                    Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia()
                        .BuscarArticulo(strCodArticulo);

                this.MostrarPanelABMArticulo(articulo, Modo.Consulta);

            }

            if (e.CommandName == Textos.ModificarCommand)
            {
                Articulo articulo =
                    Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia()
                        .BuscarArticulo(strCodArticulo);

                this.MostrarPanelABMArticulo(articulo, Modo.Modificacion);
            }

            if (e.CommandName == Textos.EliminarCommand)
            {
                Articulo articulo =
                    Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia()
                        .BuscarArticulo(strCodArticulo);

                this.Eliminar(articulo);
            }
        }

        private void Eliminar(Articulo articulo)
        {
            try
            {
                Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().EliminarArticulo(articulo);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleArticulo.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, String.Format(Textos.NoSePuedeEliminarXVerifiqueLasReferenciasDeOtrasTablas, articulo.GetType().Name), TypeNotif.Error);
            }
        }

        private void MostrarPanelABMArticulo(Articulo articulo, Modo mode)
        {
            this.ModoSessionVariable = mode;

            this.ArticuloSessionVariable = articulo;

            this.PanelDetalleArticulo.Enabled = (mode != Modo.Consulta);

            if (mode == Modo.Consulta || mode == Modo.Modificacion)
            {
                this.SetearControles(articulo);
            }

            this.PanelDetalleArticulo.Visible = true;
        }

        private void SetearControles(Articulo articulo)
        {
            this.LimpiarControles();

            this.txtCodigo.Text = articulo.CodArticulo;

            this.txtDescripcion.Text = articulo.Descripcion;

            this.ddlAlicuota.ClearSelection();
            this.ddlAlicuota.Items.FindByValue(articulo.Alicuota.ToString()).Selected = true;

            this.ddlGruposArticulos.ClearSelection();
            this.ddlGruposArticulos.Items.FindByValue(articulo.Grupo.Descripcion).Selected = true;

            this.txtCantMinima.Text = articulo.CantMinima.ToString();
            this.txtCantActual.Text = articulo.CantActual.ToString();

            this.txtPrecioVenta.Text = articulo.PrecioVenta.ToString();

            this.ProveedoresArticuloSessionVariable = articulo.Proveedores;
            this.CargarControl(this.lsbProvSeleccionados, articulo.Proveedores);
        }

        private void LimpiarControles()
        {
            this.lblMensajes.Text = String.Empty;
            this.txtCodigo.Text = String.Empty;
            this.txtDescripcion.Text = String.Empty;
            this.txtCantMinima.Text = String.Empty;
            this.txtCantActual.Text = String.Empty;
            this.txtPrecioVenta.Text = String.Empty;
            this.CargarControl(this.lsbProvSeleccionados, new List<Proveedor>());
        }

        protected void btnAgregarProveedor_Click(object sender, EventArgs e)
        {
            Proveedor proveedorSeleccionado = new Proveedor();

            int index = ddlProveedores.SelectedIndex;

            proveedorSeleccionado = this.ProveedoresSessionVariable.ElementAt(index);

            if (this.ProveedoresArticuloSessionVariable.All(p => p.CodProveedor != proveedorSeleccionado.CodProveedor))
            {
                ProveedoresArticuloSessionVariable.Add(proveedorSeleccionado);
            }

            lsbProvSeleccionados.DataSource = ProveedoresArticuloSessionVariable;
            lsbProvSeleccionados.DataBind();
        }

        protected void btnQuitarProveedor_Click(object sender, EventArgs e)
        {
            int index = lsbProvSeleccionados.SelectedIndex;

            if (index > -1)
            {
                string strRazonSocialProvSelected = lsbProvSeleccionados.SelectedItem.ToString();

                Proveedor proveedorSeleccionado =
                    this.ProveedoresArticuloSessionVariable.Find(p => p.RazonSocial == strRazonSocialProvSelected);

                this.ProveedoresArticuloSessionVariable.Remove(proveedorSeleccionado);

                this.CargarControl(this.lsbProvSeleccionados, this.ProveedoresArticuloSessionVariable);
            }
        }

        private void VolverAMenu()
        {
            Page.Response.Redirect("FormMenu.aspx");
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Guardar();
        }

        private void Guardar()
        {
            try
            {
                this.GuardarArticulo(this.ArticuloSessionVariable);

                this.Notify(this.lblMensajes, Textos.SeHanGuardadoLosCambios, TypeNotif.Success);

                this.Inicializar();

                this.PanelDetalleArticulo.Visible = false;
            }
            catch (Exception e)
            {
                this.Notify(this.lblMensajes, e.Message, TypeNotif.Error);
            }
        }

        private void GuardarArticulo(Articulo articulo)
        {
            if (ModoSessionVariable == Modo.Alta)
                articulo = new Articulo();


            articulo.CodArticulo = this.txtCodigo.Text;
            articulo.Descripcion = this.txtDescripcion.Text;


            articulo.Alicuota =
                Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().BuscarAlicuota(Decimal.Parse(this.ddlAlicuota.SelectedValue));

            new Alicuota(Decimal.Parse(this.ddlAlicuota.SelectedValue));

            String grupoArticuloDescription = this.ddlGruposArticulos.SelectedValue;
            articulo.Grupo =
                Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().BuscarGrupoArticulo(grupoArticuloDescription);

            articulo.CantMinima = Convert.ToInt32(this.txtCantMinima.Text);
            articulo.CantActual = Convert.ToInt32(this.txtCantActual.Text);
            articulo.PrecioVenta = Convert.ToInt32(this.txtPrecioVenta.Text);


            articulo.AgregarProveedores(this.ProveedoresArticuloSessionVariable);

            Controladora.Sistema.ControladoraCUGestionarArticulos.ObtenerInstancia().GuardarArticulo(articulo);
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnVolverAMenu_Click(object sender, EventArgs e)
        {
            this.VolverAMenu();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            this.LimpiarControles();

            this.Inicializar();

            this.AgregarNuevoArticulo();
        }

        private void AgregarNuevoArticulo()
        {
            this.MostrarPanelABMArticulo(new Articulo(), Modo.Alta);
        }

        private void Notify(ITextControl textControl, string message, TypeNotif typeNotif)
        {
            Label l = textControl as Label;

            if (typeNotif == TypeNotif.Error)
            {
                l.ForeColor = Color.Red;
            }

            if (typeNotif == TypeNotif.Success)
            {
                l.ForeColor = Color.Green;
            }

            l.Text = message;
        }

        protected void GrillaArticulos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var btnModificarArticulo = e.Row.Cells[1];

            Boolean puedeModificarArticulo =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == "ModificarArticulo"));

            btnModificarArticulo.Enabled = puedeModificarArticulo;

            var btnEliminarArticulo = e.Row.Cells[2];

            Boolean puedeEliminarArticulo =
                this.UserSessionVariable.ColPerfiles.Any(p => p.ColAcciones.Any(a => a.NombreAccion == "EliminarArticulo"));

            btnEliminarArticulo.Enabled = puedeEliminarArticulo; 

        }


    }
}