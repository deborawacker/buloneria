﻿<%@ Page Title="Proveedor" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormProveedor.aspx.cs" Inherits="VistaWeb.FormProveedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        <asp:Label ID="Label2" runat="server" Text="Código:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCodigo" runat="server" Width="260px"></asp:TextBox>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Text="Cuit:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCuit" runat="server" Width="278px"></asp:TextBox>
    </p>
    <asp:Label ID="Label4" runat="server" Text="Tipo IVA:"></asp:Label>
&nbsp;<asp:DropDownList ID="dplTipoIva" runat="server" Height="30px" Width="253px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label5" runat="server" Text="Razón Social:"></asp:Label>
&nbsp;<asp:TextBox ID="txtRazonSocial" runat="server" Width="223px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label6" runat="server" Text="Nombre Fantasía:"></asp:Label>
&nbsp;<asp:TextBox ID="txtNombreFantasia" runat="server" Width="195px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label7" runat="server" Text="Dirección:"></asp:Label>
&nbsp;<asp:TextBox ID="txtDireccion" runat="server" Width="240px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label8" runat="server" Text="Provincia:"></asp:Label>
&nbsp;<asp:DropDownList ID="dplProvincia" AutoPostBack="true" OnSelectedIndexChanged="dplProvincia_SelectedIndexChanged" runat="server" Height="36px" Width="251px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label9" runat="server" Text="Ciudad:"></asp:Label>
&nbsp;<asp:DropDownList ID="dplCiudad" runat="server" Height="40px" Width="262px">
    </asp:DropDownList>
    <br />
    <br />
    <asp:Label ID="Label10" runat="server" Text="Código Postal:"></asp:Label>
&nbsp;<asp:TextBox ID="txtCodigoPostal" runat="server" Width="211px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label11" runat="server" Text="Teléfono:"></asp:Label>
&nbsp;<asp:TextBox ID="txtTelefono" runat="server" style="margin-bottom: 0px" Width="242px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label12" runat="server" Text="Fax (*):"></asp:Label>
&nbsp;<asp:TextBox ID="txtFax" runat="server" Width="250px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label13" runat="server" Text="Celular (*):"></asp:Label>
&nbsp;<asp:TextBox ID="txtCelular" runat="server" Width="229px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label14" runat="server" Text="E-Mail (*):"></asp:Label>
&nbsp;<asp:TextBox ID="txtEmail" runat="server" Width="229px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label15" runat="server" Text="(*) Campos Opcionales"></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnAceptar" runat="server" OnClick="btnAceptar_Click" Text="ACEPTAR" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" />
    <br />
</asp:Content>
