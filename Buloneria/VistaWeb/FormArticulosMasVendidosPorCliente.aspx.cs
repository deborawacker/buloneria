﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VistaWeb
{
    public partial class FormArticulosMasVendidosPorCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            String codCliente = this.txtCodCliente.Text;
            String razonSocial = txtRazonSocial.Text.Trim();

            List<Entidades.Sistema.Cliente> listaClientes = Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarClientePorCodigoORazonSocial(codCliente,razonSocial);
            
            if(listaClientes!=null && listaClientes.Count>0)
            {
                lblMensaje.Text = "";
               // lblMensaje.Text = String.Format("{0} {1}", listaClientes.First().RazonSocial, listaClientes.First().Telefono);
                LlenarGrilla(listaClientes);
            }
            else
            {

                this.grdClientes.DataSource = null;
                this.grdClientes.DataBind();
                lblMensaje.Text = "No se encontro ningun resultado con estos criterios de Busqueda !!!";
            }
        
        }

        private void LlenarGrilla(List<Entidades.Sistema.Cliente> listaClientes)
        {
            this.grdClientes.DataSource = listaClientes;
            this.grdClientes.DataBind();
        }

        protected void grdClientes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int indexGrilla = Convert.ToInt32(e.CommandArgument);
            switch (e.CommandName)
            {
                case "VerReporte":
                    generarReporteCliente(seleccionarCliente(indexGrilla)); 
                    break;
                default:
                    break;
            }      
        }

        private void generarReporteCliente(Entidades.Sistema.Cliente cliente)
        {
            Session["cuitCliente"] = cliente.Cuit;
            Page.Response.Redirect("FormReporteArticulosMasVendidosPorCliente.aspx");
        }


        private Entidades.Sistema.Cliente seleccionarCliente(int indexGrilla)
        {
            #region BusquedaIndiceGrilla
            GridViewRow selectedRow;
            TableCell tbcCuitCliente;
            string strCuitCliente;
            Int64 nroCuitClienteSeleccionado;
            Entidades.Sistema.Cliente clienteSeleccionado;

            selectedRow = this.grdClientes.Rows[indexGrilla];
            tbcCuitCliente = selectedRow.Cells[1];
            strCuitCliente = tbcCuitCliente.Text;
            nroCuitClienteSeleccionado = Convert.ToInt64(strCuitCliente);
            #endregion
            
            clienteSeleccionado= Controladora.Sistema.ControladoraReportes.ObtenerInstancia().BuscarClientePorCuit(nroCuitClienteSeleccionado);
            return clienteSeleccionado;
        }

    }
}