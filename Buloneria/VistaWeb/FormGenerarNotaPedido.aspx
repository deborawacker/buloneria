﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGenerarNotaPedido.aspx.cs" Inherits="VistaWeb.FormGenerarNotaPedido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     

    <div class="container">
        <br />
        <label for="lblFechaEmision">Fecha de Emisión Nota Pedido:</label>
        <asp:Label ID="lblFechaEmision" runat="server" />
        <br />
        <asp:Panel ID="PanelSeleccionCliente" runat="server" CssClass="container">
            <label>Seleccione un Cliente:</label>
            <asp:GridView ID="GrillaClientes" runat="server" CssClass="table-condensed" AutoGenerateColumns="false" OnRowCommand="GrillaClientes_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="SeleccionarCliente" Text="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField DataField="CodCliente" HeaderText="Codigo" />
                    <asp:BoundField DataField="NombreFantasia" HeaderText="Cliente" />
                    <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                    <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="PanelIngresoNotaPedido" runat="server" CssClass="container">
            <asp:Panel runat="server" CssClass="container">
                <h4>Datos Cliente</h4>
                <label for="lblNombreCliente">Cliente:</label>
                <asp:Label ID="lblNombreCliente" runat="server" />&nbsp;
                <label for="lblCUITCliente">CUIT:</label>
                <asp:Label ID="lblCUITCliente" runat="server" /><br />
                <label for="lblDireccion">Direccion:</label>
                <asp:Label ID="lblDireccion" runat="server" />&nbsp;
                <label for="lblTelefono">Teléfono:</label>
                <asp:Label ID="lblTelefono" runat="server" />
                <br/>
                <label for="ddlFormaPago">Forma Pago:</label>
                <asp:DropDownList ID="ddlFormaPago" runat="server" CssClass="form-control" Width="300px" />
            </asp:Panel>
            <br />
            <h4>Articulos Disponibles</h4>
            <asp:GridView ID="GrillaArticulosDisponibles" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaArticulosDisponibles_RowCommand">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Seleccionar" Text="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField HeaderText="Id" DataField="IdArticulo" />
                    <asp:BoundField HeaderText="Codigo" DataField="CodArticulo" />
                    <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Grupo Articulo" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Precio unitario" DataField="PrecioVenta" />
                    <asp:BoundField HeaderText="Cantidad Minima" DataField="CantMinima" />
                    <asp:BoundField HeaderText="Cantidad Actual" DataField="CantActual" />
                </Columns>
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
            <br />&nbsp;&nbsp;
             <asp:Button ID="btnDeshacer" runat="server" Text="Deshacer" CssClass="btn-primary" OnClick="btnDeshacer_Click" Visible="False" />
            <br/>
            <asp:Panel ID="PanelIngresoCantidadArticulo" runat="server" CssClass="container" Visible="false">
                <asp:Panel ID="PanelDatosArticuloSeleccionado" runat="server" CssClass="container">
                    <h5>Datos Articulo</h5>
                    <label for="lblDescripcionArticulo">Articulo:</label>
                    <asp:Label ID="lblDescripcionArticulo" runat="server" Text=""></asp:Label><br />
                    <label for="txtCantidadArticulo">Ingrese Cantidad:</label>
                    <asp:TextBox ID="txtCantidadArticulo" runat="server"></asp:TextBox>
                    <asp:Button ID="btnIngresarCantidad" runat="server" Text="Ingresar" CssClass="btn-primary" OnClick="btnIngresarCantidad_Click" />&nbsp;
                    <asp:Label ID="lblErrorIngresoCantidadArticulo" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <h4>Articulos Agregados a la Nota de Pedido</h4>
            <asp:GridView ID="GrillaArticulosAgregadosNotaPedido" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaArticulosAgregadosNotaPedido_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Eliminar" Text="Eliminar" ControlStyle-CssClass="btn-warning" />
                    <%--<asp:BoundField HeaderText="CodArticulo" DataField="Articulo.IdArticulo" /> --%>
                    <asp:BoundField HeaderText="CodArticulo" DataField="Articulo.CodArticulo" />
                    <asp:BoundField HeaderText="Descripcion" DataField="Articulo.Descripcion" />
                    <asp:BoundField HeaderText="Precio unitario" DataField="Articulo.PrecioVenta" />
                    <asp:BoundField HeaderText="Cantidad pedida" DataField="CantPedida" />
                    <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
                </Columns>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
            <br /> 
            <asp:Panel ID="PanelAceptarCancelar" runat="server" CssClass="container" Visible="false">
                <label for="lblSubtotal">Subtotal:</label>
                <asp:Label ID="lblSubtotal" runat="server" Text=""></asp:Label><br />
                <label for="lblIVA">IVA:</label>
                <asp:Label ID="lblIVA" runat="server" Text=""></asp:Label><br />
                <label for="lblTotal">Total:</label>
                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label><br />
                <br />
                <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" CssClass="btn-success" OnClick="btnAceptar_Click" />
                <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" CssClass="btn-primary" OnClick="btnCancelar_Click" />
            </asp:Panel>
        </asp:Panel>
        <br />
        <br />
        <asp:Button ID="btnVolverAMenu" runat="server" Text="Volver a menu" CssClass="btn-primary" OnClick="btnVolverAMenu_Click" />
    </div>
</asp:Content>
