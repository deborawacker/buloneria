﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormEgresoMercaderia.aspx.cs" Inherits="VistaWeb.FormEgresoMercaderia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <div class="container">
        <br />
        <asp:Button ID="btnVolver" runat="server" OnClick="btnVolver_Click" Text="Volver a Menu" CssClass="btn-success" />
        <br />
        <label for="lblFechaEgreso">Fecha de Egreso:</label>
        <asp:Label ID="lblFechaEgreso" runat="server" />
        <br />
        <asp:Label ID="lblDebug" runat="server" ForeColor="Red"></asp:Label><br />

        <asp:Panel ID="PanelFiltro" runat="server">
            <span>FILTRO</span>
            <br />
            <span>Numero de Nota de Pedido:</span>
            <asp:TextBox ID="txtNroNotaPedido" runat="server"></asp:TextBox>
            <br />
            <span>Codigo de cliente:</span>
            <asp:TextBox ID="txtCodigoCliente" runat="server"></asp:TextBox>
            <br />
            <span>Razon Social</span>
            <asp:TextBox ID="txtRazonSocial" Width="200" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="btnBuscarNotaPedido" runat="server" Text="Buscar" OnClick="btnBuscarNotaPedido_Click" Height="26px" CssClass="btn-primary" />
            &nbsp;&nbsp;
            <asp:Button ID="btnLimpiarCampos" runat="server" Text="Limpiar" OnClick="btnLimpiarCampos_Click" CssClass="btn-info" />
            <br />
            <asp:Label ID="lblMensajeFiltro" runat="server" ForeColor="Blue"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="PanelLista" runat="server">
            <h4>Notas Pedido Encontradas</h4>

            <asp:GridView ID="GrillaNotasPedido" runat="server" AutoGenerateColumns="False" CssClass="table-condensed" OnRowCommand="GrillaNotasPedido_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" Text="Seleccionar" CommandName="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField DataField="NroNotaPedido" HeaderText="NroNotaPedido" />
                    <asp:BoundField DataField="Cliente.RazonSocial" HeaderText="RazonSocial" />
                    <asp:BoundField DataField="FechaEmisionCorta" HeaderText="Fecha Emision" />
                </Columns>
            </asp:GridView>
            <br />
            <asp:Label ID="lblMensajeLista" runat="server" ForeColor="Red"></asp:Label><br />
            <br />

            <asp:Button ID="btnVolverAFiltro" runat="server" Text="Volver a Filtro" OnClick="btnVolverAFiltro_Click" CssClass="btn-primary" />

        </asp:Panel>

        <asp:Panel ID="PanelDetalle" runat="server">
            <span>Cliente:</span>
            <asp:Label ID="lblRazonSocialCliente" runat="server"></asp:Label>
            <br />
            <span>Numero de Nota de Pedido:</span>
            <asp:Label ID="lblNroNotaPedido" runat="server"></asp:Label>
            <br />
            <asp:GridView ID="GrillaDetalleArticulosNotaPedido" runat="server" AutoGenerateColumns="False" CssClass="table-condensed" OnRowCommand="GrillaDetalleArticulosNotaPedido_RowCommand">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:ButtonField ButtonType="Button" Text="Seleccionar" CommandName="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField DataField="NroLineaNotaPedido" HeaderText="NroLineaNotaPedido" />
                    <asp:BoundField DataField="Articulo.CodArticulo" HeaderText="Codigo Articulo" />
                    <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                    <asp:BoundField DataField="CantPedida" HeaderText="Cantidad" />
                    <asp:BoundField DataField="CantEntregada" HeaderText="Entregado" />
                    <asp:BoundField DataField="CantFaltanteEntrega" HeaderText="Faltante de Entrega" />
                </Columns>

            </asp:GridView>

            <br />
            <asp:Panel ID="PanelIngresoCantidad" runat="server">
                <span>Articulo Seleccionado:</span><br />
                <asp:Label ID="lblNombreArticulo" runat="server" Text=""></asp:Label><br />
                <br />
                <span>Cantidad:</span>
                <asp:TextBox ID="txtCantidadEntregada" runat="server" Width="69px" onKeyPress="return soloNumeros(event)"></asp:TextBox><br />
                <asp:Label ID="lblMensajeDetalle" runat="server" ForeColor="Red"></asp:Label><br />
                <br />
                <asp:Button ID="btnIngresarCantidad" runat="server" Text="Ingresar" OnClick="btnIngresarCantidad_Click" />
            </asp:Panel>
            <br />

            <asp:Panel ID="PanelGrillaArticulosAEntregar" runat="server" Visible="False">
                <h4>Articulos A Entregar</h4>
                <asp:GridView ID="GrillaArticulosAEntregar" runat="server" AutoGenerateColumns="False" CssClass="table-condensed">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="NroLineaNotaPedido" HeaderText="NroLineaNotaPedido" />
                        <asp:BoundField DataField="Articulo.CodArticulo" HeaderText="Codigo Articulo" />
                        <asp:BoundField DataField="Articulo.Descripcion" HeaderText="Articulo" />
                        <asp:BoundField DataField="CantEntregada" HeaderText="Cantidad Entregada" />
                        <asp:BoundField DataField="CantFaltanteEntrega" HeaderText="Faltante de Entrega" />
                    </Columns>
                </asp:GridView>
                <asp:Button ID="btnGuardar" runat="server" Text="Aceptar" OnClick="btnGuardar_Click" Visible="false" CssClass="btn-success" />
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" CssClass="btn-default" />
            </asp:Panel>


        </asp:Panel>



    </div>
</asp:Content>



