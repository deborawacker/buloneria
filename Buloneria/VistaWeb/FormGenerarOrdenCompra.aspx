﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.Master" AutoEventWireup="true" CodeBehind="FormGenerarOrdenCompra.aspx.cs" Inherits="VistaWeb.FormGenerarOrdenCompra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <br />
        <label for="lblFechaEmision">Fecha de Emisión Orden Compra:</label>
        <asp:Label ID="lblFechaEmision" runat="server" />
        <br />
        <asp:Panel ID="PanelSeleccionProveedor" runat="server" CssClass="container">
            <label>Seleccione un proveedor:</label>
            <asp:GridView ID="GrillaProveedores" runat="server" CssClass="table-condensed" AutoGenerateColumns="false" OnRowCommand="GrillaProveedores_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="SeleccionarProveedor" Text="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField DataField="CodProveedor" HeaderText="Codigo" />
                    <asp:BoundField DataField="NombreFantasia" HeaderText="Proveedor" />
                    <asp:BoundField DataField="Direccion" HeaderText="Direccion" />
                    <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
        </asp:Panel>

        <asp:Panel ID="PanelIngresoOrdenCompra" runat="server" CssClass="container">
            <asp:Panel runat="server" CssClass="container">
                <h4>Datos Proveedor</h4>
                <label for="lblNombreProveedor">Proveedor:</label>
                <asp:Label ID="lblNombreProveedor" runat="server" />&nbsp;
                <label for="lblCUITProveedor">CUIT:</label>
                <asp:Label ID="lblCUITProveedor" runat="server" /><br />
                <label for="lblDireccion">Direccion:</label>
                <asp:Label ID="lblDireccion" runat="server" />&nbsp;
                <label for="lblTelefono">Teléfono:</label>
                <asp:Label ID="lblTelefono" runat="server" />
            </asp:Panel>

            <!--REGION Selector Fecha Plazo y Forma Pago -->
            <asp:Panel ID="PanelFechaFormaPago" runat="server" CssClass="container">
                <label for="ddlFormaPago">Forma Pago:</label>
                <asp:DropDownList ID="ddlFormaPago" runat="server" CssClass="form-control" Width="300px" />
                <label for="txtFechaPlazo">Fecha Plazo:</label>
                <asp:TextBox ID="txtFechaPlazo" runat="server" ReadOnly="True" Width="117px"></asp:TextBox>
                <asp:ImageButton ID="btnCalendarFechaPlazo" runat="server" BackColor="White" Height="21px" ImageUrl="~/img/date_add.png" OnClick="btnCalendarFechaPlazo_Click" Width="22px" />
                <asp:Calendar ID="calendarFechaPlazo" runat="server" OnSelectionChanged="CalendarFechaPlazo_SelectionChanged"
                    ForeColor="#003399" Height="200px" Font-Size="8pt" Visible="false"
                    Font-Names="Verdana" BorderColor="#3366CC" DayNameFormat="Shortest" CellPadding="1">
                    <TodayDayStyle ForeColor="White" BackColor="#99CCCC"></TodayDayStyle>
                    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666"></SelectorStyle>
                    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF"></NextPrevStyle>
                    <DayHeaderStyle
                        BackColor="#99CCCC" ForeColor="#336666" Height="1px"></DayHeaderStyle>
                    <SelectedDayStyle Font-Bold="True" ForeColor="#CCFF99"
                        BackColor="#009999"></SelectedDayStyle>
                    <TitleStyle Font-Bold="True" BorderColor="#3366CC"
                        BackColor="#003399" BorderWidth="1px" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px"></TitleStyle>
                    <WeekendDayStyle BackColor="#CCCCFF"></WeekendDayStyle>
                    <OtherMonthDayStyle ForeColor="#999999"></OtherMonthDayStyle>
                </asp:Calendar>
                <asp:Label ID="lblErrorFecha" runat="server" Font-Size="Small" ForeColor="#CC0000" Text=""></asp:Label>
                <!--ENDREGION Selector Fecha Plazo y Forma Pago -->
            </asp:Panel>
            <br />
            <h4>Articulos Disponibles Proveedor Seleccionado</h4>
            <asp:GridView ID="GrillaArticulosDisponiblesProveedorSeleccionado" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaArticulosDisponiblesProveedorSeleccionado_RowCommand">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Seleccionar" Text="Seleccionar" ControlStyle-CssClass="btn-info" />
                    <asp:BoundField HeaderText="Id" DataField="IdArticulo" />
                    <asp:BoundField HeaderText="Codigo" DataField="CodArticulo" />
                    <asp:BoundField HeaderText="Descripcion" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Grupo Articulo" DataField="Descripcion" />
                    <asp:BoundField HeaderText="Cantidad Minima" DataField="CantMinima" />
                    <asp:BoundField HeaderText="Cantidad Actual" DataField="CantActual" />
                </Columns>
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>

            <asp:Panel ID="PanelIngresoCantidadArticulo" runat="server" CssClass="container" Visible="false">
                <asp:Panel ID="PanelDatosArticuloSeleccionado" runat="server" CssClass="container">
                    <h5>Datos Articulo</h5>
                    <label for="lblDescripcionArticulo">Articulo:</label>
                    <asp:Label ID="lblDescripcionArticulo" runat="server" Text=""></asp:Label><br />
                    <label for="txtCantidadArticulo">Ingrese Cantidad:</label>
                    <asp:TextBox ID="txtCantidadArticulo" runat="server"></asp:TextBox>
                    <asp:Button ID="btnIngresarCantidad" runat="server" Text="Ingresar" CssClass="btn-primary" OnClick="btnIngresarCantidad_Click" />&nbsp;
                    <asp:Label ID="lblErrorIngresoCantidadArticulo" runat="server" Text="" Font-Size="Small" ForeColor="#CC0000"></asp:Label>
                </asp:Panel>
            </asp:Panel>

            <h4>Articulos Agregados a la Orden de compra</h4>
            <asp:GridView ID="GrillaArticulosAgregadosOrdenCompra" runat="server" AutoGenerateColumns="false" CssClass="table-condensed" OnRowCommand="GrillaArticulosAgregadosOrdenCompra_RowCommand">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Eliminar" Text="Eliminar" ControlStyle-CssClass="btn-warning" />
                    <%--<asp:BoundField HeaderText="CodArticulo" DataField="Articulo.IdArticulo" /> --%>
                    <asp:BoundField HeaderText="CodArticulo" DataField="Articulo.CodArticulo" />
                    <asp:BoundField HeaderText="Descripcion" DataField="Articulo.Descripcion" />
                    <asp:BoundField HeaderText="Precio unitario" DataField="Articulo.PrecioVenta" />
                    <asp:BoundField HeaderText="Cantidad a pedir" DataField="CantPedir" />
                    <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" />
                </Columns>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
            <br />
            <asp:Label ID="lblErrorCompraSinArticulos" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
            <br />
            <asp:Panel ID="PanelAceptarCancelar" runat="server" CssClass="container" Visible="false">
                <label for="lblSubtotal">Subtotal:</label>
                <asp:Label ID="lblSubtotal" runat="server" Text=""></asp:Label><br />
                <label for="lblIVA">IVA:</label>
                <asp:Label ID="lblIVA" runat="server" Text=""></asp:Label><br />
                <label for="lblTotal">Total:</label>
                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label><br />
                <br />
                <asp:Button ID="btnAceptar" runat="server" Text="ACEPTAR" CssClass="btn-success" OnClick="btnAceptar_Click" />
                <asp:Button ID="btnCancelar" runat="server" Text="CANCELAR" CssClass="btn-primary" OnClick="btnCancelar_Click" />
            </asp:Panel>
        </asp:Panel>
        <br />
        <br />
        <asp:Button ID="btnVolverAMenu" runat="server" Text="Volver a menu" CssClass="btn-primary" OnClick="btnVolverAMenu_Click" />

    </div>
</asp:Content>
