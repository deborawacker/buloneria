USE [master]
GO
/****** Object:  Database [auditoria]    Script Date: 28/09/2015 15:36:45 ******/
CREATE DATABASE [auditoria]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'auditoria', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\auditoria.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'auditoria_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\auditoria_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [auditoria] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [auditoria].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [auditoria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [auditoria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [auditoria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [auditoria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [auditoria] SET ARITHABORT OFF 
GO
ALTER DATABASE [auditoria] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [auditoria] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [auditoria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [auditoria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [auditoria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [auditoria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [auditoria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [auditoria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [auditoria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [auditoria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [auditoria] SET  ENABLE_BROKER 
GO
ALTER DATABASE [auditoria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [auditoria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [auditoria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [auditoria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [auditoria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [auditoria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [auditoria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [auditoria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [auditoria] SET  MULTI_USER 
GO
ALTER DATABASE [auditoria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [auditoria] SET DB_CHAINING OFF 
GO
ALTER DATABASE [auditoria] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [auditoria] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [auditoria]
GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarRegistros]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarRegistros]
AS
Select Usuario, Operacion, Equipo, FechaHora From LoginOut

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarRegistrosOC]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ConsultarRegistrosOC] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT nroOrdenCompra,fechaRegistro,usuario,estado
	FROM OrdenCompra
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RegistrarCambioEstadoOrdenCompra]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RegistrarCambioEstadoOrdenCompra]
@nroOC int,
@FechaHora datetime,
@Usuario varchar(50),
@estado varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO OrdenCompra(nroOrdenCompra,fechaRegistro,usuario,estado)
	VALUES(@nroOC,@FechaHora,@Usuario,@estado)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_RegistrarLogInOut]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_RegistrarLogInOut](@Usuario varchar(50), @FechaHora Datetime, @Operacion varchar(50), @Equipo varchar(50))
AS
INSERT INTO LoginOut (Usuario,FechaHora,Operacion,Equipo) Values (@Usuario,@FechaHora,@Operacion,@Equipo)

GO
/****** Object:  Table [dbo].[Faltantes]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faltantes](
	[id_oc] [int] IDENTITY(1,1) NOT NULL,
	[nro_oc] [int] NOT NULL,
	[descripcionArticulo] [varchar](50) NOT NULL,
	[cantidad] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_oc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoginOut]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoginOut](
	[Id_Auditoria] [int] IDENTITY(1,1) NOT NULL,
	[FechaHora] [datetime] NULL,
	[Equipo] [varchar](50) NULL,
	[Operacion] [varchar](50) NULL,
	[Usuario] [varchar](50) NULL,
 CONSTRAINT [PK_LoginOut] PRIMARY KEY CLUSTERED 
(
	[Id_Auditoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdenCompra]    Script Date: 28/09/2015 15:36:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdenCompra](
	[nroOrdenCompra] [int] NULL,
	[fechaRegistro] [datetime] NULL,
	[usuario] [varchar](50) NULL,
	[estado] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[LoginOut] ON 

INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (1, CAST(0x0000A07C011CCC19 AS DateTime), N'JUANDROLL-PC', N'Ingreso', N'juandroll')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (2, CAST(0x0000A316010B7909 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (3, CAST(0x0000A316010B97C3 AS DateTime), N'DEBI-PC', N'Egreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (4, CAST(0x0000A452011350C9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (5, CAST(0x0000A4520113A8D7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (6, CAST(0x0000A45201147701 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (7, CAST(0x0000A452011FC579 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (8, CAST(0x0000A452011FC902 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (9, CAST(0x0000A452011FCD12 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (10, CAST(0x0000A452011FCF75 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (11, CAST(0x0000A452011FD47B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (12, CAST(0x0000A452011FD7E8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (13, CAST(0x0000A4520120563E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (14, CAST(0x0000A4520122E516 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (15, CAST(0x0000A45201265FE2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (16, CAST(0x0000A459011CE278 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (17, CAST(0x0000A459012172BA AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (18, CAST(0x0000A4590124E2F9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (19, CAST(0x0000A459012679F8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (20, CAST(0x0000A459012AC2FB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (21, CAST(0x0000A459012BD2A2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (22, CAST(0x0000A459012C0359 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (23, CAST(0x0000A459012C30DF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (24, CAST(0x0000A459012C868D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (25, CAST(0x0000A459012D3C57 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (26, CAST(0x0000A459012DCE5D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (27, CAST(0x0000A459012E7DC6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (28, CAST(0x0000A459012F1D6D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (29, CAST(0x0000A459012F4089 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (30, CAST(0x0000A459012FA032 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (31, CAST(0x0000A459012FDABD AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (32, CAST(0x0000A45901320B12 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (33, CAST(0x0000A459013727DC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (34, CAST(0x0000A45901378689 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (35, CAST(0x0000A4590137AF5D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (36, CAST(0x0000A4590137F05C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (37, CAST(0x0000A45901381A8A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (38, CAST(0x0000A4590139D7BD AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (39, CAST(0x0000A460011A5D4D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (40, CAST(0x0000A460011ADBA6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (41, CAST(0x0000A460011C9428 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (42, CAST(0x0000A460011DA192 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (43, CAST(0x0000A46001220BF1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (44, CAST(0x0000A46001234B44 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (45, CAST(0x0000A4600129726D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (46, CAST(0x0000A460012D4EC1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (47, CAST(0x0000A460012E2071 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (48, CAST(0x0000A460012FD9E1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (49, CAST(0x0000A460013192E2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (50, CAST(0x0000A460013363FA AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (51, CAST(0x0000A4600133A22A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (52, CAST(0x0000A4600135AC6E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (53, CAST(0x0000A46001379A69 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (54, CAST(0x0000A46001380E35 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (55, CAST(0x0000A4600138D38B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (56, CAST(0x0000A47C011503A0 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (57, CAST(0x0000A4830115D719 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (58, CAST(0x0000A48301174D1C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (59, CAST(0x0000A4830118F045 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (60, CAST(0x0000A483011937A6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (61, CAST(0x0000A483011A9885 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (62, CAST(0x0000A483011AD154 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (63, CAST(0x0000A483011AF18C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (64, CAST(0x0000A483011C04B6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (65, CAST(0x0000A483011E4365 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (66, CAST(0x0000A483011FC675 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (67, CAST(0x0000A48301201CDB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (68, CAST(0x0000A48301251CB8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (69, CAST(0x0000A4830126467D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (70, CAST(0x0000A48301272AC9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (71, CAST(0x0000A48301287A9F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (72, CAST(0x0000A4830129FECA AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (73, CAST(0x0000A483012ADB6A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (74, CAST(0x0000A48A010AA5D4 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (75, CAST(0x0000A48A010B0A1C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (76, CAST(0x0000A48A010B4218 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (77, CAST(0x0000A48A010E13F7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (78, CAST(0x0000A48A010E5A39 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (79, CAST(0x0000A48A010EE20E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (80, CAST(0x0000A48A0113BC0E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (81, CAST(0x0000A48A01194738 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (82, CAST(0x0000A48A011C7415 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (83, CAST(0x0000A48A011E7651 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (84, CAST(0x0000A48A011F9760 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (85, CAST(0x0000A48A012326EC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (86, CAST(0x0000A49101124E91 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (87, CAST(0x0000A49101126678 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (88, CAST(0x0000A49101254D34 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (89, CAST(0x0000A49101270F68 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (90, CAST(0x0000A49101277B3A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (91, CAST(0x0000A4910129C69E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (92, CAST(0x0000A491012A2841 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (93, CAST(0x0000A491012A652C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (94, CAST(0x0000A491012B1D85 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (95, CAST(0x0000A498010AF756 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (96, CAST(0x0000A498010C44F2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (97, CAST(0x0000A498010D2694 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (98, CAST(0x0000A498010E81CB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (99, CAST(0x0000A498010EBB37 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
GO
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (100, CAST(0x0000A498010F415A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (101, CAST(0x0000A498010FD3C1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (102, CAST(0x0000A498011058BB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (103, CAST(0x0000A49801109D6F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (104, CAST(0x0000A4980112A641 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (105, CAST(0x0000A49801143B73 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (106, CAST(0x0000A4980115D36D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (107, CAST(0x0000A498011AA936 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (108, CAST(0x0000A498011BB2F7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (109, CAST(0x0000A498011CFFD0 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (110, CAST(0x0000A49801265890 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (111, CAST(0x0000A49801295437 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (112, CAST(0x0000A4980129B914 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (113, CAST(0x0000A49F010A3682 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (114, CAST(0x0000A49F010C31EA AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (115, CAST(0x0000A49F010D4CBE AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (116, CAST(0x0000A49F010FAE80 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (117, CAST(0x0000A49F010FAF0B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (118, CAST(0x0000A49F011651DF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (119, CAST(0x0000A49F011A7F08 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (120, CAST(0x0000A49F011D89C3 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (121, CAST(0x0000A49F0121E724 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (122, CAST(0x0000A49F0122699D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (123, CAST(0x0000A49F0123C61F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (124, CAST(0x0000A49F01240B2C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (125, CAST(0x0000A49F01258D09 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (126, CAST(0x0000A49F01274F58 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (127, CAST(0x0000A49F0127AC31 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (128, CAST(0x0000A4A1012ED83F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (129, CAST(0x0000A4A10132E0BB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (130, CAST(0x0000A4A6010A9E1B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (131, CAST(0x0000A4A6010AD59B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (132, CAST(0x0000A4A6010B2DD9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (133, CAST(0x0000A4A6010D7F7C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (134, CAST(0x0000A4A6010E23D3 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (135, CAST(0x0000A4A60110464C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (136, CAST(0x0000A4A60110EBD3 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (137, CAST(0x0000A4A601112A1F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (138, CAST(0x0000A4A601141E2B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (139, CAST(0x0000A4A601149A6F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (140, CAST(0x0000A4A60114FCC6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (141, CAST(0x0000A4A601163C05 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (142, CAST(0x0000A4A60116A404 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (143, CAST(0x0000A4A60118006C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (144, CAST(0x0000A4A601191B41 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (145, CAST(0x0000A4A6011969DE AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (146, CAST(0x0000A4A60119BEE2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (147, CAST(0x0000A4A6011A6812 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (148, CAST(0x0000A4A6011F7930 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (149, CAST(0x0000A4A60128B910 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (150, CAST(0x0000A4AD01097DF2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (151, CAST(0x0000A4AD010A9220 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (152, CAST(0x0000A4AD010D8386 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (153, CAST(0x0000A4AD0111C64B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (154, CAST(0x0000A4AD01123989 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (155, CAST(0x0000A4AD011365DC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (156, CAST(0x0000A4AD011466E9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (157, CAST(0x0000A4AD01199ADF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (158, CAST(0x0000A4AD011B317C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (159, CAST(0x0000A4AD011CC237 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (160, CAST(0x0000A4AD011F2EBB AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (161, CAST(0x0000A4AD011FAF2B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (162, CAST(0x0000A4AD012031E2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (163, CAST(0x0000A4AD012208B5 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (164, CAST(0x0000A4AD012287CD AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (165, CAST(0x0000A4AD01236949 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (166, CAST(0x0000A4AD012382E6 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (167, CAST(0x0000A4AD012486ED AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (168, CAST(0x0000A4AD012585C0 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (169, CAST(0x0000A4AD0125F37B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (170, CAST(0x0000A4AD01263421 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (171, CAST(0x0000A4AD01266B43 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (172, CAST(0x0000A4AD0128126E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (173, CAST(0x0000A4AD01293690 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (174, CAST(0x0000A4BD00C26DC1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (175, CAST(0x0000A4BD00C418D0 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (176, CAST(0x0000A4BD00CB1524 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (177, CAST(0x0000A4C00179ED3F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (178, CAST(0x0000A4C0017DC7DE AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (179, CAST(0x0000A4C2010945B3 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (180, CAST(0x0000A4C2011881B4 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (181, CAST(0x0000A4C2011ABCDD AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (182, CAST(0x0000A4C2017CE6E9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (183, CAST(0x0000A4C400D2B21B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (184, CAST(0x0000A4C400D5FD84 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (185, CAST(0x0000A4C400D7284A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (186, CAST(0x0000A4C400DA8A61 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (187, CAST(0x0000A4C400DB7358 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (188, CAST(0x0000A4C4012CA5A8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (189, CAST(0x0000A4C4013B5A42 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (190, CAST(0x0000A4C401597252 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (191, CAST(0x0000A4C50178F9E4 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (192, CAST(0x0000A4C501813B6D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (193, CAST(0x0000A4C50183C545 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (194, CAST(0x0000A4C50183DABE AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (195, CAST(0x0000A4C600CF8F2C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (196, CAST(0x0000A4C600D70C2B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (197, CAST(0x0000A4C600DAC44E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (198, CAST(0x0000A4C600DE2EF2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (199, CAST(0x0000A4C600DE8DA5 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
GO
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (200, CAST(0x0000A4C600DF3754 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (201, CAST(0x0000A4C600E0575F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (202, CAST(0x0000A4C600E94CEF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (203, CAST(0x0000A4C600EB4CA8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (204, CAST(0x0000A4C600EC0257 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (205, CAST(0x0000A4C600EF9019 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (206, CAST(0x0000A4C601555552 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (207, CAST(0x0000A4C700C654EC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (208, CAST(0x0000A4CB00D47387 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (209, CAST(0x0000A4CB00DCFD38 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (210, CAST(0x0000A4CB00DFB43C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (211, CAST(0x0000A4CB00E034C8 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (212, CAST(0x0000A4CB00E08296 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (213, CAST(0x0000A4CB00E0F1EF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (214, CAST(0x0000A4CB00E1AE30 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (215, CAST(0x0000A4CB00E1E763 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (216, CAST(0x0000A4CB00E2850A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (217, CAST(0x0000A4CB00E2EE08 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (218, CAST(0x0000A4CB00E48D9A AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (219, CAST(0x0000A4CB00E54A5F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (220, CAST(0x0000A4CB00E576D1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (221, CAST(0x0000A4CB00EA1809 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (222, CAST(0x0000A4CB00EB154C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (223, CAST(0x0000A4CB00EBB6FC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (224, CAST(0x0000A4CB00ED2996 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (225, CAST(0x0000A4CB00EF6935 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (226, CAST(0x0000A4CB00F03814 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (227, CAST(0x0000A4CB00F0EE83 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (228, CAST(0x0000A4CB00F114D5 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (229, CAST(0x0000A4CB00F1FD25 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (230, CAST(0x0000A4CE014CCED3 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (231, CAST(0x0000A4CE014D9AC7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (232, CAST(0x0000A4CE0150939E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (233, CAST(0x0000A4CE01526D88 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (234, CAST(0x0000A4CE0152A27B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (235, CAST(0x0000A4CE015C9CC1 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (236, CAST(0x0000A4D000EA2EB5 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (237, CAST(0x0000A4D000EADD6F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (238, CAST(0x0000A4D000EB2513 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (239, CAST(0x0000A4D000EB805F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (240, CAST(0x0000A4D000EE00AC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (241, CAST(0x0000A4D000EEBD33 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (242, CAST(0x0000A4D000EEE5C7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (243, CAST(0x0000A4D000F1C627 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (244, CAST(0x0000A4D000FA3D2D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (245, CAST(0x0000A4D000FA7D26 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (246, CAST(0x0000A4D000FABBF2 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (247, CAST(0x0000A4D000FBE2DE AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (248, CAST(0x0000A4D200D092DA AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (249, CAST(0x0000A4D200D44AC7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (250, CAST(0x0000A4D200D4BE3E AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (251, CAST(0x0000A4D200DD416C AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (252, CAST(0x0000A4D200DF746D AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (253, CAST(0x0000A4D200E1C0FC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (254, CAST(0x0000A4D200E20EB9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (255, CAST(0x0000A4D200E65460 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (256, CAST(0x0000A4D200E78BD9 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (257, CAST(0x0000A4D200EB5983 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (258, CAST(0x0000A4D200EBF294 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (259, CAST(0x0000A4D200ED3C39 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (260, CAST(0x0000A4D3015A1A51 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (261, CAST(0x0000A4D30166A269 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (262, CAST(0x0000A4D3016AFB8F AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (263, CAST(0x0000A4D3016C1660 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (264, CAST(0x0000A4D3017407FF AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (265, CAST(0x0000A4D301740877 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (266, CAST(0x0000A4D40153263B AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (267, CAST(0x0000A4D401539DDC AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (268, CAST(0x0000A4D4015497F7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (269, CAST(0x0000A4D600D29281 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (270, CAST(0x0000A4D600D30DC7 AS DateTime), N'DEBI-PC', N'Ingreso', N'usu')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (271, CAST(0x0000A4D900DC78B5 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (272, CAST(0x0000A4D900DD5120 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (273, CAST(0x0000A4D900DE6F78 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (274, CAST(0x0000A4D900EC85DF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (275, CAST(0x0000A4D900ECB59E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (276, CAST(0x0000A4D900ED12AD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (277, CAST(0x0000A4D900ED871E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (278, CAST(0x0000A4D900EE436E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (279, CAST(0x0000A4D900EE6857 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (280, CAST(0x0000A4D900EE9ECF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (281, CAST(0x0000A4DB00ECCACE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (282, CAST(0x0000A4DB010DEAF8 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (283, CAST(0x0000A4DB01171441 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (284, CAST(0x0000A4DB011F2D9C AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (285, CAST(0x0000A4DB018AAAAB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (286, CAST(0x0000A4DD00CD3BFD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (287, CAST(0x0000A4DD00CD46B1 AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (288, CAST(0x0000A4DD00CDEC2D AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (289, CAST(0x0000A4DD00CDF558 AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (290, CAST(0x0000A4DD00CE2241 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (291, CAST(0x0000A4DD00CE2D02 AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (292, CAST(0x0000A4DD00CE34EE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (293, CAST(0x0000A4DD00CE46BE AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (294, CAST(0x0000A4DD00CEE86A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (295, CAST(0x0000A4DD00DD564F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (296, CAST(0x0000A4DD00E0BEFE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (297, CAST(0x0000A4DD00E549D7 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (298, CAST(0x0000A4DE00FC7778 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (299, CAST(0x0000A4DE00FD4652 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
GO
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (300, CAST(0x0000A4DE00FD8B44 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (301, CAST(0x0000A4DE00FF0EBD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (302, CAST(0x0000A4DE00FF2918 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (303, CAST(0x0000A4DE00FFE559 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (304, CAST(0x0000A4DE010214DD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (305, CAST(0x0000A4DE0105B073 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (306, CAST(0x0000A4DE0105E5BA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (307, CAST(0x0000A4DE01063BF0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (308, CAST(0x0000A4DE010716C7 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (309, CAST(0x0000A4DE01079EFF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (310, CAST(0x0000A4DE01080EEB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (311, CAST(0x0000A4DE01090B39 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (312, CAST(0x0000A4DE01093745 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (313, CAST(0x0000A4DE01098331 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (314, CAST(0x0000A4DE0109C6F7 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (315, CAST(0x0000A4DE0109F821 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (316, CAST(0x0000A4DE010A25AE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (317, CAST(0x0000A4DE010A7A26 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (318, CAST(0x0000A4DE010B6611 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (319, CAST(0x0000A4DE010CA342 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (320, CAST(0x0000A4DE010CD9D4 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (321, CAST(0x0000A4DE010D886F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (322, CAST(0x0000A4DE010E2F8E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (323, CAST(0x0000A4DE010E7647 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (324, CAST(0x0000A4DE010EEB0F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (325, CAST(0x0000A4DE010F78AB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (326, CAST(0x0000A4DE01104D9F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (327, CAST(0x0000A4DE01149F0B AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (328, CAST(0x0000A4DE01194711 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (329, CAST(0x0000A4DE01198E88 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (330, CAST(0x0000A4DE0119DE15 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (331, CAST(0x0000A4DF0183DAEB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (332, CAST(0x0000A4DF01842F67 AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (333, CAST(0x0000A4DF01843A62 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (334, CAST(0x0000A4DF0184534A AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (335, CAST(0x0000A4DF01845C45 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (336, CAST(0x0000A4DF01848EE7 AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (337, CAST(0x0000A4E000D1714B AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (338, CAST(0x0000A4E000E48A90 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (339, CAST(0x0000A4E000E76878 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (340, CAST(0x0000A4E000E867F5 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (341, CAST(0x0000A4E000E8C2B3 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (342, CAST(0x0000A4E000F0F31B AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (343, CAST(0x0000A4E000F19E07 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (344, CAST(0x0000A4E000F1E85A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (345, CAST(0x0000A4E100D9DE7F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (346, CAST(0x0000A4E100DB71B1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (347, CAST(0x0000A4E100DBB07E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (348, CAST(0x0000A4E100DDA9B5 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (349, CAST(0x0000A4E100DF324A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (350, CAST(0x0000A4E100DFA317 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (351, CAST(0x0000A4E100E03885 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (352, CAST(0x0000A4E100E0E7F9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (353, CAST(0x0000A4E100E1B5BD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (354, CAST(0x0000A4E100E1F7EA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (355, CAST(0x0000A4E100E38779 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (356, CAST(0x0000A4E100E422CB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (357, CAST(0x0000A4E100E4FB08 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (358, CAST(0x0000A4E100E57721 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (359, CAST(0x0000A4E100F25991 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (360, CAST(0x0000A4E100F34432 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (361, CAST(0x0000A4E100F608F9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (362, CAST(0x0000A4E100F73DBD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (363, CAST(0x0000A4E100F8025A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (364, CAST(0x0000A4E4013540DC AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (365, CAST(0x0000A4E4014FDA1D AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (366, CAST(0x0000A4E50109C646 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (367, CAST(0x0000A4E5010B91A1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (368, CAST(0x0000A4E5010BF5CF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (369, CAST(0x0000A4E5010E2760 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (370, CAST(0x0000A4E501107B12 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (371, CAST(0x0000A4E501114359 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (372, CAST(0x0000A4E5011190B3 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (373, CAST(0x0000A4E50111FB94 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (374, CAST(0x0000A4E50112344A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (375, CAST(0x0000A4E501127EDA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (376, CAST(0x0000A4E501134972 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (377, CAST(0x0000A4E50113BFBD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (378, CAST(0x0000A4E50113DFDE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (379, CAST(0x0000A4E50113FC9E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (380, CAST(0x0000A4E501152AB9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (381, CAST(0x0000A4E5011EFD95 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (382, CAST(0x0000A4E5012045ED AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (383, CAST(0x0000A4E50120DF11 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (384, CAST(0x0000A4E5012345FF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (385, CAST(0x0000A4F500D22405 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (386, CAST(0x0000A4F500D2AB87 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (387, CAST(0x0000A4F500DABA4B AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (388, CAST(0x0000A4F500DDED85 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (389, CAST(0x0000A4F500DEA854 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (390, CAST(0x0000A4F500E10FB5 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (391, CAST(0x0000A4F500E1635A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (392, CAST(0x0000A4F500E2DC1A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (393, CAST(0x0000A4F500E424A4 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (394, CAST(0x0000A4F500E44F99 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (395, CAST(0x0000A4F500E48EC4 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (396, CAST(0x0000A4F500E5E410 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (397, CAST(0x0000A4F500E82A43 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (398, CAST(0x0000A4F500E87530 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (399, CAST(0x0000A4F500EABF70 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
GO
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (400, CAST(0x0000A4F500EB347C AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (401, CAST(0x0000A4F500EBBD4A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (402, CAST(0x0000A4F70170E1E8 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (403, CAST(0x0000A4F70174769A AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (404, CAST(0x0000A4F800C723C9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (405, CAST(0x0000A4F800CA4E8D AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (406, CAST(0x0000A4F800CB732E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (407, CAST(0x0000A4F800CEF927 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (408, CAST(0x0000A4FC00D4DCF7 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (409, CAST(0x0000A4FC00D86211 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (410, CAST(0x0000A4FC00D9881A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (411, CAST(0x0000A4FC00E0F529 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (412, CAST(0x0000A4FC00E12709 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (413, CAST(0x0000A4FC00EA1BBB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (414, CAST(0x0000A4FC00ECAACF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (415, CAST(0x0000A4FC00ED4EEE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (416, CAST(0x0000A4FC00EFE3FB AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (417, CAST(0x0000A4FE00F60011 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (418, CAST(0x0000A4FE0105EC74 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (419, CAST(0x0000A4FE010A2636 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (420, CAST(0x0000A4FE010C1B78 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (421, CAST(0x0000A4FE0112C320 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (422, CAST(0x0000A4FE01134B38 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (423, CAST(0x0000A4FE011524E1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (424, CAST(0x0000A4FE01156230 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (425, CAST(0x0000A4FE01173DAA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (426, CAST(0x0000A4FE0117FDB0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (427, CAST(0x0000A50300D4E4E0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (428, CAST(0x0000A50300D5E9EE AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (429, CAST(0x0000A50300D64B22 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (430, CAST(0x0000A50300D7F99A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (431, CAST(0x0000A50300D8487A AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (432, CAST(0x0000A50300D89D9B AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (433, CAST(0x0000A50300E392B9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (434, CAST(0x0000A50300E3A1BA AS DateTime), N'DEBI-PC', N'Egreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (435, CAST(0x0000A505000C59B4 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (436, CAST(0x0000A50500FDA1CF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (437, CAST(0x0000A50C00F496DD AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (438, CAST(0x0000A50C00F9788C AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (439, CAST(0x0000A50C00F9C662 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (440, CAST(0x0000A50C00FA81F8 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (441, CAST(0x0000A50C00FAD38C AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (442, CAST(0x0000A50C00FF3493 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (443, CAST(0x0000A50C01005C36 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (444, CAST(0x0000A50C01010052 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (445, CAST(0x0000A50C0105AB30 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (446, CAST(0x0000A50C01063E6F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (447, CAST(0x0000A50C0107BD8F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (448, CAST(0x0000A50C01082385 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (449, CAST(0x0000A50C010C8EC1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (450, CAST(0x0000A50C011048C5 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (451, CAST(0x0000A50C011178E9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (452, CAST(0x0000A51600B99BC1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (453, CAST(0x0000A51600BA97BA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (454, CAST(0x0000A51600BB4FE6 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (455, CAST(0x0000A51600BBAEBA AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (456, CAST(0x0000A51600BCAF3C AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (457, CAST(0x0000A51600BFCB5E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (458, CAST(0x0000A51600C02AF8 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (459, CAST(0x0000A51600C0506E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (460, CAST(0x0000A51600C0F24E AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (461, CAST(0x0000A51600C2F1D0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (462, CAST(0x0000A51600C3BE77 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (463, CAST(0x0000A51F00E16B67 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (464, CAST(0x0000A51F00E2D36F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (465, CAST(0x0000A51F00E84160 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (466, CAST(0x0000A51F00E91FA9 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (467, CAST(0x0000A51F00FCC859 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (468, CAST(0x0000A51F00FE1375 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (469, CAST(0x0000A51F01004282 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (470, CAST(0x0000A51F01032F12 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (471, CAST(0x0000A51F0103C2A4 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (472, CAST(0x0000A51F010744A0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (473, CAST(0x0000A51F0107D10F AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (474, CAST(0x0000A51F010881C0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (475, CAST(0x0000A52100F29241 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (476, CAST(0x0000A52100F6E5B0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (477, CAST(0x0000A52100F844D2 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (478, CAST(0x0000A52100FBF6EF AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (479, CAST(0x0000A52100FD16A0 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (480, CAST(0x0000A52100FF8904 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
INSERT [dbo].[LoginOut] ([Id_Auditoria], [FechaHora], [Equipo], [Operacion], [Usuario]) VALUES (481, CAST(0x0000A52101004FB1 AS DateTime), N'DEBI-PC', N'Ingreso', N'debi')
SET IDENTITY_INSERT [dbo].[LoginOut] OFF
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (16, CAST(0x0000A4BD00CB69F6 AS DateTime), N'usu', N'Anulado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (30, CAST(0x0000A50C00FF5A20 AS DateTime), N'debi', N'EnCurso')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (30, CAST(0x0000A50C00FF59A6 AS DateTime), N'debi', N'Anulado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (29, CAST(0x0000A50C00FB382D AS DateTime), N'debi', N'Anulado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (28, CAST(0x0000A4F701718762 AS DateTime), N'debi', N'Anulado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (22, CAST(0x0000A51F00FE53FC AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (22, CAST(0x0000A51F00FE5D11 AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (23, CAST(0x0000A51F00FEB4C1 AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (19, CAST(0x0000A51F0100D73D AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (20, CAST(0x0000A51F01034C5C AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (21, CAST(0x0000A51F0103DC62 AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (22, CAST(0x0000A51F010755FD AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (22, CAST(0x0000A51F0107F6AE AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (24, CAST(0x0000A51F010897C0 AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (25, CAST(0x0000A52100F2CF6C AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (26, CAST(0x0000A52100F3BADD AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (26, CAST(0x0000A52100F70E57 AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (26, CAST(0x0000A52100F7FB05 AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (31, CAST(0x0000A52100F88554 AS DateTime), N'debi', N'EnCurso')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (32, CAST(0x0000A52100F8B1D1 AS DateTime), N'debi', N'EnCurso')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (31, CAST(0x0000A52100F9841A AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (32, CAST(0x0000A52100FA5CAC AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (32, CAST(0x0000A52100FC378D AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (32, CAST(0x0000A52100FCAC11 AS DateTime), N'debi', N'Pendiente')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (32, CAST(0x0000A52100FD60B3 AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (27, CAST(0x0000A52100FFF2B3 AS DateTime), N'debi', N'Finalizado')
INSERT [dbo].[OrdenCompra] ([nroOrdenCompra], [fechaRegistro], [usuario], [estado]) VALUES (26, CAST(0x0000A52101006F8E AS DateTime), N'debi', N'Pendiente')
USE [master]
GO
ALTER DATABASE [auditoria] SET  READ_WRITE 
GO
