USE [master]
GO
/****** Object:  Database [seguridad]    Script Date: 24/10/2015 14:18:55 ******/
CREATE DATABASE [seguridad]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'seguridad', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\seguridad.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'seguridad_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\seguridad_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [seguridad] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [seguridad].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [seguridad] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [seguridad] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [seguridad] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [seguridad] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [seguridad] SET ARITHABORT OFF 
GO
ALTER DATABASE [seguridad] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [seguridad] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [seguridad] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [seguridad] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [seguridad] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [seguridad] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [seguridad] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [seguridad] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [seguridad] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [seguridad] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [seguridad] SET  ENABLE_BROKER 
GO
ALTER DATABASE [seguridad] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [seguridad] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [seguridad] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [seguridad] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [seguridad] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [seguridad] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [seguridad] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [seguridad] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [seguridad] SET  MULTI_USER 
GO
ALTER DATABASE [seguridad] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [seguridad] SET DB_CHAINING OFF 
GO
ALTER DATABASE [seguridad] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [seguridad] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [seguridad]
GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarAccion]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
 CREATE PROCEDURE [dbo].[sp_AgregarAccion] (@IdPerfil int,@IdAccion int)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO perfilesAcciones(idPerfil,idAccion)
	VALUES(@IdPerfil,@IdAccion)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarGrupo]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarGrupo] (@Nombre varchar(30), @Descripcion varchar(150))
as
BEGIN TRANSACTION
	BEGIN TRY
		IF NOT EXISTS(SELECT * FROM Grupos WHERE NombreGrupo=@Nombre)
				insert into Grupos (NombreGrupo, Descripcion) values (@Nombre,@Descripcion)
		ELSE IF EXISTS(SELECT EstadoGrupo FROM Grupos WHERE (NombreGrupo=@Nombre AND EstadoGrupo=0))
		UPDATE Grupos SET Descripcion=@Descripcion, EstadoGrupo=1 WHERE NombreGrupo=@Nombre
		COMMIT
	END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION		
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarGrupoUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_AgregarGrupoUsuario] (@NombreUsuario varchar(30),@NombreGrupo varchar(30))
AS
INSERT INTO UsuariosGrupos (NombreGrupo, NombreUsuario) VALUES (@NombreGrupo,@NombreUsuario)

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarPerfil]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_AgregarPerfil](@NombreGrupo varchar(30), @NombreFormulario varchar(150), @TipoPermiso varchar(20))
as insert into Perfiles (NombreGrupo, NombreFormulario, TipoPermiso)
values (@NombreGrupo, @NombreFormulario, @TipoPermiso)

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarPerfilUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_AgregarPerfilUsuario]
	-- Add the parameters for the stored procedure here
	@IdUsuario int,
	@IdPerfil int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO usuariosPerfiles
	VALUES (@IdUsuario,@IdPerfil)
END

GO
/****** Object:  StoredProcedure [dbo].[sp_AgregarUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_AgregarUsuario] 
(@NombreUsuario varchar(30), 
@Clave varchar(100), 
@NombreApellido varchar(50), 
@Email varchar(50))
as 
insert into usuarios (nombreUsuario, clave, nombreApellido, email)
values(@NombreUsuario, @Clave, @NombreApellido, @Email)
GO
/****** Object:  StoredProcedure [dbo].[sp_CambiarClaveUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CambiarClaveUsuario]
@IdUsuario int,
@Clave varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE usuarios 
	SET clave=@Clave
	WHERE idUsuario=@IdUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarAccionesPerfiles]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ConsultarAccionesPerfiles] 
	-- Add the parameters for the stored procedure here
	@idPerfil int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT acciones.idAccion, acciones.nombreAccion, acciones.descripcion
	FROM perfiles INNER JOIN perfilesAcciones
	ON perfiles.idPerfil=perfilesAcciones.idPerfil
	INNER JOIN acciones 
	ON perfilesAcciones.idAccion=acciones.idAccion
	WHERE perfiles.idPerfil=@idPerfil
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarFormularios]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarFormularios]
as select * from Formularios

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarGrupos]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ConsultarGrupos]
as select NombreGrupo,Descripcion from Grupos where EstadoGrupo=1

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarPerfiles]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ConsultarPerfiles]
as
SELECT idPerfil, nombrePerfil, descripcion 
	FROM perfiles 
GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarPerfilesUsuarios]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ConsultarPerfilesUsuarios] 
	-- Add the parameters for the stored procedure here
	@idUsuario int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT perfiles.idPerfil, nombrePerfil, descripcion 
	FROM usuarios INNER JOIN usuariosPerfiles
		ON usuarios.idUsuario=usuariosPerfiles.idUsuario
		INNER JOIN perfiles 
		ON usuariosPerfiles.idPerfil=perfiles.idPerfil
	WHERE usuarios.idUsuario=@idUsuario
											
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarPermisos]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ConsultarPermisos]
as select * from Permisos

GO
/****** Object:  StoredProcedure [dbo].[sp_ConsultarUsuarios]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ConsultarUsuarios]
as select idUsuario, nombreUsuario, clave, nombreApellido, email from usuarios
/* where Estado = 1*/

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarAccionesPerfil]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_EliminarAccionesPerfil] (@IdPerfil INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM perfilesAcciones WHERE idPerfil=@IdPerfil
END

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarGrupo]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarGrupo] (@Nombre varchar(30))
as
update Grupos set EstadoGrupo=0 where (Grupos.NombreGrupo=@Nombre)

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarGruposUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarGruposUsuario](@NombreUsuario varchar(30))
AS
DELETE FROM UsuariosGrupos Where NombreUsuario=@NombreUsuario

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarPerfil]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarPerfil](@NombreGrupo varchar(30), @NombreFormulario varchar(150), @TipoPermiso varchar(20))
as delete from Perfiles 
where NombreGrupo = @NombreGrupo and NombreFormulario =  @NombreFormulario and TipoPermiso = @TipoPermiso

GO
/****** Object:  StoredProcedure [dbo].[sp_EliminarUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_EliminarUsuario](@NombreUsuario varchar(30))
as update Usuarios set Estado = 0 where NombreUsuario = @NombreUsuario

GO
/****** Object:  StoredProcedure [dbo].[sp_HabilitarUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_HabilitarUsuario](@NombreUsuario varchar(30))
as update Usuarios set EstadoSituacion = 1 where NombreUsuario = @NombreUsuario

GO
/****** Object:  StoredProcedure [dbo].[sp_InhabilitarUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_InhabilitarUsuario](@NombreUsuario varchar(30))
as update Usuarios set EstadoSituacion = 0 where NombreUsuario = @NombreUsuario

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarGrupo]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_ModificarGrupo](@NombreGrupo varchar(30), @Descripcion varchar(150))
AS
UPDATE Grupos SET Descripcion = @Descripcion WHERE NombreGrupo=@NombreGrupo

GO
/****** Object:  StoredProcedure [dbo].[sp_ModificarUsuario]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_ModificarUsuario](
@IdUsuario int,  
@Clave varchar(30))
as 

UPDATE usuarios set clave = @Clave
WHERE idUsuario = @IdUsuario

GO
/****** Object:  StoredProcedure [dbo].[sp_RecuperarAcciones]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_RecuperarAcciones] 
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
	SELECT idAccion,nombreAccion,descripcion FROM acciones
END

GO
/****** Object:  Table [dbo].[acciones]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[acciones](
	[idAccion] [int] IDENTITY(1,1) NOT NULL,
	[nombreAccion] [varchar](50) NOT NULL,
	[descripcion] [varchar](100) NULL,
 CONSTRAINT [PK_acciones] PRIMARY KEY CLUSTERED 
(
	[idAccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[perfiles]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[perfiles](
	[idPerfil] [int] IDENTITY(1,1) NOT NULL,
	[nombrePerfil] [varchar](50) NOT NULL,
	[descripcion] [varchar](200) NULL,
 CONSTRAINT [PK_Perfiles] PRIMARY KEY CLUSTERED 
(
	[idPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[perfilesAcciones]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[perfilesAcciones](
	[idPerfil] [int] NULL,
	[idAccion] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarios](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombreUsuario] [varchar](50) NOT NULL,
	[clave] [varchar](20) NOT NULL,
	[nombreApellido] [varchar](50) NOT NULL,
	[email] [varchar](50) NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuariosPerfiles]    Script Date: 24/10/2015 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuariosPerfiles](
	[idUsuario] [int] NULL,
	[idPerfil] [int] NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[acciones] ON 

INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (1, N'cambiarClave', N'cambiarClave')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (2, N'agregarCliente', N'agregarCliente')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (3, N'modificarCliente', N'modificarCliente')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (4, N'consultarCliente', N'consultarCliente')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (5, N'eliminarCliente', N'eliminarCliente')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (6, N'agregarProveedor', N'agregarProveedor')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (7, N'modificarProveedor', N'modificarProveedor')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (8, N'consultarProveedor', N'consultarProveedor')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (9, N'eliminarProveedor', N'eliminarProveedor')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (10, N'agregarGrupoArticulo', N'agregarGrupoArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (11, N'modificarGrupoArticulo', N'modificarGrupoArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (12, N'consultarGrupoArticulo', N'consultarGrupoArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (13, N'eliminarGrupoArticulo', N'eliminarGrupoArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (14, N'agregarArticulo', N'agregarArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (15, N'modificarArticulo', N'modificarArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (16, N'consultarArticulo', N'consultarArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (17, N'eliminarArticulo', N'eliminarArticulo')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (18, N'consultarOC', N'consultarOC')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (19, N'imprimirOC', N'imprimirOC')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (20, N'anularOC', N'anularOC')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (21, N'consultarNP', N'consultarNP')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (22, N'imprimirNP', N'imprimirNP')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (23, N'anularNP', N'anularNP')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (24, N'generarNP', N'generarNP')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (25, N'generarOC', N'generarOC')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (26, N'auditoriaLoginOut', N'auditoriaLoginOut')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (27, N'auditoriaOC', N'auditoriaOC')
INSERT [dbo].[acciones] ([idAccion], [nombreAccion], [descripcion]) VALUES (28, N'reportes', N'reportes')
SET IDENTITY_INSERT [dbo].[acciones] OFF
SET IDENTITY_INSERT [dbo].[perfiles] ON 

INSERT [dbo].[perfiles] ([idPerfil], [nombrePerfil], [descripcion]) VALUES (1, N'Administrador', N'Compras, ventas, seguridad, auditoria y reportes')
INSERT [dbo].[perfiles] ([idPerfil], [nombrePerfil], [descripcion]) VALUES (2, N'Compras', N'Encargado de compras.')
INSERT [dbo].[perfiles] ([idPerfil], [nombrePerfil], [descripcion]) VALUES (3, N'Ventas', N'Encargado de ventas')
SET IDENTITY_INSERT [dbo].[perfiles] OFF
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 3)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 18)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 20)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 22)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 23)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 24)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 25)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 26)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 27)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 28)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (2, 1)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (2, 2)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (2, 3)
INSERT [dbo].[perfilesAcciones] ([idPerfil], [idAccion]) VALUES (1, 1)
SET IDENTITY_INSERT [dbo].[usuarios] ON 

INSERT [dbo].[usuarios] ([idUsuario], [nombreUsuario], [clave], [nombreApellido], [email]) VALUES (1, N'debi', N'MQAyADMA', N'Debora Wacker', N'deborawacker@hotmail.com')
INSERT [dbo].[usuarios] ([idUsuario], [nombreUsuario], [clave], [nombreApellido], [email]) VALUES (2, N'nahu', N'ZQBLAFAAaQBhAGsANgBh', N'Nahuel Escobar', N'nahuelmisc@yahoo.com.ar')
SET IDENTITY_INSERT [dbo].[usuarios] OFF
INSERT [dbo].[usuariosPerfiles] ([idUsuario], [idPerfil]) VALUES (1, 1)
ALTER TABLE [dbo].[perfilesAcciones]  WITH CHECK ADD  CONSTRAINT [FK_perfilesAcciones_acciones] FOREIGN KEY([idAccion])
REFERENCES [dbo].[acciones] ([idAccion])
GO
ALTER TABLE [dbo].[perfilesAcciones] CHECK CONSTRAINT [FK_perfilesAcciones_acciones]
GO
USE [master]
GO
ALTER DATABASE [seguridad] SET  READ_WRITE 
GO
